/*
  This file is part of TALER
  (C) 2014--2021 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Lesser General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file taler-merchant-httpd_helper.c
 * @brief shared logic for various handlers
 * @author Christian Grothoff
 */
#include "platform.h"
#include <gnunet/gnunet_util_lib.h>
#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler-merchant-httpd_helper.h"

/**
 * check @a payto_uris for well-formedness
 *
 * @param payto_uris JSON array of payto URIs (presumably)
 * @return true if they are all valid URIs (and this is an array of strings)
 */
bool
TMH_payto_uri_array_valid (const json_t *payto_uris)
{
  bool payto_ok = true;

  if (! json_is_array (payto_uris))
  {
    GNUNET_break_op (0);
    payto_ok = false;
  }
  else
  {
    unsigned int len = json_array_size (payto_uris);

    for (unsigned int i = 0; i<len; i++)
    {
      json_t *payto_uri = json_array_get (payto_uris,
                                          i);
      const char *uri;

      if (! json_is_string (payto_uri))
        payto_ok = false;
      uri = json_string_value (payto_uri);
      /* Test for the same payto:// URI being given twice */
      for (unsigned int j = 0; j<i; j++)
      {
        json_t *old_uri = json_array_get (payto_uris,
                                          j);
        if (json_equal (payto_uri,
                        old_uri))
        {
          GNUNET_break_op (0);
          payto_ok = false;
          break;
        }
      }
      if (! payto_ok)
        break;
      {
        char *err;

        if (NULL !=
            (err = TALER_payto_validate (uri)))
        {
          GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                      "Encountered invalid payto://-URI `%s': %s\n",
                      uri,
                      err);
          GNUNET_free (err);
          payto_ok = false;
          break;
        }
      }
    }
  }
  return payto_ok;
}


bool
TMH_location_object_valid (const json_t *location)
{
  const char *country;
  const char *subdivision;
  const char *district;
  const char *town;
  const char *town_loc;
  const char *postcode;
  const char *street;
  const char *building;
  const char *building_no;
  json_t *lines = NULL;
  struct GNUNET_JSON_Specification spec[] = {
    GNUNET_JSON_spec_mark_optional (
      GNUNET_JSON_spec_string ("country",
                               &country)),
    GNUNET_JSON_spec_mark_optional (
      GNUNET_JSON_spec_string ("country_subdivision",
                               &subdivision)),
    GNUNET_JSON_spec_mark_optional (
      GNUNET_JSON_spec_string ("district",
                               &district)),
    GNUNET_JSON_spec_mark_optional (
      GNUNET_JSON_spec_string ("town",
                               &town)),
    GNUNET_JSON_spec_mark_optional (
      GNUNET_JSON_spec_string ("town_location",
                               &town_loc)),
    GNUNET_JSON_spec_mark_optional (
      GNUNET_JSON_spec_string ("post_code",
                               &postcode)),
    GNUNET_JSON_spec_mark_optional (
      GNUNET_JSON_spec_string ("street",
                               &street)),
    GNUNET_JSON_spec_mark_optional (
      GNUNET_JSON_spec_string ("building_name",
                               &building)),
    GNUNET_JSON_spec_mark_optional (
      GNUNET_JSON_spec_string ("building_number",
                               &building_no)),
    GNUNET_JSON_spec_mark_optional (
      GNUNET_JSON_spec_json ("address_lines",
                             &lines)),
    GNUNET_JSON_spec_end ()
  };
  const char *ename;
  unsigned int eline;

  if (GNUNET_OK !=
      GNUNET_JSON_parse (location,
                         spec,
                         &ename,
                         &eline))
  {
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "Invalid location for field %s\n",
                ename);
    return false;
  }
  if (NULL != lines)
  {
    size_t idx;
    json_t *line;

    if (! json_is_array (lines))
    {
      GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                  "Invalid location for field %s\n",
                  "lines");
      return false;
    }
    json_array_foreach (lines, idx, line)
    {
      if (! json_is_string (line))
      {
        GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                    "Invalid address line #%u in location\n",
                    (unsigned int) idx);
        return false;
      }
    }
  }
  return true;
}


bool
TMH_products_array_valid (const json_t *products)
{
  const json_t *product;
  size_t idx;
  bool valid = true;

  if (! json_is_array (products))
    return false;
  json_array_foreach ((json_t *) products, idx, product)
  {
    const char *product_id;
    const char *description;
    json_t *description_i18n = NULL;
    uint64_t quantity;
    const char *unit;
    struct TALER_Amount price;
    const char *image_data_url = NULL;
    json_t *taxes = NULL;
    struct GNUNET_TIME_Timestamp delivery_date;
    struct GNUNET_JSON_Specification spec[] = {
      GNUNET_JSON_spec_mark_optional (
        GNUNET_JSON_spec_string ("product_id",
                                 &product_id)),
      GNUNET_JSON_spec_string ("description",
                               &description),
      GNUNET_JSON_spec_mark_optional (
        GNUNET_JSON_spec_json ("description_i18n",
                               &description_i18n)),
      GNUNET_JSON_spec_mark_optional (
        GNUNET_JSON_spec_uint64 ("quantity",
                                 &quantity)),
      GNUNET_JSON_spec_mark_optional (
        GNUNET_JSON_spec_string ("unit",
                                 &unit)),
      GNUNET_JSON_spec_mark_optional (
        TALER_JSON_spec_amount ("price",
                                TMH_currency,
                                &price)),
      GNUNET_JSON_spec_mark_optional (
        GNUNET_JSON_spec_string ("image",
                                 &image_data_url)),
      GNUNET_JSON_spec_mark_optional (
        GNUNET_JSON_spec_json ("taxes",
                               &taxes)),
      GNUNET_JSON_spec_mark_optional (
        GNUNET_JSON_spec_timestamp ("delivery_date",
                                    &delivery_date)),
      GNUNET_JSON_spec_end ()
    };
    const char *ename;
    unsigned int eline;

    if (GNUNET_OK !=
        GNUNET_JSON_parse (product,
                           spec,
                           &ename,
                           &eline))
    {
      GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                  "Invalid product #%u for field %s\n",
                  (unsigned int) idx,
                  ename);
      return false;
    }
    if ( (NULL != image_data_url) &&
         (! TMH_image_data_url_valid (image_data_url)) )
      valid = false;
    if ( (NULL != taxes) &&
         (! TMH_taxes_array_valid (taxes)) )
      valid = false;
    if ( (NULL != description_i18n) &&
         (! TALER_JSON_check_i18n (description_i18n)) )
      valid = false;
    GNUNET_JSON_parse_free (spec);
    if (! valid)
      break;
  }

  return valid;
}


bool
TMH_image_data_url_valid (const char *image_data_url)
{
  if (0 == strcmp (image_data_url,
                   ""))
    return true;
  if (0 != strncasecmp ("data:image/",
                        image_data_url,
                        strlen ("data:image/")))
    return false;
  if (NULL == strstr (image_data_url,
                      ";base64,"))
    return false;
  if (! TALER_url_valid_charset (image_data_url))
    return false;
  return true;
}


bool
TMH_taxes_array_valid (const json_t *taxes)
{
  json_t *tax;
  size_t idx;

  if (! json_is_array (taxes))
    return false;
  json_array_foreach (taxes, idx, tax)
  {
    struct TALER_Amount amount;
    const char *name;
    struct GNUNET_JSON_Specification spec[] = {
      GNUNET_JSON_spec_string ("name",
                               &name),
      TALER_JSON_spec_amount_any ("tax",
                                  &amount),
      GNUNET_JSON_spec_end ()
    };
    enum GNUNET_GenericReturnValue res;

    res = TALER_MHD_parse_json_data (NULL,
                                     tax,
                                     spec);
    if (GNUNET_OK != res)
    {
      GNUNET_break_op (0);
      return false;
    }
  }
  return true;
}


struct TMH_WireMethod *
TMH_setup_wire_account (const char *payto_uri)
{
  struct TMH_WireMethod *wm;
  char *emsg;

  if (NULL != (emsg = TALER_payto_validate (payto_uri)))
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Invalid URI `%s': %s\n",
                payto_uri,
                emsg);
    GNUNET_free (emsg);
    return NULL;
  }

  wm = GNUNET_new (struct TMH_WireMethod);
  GNUNET_CRYPTO_random_block (GNUNET_CRYPTO_QUALITY_NONCE,
                              &wm->wire_salt,
                              sizeof (wm->wire_salt));
  wm->payto_uri = GNUNET_strdup (payto_uri);
  TALER_merchant_wire_signature_hash (payto_uri,
                                      &wm->wire_salt,
                                      &wm->h_wire);
  wm->wire_method
    = TALER_payto_get_method (payto_uri);
  wm->active = true;
  return wm;
}


enum GNUNET_GenericReturnValue
TMH_check_auth_config (struct MHD_Connection *connection,
                       const json_t *jauth,
                       const char **auth_token)
{
  bool auth_wellformed = false;
  const char *auth_method = json_string_value (json_object_get (jauth,
                                                                "method"));

  *auth_token = NULL;
  if (NULL == auth_method)
  {
    GNUNET_break_op (0);
  }
  else if (0 == strcmp (auth_method,
                        "external"))
  {
    auth_wellformed = true;
  }
  else if (0 == strcmp (auth_method,
                        "token"))
  {
    *auth_token = json_string_value (json_object_get (jauth,
                                                      "token"));
    if (NULL == *auth_token)
    {
      GNUNET_break_op (0);
    }
    else
    {
      if (0 != strncasecmp (RFC_8959_PREFIX,
                            *auth_token,
                            strlen (RFC_8959_PREFIX)))
        GNUNET_break_op (0);
      else
        auth_wellformed = true;
    }
  }

  if (! auth_wellformed)
  {
    GNUNET_break_op (0);
    return (MHD_YES ==
            TALER_MHD_reply_with_error (connection,
                                        MHD_HTTP_BAD_REQUEST,
                                        TALER_EC_MERCHANT_PRIVATE_POST_INSTANCES_BAD_AUTH,
                                        "bad authentication config")) ?
           GNUNET_NO : GNUNET_SYSERR;
  }
  return GNUNET_OK;
}


/**
 * Generate binary UUID from client-provided UUID-string.
 *
 * @param uuids string intpu
 * @param[out] uuid set to binary UUID
 */
void
TMH_uuid_from_string (const char *uuids,
                      struct GNUNET_Uuid *uuid)
{
  struct GNUNET_HashCode hc;

  GNUNET_CRYPTO_hash (uuids,
                      strlen (uuids),
                      &hc);
  GNUNET_static_assert (sizeof (hc) > sizeof (*uuid));
  memcpy (uuid,
          &hc,
          sizeof (*uuid));
}
