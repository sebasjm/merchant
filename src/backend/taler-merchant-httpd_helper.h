/*
  This file is part of TALER
  Copyright (C) 2021 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/

/**
 * @file taler-merchant-httpd_helper.h
 * @brief helpers for shared logic
 * @author Florian Dold
 * @author Benedikt Mueller
 * @author Christian Grothoff
 */
#ifndef TALER_EXCHANGE_HTTPD_HELPER_H
#define TALER_EXCHANGE_HTTPD_HELPER_H


#include "taler-merchant-httpd.h"

/**
 * check @a payto_uris for well-formedness
 *
 * @param payto_uris JSON array of payto URIs (presumably)
 * @return true if they are all valid URIs (and this is an array of strings)
 */
bool
TMH_payto_uri_array_valid (const json_t *payto_uris);


/**
 * Check if @a taxes is an array of valid Taxes in the sense of
 * Taler's API definition.
 *
 * @param taxes array to check
 * @return true if @a taxes is an array and all
 *         entries are valid Taxes.
 */
bool
TMH_taxes_array_valid (const json_t *taxes);


/**
 * Check if @a location is a valid Location object in the sense of Taler's API
 * definition.
 *
 * @param location object to check
 * @return true if @a location is an object
 *         representing a Location.
 */
bool
TMH_location_object_valid (const json_t *location);


/**
 * Check if @a products is an array of valid Product(s) in the sense of
 * Taler's API definition.
 *
 * @param products array to check
 * @return true if @a products is an array and all
 *         entries are valid Products.
 */
bool
TMH_products_array_valid (const json_t *products);


/**
 * Check if @a image_data_url is a valid image
 * data URL. Does not validate the actual payload,
 * only the syntax and that it properly claims to
 * be an image.
 *
 * @param image_data_url string to check
 * @return true if @a image_data_url is a data
 *         URL with an "image/" mime-type
 */
bool
TMH_image_data_url_valid (const char *image_data_url);


/**
 * Setup new wire method for the given @ payto_uri.
 *
 * @param payto_uri already validated payto URI
 * @return new wire method object, never fails
 */
struct TMH_WireMethod *
TMH_setup_wire_account (const char *payto_uri);

/**
 * FIXME: document
 */
enum GNUNET_GenericReturnValue
TMH_check_auth_config (struct MHD_Connection *connection,
                       const json_t *jauth,
                       const char **auth_token);


/**
 * Generate binary UUID from client-provided UUID-string.
 *
 * @param uuids string intpu
 * @param[out] uuid set to binary UUID
 */
void
TMH_uuid_from_string (const char *uuids,
                      struct GNUNET_Uuid *uuid);


/**
 * Put data from an exchange's HTTP response into
 * a JSON reply
 *
 * @param hr a `TALER_EXCHANGE_HttpResponse`
 */
#define TMH_pack_exchange_reply(hr) \
  GNUNET_JSON_pack_uint64 ("exchange_code", (hr)->ec),                \
  GNUNET_JSON_pack_uint64 ("exchange_http_status", (hr)->http_status), \
  GNUNET_JSON_pack_uint64 ("exchange_ec", (hr)->ec),      /* LEGACY */  \
  GNUNET_JSON_pack_uint64 ("exchange_hc", (hr)->http_status),     /* LEGACY */ \
  GNUNET_JSON_pack_allow_null ( \
    GNUNET_JSON_pack_object_incref ("exchange_reply", (json_t *) (hr)->reply))

#endif
