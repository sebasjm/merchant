/*
  This file is part of TALER
  Copyright (C) 2020 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file taler-merchant-httpd_qr.h
 * @brief logic to create QR codes in HTML
 * @author Christian Grothoff
 */
#ifndef TALER_MERCHANT_HTTPD_QR_H
#define TALER_MERCHANT_HTTPD_QR_H


/**
 * Create the HTML for a QR code for a URI.
 *
 * @param uri input string to encode
 * @return NULL on error, encoded URI otherwise
 */
char *
TMH_create_qrcode (const char *uri);


#endif
