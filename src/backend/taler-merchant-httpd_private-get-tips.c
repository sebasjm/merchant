/*
  This file is part of TALER
  (C) 2020-2021 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file taler-merchant-httpd_private-get-tips.c
 * @brief implementation of a GET /private/tips handler
 * @author Jonathan Buchanan
 */
#include "platform.h"
#include "taler-merchant-httpd_private-get-tips.h"
#include <taler/taler_json_lib.h>

/**
 * Add tip details to our JSON array.
 *
 * @param[in,out] cls a `json_t *` JSON array to build
 * @param row_id row number of the tip
 * @param tip_id ID of the tip
 * @param amount the amount of the tip
 */
static void
add_tip (void *cls,
         uint64_t row_id,
         struct GNUNET_HashCode tip_id,
         struct TALER_Amount amount)
{
  json_t *pa = cls;

  GNUNET_assert (0 ==
                 json_array_append_new (
                   pa,
                   GNUNET_JSON_PACK (
                     GNUNET_JSON_pack_uint64 ("row_id",
                                              row_id),
                     GNUNET_JSON_pack_data_auto ("tip_id",
                                                 &tip_id),
                     TALER_JSON_pack_amount ("tip_amount",
                                             &amount))));
}


MHD_RESULT
TMH_private_get_tips (const struct TMH_RequestHandler *rh,
                      struct MHD_Connection *connection,
                      struct TMH_HandlerContext *hc)
{
  json_t *pa;
  enum GNUNET_DB_QueryStatus qs;
  enum TALER_EXCHANGE_YesNoAll expired;
  uint64_t offset;
  int64_t limit;

  if (! (TALER_arg_to_yna (connection,
                           "expired",
                           TALER_EXCHANGE_YNA_NO,
                           &expired)) )
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_BAD_REQUEST,
                                       TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                       "expired");
  {
    const char *limit_str;

    limit_str = MHD_lookup_connection_value (connection,
                                             MHD_GET_ARGUMENT_KIND,
                                             "limit");
    if (NULL == limit_str)
    {
      limit = -20;
    }
    else
    {
      char dummy[2];
      long long ll;

      if (1 !=
          sscanf (limit_str,
                  "%lld%1s",
                  &ll,
                  dummy))
        return TALER_MHD_reply_with_error (connection,
                                           MHD_HTTP_BAD_REQUEST,
                                           TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                           "limit");
      limit = (uint64_t) ll;
    }
  }
  {
    const char *offset_str;

    offset_str = MHD_lookup_connection_value (connection,
                                              MHD_GET_ARGUMENT_KIND,
                                              "offset");
    if (NULL == offset_str)
    {
      if (limit > 0)
        offset = 0;
      else
        offset = INT64_MAX;
    }
    else
    {
      char dummy[2];
      unsigned long long ull;

      if (1 !=
          sscanf (offset_str,
                  "%llu%1s",
                  &ull,
                  dummy))
        return TALER_MHD_reply_with_error (connection,
                                           MHD_HTTP_BAD_REQUEST,
                                           TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                           "offset");
      offset = (uint64_t) ull;
    }
  }

  pa = json_array ();
  GNUNET_assert (NULL != pa);
  qs = TMH_db->lookup_tips (TMH_db->cls,
                            hc->instance->settings.id,
                            expired,
                            limit,
                            offset,
                            &add_tip,
                            pa);

  if (0 > qs)
  {
    GNUNET_break (0);
    json_decref (pa);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_INTERNAL_SERVER_ERROR,
                                       TALER_EC_GENERIC_DB_FETCH_FAILED,
                                       "tips");
  }

  return TALER_MHD_REPLY_JSON_PACK (
    connection,
    MHD_HTTP_OK,
    GNUNET_JSON_pack_array_steal ("tips",
                                  pa));
}
