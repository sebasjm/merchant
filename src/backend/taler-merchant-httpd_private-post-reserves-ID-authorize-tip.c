/*
  This file is part of TALER
  (C) 2014-2020 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file taler-merchant-httpd_private-post-reserves-ID-authorize-tip.c
 * @brief implement API for authorizing tips to be paid to visitors
 * @author Christian Grothoff
 */
#include "platform.h"
#include <jansson.h>
#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler-merchant-httpd.h"
#include "taler-merchant-httpd_mhd.h"
#include "taler-merchant-httpd_get-tips-ID.h"
#include "taler-merchant-httpd_private-post-reserves-ID-authorize-tip.h"


/**
 * Handle a "tip-authorize" request.
 *
 * @param rh context of the handler
 * @param connection the MHD connection to handle
 * @param[in,out] hc context with further information about the request
 * @param reserve_pub reserve to use, or NULL for "any"
 * @return MHD result code
 */
static MHD_RESULT
authorize_tip (const struct TMH_RequestHandler *rh,
               struct MHD_Connection *connection,
               struct TMH_HandlerContext *hc,
               const struct TALER_ReservePublicKeyP *reserve_pub)
{
  enum TALER_ErrorCode ec;
  struct GNUNET_TIME_Timestamp expiration;
  struct GNUNET_HashCode tip_id;
  const char *justification;
  const char *next_url;
  struct TALER_Amount amount;
  {
    struct GNUNET_JSON_Specification spec[] = {
      TALER_JSON_spec_amount ("amount",
                              TMH_currency,
                              &amount),
      GNUNET_JSON_spec_string ("justification",
                               &justification),
      GNUNET_JSON_spec_string ("next_url",
                               &next_url),
      GNUNET_JSON_spec_end ()
    };
    enum GNUNET_GenericReturnValue res;

    res = TALER_MHD_parse_json_data (connection,
                                     hc->request_body,
                                     spec);
    if (GNUNET_YES != res)
    {
      GNUNET_break_op (0);
      return (GNUNET_NO == res)
             ? MHD_YES
             : MHD_NO;
    }
  }
  TMH_db->preflight (TMH_db->cls);
  ec = TMH_db->authorize_tip (TMH_db->cls,
                              hc->instance->settings.id,
                              reserve_pub,
                              &amount,
                              justification,
                              next_url,
                              &tip_id,
                              &expiration);
  /* handle errors */
  if (TALER_EC_NONE != ec)
  {
    unsigned int http_status;

    switch (ec)
    {
    case TALER_EC_MERCHANT_PRIVATE_POST_TIP_AUTHORIZE_INSUFFICIENT_FUNDS:
      http_status = MHD_HTTP_PRECONDITION_FAILED;
      break;
    case TALER_EC_MERCHANT_PRIVATE_POST_TIP_AUTHORIZE_RESERVE_EXPIRED:
      http_status = MHD_HTTP_GONE;
      break;
    case TALER_EC_MERCHANT_PRIVATE_POST_TIP_AUTHORIZE_RESERVE_UNKNOWN:
      http_status = MHD_HTTP_SERVICE_UNAVAILABLE;
      break;
    case TALER_EC_MERCHANT_PRIVATE_POST_TIP_AUTHORIZE_RESERVE_NOT_FOUND:
      http_status = MHD_HTTP_NOT_FOUND;
      break;
    default:
      http_status = MHD_HTTP_INTERNAL_SERVER_ERROR;
      break;
    }

    return TALER_MHD_reply_with_error (connection,
                                       http_status,
                                       ec,
                                       NULL);
  }

  /* generate success response */
  {
    char *taler_tip_uri;
    char *tip_status_url;
    struct GNUNET_CRYPTO_HashAsciiEncoded hash_enc;
    MHD_RESULT res;

    GNUNET_CRYPTO_hash_to_enc (&tip_id,
                               &hash_enc);
    taler_tip_uri = TMH_make_taler_tip_uri (connection,
                                            &tip_id,
                                            hc->instance->settings.id);
    tip_status_url = TMH_make_tip_status_url (connection,
                                              &tip_id,
                                              hc->instance->settings.id);
    res = TALER_MHD_REPLY_JSON_PACK (
      connection,
      MHD_HTTP_OK,
      GNUNET_JSON_pack_string ("tip_id",
                               (const char *) hash_enc.encoding),
      GNUNET_JSON_pack_string ("taler_tip_uri",
                               taler_tip_uri),
      GNUNET_JSON_pack_string ("tip_status_url",
                               tip_status_url),
      GNUNET_JSON_pack_timestamp ("tip_expiration",
                                  expiration));
    GNUNET_free (taler_tip_uri);
    GNUNET_free (tip_status_url);
    return res;
  }
}


MHD_RESULT
TMH_private_post_reserves_ID_authorize_tip (const struct TMH_RequestHandler *rh,
                                            struct MHD_Connection *connection,
                                            struct TMH_HandlerContext *hc)
{
  struct TALER_ReservePublicKeyP reserve_pub;

  if (GNUNET_OK !=
      GNUNET_STRINGS_string_to_data (hc->infix,
                                     strlen (hc->infix),
                                     &reserve_pub,
                                     sizeof (reserve_pub)))
  {
    GNUNET_break_op (0);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_BAD_REQUEST,
                                       TALER_EC_MERCHANT_GENERIC_RESERVE_PUB_MALFORMED,
                                       hc->infix);
  }
  return authorize_tip (rh,
                        connection,
                        hc,
                        &reserve_pub);
}


MHD_RESULT
TMH_private_post_tips (const struct TMH_RequestHandler *rh,
                       struct MHD_Connection *connection,
                       struct TMH_HandlerContext *hc)
{
  return authorize_tip (rh,
                        connection,
                        hc,
                        NULL);
}


/* end of taler-merchant-httpd_private-post-reserves-ID-authorize-tip.c */
