/*
  This file is part of TALER
  (C) 2020 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file taler-merchant-httpd_reserves.h
 * @brief logic for initially tracking a reserve's status
 * @author Christian Grothoff
 */
#ifndef TALER_MERCHANT_HTTPD_RESERVES_H
#define TALER_MERCHANT_HTTPD_RESERVES_H

#include <jansson.h>
#include <gnunet/gnunet_util_lib.h>
#include <taler/taler_util.h>
#include <taler/taler_exchange_service.h>
#include "taler-merchant-httpd.h"


/**
 * Load information about reserves and start querying reserve status.
 * Must be called after the database is available.
 */
void
TMH_RESERVES_init (void);


/**
 * Add a reserve to the list of reserves to check.
 *
 * @param instance_id which instance is the reserve for
 * @param exchange_url URL of the exchange with the reserve
 * @param reserve_pub public key of the reserve to check
 * @param expected_amount amount the merchant expects to see initially in the reserve
 */
void
TMH_RESERVES_check (const char *instance_id,
                    const char *exchange_url,
                    const struct TALER_ReservePublicKeyP *reserve_pub,
                    const struct TALER_Amount *expected_amount);


/**
 * Stop checking reserve status.
 */
void
TMH_RESERVES_done (void);


#endif
