/*
  This file is part of TALER
  (C) 2014, 2015, 2016, 2017, 2020 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file taler-merchant-httpd_get-orders-ID.h
 * @brief implementation of GET /orders/$ID
 * @author Marcello Stanisci
 */
#ifndef TALER_MERCHANT_HTTPD_GET_ORDERS_ID_H
#define TALER_MERCHANT_HTTPD_GET_ORDERS_ID_H
#include <microhttpd.h>
#include "taler-merchant-httpd.h"


/**
 * Force resuming all suspended order lookups, needed during shutdown.
 */
void
TMH_force_wallet_get_order_resume (void);


/**
 * Create a taler://pay/ URI for the given @a con and @a order_id
 * and @a session_id and @a instance_id.
 *
 * @param con HTTP connection
 * @param order_id the order id
 * @param session_id session, may be NULL
 * @param instance_id instance, may be "default"
 * @param claim_token claim token for the order, may be NULL
 * @return corresponding taler://pay/ URI, or NULL on missing "host"
 */
char *
TMH_make_taler_pay_uri (struct MHD_Connection *con,
                        const char *order_id,
                        const char *session_id,
                        const char *instance_id,
                        struct TALER_ClaimTokenP *claim_token);

/**
 * Create a http(s) URL for the given @a con and @a order_id
 * and @a instance_id to display the /orders/{order_id} page.
 *
 * @param con HTTP connection
 * @param order_id the order id
 * @param session_id session, may be NULL
 * @param instance_id instance, may be "default"
 * @param claim_token claim token for the order, may be NULL
 * @param h_contract contract hash for authentication, may be NULL
 * @return corresponding http(s):// URL, or NULL on missing "host"
 */
char *
TMH_make_order_status_url (struct MHD_Connection *con,
                           const char *order_id,
                           const char *session_id,
                           const char *instance_id,
                           struct TALER_ClaimTokenP *claim_token,
                           struct TALER_PrivateContractHash *h_contract);


/**
 * Handle a GET "/orders/$ID" request.
 *
 * @param rh context of the handler
 * @param connection the MHD connection to handle
 * @param[in,out] hc context with further information about the request
 * @return MHD result code
 */
MHD_RESULT
TMH_get_orders_ID (const struct TMH_RequestHandler *rh,
                   struct MHD_Connection *connection,
                   struct TMH_HandlerContext *hc);

#endif
