/*
  This file is part of TALER
  (C) 2017 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file taler-merchant-httpd_post-tips-ID-pickup.h
 * @brief headers for POST /tips/ID/pickup handler
 * @author Christian Grothoff
 */
#ifndef TALER_MERCHANT_HTTPD_POST_TIPS_ID_PICKUP_H
#define TALER_MERCHANT_HTTPD_POST_TIPS_ID_PICKUP_H
#include <microhttpd.h>
#include "taler-merchant-httpd.h"


/**
 * We are shutting down, force resuming all suspended pickup operations.
 */
void
TMH_force_tip_pickup_resume (void);


/**
 * Manages a POST /tips/$ID/pickup call, checking that the tip is authorized,
 * and if so, returning the blind signatures.
 *
 * @param rh context of the handler
 * @param connection the MHD connection to handle
 * @param[in,out] hc context with further information about the request
 * @return MHD result code
 */
MHD_RESULT
TMH_post_tips_ID_pickup (const struct TMH_RequestHandler *rh,
                         struct MHD_Connection *connection,
                         struct TMH_HandlerContext *hc);


#endif
