/*
  This file is part of TALER
  (C) 2017, 2020 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file taler-merchant-httpd_private-post-reserves-ID-authorize-tip.h
 * @brief headers for /reserves/ID/tip-authorize
 * @author Christian Grothoff
 */
#ifndef TALER_MERCHANT_HTTPD_PRIVATE_POST_RESERVES_ID_AUTHORIZE_TIP_H
#define TALER_MERCHANT_HTTPD_PRIVATE_POST_RESERVES_ID_AUTHORIZE_TIP_H
#include <microhttpd.h>
#include "taler-merchant-httpd.h"


/**
 * Handle a "/reserves/$ID/tip-authorize" request.
 *
 * @param rh context of the handler
 * @param connection the MHD connection to handle
 * @param[in,out] hc context with further information about the request
 * @return MHD result code
 */
MHD_RESULT
TMH_private_post_reserves_ID_authorize_tip (const struct TMH_RequestHandler *rh,
                                            struct MHD_Connection *connection,
                                            struct TMH_HandlerContext *hc);


/**
 * Handle a POST "/tips" request.
 * Here the client does not specify the reserve public key, so we
 * are free to pick "any" available reserve.
 *
 * @param rh context of the handler
 * @param connection the MHD connection to handle
 * @param[in,out] hc context with further information about the request
 * @return MHD result code
 */
MHD_RESULT
TMH_private_post_tips (const struct TMH_RequestHandler *rh,
                       struct MHD_Connection *connection,
                       struct TMH_HandlerContext *hc);


#endif
