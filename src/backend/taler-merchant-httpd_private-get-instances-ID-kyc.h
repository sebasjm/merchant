/*
  This file is part of GNU Taler
  (C) 2021 Taler Systems SA

  GNU Taler is free software; you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as
  published by the Free Software Foundation; either version 3,
  or (at your option) any later version.

  GNU Taler is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not,
  see <http://www.gnu.org/licenses/>
*/

/**
 * @file taler-merchant-httpd_private-get-instances-ID-kyc.h
 * @brief implements GET /instances/$ID/kyc request handling
 * @author Christian Grothoff
 */
#ifndef TALER_MERCHANT_HTTPD_PRIVATE_GET_INSTANCES_ID_KYC_H
#define TALER_MERCHANT_HTTPD_PRIVATE_GET_INSTANCES_ID_KYC_H
#include "taler-merchant-httpd.h"


/**
 * Force all KYC contexts to be resumed as we are about
 * to shut down MHD.
 */
void
TMH_force_kyc_resume (void);


/**
 * Change the instance's kyc settings.
 * This is the handler called using the instance's own kycentication.
 *
 * @param rh context of the handler
 * @param connection the MHD connection to handle
 * @param[in,out] hc context with further information about the request
 * @return MHD result code
 */
MHD_RESULT
TMH_private_get_instances_ID_kyc (const struct TMH_RequestHandler *rh,
                                  struct MHD_Connection *connection,
                                  struct TMH_HandlerContext *hc);


/**
 * Change the instance's kyc settings.
 * This is the handler called using the default instance's kycentication.
 *
 * @param rh context of the handler
 * @param connection the MHD connection to handle
 * @param[in,out] hc context with further information about the request
 * @return MHD result code
 */
MHD_RESULT
TMH_private_get_instances_default_ID_kyc (const struct TMH_RequestHandler *rh,
                                          struct MHD_Connection *connection,
                                          struct TMH_HandlerContext *hc);

#endif
