/*
  This file is part of TALER
  (C) 2020 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as
  published by the Free Software Foundation; either version 3,
  or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not,
  see <http://www.gnu.org/licenses/>
*/

/**
 * @file taler-merchant-httpd_private-patch-products-ID.c
 * @brief implementing PATCH /products/$ID request handling
 * @author Christian Grothoff
 */
#include "platform.h"
#include "taler-merchant-httpd_private-patch-products-ID.h"
#include "taler-merchant-httpd_helper.h"
#include <taler/taler_json_lib.h>


/**
 * How often do we retry the simple INSERT database transaction?
 */
#define MAX_RETRIES 3


/**
 * Determine the cause of the PATCH failure in more detail and report.
 *
 * @param connection connection to report on
 * @param instance_id instance we are processing
 * @param product_id ID of the product to patch
 * @param pd product details we failed to set
 */
static MHD_RESULT
determine_cause (struct MHD_Connection *connection,
                 const char *instance_id,
                 const char *product_id,
                 const struct TALER_MERCHANTDB_ProductDetails *pd)
{
  struct TALER_MERCHANTDB_ProductDetails pdx;
  enum GNUNET_DB_QueryStatus qs;

  qs = TMH_db->lookup_product (TMH_db->cls,
                               instance_id,
                               product_id,
                               &pdx);
  switch (qs)
  {
  case GNUNET_DB_STATUS_HARD_ERROR:
    GNUNET_break (0);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_INTERNAL_SERVER_ERROR,
                                       TALER_EC_GENERIC_DB_FETCH_FAILED,
                                       NULL);
  case GNUNET_DB_STATUS_SOFT_ERROR:
    GNUNET_break (0);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_INTERNAL_SERVER_ERROR,
                                       TALER_EC_GENERIC_INTERNAL_INVARIANT_FAILURE,
                                       "unexpected serialization problem");
  case GNUNET_DB_STATUS_SUCCESS_NO_RESULTS:
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_NOT_FOUND,
                                       TALER_EC_MERCHANT_GENERIC_PRODUCT_UNKNOWN,
                                       product_id);
  case GNUNET_DB_STATUS_SUCCESS_ONE_RESULT:
    break; /* do below */
  }

  {
    enum TALER_ErrorCode ec;

    ec = TALER_EC_GENERIC_INTERNAL_INVARIANT_FAILURE;
    if (pdx.total_lost > pd->total_lost)
      ec = TALER_EC_MERCHANT_PRIVATE_PATCH_PRODUCTS_TOTAL_LOST_REDUCED;
    if (pdx.total_sold > pd->total_sold)
      ec = TALER_EC_MERCHANT_PRIVATE_PATCH_PRODUCTS_TOTAL_SOLD_REDUCED;
    if (pdx.total_stock > pd->total_stock)
      ec = TALER_EC_MERCHANT_PRIVATE_PATCH_PRODUCTS_TOTAL_STOCKED_REDUCED;
    TALER_MERCHANTDB_product_details_free (&pdx);
    GNUNET_break (TALER_EC_GENERIC_INTERNAL_INVARIANT_FAILURE != ec);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_CONFLICT,
                                       ec,
                                       NULL);
  }
}


/**
 * PATCH configuration of an existing instance, given its configuration.
 *
 * @param rh context of the handler
 * @param connection the MHD connection to handle
 * @param[in,out] hc context with further information about the request
 * @return MHD result code
 */
MHD_RESULT
TMH_private_patch_products_ID (const struct TMH_RequestHandler *rh,
                               struct MHD_Connection *connection,
                               struct TMH_HandlerContext *hc)
{
  struct TMH_MerchantInstance *mi = hc->instance;
  const char *product_id = hc->infix;
  struct TALER_MERCHANTDB_ProductDetails pd = {0};
  int64_t total_stock;
  enum GNUNET_DB_QueryStatus qs;
  struct GNUNET_JSON_Specification spec[] = {
    GNUNET_JSON_spec_string ("description",
                             (const char **) &pd.description),
    GNUNET_JSON_spec_mark_optional (
      GNUNET_JSON_spec_json ("description_i18n",
                             &pd.description_i18n)),
    GNUNET_JSON_spec_string ("unit",
                             (const char **) &pd.unit),
    TALER_JSON_spec_amount ("price",
                            TMH_currency,
                            &pd.price),
    GNUNET_JSON_spec_mark_optional (
      GNUNET_JSON_spec_string ("image",
                               (const char **) &pd.image)),
    GNUNET_JSON_spec_mark_optional (
      GNUNET_JSON_spec_json ("taxes",
                             &pd.taxes)),
    GNUNET_JSON_spec_int64 ("total_stock",
                            &total_stock),
    GNUNET_JSON_spec_mark_optional (
      GNUNET_JSON_spec_uint64 ("total_lost",
                               &pd.total_lost)),
    GNUNET_JSON_spec_mark_optional (
      GNUNET_JSON_spec_json ("address",
                             &pd.address)),
    GNUNET_JSON_spec_mark_optional (
      GNUNET_JSON_spec_timestamp ("next_restock",
                                     &pd.next_restock)),
    GNUNET_JSON_spec_end ()
  };

  pd.total_sold = 0; /* will be ignored anyway */
  GNUNET_assert (NULL != mi);
  GNUNET_assert (NULL != product_id);
  {
    enum GNUNET_GenericReturnValue res;

    res = TALER_MHD_parse_json_data (connection,
                                     hc->request_body,
                                     spec);
    if (GNUNET_OK != res)
      return (GNUNET_NO == res)
             ? MHD_YES
             : MHD_NO;
  }
  if (total_stock < -1)
  {
    GNUNET_break_op (0);
    GNUNET_JSON_parse_free (spec);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_BAD_REQUEST,
                                       TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                       "total_stock");
  }
  if (-1 == total_stock)
    pd.total_stock = INT64_MAX;
  else
    pd.total_stock = (uint64_t) total_stock;
  if (NULL == pd.address)
    pd.address = json_object ();

  if (! TMH_location_object_valid (pd.address))
  {
    GNUNET_break_op (0);
    GNUNET_JSON_parse_free (spec);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_BAD_REQUEST,
                                       TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                       "address");
  }
  if (NULL == pd.description_i18n)
    pd.description_i18n = json_object ();

  if (! TALER_JSON_check_i18n (pd.description_i18n))
  {
    GNUNET_break_op (0);
    GNUNET_JSON_parse_free (spec);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_BAD_REQUEST,
                                       TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                       "description_i18n");
  }

  if (NULL == pd.taxes)
    pd.taxes = json_array ();
  /* check taxes is well-formed */
  if (! TMH_taxes_array_valid (pd.taxes))
  {
    GNUNET_break_op (0);
    GNUNET_JSON_parse_free (spec);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_BAD_REQUEST,
                                       TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                       "taxes");
  }
  if (NULL == pd.image)
    pd.image = "";
  if (! TMH_image_data_url_valid (pd.image))
  {
    GNUNET_break_op (0);
    GNUNET_JSON_parse_free (spec);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_BAD_REQUEST,
                                       TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                       "image");
  }
  if ( (pd.total_stock < pd.total_sold + pd.total_lost) ||
       (pd.total_sold + pd.total_lost < pd.total_sold) /* integer overflow */)
  {
    GNUNET_break_op (0);
    GNUNET_JSON_parse_free (spec);
    return TALER_MHD_reply_with_error (
      connection,
      MHD_HTTP_BAD_REQUEST,
      TALER_EC_MERCHANT_PRIVATE_PATCH_PRODUCTS_TOTAL_LOST_EXCEEDS_STOCKS,
      NULL);
  }
  qs = TMH_db->update_product (TMH_db->cls,
                               mi->settings.id,
                               product_id,
                               &pd);
  {
    MHD_RESULT ret = MHD_NO;

    switch (qs)
    {
    case GNUNET_DB_STATUS_HARD_ERROR:
      GNUNET_break (0);
      ret = TALER_MHD_reply_with_error (connection,
                                        MHD_HTTP_INTERNAL_SERVER_ERROR,
                                        TALER_EC_GENERIC_DB_STORE_FAILED,
                                        NULL);
      break;
    case GNUNET_DB_STATUS_SOFT_ERROR:
      GNUNET_break (0);
      ret = TALER_MHD_reply_with_error (connection,
                                        MHD_HTTP_INTERNAL_SERVER_ERROR,
                                        TALER_EC_GENERIC_INTERNAL_INVARIANT_FAILURE,
                                        "unexpected serialization problem");
      break;
    case GNUNET_DB_STATUS_SUCCESS_NO_RESULTS:
      ret = determine_cause (connection,
                             mi->settings.id,
                             product_id,
                             &pd);
      break;
    case GNUNET_DB_STATUS_SUCCESS_ONE_RESULT:
      ret = TALER_MHD_reply_static (connection,
                                    MHD_HTTP_NO_CONTENT,
                                    NULL,
                                    NULL,
                                    0);
      break;
    }
    GNUNET_JSON_parse_free (spec);
    return ret;
  }
}


/* end of taler-merchant-httpd_private-patch-products-ID.c */
