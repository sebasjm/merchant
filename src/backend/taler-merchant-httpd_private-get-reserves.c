/*
  This file is part of TALER
  (C) 2019-2021 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file taler-merchant-httpd_private-get-reserves.c
 * @brief implement GET /reserves
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_json_lib.h>
#include "taler-merchant-httpd_private-get-reserves.h"


/**
 * Add reserve details to our JSON array.
 *
 * @param cls a `json_t *` JSON array to build
 * @param reserve_pub public key of the reserve
 * @param creation_time time when the reserve was setup
 * @param expiration_time time when the reserve will be closed by the exchange
 * @param merchant_initial_amount initial amount that the merchant claims to have filled the
 *           reserve with
 * @param exchange_initial_amount initial amount that the exchange claims to have received
 * @param pickup_amount total of tips that were picked up from this reserve
 * @param committed_amount total of tips that the merchant committed to, but that were not
 *           picked up yet
 * @param active  true if the reserve is still active (we have the private key)
 */
static void
add_reserve (void *cls,
             const struct TALER_ReservePublicKeyP *reserve_pub,
             struct GNUNET_TIME_Timestamp creation_time,
             struct GNUNET_TIME_Timestamp expiration_time,
             const struct TALER_Amount *merchant_initial_amount,
             const struct TALER_Amount *exchange_initial_amount,
             const struct TALER_Amount *pickup_amount,
             const struct TALER_Amount *committed_amount,
             bool active)
{
  json_t *pa = cls;

  GNUNET_assert (0 ==
                 json_array_append_new (
                   pa,
                   GNUNET_JSON_PACK (
                     GNUNET_JSON_pack_data_auto ("reserve_pub",
                                                 reserve_pub),
                     GNUNET_JSON_pack_timestamp ("creation_time",
                                                 creation_time),
                     GNUNET_JSON_pack_timestamp ("expiration_time",
                                                 expiration_time),
                     TALER_JSON_pack_amount ("merchant_initial_amount",
                                             merchant_initial_amount),
                     TALER_JSON_pack_amount ("exchange_initial_amount",
                                             exchange_initial_amount),
                     TALER_JSON_pack_amount ("pickup_amount",
                                             pickup_amount),
                     TALER_JSON_pack_amount ("committed_amount",
                                             committed_amount),
                     GNUNET_JSON_pack_bool ("active",
                                            active))));
}


/**
 * Handle a GET "/reserves" request.
 *
 * @param rh context of the handler
 * @param connection the MHD connection to handle
 * @param[in,out] hc context with further information about the request
 * @return MHD result code
 */
MHD_RESULT
TMH_private_get_reserves (const struct TMH_RequestHandler *rh,
                          struct MHD_Connection *connection,
                          struct TMH_HandlerContext *hc)
{
  json_t *ra;
  enum GNUNET_DB_QueryStatus qs;
  struct GNUNET_TIME_Timestamp created_after
    = GNUNET_TIME_UNIT_ZERO_TS;
  enum TALER_EXCHANGE_YesNoAll active;
  enum TALER_EXCHANGE_YesNoAll failures;

  if (! (TALER_arg_to_yna (connection,
                           "active",
                           TALER_EXCHANGE_YNA_ALL,
                           &active)) )
  {
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_BAD_REQUEST,
                                       TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                       "active");
  }

  if (! (TALER_arg_to_yna (connection,
                           "failures",
                           TALER_EXCHANGE_YNA_ALL,
                           &failures)) )
  {
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_BAD_REQUEST,
                                       TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                       "failures");
  }

  ra = json_array ();
  GNUNET_assert (NULL != ra);
  qs = TMH_db->lookup_reserves (TMH_db->cls,
                                hc->instance->settings.id,
                                created_after,
                                active,
                                failures,
                                &add_reserve,
                                ra);
  if (0 > qs)
  {
    GNUNET_break (0);
    json_decref (ra);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_INTERNAL_SERVER_ERROR,
                                       TALER_EC_GENERIC_DB_FETCH_FAILED,
                                       "reserves");
  }
  return TALER_MHD_REPLY_JSON_PACK (
    connection,
    MHD_HTTP_OK,
    GNUNET_JSON_pack_array_steal ("reserves",
                                  ra));
}


/* end of taler-merchant-httpd_private-get-reserves.c */
