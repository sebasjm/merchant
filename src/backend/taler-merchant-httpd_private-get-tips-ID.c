/*
  This file is part of TALER
  (C) 2017-2021 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file taler-merchant-httpd_get-tips-ID.c
 * @brief implementation of a GET /tips/ID handler
 * @author Christian Grothoff
 */
#include "platform.h"
#include <microhttpd.h>
#include <jansson.h>
#include <taler/taler_json_lib.h>
#include <taler/taler_signatures.h>
#include "taler-merchant-httpd.h"
#include "taler-merchant-httpd_mhd.h"
#include "taler-merchant-httpd_exchanges.h"


MHD_RESULT
TMH_private_get_tips_ID (const struct TMH_RequestHandler *rh,
                         struct MHD_Connection *connection,
                         struct TMH_HandlerContext *hc)
{
  struct GNUNET_HashCode tip_id;
  struct TALER_Amount total_authorized;
  struct TALER_Amount total_picked_up;
  char *reason;
  struct GNUNET_TIME_Timestamp expiration;
  struct TALER_ReservePublicKeyP reserve_pub;
  unsigned int pickups_length = 0;
  struct TALER_MERCHANTDB_PickupDetails *pickups = NULL;
  enum GNUNET_DB_QueryStatus qs;
  bool fpu;
  json_t *pickups_json = NULL;

  GNUNET_assert (NULL != hc->infix);
  if (GNUNET_OK !=
      GNUNET_CRYPTO_hash_from_string (hc->infix,
                                      &tip_id))
  {
    /* tip_id has wrong encoding */
    GNUNET_break_op (0);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_BAD_REQUEST,
                                       TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                       hc->infix);
  }
  {
    const char *pstr;

    pstr = MHD_lookup_connection_value (connection,
                                        MHD_GET_ARGUMENT_KIND,
                                        "pickups");
    fpu = (NULL != pstr)
          ? 0 == strcasecmp (pstr, "yes")
          : false;
  }
  TMH_db->preflight (TMH_db->cls);
  qs = TMH_db->lookup_tip_details (TMH_db->cls,
                                   hc->instance->settings.id,
                                   &tip_id,
                                   fpu,
                                   &total_authorized,
                                   &total_picked_up,
                                   &reason,
                                   &expiration,
                                   &reserve_pub,
                                   &pickups_length,
                                   &pickups);
  if (GNUNET_DB_STATUS_SUCCESS_ONE_RESULT != qs)
  {
    unsigned int response_code;
    enum TALER_ErrorCode ec;

    switch (qs)
    {
    case GNUNET_DB_STATUS_SUCCESS_NO_RESULTS:
      ec = TALER_EC_MERCHANT_GENERIC_TIP_ID_UNKNOWN;
      response_code = MHD_HTTP_NOT_FOUND;
      break;
    case GNUNET_DB_STATUS_SOFT_ERROR:
      ec = TALER_EC_GENERIC_DB_SOFT_FAILURE;
      response_code = MHD_HTTP_INTERNAL_SERVER_ERROR;
      break;
    case GNUNET_DB_STATUS_HARD_ERROR:
      ec = TALER_EC_GENERIC_DB_COMMIT_FAILED;
      response_code = MHD_HTTP_INTERNAL_SERVER_ERROR;
      break;
    default:
      GNUNET_break (0);
      ec = TALER_EC_GENERIC_INTERNAL_INVARIANT_FAILURE;
      response_code = MHD_HTTP_INTERNAL_SERVER_ERROR;
      break;
    }
    return TALER_MHD_reply_with_error (connection,
                                       response_code,
                                       ec,
                                       NULL);
  }
  if (fpu)
  {
    pickups_json = json_array ();
    GNUNET_assert (NULL != pickups_json);
    for (unsigned int i = 0; i<pickups_length; i++)
    {
      GNUNET_assert (0 ==
                     json_array_append_new (
                       pickups_json,
                       GNUNET_JSON_PACK (
                         GNUNET_JSON_pack_data_auto ("pickup_id",
                                                     &pickups[i].pickup_id),
                         GNUNET_JSON_pack_uint64 ("num_planchets",
                                                  pickups[i].num_planchets),
                         TALER_JSON_pack_amount ("requested_amount",
                                                 &pickups[i].requested_amount))));
    }
  }
  GNUNET_array_grow (pickups,
                     pickups_length,
                     0);
  {
    MHD_RESULT ret;

    ret = TALER_MHD_REPLY_JSON_PACK (
      connection,
      MHD_HTTP_OK,
      TALER_JSON_pack_amount ("total_authorized",
                              &total_authorized),
      TALER_JSON_pack_amount ("total_picked_up",
                              &total_picked_up),
      GNUNET_JSON_pack_string ("reason",
                               reason),
      GNUNET_JSON_pack_timestamp ("expiration",
                                  expiration),
      GNUNET_JSON_pack_data_auto ("reserve_pub",
                                  &reserve_pub),
      GNUNET_JSON_pack_allow_null (
        GNUNET_JSON_pack_array_steal ("pickups",
                                      pickups_json)));
    GNUNET_free (reason);
    return ret;
  }
}
