/*
  This file is part of TALER
  Copyright (C) 2021 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Lesser General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file merchantdb_helper.c
 * @brief Helper functions for the merchant database logic
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_util.h>
#include "taler_merchantdb_lib.h"


void
TALER_MERCHANTDB_product_details_free (
  struct TALER_MERCHANTDB_ProductDetails *pd)
{
  GNUNET_free (pd->description);
  json_decref (pd->description_i18n);
  GNUNET_free (pd->unit);
  json_decref (pd->taxes);
  GNUNET_free (pd->image);
  json_decref (pd->address);
}


/* end of merchantdb_helper.c */
