--
-- This file is part of TALER
-- Copyright (C) 2021 Taler Systems SA
--
-- TALER is free software; you can redistribute it and/or modify it under the
-- terms of the GNU General Public License as published by the Free Software
-- Foundation; either version 3, or (at your option) any later version.
--
-- TALER is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License along with
-- TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
--

-- This file includes migrations up to 0.8.2.
-- All migrations after that release should
-- to into a different file.

-- Everything in one big transaction
BEGIN;

-- Check patch versioning is in place.
SELECT _v.register_patch('merchant-0002', NULL, NULL);


-- need serial IDs on various tables for exchange-auditor replication
ALTER TABLE merchant_instances
  ADD COLUMN auth_hash BYTEA CHECK(LENGTH(auth_hash)=64),
  ADD COLUMN auth_salt BYTEA CHECK(LENGTH(auth_salt)=32);
COMMENT ON COLUMN merchant_instances.auth_hash
  IS 'hash used for merchant back office Authorization, NULL for no check';
COMMENT ON COLUMN merchant_instances.auth_salt
  IS 'salt to use when hashing Authorization header before comparing with auth_hash';



-- need to preserve payto_uri for extended reserve API (easier than to reconstruct)
ALTER TABLE merchant_tip_reserve_keys
  ADD COLUMN payto_uri VARCHAR;
COMMENT ON COLUMN merchant_tip_reserve_keys.payto_uri
  IS 'payto:// URI used to fund the reserve, may be NULL once reserve is funded';


-- need serial IDs on various tables for exchange-auditor replication
ALTER TABLE merchant_transfer_signatures
  ADD COLUMN credit_amount_val INT8,
  ADD COLUMN credit_amount_frac INT4;
COMMENT ON COLUMN merchant_transfers.credit_amount_val
  IS 'actual value of the (aggregated) wire transfer, excluding the wire fee, according to the exchange';


-- support different amounts claimed by exchange and merchant about wire transfers,
-- add column to tell when this happens; but "believe" existing amounts match, as
-- otherwise earlier version of the code would have failed hard.
UPDATE merchant_transfer_signatures
  SET credit_amount_val=mt.credit_amount_val,
      credit_amount_frac=mt.credit_amount_frac
  FROM merchant_transfer_signatures mts
  INNER JOIN merchant_transfers mt USING(credit_serial);
ALTER TABLE merchant_transfer_signatures
  ALTER COLUMN credit_amount_val SET NOT NULL,
  ALTER COLUMN credit_amount_frac SET NOT NULL;

-- contract terms now also need to check the claim_token,
-- as we consider the fulfillment_url private
ALTER TABLE merchant_contract_terms
  ADD COLUMN claim_token BYTEA;
UPDATE merchant_contract_terms mct
  SET claim_token=ord.claim_token
  FROM merchant_orders ord
  WHERE mct.order_serial=ord.order_serial;
-- If the merchant_orders row already has been GCed,
-- we can't migrate to the correct claim_token anymore.
-- Instead of setting it to all zeroes (no auth),
-- we set it to a prefix of the h_contract_terms.
UPDATE merchant_contract_terms
  SET claim_token = substring(h_contract_terms for 16)
  WHERE claim_token IS NULL;
ALTER TABLE merchant_contract_terms
  ALTER COLUMN claim_token SET NOT NULL,
  ADD CHECK (LENGTH(claim_token)=16);
COMMENT ON COLUMN merchant_contract_terms.claim_token
  IS 'Token optionally used to access the status of the order. All zeros (not NULL) if not used';

-- Complete transaction
COMMIT;
