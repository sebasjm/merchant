--
-- This file is part of TALER
-- Copyright (C) 2021 Taler Systems SA
--
-- TALER is free software; you can redistribute it and/or modify it under the
-- terms of the GNU General Public License as published by the Free Software
-- Foundation; either version 3, or (at your option) any later version.
--
-- TALER is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License along with
-- TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
--

-- This file includes migrations up to 0.8.5.
-- All migrations after that release should
-- to into a different file.

-- Everything in one big transaction
BEGIN;

-- Check patch versioning is in place.
SELECT _v.register_patch('merchant-0003', NULL, NULL);

CREATE TABLE IF NOT EXISTS merchant_kyc
(kyc_serial_id BIGSERIAL UNIQUE
,kyc_timestamp INT8 NOT NULL
,kyc_ok BOOLEAN NOT NULL DEFAULT (FALSE)
,exchange_sig BYTEA CHECK(LENGTH(exchange_sig)=64)
,exchange_pub BYTEA CHECK(LENGTH(exchange_pub)=32)
,exchange_kyc_serial INT8 NOT NULL DEFAULT(0)
,account_serial INT8 NOT NULL
  REFERENCES merchant_accounts (account_serial) ON DELETE CASCADE
,exchange_url VARCHAR NOT NULL
,PRIMARY KEY (account_serial,exchange_url)
);
COMMENT ON TABLE merchant_kyc
  IS 'Status of the KYC process of a merchant account at an exchange';
COMMENT ON COLUMN merchant_kyc.kyc_timestamp
  IS 'Last time we checked our KYC status at the exchange. Useful to re-check if the status is very stale. Also the timestamp used for the exchange signature (if present).';
COMMENT ON COLUMN merchant_kyc.exchange_kyc_serial
  IS 'Number to use in the KYC-endpoints of the exchange to check the KYC status or begin the KYC process. 0 if we do not know it yet.';
COMMENT ON COLUMN merchant_kyc.kyc_ok
  IS 'true if the KYC check was passed successfully';
COMMENT ON COLUMN merchant_kyc.exchange_sig
  IS 'signature of the exchange affirming the KYC passed (or NULL if exchange does not require KYC or not kyc_ok)';
COMMENT ON COLUMN merchant_kyc.exchange_pub
  IS 'public key used with exchange_sig (or NULL if exchange_sig is NULL)';
COMMENT ON COLUMN merchant_kyc.account_serial
  IS 'Which bank account of the merchant is the KYC status for';
COMMENT ON COLUMN merchant_kyc.exchange_url
  IS 'Which exchange base URL is this KYC status valid for';


-- Complete transaction
COMMIT;
