/*
  This file is part of TALER
  Copyright (C) 2014-2021 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Lesser General Public License as published by the Free Software
  Foundation; either version 2.1, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License along with
  TALER; see the file COPYING.LGPL.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file merchant_api_post_instance_auth.c
 * @brief Implementation of the POST /instance/$ID/private/auth request
 * @author Christian Grothoff
 */
#include "platform.h"
#include <curl/curl.h>
#include <jansson.h>
#include <microhttpd.h> /* just for HTTP status codes */
#include <gnunet/gnunet_util_lib.h>
#include <gnunet/gnunet_curl_lib.h>
#include "taler_merchant_service.h"
#include "merchant_api_curl_defaults.h"
#include <taler/taler_json_lib.h>
#include <taler/taler_signatures.h>
#include <taler/taler_curl_lib.h>


/**
 * Handle for a POST /instances/$ID/private/auth operation.
 */
struct TALER_MERCHANT_InstanceAuthPostHandle
{
  /**
   * The url for this request.
   */
  char *url;

  /**
   * Handle for the request.
   */
  struct GNUNET_CURL_Job *job;

  /**
   * Function to call with the result.
   */
  TALER_MERCHANT_InstanceAuthPostCallback cb;

  /**
   * Closure for @a cb.
   */
  void *cb_cls;

  /**
   * Reference to the execution context.
   */
  struct GNUNET_CURL_Context *ctx;

  /**
   * Minor context that holds body and headers.
   */
  struct TALER_CURL_PostContext post_ctx;

};


/**
 * Function called when we're done processing the
 * HTTP GET /instances/$ID/private/auth request.
 *
 * @param cls the `struct TALER_MERCHANT_InstanceAuthPostHandle`
 * @param response_code HTTP response code, 0 on error
 * @param response response body, NULL if not in JSON
 */
static void
handle_post_instance_auth_finished (void *cls,
                                    long response_code,
                                    const void *response)
{
  struct TALER_MERCHANT_InstanceAuthPostHandle *iaph = cls;
  const json_t *json = response;
  struct TALER_MERCHANT_HttpResponse hr = {
    .http_status = (unsigned int) response_code,
    .reply = json
  };

  iaph->job = NULL;
  GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
              "Got /instances/$ID response with status code %u\n",
              (unsigned int) response_code);
  switch (response_code)
  {
  case MHD_HTTP_NO_CONTENT:
    break;
  case MHD_HTTP_BAD_REQUEST:
    /* happens if the auth token is malformed */
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    /* Nothing really to verify, merchant says we need to authenticate. */
    break;
  case MHD_HTTP_UNAUTHORIZED:
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    /* Nothing really to verify, merchant says we need to authenticate. */
    break;
  default:
    /* unexpected response code */
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u/%d\n",
                (unsigned int) response_code,
                (int) hr.ec);
    break;
  }
  iaph->cb (iaph->cb_cls,
            &hr);
  TALER_MERCHANT_instance_auth_post_cancel (iaph);
}


struct TALER_MERCHANT_InstanceAuthPostHandle *
TALER_MERCHANT_instance_auth_post (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  const char *instance_id,
  const char *auth_token,
  TALER_MERCHANT_InstanceAuthPostCallback cb,
  void *cb_cls)
{
  struct TALER_MERCHANT_InstanceAuthPostHandle *iaph;
  json_t *req_obj;

  iaph = GNUNET_new (struct TALER_MERCHANT_InstanceAuthPostHandle);
  iaph->ctx = ctx;
  iaph->cb = cb;
  iaph->cb_cls = cb_cls;
  if (NULL != instance_id)
  {
    char *path;

    GNUNET_asprintf (&path,
                     "management/instances/%s/auth",
                     instance_id);
    iaph->url = TALER_url_join (backend_url,
                                path,
                                NULL);
    GNUNET_free (path);
  }
  else
  {
    /* backend_url is already identifying the instance */
    iaph->url = TALER_url_join (backend_url,
                                "private/auth",
                                NULL);
  }
  if (NULL == iaph->url)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Could not construct request URL.\n");
    GNUNET_free (iaph);
    return NULL;
  }
  if (NULL == auth_token)
  {
    req_obj = GNUNET_JSON_PACK (
      GNUNET_JSON_pack_string ("method",
                               "external"));
  }
  else
  {
    if (0 != strncasecmp (RFC_8959_PREFIX,
                          auth_token,
                          strlen (RFC_8959_PREFIX)))
    {
      GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                  "Authentication token must start with `%s'\n",
                  RFC_8959_PREFIX);
      GNUNET_free (iaph->url);
      GNUNET_free (iaph);
      return NULL;
    }
    req_obj = GNUNET_JSON_PACK (
      GNUNET_JSON_pack_string ("method",
                               "token"),
      GNUNET_JSON_pack_string ("token",
                               auth_token));
  }
  GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
              "Requesting URL '%s'\n",
              iaph->url);
  {
    CURL *eh;

    eh = TALER_MERCHANT_curl_easy_get_ (iaph->url);
    if (GNUNET_OK !=
        TALER_curl_easy_post (&iaph->post_ctx,
                              eh,
                              req_obj))
    {
      GNUNET_break (0);
      curl_easy_cleanup (eh);
      json_decref (req_obj);
      GNUNET_free (iaph->url);
      GNUNET_free (iaph);
      return NULL;
    }
    json_decref (req_obj);
    GNUNET_assert (CURLE_OK ==
                   curl_easy_setopt (eh,
                                     CURLOPT_CUSTOMREQUEST,
                                     MHD_HTTP_METHOD_POST));
    iaph->job = GNUNET_CURL_job_add2 (ctx,
                                      eh,
                                      iaph->post_ctx.headers,
                                      &handle_post_instance_auth_finished,
                                      iaph);
  }
  return iaph;
}


void
TALER_MERCHANT_instance_auth_post_cancel (
  struct TALER_MERCHANT_InstanceAuthPostHandle *iaph)
{
  if (NULL != iaph->job)
    GNUNET_CURL_job_cancel (iaph->job);
  TALER_curl_easy_post_finished (&iaph->post_ctx);
  GNUNET_free (iaph->url);
  GNUNET_free (iaph);
}
