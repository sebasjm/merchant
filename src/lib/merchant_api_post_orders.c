/*
  This file is part of TALER
  Copyright (C) 2014-2021 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation; either version 2.1,
  or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with TALER; see the file COPYING.LGPL.  If not,
  see <http://www.gnu.org/licenses/>
*/
/**
 * @file merchant_api_post_orders.c
 * @brief Implementation of the POST /orders
 * @author Christian Grothoff
 * @author Marcello Stanisci
 */
#include "platform.h"
#include <curl/curl.h>
#include <jansson.h>
#include <microhttpd.h> /* just for HTTP status codes */
#include <gnunet/gnunet_util_lib.h>
#include <gnunet/gnunet_curl_lib.h>
#include "taler_merchant_service.h"
#include "merchant_api_curl_defaults.h"
#include <taler/taler_json_lib.h>
#include <taler/taler_signatures.h>
#include <taler/taler_curl_lib.h>


/**
 * @brief A POST /orders Handle
 */
struct TALER_MERCHANT_PostOrdersHandle
{

  /**
   * The url for this request.
   */
  char *url;

  /**
   * Handle for the request.
   */
  struct GNUNET_CURL_Job *job;

  /**
   * Function to call with the result.
   */
  TALER_MERCHANT_PostOrdersCallback cb;

  /**
   * Closure for @a cb.
   */
  void *cb_cls;

  /**
   * Reference to the execution context.
   */
  struct GNUNET_CURL_Context *ctx;

  /**
   * Minor context that holds body and headers.
   */
  struct TALER_CURL_PostContext post_ctx;
};


/**
 * Function called when we're done processing the
 * HTTP POST /orders request.
 *
 * @param cls the `struct TALER_MERCHANT_PostOrdersHandle`
 * @param response_code HTTP response code, 0 on error
 * @param response response body, NULL if not JSON
 */
static void
handle_post_order_finished (void *cls,
                            long response_code,
                            const void *response)
{
  struct TALER_MERCHANT_PostOrdersHandle *po = cls;
  const json_t *json = response;
  struct TALER_MERCHANT_PostOrdersReply por = {
    .hr.http_status = (unsigned int) response_code,
    .hr.reply = json
  };
  struct TALER_ClaimTokenP token = {0};

  po->job = NULL;
  switch (response_code)
  {
  case 0:
    por.hr.ec = TALER_EC_GENERIC_INVALID_RESPONSE;
    break;
  case MHD_HTTP_OK:
    {
      struct GNUNET_JSON_Specification spec[] = {
        GNUNET_JSON_spec_string ("order_id",
                                 &por.details.ok.order_id),
        GNUNET_JSON_spec_mark_optional (
          GNUNET_JSON_spec_fixed_auto ("token",
                                       &token)),
        GNUNET_JSON_spec_end ()
      };

      if (GNUNET_OK !=
          GNUNET_JSON_parse (json,
                             spec,
                             NULL, NULL))
      {
        GNUNET_break_op (0);
        por.hr.http_status = 0;
        por.hr.ec = TALER_EC_GENERIC_REPLY_MALFORMED;
      }
      else
      {
        if (! GNUNET_is_zero (&token))
          por.details.ok.token = &token;
      }
    }
    break;
  case MHD_HTTP_BAD_REQUEST:
    json_dumpf (json,
                stderr,
                JSON_INDENT (2));
    por.hr.ec = TALER_JSON_get_error_code (json);
    por.hr.hint = TALER_JSON_get_error_hint (json);
    /* This should never happen, either us or
       the merchant is buggy (or API version conflict);
       just pass JSON reply to the application */
    break;
  case MHD_HTTP_UNAUTHORIZED:
    por.hr.ec = TALER_JSON_get_error_code (json);
    por.hr.hint = TALER_JSON_get_error_hint (json);
    /* Nothing really to verify, merchant says we need to authenticate. */
    break;
  case MHD_HTTP_FORBIDDEN:
    /* Nothing really to verify, merchant says one
       of the signatures is invalid; as we checked them,
       this should never happen, we should pass the JSON
       reply to the application */
    por.hr.ec = TALER_JSON_get_error_code (json);
    por.hr.hint = TALER_JSON_get_error_hint (json);
    break;
  case MHD_HTTP_NOT_FOUND:
    /* Nothing really to verify, this should never
       happen, we should pass the JSON reply to the application */
    por.hr.ec = TALER_JSON_get_error_code (json);
    por.hr.hint = TALER_JSON_get_error_hint (json);
    break;
  case MHD_HTTP_CONFLICT:
    por.hr.ec = TALER_JSON_get_error_code (json);
    por.hr.hint = TALER_JSON_get_error_hint (json);
    break;
  case MHD_HTTP_GONE:
    /* The quantity of some product requested was not available. */
    {

      struct GNUNET_JSON_Specification spec[] = {
        GNUNET_JSON_spec_string (
          "product_id",
          &por.details.gone.product_id),
        GNUNET_JSON_spec_uint64 (
          "requested_quantity",
          &por.details.gone.requested_quantity),
        GNUNET_JSON_spec_uint64 (
          "available_quantity",
          &por.details.gone.available_quantity),
        GNUNET_JSON_spec_mark_optional (
          GNUNET_JSON_spec_timestamp (
            "restock_expected",
            &por.details.gone.restock_expected)),
        GNUNET_JSON_spec_end ()
      };

      if (GNUNET_OK !=
          GNUNET_JSON_parse (json,
                             spec,
                             NULL, NULL))
      {
        GNUNET_break_op (0);
        por.hr.http_status = 0;
        por.hr.ec = TALER_EC_GENERIC_REPLY_MALFORMED;
      }
      break;
    }
  case MHD_HTTP_INTERNAL_SERVER_ERROR:
    /* Server had an internal issue; we should retry,
       but this API leaves this to the application */
    por.hr.ec = TALER_JSON_get_error_code (json);
    por.hr.hint = TALER_JSON_get_error_hint (json);
    break;
  default:
    /* unexpected response code */
    por.hr.ec = TALER_JSON_get_error_code (json);
    por.hr.hint = TALER_JSON_get_error_hint (json);
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u/%d\n",
                (unsigned int) response_code,
                (int) por.hr.ec);
    GNUNET_break_op (0);
    break;
  }
  po->cb (po->cb_cls,
          &por);
  TALER_MERCHANT_orders_post_cancel (po);
}


struct TALER_MERCHANT_PostOrdersHandle *
TALER_MERCHANT_orders_post (struct GNUNET_CURL_Context *ctx,
                            const char *backend_url,
                            const json_t *order,
                            struct GNUNET_TIME_Relative refund_delay,
                            TALER_MERCHANT_PostOrdersCallback cb,
                            void *cb_cls)
{
  return TALER_MERCHANT_orders_post2 (ctx,
                                      backend_url,
                                      order,
                                      refund_delay,
                                      NULL,
                                      0,
                                      NULL,
                                      0,
                                      NULL,
                                      true,
                                      cb,
                                      cb_cls);
}


struct TALER_MERCHANT_PostOrdersHandle *
TALER_MERCHANT_orders_post2 (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  const json_t *order,
  struct GNUNET_TIME_Relative refund_delay,
  const char *payment_target,
  unsigned int inventory_products_length,
  const struct TALER_MERCHANT_InventoryProduct inventory_products[],
  unsigned int uuids_length,
  const char *uuids[],
  bool create_token,
  TALER_MERCHANT_PostOrdersCallback cb,
  void *cb_cls)
{
  struct TALER_MERCHANT_PostOrdersHandle *po;
  json_t *req;
  CURL *eh;

  po = GNUNET_new (struct TALER_MERCHANT_PostOrdersHandle);
  po->ctx = ctx;
  po->cb = cb;
  po->cb_cls = cb_cls;
  po->url = TALER_url_join (backend_url,
                            "private/orders",
                            NULL);
  req = GNUNET_JSON_PACK (
    GNUNET_JSON_pack_object_incref ("order",
                                    (json_t *) order),
    GNUNET_JSON_pack_allow_null (
      GNUNET_JSON_pack_string ("payment_target",
                               payment_target)));
  if (0 != refund_delay.rel_value_us)
  {
    GNUNET_assert (0 ==
                   json_object_set_new (req,
                                        "refund_delay",
                                        GNUNET_JSON_from_time_rel (
                                          refund_delay)));
  }
  if (0 != inventory_products_length)
  {
    json_t *ipa = json_array ();

    GNUNET_assert (NULL != ipa);
    for (unsigned int i = 0; i<inventory_products_length; i++)
    {
      json_t *ip;

      ip = GNUNET_JSON_PACK (
        GNUNET_JSON_pack_string ("product_id",
                                 inventory_products[i].product_id),
        GNUNET_JSON_pack_uint64 ("quantity",
                                 inventory_products[i].quantity));
      GNUNET_assert (NULL != ip);
      GNUNET_assert (0 ==
                     json_array_append_new (ipa,
                                            ip));
    }
    GNUNET_assert (0 ==
                   json_object_set_new (req,
                                        "inventory_products",
                                        ipa));
  }
  if (0 != uuids_length)
  {
    json_t *ua = json_array ();

    GNUNET_assert (NULL != ua);
    for (unsigned int i = 0; i<uuids_length; i++)
    {
      json_t *u;

      u = json_string (uuids[i]);
      GNUNET_assert (0 ==
                     json_array_append_new (ua,
                                            u));
    }
    GNUNET_assert (0 ==
                   json_object_set_new (req,
                                        "lock_uuids",
                                        ua));
  }
  if (! create_token)
  {
    GNUNET_assert (0 ==
                   json_object_set_new (req,
                                        "create_token",
                                        json_boolean (create_token)));
  }
  eh = TALER_MERCHANT_curl_easy_get_ (po->url);
  if (GNUNET_OK !=
      TALER_curl_easy_post (&po->post_ctx,
                            eh,
                            req))
  {
    GNUNET_break (0);
    curl_easy_cleanup (eh);
    json_decref (req);
    GNUNET_free (po);
    return NULL;
  }
  json_decref (req);
  po->job = GNUNET_CURL_job_add2 (ctx,
                                  eh,
                                  po->post_ctx.headers,
                                  &handle_post_order_finished,
                                  po);
  return po;
}


void
TALER_MERCHANT_orders_post_cancel (
  struct TALER_MERCHANT_PostOrdersHandle *po)
{
  if (NULL != po->job)
  {
    GNUNET_CURL_job_cancel (po->job);
    po->job = NULL;
  }
  GNUNET_free (po->url);
  TALER_curl_easy_post_finished (&po->post_ctx);
  GNUNET_free (po);
}


/* end of merchant_api_post_orders.c */
