/*
  This file is part of TALER
  Copyright (C) 2014-2021 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Lesser General Public License as published by the Free Software
  Foundation; either version 2.1, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License along with
  TALER; see the file COPYING.LGPL.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file merchant_api_post_order_refund.c
 * @brief Implementation of the POST /orders/ID/refund request
 * @author Christian Grothoff
 * @author Marcello Stanisci
 */
#include "platform.h"
#include <curl/curl.h>
#include <jansson.h>
#include <microhttpd.h> /* just for HTTP status codes */
#include <gnunet/gnunet_util_lib.h>
#include <gnunet/gnunet_curl_lib.h>
#include "taler_merchant_service.h"
#include "merchant_api_curl_defaults.h"
#include <taler/taler_json_lib.h>
#include <taler/taler_signatures.h>
#include <taler/taler_curl_lib.h>


/**
 * Handle for a POST /orders/ID/refund operation.
 */
struct TALER_MERCHANT_OrderRefundHandle
{
  /**
   * Complete URL where the backend offers /refund
   */
  char *url;

  /**
   * Minor context that holds body and headers.
   */
  struct TALER_CURL_PostContext post_ctx;

  /**
   * The CURL context to connect to the backend
   */
  struct GNUNET_CURL_Context *ctx;

  /**
   * The callback to pass the backend response to
   */
  TALER_MERCHANT_RefundCallback cb;

  /**
   * Clasure to pass to the callback
   */
  void *cb_cls;

  /**
   * Handle for the request
   */
  struct GNUNET_CURL_Job *job;
};


/**
 * Callback to process POST /orders/ID/refund response
 *
 * @param cls the `struct TALER_MERCHANT_OrderRefundHandle`
 * @param response_code HTTP response code, 0 on error
 * @param response response body, NULL if not JSON
 */
static void
handle_refund_finished (void *cls,
                        long response_code,
                        const void *response)
{
  struct TALER_MERCHANT_OrderRefundHandle *orh = cls;
  const json_t *json = response;
  struct TALER_MERCHANT_HttpResponse hr = {
    .http_status = (unsigned int) response_code,
    .reply = json
  };

  orh->job = NULL;
  switch (response_code)
  {
  case 0:
    hr.ec = TALER_EC_GENERIC_INVALID_RESPONSE;
    orh->cb (orh->cb_cls,
             &hr,
             NULL,
             NULL);
    break;
  case MHD_HTTP_OK:
    {
      const char *taler_refund_uri;
      struct TALER_PrivateContractHash h_contract;
      struct GNUNET_JSON_Specification spec[] = {
        GNUNET_JSON_spec_string ("taler_refund_uri",
                                 &taler_refund_uri),
        GNUNET_JSON_spec_fixed_auto ("h_contract",
                                     &h_contract),
        GNUNET_JSON_spec_end ()
      };

      if (GNUNET_OK !=
          GNUNET_JSON_parse (json,
                             spec,
                             NULL, NULL))
      {
        GNUNET_break_op (0);
        hr.http_status = 0;
        hr.ec = TALER_EC_GENERIC_INVALID_RESPONSE;
        orh->cb (orh->cb_cls,
                 &hr,
                 NULL,
                 NULL);
        break;
      }
      orh->cb (orh->cb_cls,
               &hr,
               taler_refund_uri,
               &h_contract);
      GNUNET_JSON_parse_free (spec);
    }
    break;
  case MHD_HTTP_UNAUTHORIZED:
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    /* Nothing really to verify, merchant says we need to authenticate. */
    break;
  case MHD_HTTP_FORBIDDEN:
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    /* Nothing really to verify, merchant says we need to authenticate. */
    break;
  case MHD_HTTP_NOT_FOUND:
  case MHD_HTTP_CONFLICT:
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    orh->cb (orh->cb_cls,
             &hr,
             NULL,
             NULL);
    break;
  default:
    GNUNET_break_op (0); /* unexpected status code */
    TALER_MERCHANT_parse_error_details_ (json,
                                         response_code,
                                         &hr);
    orh->cb (orh->cb_cls,
             &hr,
             NULL,
             NULL);
    break;
  }
  TALER_MERCHANT_post_order_refund_cancel (orh);
}


void
TALER_MERCHANT_post_order_refund_cancel (
  struct TALER_MERCHANT_OrderRefundHandle *orh)
{
  if (NULL != orh->job)
  {
    GNUNET_CURL_job_cancel (orh->job);
    orh->job = NULL;
  }
  TALER_curl_easy_post_finished (&orh->post_ctx);
  GNUNET_free (orh->url);
  GNUNET_free (orh);
}


struct TALER_MERCHANT_OrderRefundHandle *
TALER_MERCHANT_post_order_refund (struct GNUNET_CURL_Context *ctx,
                                  const char *backend_url,
                                  const char *order_id,
                                  const struct TALER_Amount *refund,
                                  const char *reason,
                                  TALER_MERCHANT_RefundCallback cb,
                                  void *cb_cls)
{
  struct TALER_MERCHANT_OrderRefundHandle *orh;
  json_t *req;
  CURL *eh;

  orh = GNUNET_new (struct TALER_MERCHANT_OrderRefundHandle);
  orh->ctx = ctx;
  orh->cb = cb;
  orh->cb_cls = cb_cls;
  {
    char *path;

    GNUNET_asprintf (&path,
                     "private/orders/%s/refund",
                     order_id);
    orh->url = TALER_url_join (backend_url,
                               path,
                               NULL);
    GNUNET_free (path);
  }
  if (NULL == orh->url)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Could not construct request URL.\n");
    GNUNET_free (orh);
    return NULL;
  }
  req = GNUNET_JSON_PACK (
    TALER_JSON_pack_amount ("refund",
                            refund),
    GNUNET_JSON_pack_string ("reason",
                             reason));
  GNUNET_assert (NULL != req);
  eh = TALER_MERCHANT_curl_easy_get_ (orh->url);
  if (GNUNET_OK !=
      TALER_curl_easy_post (&orh->post_ctx,
                            eh,
                            req))
  {
    GNUNET_break (0);
    curl_easy_cleanup (eh);
    json_decref (req);
    GNUNET_free (orh->url);
    GNUNET_free (orh);
    return NULL;
  }
  json_decref (req);
  orh->job = GNUNET_CURL_job_add2 (ctx,
                                   eh,
                                   orh->post_ctx.headers,
                                   &handle_refund_finished,
                                   orh);
  if (NULL == orh->job)
  {
    GNUNET_free (orh->url);
    GNUNET_free (orh);
    return NULL;
  }
  return orh;
}


/* end of merchant_api_post_order_refund.c */
