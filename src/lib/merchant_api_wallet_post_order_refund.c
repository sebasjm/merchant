/*
  This file is part of TALER
  Copyright (C) 2020 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Lesser General Public License as published by the Free Software
  Foundation; either version 2.1, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License along with
  TALER; see the file COPYING.LGPL.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file merchant_api_wallet_post_order_refund.c
 * @brief Implementation of the (public) POST /orders/ID/refund request
 * @author Jonathan Buchanan
 */
#include "platform.h"
#include <curl/curl.h>
#include <jansson.h>
#include <microhttpd.h> /* just for HTTP status codes */
#include <gnunet/gnunet_util_lib.h>
#include <gnunet/gnunet_curl_lib.h>
#include "taler_merchant_service.h"
#include "merchant_api_curl_defaults.h"
#include <taler/taler_json_lib.h>
#include <taler/taler_signatures.h>
#include <taler/taler_curl_lib.h>


/**
 * Handle for a (public) POST /orders/ID/refund operation.
 */
struct TALER_MERCHANT_WalletOrderRefundHandle
{
  /**
   * Complete URL where the backend offers /refund
   */
  char *url;

  /**
   * Minor context that holds body and headers.
   */
  struct TALER_CURL_PostContext post_ctx;

  /**
   * The CURL context to connect to the backend
   */
  struct GNUNET_CURL_Context *ctx;

  /**
   * The callback to pass the backend response to
   */
  TALER_MERCHANT_WalletRefundCallback cb;

  /**
   * Clasure to pass to the callback
   */
  void *cb_cls;

  /**
   * Handle for the request
   */
  struct GNUNET_CURL_Job *job;
};


/**
 * Convenience function to call the callback in @a owgh with an error code of
 * @a ec and the exchange body being set to @a reply.
 *
 * @param orh handle providing callback
 * @param ec error code to return to application
 * @param reply JSON reply we got from the exchange, can be NULL
 */
static void
cb_failure (struct TALER_MERCHANT_WalletOrderRefundHandle *orh,
            enum TALER_ErrorCode ec,
            const json_t *reply)
{
  struct TALER_MERCHANT_HttpResponse hr = {
    .ec = ec,
    .reply = reply
  };

  orh->cb (orh->cb_cls,
           &hr,
           NULL,
           NULL,
           NULL,
           0);
}


/**
 * Callback to process (public) POST /orders/ID/refund response
 *
 * @param cls the `struct TALER_MERCHANT_OrderRefundHandle`
 * @param response_code HTTP response code, 0 on error
 * @param response response body, NULL if not JSON
 */
static void
handle_refund_finished (void *cls,
                        long response_code,
                        const void *response)
{
  struct TALER_MERCHANT_WalletOrderRefundHandle *orh = cls;
  const json_t *json = response;
  struct TALER_MERCHANT_HttpResponse hr = {
    .http_status = (unsigned int) response_code,
    .reply = json
  };

  orh->job = NULL;

  switch (response_code)
  {
  case 0:
    hr.ec = TALER_EC_GENERIC_INVALID_RESPONSE;
    orh->cb (orh->cb_cls,
             &hr,
             NULL,
             NULL,
             NULL,
             0);
    break;
  case MHD_HTTP_OK:
    {
      struct TALER_Amount refund_amount;
      json_t *refunds;
      struct TALER_MerchantPublicKeyP merchant_pub;
      unsigned int refund_len;
      struct GNUNET_JSON_Specification spec[] = {
        TALER_JSON_spec_amount_any ("refund_amount",
                                    &refund_amount),
        GNUNET_JSON_spec_json ("refunds",
                               &refunds),
        GNUNET_JSON_spec_fixed_auto ("merchant_pub",
                                     &merchant_pub),
        GNUNET_JSON_spec_end ()
      };

      if (GNUNET_OK !=
          GNUNET_JSON_parse (json,
                             spec,
                             NULL, NULL))
      {
        GNUNET_break_op (0);
        cb_failure (orh,
                    TALER_EC_GENERIC_REPLY_MALFORMED,
                    json);
        TALER_MERCHANT_wallet_post_order_refund_cancel (orh);
        return;
      }

      if (! json_is_array (refunds))
      {
        GNUNET_break_op (0);
        cb_failure (orh,
                    TALER_EC_GENERIC_REPLY_MALFORMED,
                    json);
        GNUNET_JSON_parse_free (spec);
        TALER_MERCHANT_wallet_post_order_refund_cancel (orh);
        return;
      }

      refund_len = json_array_size (refunds);
      {
        struct TALER_MERCHANT_RefundDetail rds[refund_len];

        memset (rds,
                0,
                sizeof (rds));
        for (unsigned int i = 0; i<refund_len; i++)
        {
          struct TALER_MERCHANT_RefundDetail *rd = &rds[i];
          const json_t *jrefund = json_array_get (refunds,
                                                  i);
          const char *refund_status_type;
          uint32_t exchange_status;
          int ret;
          struct GNUNET_JSON_Specification espec[] = {
            GNUNET_JSON_spec_uint32 ("exchange_status",
                                     &exchange_status),
            GNUNET_JSON_spec_end ()
          };

          if (GNUNET_OK !=
              GNUNET_JSON_parse (jrefund,
                                 espec,
                                 NULL, NULL))
          {
            GNUNET_break_op (0);
            cb_failure (orh,
                        TALER_EC_GENERIC_REPLY_MALFORMED,
                        json);
            TALER_MERCHANT_wallet_post_order_refund_cancel (orh);
            return;
          }

          if (MHD_HTTP_OK == exchange_status)
          {
            struct GNUNET_JSON_Specification rspec[] = {
              GNUNET_JSON_spec_string ("type",
                                       &refund_status_type),
              GNUNET_JSON_spec_fixed_auto ("exchange_sig",
                                           &rd->exchange_sig),
              GNUNET_JSON_spec_fixed_auto ("exchange_pub",
                                           &rd->exchange_pub),
              GNUNET_JSON_spec_uint64 ("rtransaction_id",
                                       &rd->rtransaction_id),
              GNUNET_JSON_spec_fixed_auto ("coin_pub",
                                           &rd->coin_pub),
              TALER_JSON_spec_amount_any ("refund_amount",
                                          &rd->refund_amount),
              GNUNET_JSON_spec_end ()
            };

            ret = GNUNET_JSON_parse (jrefund,
                                     rspec,
                                     NULL, NULL);
            if (GNUNET_OK == ret)
            {
              /* check that type field is correct */
              if (0 != strcmp ("success", refund_status_type))
              {
                GNUNET_break_op (0);
                ret = GNUNET_SYSERR;
              }
            }
          }
          else
          {
            struct GNUNET_JSON_Specification rspec[] = {
              GNUNET_JSON_spec_string ("type",
                                       &refund_status_type),
              GNUNET_JSON_spec_fixed_auto ("coin_pub",
                                           &rd->coin_pub),
              GNUNET_JSON_spec_uint64 ("rtransaction_id",
                                       &rd->rtransaction_id),
              TALER_JSON_spec_amount_any ("refund_amount",
                                          &rd->refund_amount),
              GNUNET_JSON_spec_end ()
            };

            ret = GNUNET_JSON_parse (jrefund,
                                     rspec,
                                     NULL, NULL);
            if (GNUNET_OK == ret)
            {
              /* parse optional arguments */
              json_t *jec;

              jec = json_object_get (jrefund,
                                     "exchange_code");
              if (NULL != jec)
              {
                if (! json_is_integer (jec))
                {
                  GNUNET_break_op (0);
                  ret = GNUNET_SYSERR;
                }
                else
                {
                  rd->hr.ec = (enum TALER_ErrorCode) json_integer_value (jec);
                }
              }
              rd->hr.reply = json_object_get (jrefund,
                                              "exchange_reply");
              /* check that type field is correct */
              if (0 != strcmp ("failure", refund_status_type))
              {
                GNUNET_break_op (0);
                ret = GNUNET_SYSERR;
              }
            }
          }
          if (GNUNET_OK != ret)
          {
            GNUNET_break_op (0);
            cb_failure (orh,
                        TALER_EC_GENERIC_REPLY_MALFORMED,
                        json);
            TALER_MERCHANT_wallet_post_order_refund_cancel (orh);
            return;
          }
          rd->hr.http_status = exchange_status;
        }

        {
          struct TALER_MERCHANT_HttpResponse hr = {
            .reply = json,
            .http_status = MHD_HTTP_OK
          };

          orh->cb (orh->cb_cls,
                   &hr,
                   &refund_amount,
                   &merchant_pub,
                   rds,
                   refund_len);
        }
      }
      GNUNET_JSON_parse_free (spec);
    }
    break;
  case MHD_HTTP_NO_CONTENT:
    orh->cb (orh->cb_cls,
             &hr,
             NULL,
             NULL,
             NULL,
             0);
    break;
  case MHD_HTTP_CONFLICT:
  case MHD_HTTP_NOT_FOUND:
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    orh->cb (orh->cb_cls,
             &hr,
             NULL,
             NULL,
             NULL,
             0);
    break;
  default:
    GNUNET_break_op (0); /* unexpected status code */
    TALER_MERCHANT_parse_error_details_ (json,
                                         response_code,
                                         &hr);
    orh->cb (orh->cb_cls,
             &hr,
             NULL,
             NULL,
             NULL,
             0);
    break;
  }
  TALER_MERCHANT_wallet_post_order_refund_cancel (orh);
}


struct TALER_MERCHANT_WalletOrderRefundHandle *
TALER_MERCHANT_wallet_post_order_refund (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  const char *order_id,
  const struct TALER_PrivateContractHash *h_contract_terms,
  TALER_MERCHANT_WalletRefundCallback cb,
  void *cb_cls)
{
  struct TALER_MERCHANT_WalletOrderRefundHandle *orh;
  json_t *req;
  CURL *eh;

  orh = GNUNET_new (struct TALER_MERCHANT_WalletOrderRefundHandle);
  orh->ctx = ctx;
  orh->cb = cb;
  orh->cb_cls = cb_cls;
  {
    char *path;

    GNUNET_asprintf (&path,
                     "orders/%s/refund",
                     order_id);
    orh->url = TALER_url_join (backend_url,
                               path,
                               NULL);
    GNUNET_free (path);
  }
  if (NULL == orh->url)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Could not construct request URL.\n");
    GNUNET_free (orh);
    return NULL;
  }
  req = GNUNET_JSON_PACK (
    GNUNET_JSON_pack_data_auto ("h_contract",
                                h_contract_terms));
  eh = TALER_MERCHANT_curl_easy_get_ (orh->url);
  if (GNUNET_OK !=
      TALER_curl_easy_post (&orh->post_ctx,
                            eh,
                            req))
  {
    GNUNET_break (0);
    json_decref (req);
    curl_easy_cleanup (eh);
    GNUNET_free (orh->url);
    GNUNET_free (orh);
    return NULL;
  }
  json_decref (req);
  orh->job = GNUNET_CURL_job_add2 (ctx,
                                   eh,
                                   orh->post_ctx.headers,
                                   &handle_refund_finished,
                                   orh);
  if (NULL == orh->job)
  {
    GNUNET_free (orh->url);
    GNUNET_free (orh);
    return NULL;
  }
  return orh;
}


void
TALER_MERCHANT_wallet_post_order_refund_cancel (
  struct TALER_MERCHANT_WalletOrderRefundHandle *orh)
{
  if (NULL != orh->job)
  {
    GNUNET_CURL_job_cancel (orh->job);
    orh->job = NULL;
  }
  TALER_curl_easy_post_finished (&orh->post_ctx);
  GNUNET_free (orh->url);
  GNUNET_free (orh);
}


/* end of merchant_api_wallet_post_order_refund.c */
