/*
  This file is part of TALER
  Copyright (C) 2014, 2015, 2016, 2020 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Lesser General Public License as published by the Free Software
  Foundation; either version 2.1, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License along with
  TALER; see the file COPYING.LGPL.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file merchant_api_post_reserves.c
 * @brief Implementation of the POST /reserves request of the merchant's HTTP API
 * @author Marcello Stanisci
 * @author Christian Grothoff
 */
#include "platform.h"
#include <curl/curl.h>
#include <jansson.h>
#include <microhttpd.h> /* just for HTTP status codes */
#include <gnunet/gnunet_util_lib.h>
#include "taler_merchant_service.h"
#include "merchant_api_curl_defaults.h"
#include <taler/taler_curl_lib.h>
#include <taler/taler_json_lib.h>


/**
 * @brief A handle for POSTing reserve data.
 */
struct TALER_MERCHANT_PostReservesHandle
{

  /**
   * The url for this request.
   */
  char *url;

  /**
   * Handle for the request.
   */
  struct GNUNET_CURL_Job *job;

  /**
   * Function to call with the result.
   */
  TALER_MERCHANT_PostReservesCallback cb;

  /**
   * Closure for @a cb.
   */
  void *cb_cls;

  /**
   * Reference to the execution context.
   */
  struct GNUNET_CURL_Context *ctx;

  /**
   * Minor context that holds body and headers.
   */
  struct TALER_CURL_PostContext post_ctx;

};


/**
 * Function called when we're done processing the
 * HTTP POST /reserves request.
 *
 * @param cls the `struct TALER_MERCHANT_PostReservesHandle`
 * @param response_code HTTP response code, 0 on error
 * @param response response body, NULL if not in JSON
 */
static void
handle_post_reserves_finished (void *cls,
                               long response_code,
                               const void *response)
{
  struct TALER_MERCHANT_PostReservesHandle *prh = cls;
  const json_t *json = response;
  struct TALER_MERCHANT_HttpResponse hr = {
    .http_status = (unsigned int) response_code,
    .reply = json
  };

  prh->job = NULL;
  switch (response_code)
  {
  case 0:
    hr.ec = TALER_EC_GENERIC_INVALID_RESPONSE;
    break;
  case MHD_HTTP_OK:
    {
      const char *payto_uri;
      struct TALER_ReservePublicKeyP reserve_pub;
      struct GNUNET_JSON_Specification spec[] = {
        GNUNET_JSON_spec_fixed_auto ("reserve_pub",
                                     &reserve_pub),
        GNUNET_JSON_spec_string ("payto_uri",
                                 &payto_uri),
        GNUNET_JSON_spec_end ()
      };

      if (GNUNET_OK !=
          GNUNET_JSON_parse (json,
                             spec,
                             NULL, NULL))
      {
        GNUNET_break_op (0);
        hr.http_status = 0;
        hr.ec = TALER_EC_GENERIC_INVALID_RESPONSE;
        break;
      }
      else
      {
        prh->cb (prh->cb_cls,
                 &hr,
                 &reserve_pub,
                 payto_uri);
        GNUNET_JSON_parse_free (spec);
        TALER_MERCHANT_reserves_post_cancel (prh);
        return;
      }
    }
  case MHD_HTTP_ACCEPTED:
    break;
  case MHD_HTTP_UNAUTHORIZED:
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    /* Nothing really to verify, merchant says we need to authenticate. */
    break;
  case MHD_HTTP_NOT_FOUND:
    /* Nothing really to verify, this should never
       happen, we should pass the JSON reply to the application */
    GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
                "Did not find any data\n");
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    break;
  case MHD_HTTP_INTERNAL_SERVER_ERROR:
    /* Server had an internal issue; we should retry, but this API
       leaves this to the application */
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    break;
  default:
    /* unexpected response code */
    GNUNET_break_op (0);
    TALER_MERCHANT_parse_error_details_ (json,
                                         response_code,
                                         &hr);
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u/%d\n",
                (unsigned int) response_code,
                (int) hr.ec);
    break;
  }
  prh->cb (prh->cb_cls,
           &hr,
           NULL,
           NULL);
  TALER_MERCHANT_reserves_post_cancel (prh);
}


struct TALER_MERCHANT_PostReservesHandle *
TALER_MERCHANT_reserves_post (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  const struct TALER_Amount *initial_balance,
  const char *exchange_url,
  const char *wire_method,
  TALER_MERCHANT_PostReservesCallback cb,
  void *cb_cls)
{
  struct TALER_MERCHANT_PostReservesHandle *prh;
  CURL *eh;
  json_t *req;

  prh = GNUNET_new (struct TALER_MERCHANT_PostReservesHandle);
  prh->ctx = ctx;
  prh->cb = cb;
  prh->cb_cls = cb_cls;
  prh->url = TALER_url_join (backend_url,
                             "private/reserves",
                             NULL);
  if (NULL == prh->url)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Could not construct request URL.\n");
    GNUNET_free (prh);
    return NULL;
  }
  req = GNUNET_JSON_PACK (
    TALER_JSON_pack_amount ("initial_balance",
                            initial_balance),
    GNUNET_JSON_pack_string ("wire_method",
                             wire_method),
    GNUNET_JSON_pack_string ("exchange_url",
                             exchange_url));
  eh = TALER_MERCHANT_curl_easy_get_ (prh->url);
  if (GNUNET_OK !=
      TALER_curl_easy_post (&prh->post_ctx,
                            eh,
                            req))
  {
    GNUNET_break (0);
    curl_easy_cleanup (eh);
    json_decref (req);
    GNUNET_free (prh->url);
    GNUNET_free (prh);
    return NULL;
  }
  json_decref (req);
  prh->job = GNUNET_CURL_job_add2 (ctx,
                                   eh,
                                   prh->post_ctx.headers,
                                   &handle_post_reserves_finished,
                                   prh);
  return prh;
}


void
TALER_MERCHANT_reserves_post_cancel (
  struct TALER_MERCHANT_PostReservesHandle *prh)
{
  if (NULL != prh->job)
  {
    GNUNET_CURL_job_cancel (prh->job);
    prh->job = NULL;
  }
  GNUNET_free (prh->url);
  TALER_curl_easy_post_finished (&prh->post_ctx);
  GNUNET_free (prh);
}


/* end of merchant_api_post_reserves.c */
