/*
  This file is part of TALER
  Copyright (C) 2014-2018, 2020 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Lesser General Public License as published by the Free Software
  Foundation; either version 2.1, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License along with
  TALER; see the file COPYING.LGPL.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file merchant_api_wallet_get_tip.c
 * @brief Implementation of the GET /tips/$TIP_ID request of the merchant's HTTP API
 * @author Florian Dold
 */
#include "platform.h"
#include <curl/curl.h>
#include <jansson.h>
#include <microhttpd.h> /* just for HTTP status codes */
#include <gnunet/gnunet_util_lib.h>
#include <gnunet/gnunet_curl_lib.h>
#include "taler_merchant_service.h"
#include "merchant_api_curl_defaults.h"
#include <taler/taler_json_lib.h>
#include <taler/taler_signatures.h>


/**
 * @brief A handle for tracking /tip-get operations
 */
struct TALER_MERCHANT_TipWalletGetHandle
{
  /**
   * The url for this request.
   */
  char *url;

  /**
   * Handle for the request.
   */
  struct GNUNET_CURL_Job *job;

  /**
   * Function to call with the result.
   */
  TALER_MERCHANT_TipWalletGetCallback cb;

  /**
   * Closure for @a cb.
   */
  void *cb_cls;

  /**
   * Reference to the execution context.
   */
  struct GNUNET_CURL_Context *ctx;

};


/**
 * Function called when we're done processing the
 * HTTP /track/transaction request.
 *
 * @param cls the `struct TALER_MERCHANT_TipGetHandle`
 * @param response_code HTTP response code, 0 on error
 * @param response response body, NULL if not in JSON
 */
static void
handle_wallet_tip_get_finished (void *cls,
                                long response_code,
                                const void *response)
{
  struct TALER_MERCHANT_TipWalletGetHandle *tgh = cls;
  const json_t *json = response;
  struct TALER_MERCHANT_HttpResponse hr = {
    .http_status = (unsigned int) response_code,
    .reply = json
  };

  GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
              "Got /tip/$TIP_ID response with status code %u\n",
              (unsigned int) response_code);

  tgh->job = NULL;
  switch (response_code)
  {
  case MHD_HTTP_OK:
    {
      const char *exchange_url;
      struct TALER_Amount amount_remaining;
      struct GNUNET_TIME_Timestamp expiration;
      struct GNUNET_JSON_Specification spec[] = {
        GNUNET_JSON_spec_timestamp ("expiration",
                                    &expiration),
        GNUNET_JSON_spec_string ("exchange_url",
                                 &exchange_url),
        TALER_JSON_spec_amount_any ("tip_amount",
                                    &amount_remaining),
        GNUNET_JSON_spec_end ()
      };

      if (GNUNET_OK !=
          GNUNET_JSON_parse (json,
                             spec,
                             NULL, NULL))
      {
        GNUNET_break_op (0);
        hr.http_status = 0;
        hr.ec = TALER_EC_GENERIC_INVALID_RESPONSE;
        break;
      }
      tgh->cb (tgh->cb_cls,
               &hr,
               expiration,
               exchange_url,
               &amount_remaining);
      TALER_MERCHANT_wallet_tip_get_cancel (tgh);
      return;
    }
  case MHD_HTTP_INTERNAL_SERVER_ERROR:
    /* Server had an internal issue; we should retry, but this API
       leaves this to the application */
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    break;
  case MHD_HTTP_NOT_FOUND:
    /* legal, can happen if instance or tip reserve is unknown */
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    break;
  default:
    /* unexpected response code */
    GNUNET_break_op (0);
    TALER_MERCHANT_parse_error_details_ (json,
                                         response_code,
                                         &hr);
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u/%d\n",
                (unsigned int) response_code,
                (int) hr.ec);
    break;
  }
  tgh->cb (tgh->cb_cls,
           &hr,
           GNUNET_TIME_UNIT_ZERO_TS,
           NULL,
           NULL);
  TALER_MERCHANT_wallet_tip_get_cancel (tgh);
}


struct TALER_MERCHANT_TipWalletGetHandle *
TALER_MERCHANT_wallet_tip_get (struct GNUNET_CURL_Context *ctx,
                               const char *backend_url,
                               const struct GNUNET_HashCode *tip_id,
                               TALER_MERCHANT_TipWalletGetCallback cb,
                               void *cb_cls)
{
  struct TALER_MERCHANT_TipWalletGetHandle *tgh;
  CURL *eh;

  tgh = GNUNET_new (struct TALER_MERCHANT_TipWalletGetHandle);
  tgh->ctx = ctx;
  tgh->cb = cb;
  tgh->cb_cls = cb_cls;
  {
    char res_str[sizeof (*tip_id) * 2];
    char arg_str[sizeof (res_str) + 48];
    char *end;

    end = GNUNET_STRINGS_data_to_string (tip_id,
                                         sizeof (*tip_id),
                                         res_str,
                                         sizeof (res_str));
    *end = '\0';
    GNUNET_snprintf (arg_str,
                     sizeof (arg_str),
                     "tips/%s",
                     res_str);
    tgh->url = TALER_url_join (backend_url,
                               arg_str,
                               NULL);
  }

  if (NULL == tgh->url)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Could not construct request URL.\n");
    GNUNET_free (tgh);
    return NULL;
  }

  GNUNET_log (GNUNET_ERROR_TYPE_INFO,
              "Requesting URL '%s'\n",
              tgh->url);
  eh = TALER_MERCHANT_curl_easy_get_ (tgh->url);
  tgh->job = GNUNET_CURL_job_add (ctx,
                                  eh,
                                  &handle_wallet_tip_get_finished,
                                  tgh);
  return tgh;
}


void
TALER_MERCHANT_wallet_tip_get_cancel (
  struct TALER_MERCHANT_TipWalletGetHandle *tgh)
{
  if (NULL != tgh->job)
  {
    GNUNET_CURL_job_cancel (tgh->job);
    tgh->job = NULL;
  }
  GNUNET_free (tgh->url);
  GNUNET_free (tgh);
}


/* end of merchant_api_wallet_get_tip.c */
