/*
  This file is part of TALER
  Copyright (C) 2020 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Lesser General Public License as published by the Free Software
  Foundation; either version 2.1, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License along with
  TALER; see the file COPYING.LGPL.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file merchant_api_get_tips.c
 * @brief Implementation of the GET /private/tips request of the merchant's HTTP API
 * @author Jonathan Buchanan
 */
#include "platform.h"
#include <curl/curl.h>
#include <jansson.h>
#include <microhttpd.h> /* just for HTTP status codes */
#include <gnunet/gnunet_util_lib.h>
#include <gnunet/gnunet_curl_lib.h>
#include "taler_merchant_service.h"
#include "merchant_api_curl_defaults.h"
#include <taler/taler_json_lib.h>
#include <taler/taler_signatures.h>


/**
 * Handle for a GET /private/tips operation.
 */
struct TALER_MERCHANT_TipsGetHandle
{
  /**
   * The url for this request.
   */
  char *url;

  /**
   * Handle for the request.
   */
  struct GNUNET_CURL_Job *job;

  /**
   * Function to call with the result.
   */
  TALER_MERCHANT_TipsGetCallback cb;

  /**
   * Closure for @a cb.
   */
  void *cb_cls;

  /**
   * Reference to the execution context.
   */
  struct GNUNET_CURL_Context *ctx;

};


/**
 * Parse tip information from @a ia.
 *
 * @param ia JSON array (or NULL!) tip order data
 * @param tgh operation handle
 * @return #GNUNET_OK on success
 */
static int
parse_tips (const json_t *ia,
            struct TALER_MERCHANT_TipsGetHandle *tgh)
{
  unsigned int tes_len = json_array_size (ia);
  struct TALER_MERCHANT_TipEntry tes[tes_len];
  size_t index;
  json_t *value;
  int ret;

  ret = GNUNET_OK;
  json_array_foreach (ia, index, value) {
    struct TALER_MERCHANT_TipEntry *ie = &tes[index];
    struct GNUNET_JSON_Specification spec[] = {
      GNUNET_JSON_spec_uint64 ("row_id",
                               &ie->row_id),
      GNUNET_JSON_spec_fixed_auto ("tip_id",
                                   &ie->tip_id),
      TALER_JSON_spec_amount_any ("tip_amount",
                                  &ie->tip_amount),
      GNUNET_JSON_spec_end ()
    };

    if (GNUNET_OK !=
        GNUNET_JSON_parse (value,
                           spec,
                           NULL, NULL))
    {
      GNUNET_break_op (0);
      ret = GNUNET_SYSERR;
      continue;
    }
    if (GNUNET_SYSERR == ret)
      break;
  }
  if (GNUNET_OK == ret)
  {
    struct TALER_MERCHANT_HttpResponse hr = {
      .http_status = MHD_HTTP_OK
    };

    tgh->cb (tgh->cb_cls,
             &hr,
             tes_len,
             tes);
    tgh->cb = NULL; /* just to be sure */
  }
  return ret;
}


/**
 * Function called when we're done processing the
 * HTTP GET /private/tips request.
 *
 * @param cls the `struct TALER_MERCHANT_TipsGetHandle`
 * @param response_code HTTP response code, 0 on error
 * @param response response body, NULL if not in JSON
 */
static void
handle_get_tips_finished (void *cls,
                          long response_code,
                          const void *response)
{
  struct TALER_MERCHANT_TipsGetHandle *tgh = cls;
  const json_t *json = response;
  struct TALER_MERCHANT_HttpResponse hr = {
    .http_status = (unsigned int) response_code,
    .reply = json
  };

  tgh->job = NULL;
  GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
              "Got /private/tips response with status code %u\n",
              (unsigned int) response_code);
  switch (response_code)
  {
  case MHD_HTTP_OK:
    {
      json_t *tips;
      struct GNUNET_JSON_Specification spec[] = {
        GNUNET_JSON_spec_json ("tips",
                               &tips),
        GNUNET_JSON_spec_end ()
      };

      if (GNUNET_OK !=
          GNUNET_JSON_parse (json,
                             spec,
                             NULL, NULL))
      {
        hr.http_status = 0;
        hr.ec = TALER_EC_GENERIC_INVALID_RESPONSE;
      }
      else
      {
        if ( (! json_is_array (tips)) ||
             (GNUNET_OK ==
              parse_tips (tips,
                          tgh)) )
        {
          GNUNET_JSON_parse_free (spec);
          TALER_MERCHANT_tips_get_cancel (tgh);
          return;
        }
        else
        {
          hr.http_status = 0;
          hr.ec = TALER_EC_GENERIC_INVALID_RESPONSE;
        }
      }
      GNUNET_JSON_parse_free (spec);
      break;
    }
  case MHD_HTTP_UNAUTHORIZED:
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    /* Nothing really to verify, merchant says we need to authenticate. */
    break;
  default:
    /* unexpected response code */
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u/%d\n",
                (unsigned int) response_code,
                (int) hr.ec);
    break;
  }
  tgh->cb (tgh->cb_cls,
           &hr,
           0,
           NULL);
  TALER_MERCHANT_tips_get_cancel (tgh);
}


struct TALER_MERCHANT_TipsGetHandle *
TALER_MERCHANT_tips_get (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  TALER_MERCHANT_TipsGetCallback cb,
  void *cb_cls)
{
  return TALER_MERCHANT_tips_get2 (ctx,
                                   backend_url,
                                   TALER_EXCHANGE_YNA_NO,
                                   -20,
                                   UINT64_MAX,
                                   cb,
                                   cb_cls);
}


struct TALER_MERCHANT_TipsGetHandle *
TALER_MERCHANT_tips_get2 (struct GNUNET_CURL_Context *ctx,
                          const char *backend_url,
                          enum TALER_EXCHANGE_YesNoAll expired,
                          int64_t limit,
                          uint64_t offset,
                          TALER_MERCHANT_TipsGetCallback cb,
                          void *cb_cls)
{
  struct TALER_MERCHANT_TipsGetHandle *tgh;
  CURL *eh;

  GNUNET_assert (NULL != backend_url);
  if (0 == limit)
  {
    GNUNET_break (0);
    return NULL;
  }
  tgh = GNUNET_new (struct TALER_MERCHANT_TipsGetHandle);
  tgh->ctx = ctx;
  tgh->cb = cb;
  tgh->cb_cls = cb_cls;

  /* build tgh->url with the various optional arguments */
  {
    char cbuf[30];
    char lbuf[30];
    bool have_offset;

    GNUNET_snprintf (lbuf,
                     sizeof (lbuf),
                     "%lld",
                     (long long) limit);

    if (limit > 0)
      have_offset = (0 != offset);
    else
      have_offset = (UINT64_MAX != offset);
    GNUNET_snprintf (cbuf,
                     sizeof (cbuf),
                     "%llu",
                     (unsigned long long) offset);
    tgh->url = TALER_url_join (backend_url,
                               "private/tips",
                               "expired",
                               (TALER_EXCHANGE_YNA_ALL != expired)
                               ? TALER_yna_to_string (expired)
                               : NULL,
                               "offset", (have_offset) ? cbuf : NULL,
                               "limit", (-20 != limit) ? lbuf : NULL,
                               NULL);
  }
  if (NULL == tgh->url)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Could not construct request URL.\n");
    GNUNET_free (tgh);
    return NULL;
  }
  GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
              "Requesting URL '%s'\n",
              tgh->url);
  eh = TALER_MERCHANT_curl_easy_get_ (tgh->url);
  tgh->job = GNUNET_CURL_job_add (ctx,
                                  eh,
                                  &handle_get_tips_finished,
                                  tgh);
  return tgh;
}


void
TALER_MERCHANT_tips_get_cancel (
  struct TALER_MERCHANT_TipsGetHandle *tgh)
{
  if (NULL != tgh->job)
    GNUNET_CURL_job_cancel (tgh->job);
  GNUNET_free (tgh->url);
  GNUNET_free (tgh);
}
