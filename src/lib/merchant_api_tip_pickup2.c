/*
  This file is part of TALER
  Copyright (C) 2014-2021 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Lesser General Public License as published by the Free Software
  Foundation; either version 2.1, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License along with
  TALER; see the file COPYING.LGPL.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file merchant_api_tip_pickup2.c
 * @brief Implementation of the /tip-pickup request of the merchant's HTTP API
 * @author Marcello Stanisci
 * @author Christian Grothoff
 */
#include "platform.h"
#include <curl/curl.h>
#include <jansson.h>
#include <microhttpd.h> /* just for HTTP status codes */
#include <gnunet/gnunet_util_lib.h>
#include <gnunet/gnunet_curl_lib.h>
#include "taler_merchant_service.h"
#include "merchant_api_curl_defaults.h"
#include <taler/taler_json_lib.h>
#include <taler/taler_signatures.h>
#include <taler/taler_curl_lib.h>


/**
 * @brief A handle for tracking transactions.
 */
struct TALER_MERCHANT_TipPickup2Handle
{

  /**
   * The url for this request.
   */
  char *url;

  /**
   * Minor context that holds body and headers.
   */
  struct TALER_CURL_PostContext post_ctx;

  /**
   * Handle for the request.
   */
  struct GNUNET_CURL_Job *job;

  /**
   * Function to call with the result.
   */
  TALER_MERCHANT_TipPickup2Callback cb;

  /**
   * Closure for @a cb.
   */
  void *cb_cls;

  /**
   * Reference to the execution context.
   */
  struct GNUNET_CURL_Context *ctx;

  /**
   * Expected number of planchets.
   */
  unsigned int num_planchets;
};


/**
 * We got a 200 response back from the exchange (or the merchant).
 * Now we need to parse the response and if it is well-formed,
 * call the callback (and set it to NULL afterwards).
 *
 * @param tpo handle of the original authorization operation
 * @param json cryptographic proof returned by the exchange/merchant
 * @return #GNUNET_OK if response is valid
 */
static int
check_ok (struct TALER_MERCHANT_TipPickup2Handle *tpo,
          const json_t *json)
{
  json_t *ja;
  unsigned int ja_len;
  struct GNUNET_JSON_Specification spec[] = {
    GNUNET_JSON_spec_json ("blind_sigs", &ja),
    GNUNET_JSON_spec_end ()
  };
  struct TALER_MERCHANT_HttpResponse hr = {
    .http_status = MHD_HTTP_OK,
    .reply = json
  };

  if (GNUNET_OK !=
      GNUNET_JSON_parse (json,
                         spec,
                         NULL, NULL))
  {
    GNUNET_break_op (0);
    return GNUNET_SYSERR;
  }
  ja_len = json_array_size (ja);
  if (ja_len != tpo->num_planchets)
  {
    GNUNET_break_op (0);
    GNUNET_JSON_parse_free (spec);
    return GNUNET_SYSERR;
  }
  {
    struct TALER_BlindedDenominationSignature mblind_sigs[ja_len];

    for (unsigned int i = 0; i<ja_len; i++)
    {
      json_t *pj = json_array_get (ja, i);
      struct GNUNET_JSON_Specification ispec[] = {
        TALER_JSON_spec_blinded_denom_sig ("blind_sig",
                                           &mblind_sigs[i]),
        GNUNET_JSON_spec_end ()
      };

      if (GNUNET_OK !=
          GNUNET_JSON_parse (pj,
                             ispec,
                             NULL, NULL))
      {
        GNUNET_break_op (0);
        GNUNET_JSON_parse_free (spec);
        return GNUNET_SYSERR;
      }
    }
    tpo->cb (tpo->cb_cls,
             &hr,
             ja_len,
             mblind_sigs);
    for (unsigned int i = 0; i<ja_len; i++)
      TALER_blinded_denom_sig_free (&mblind_sigs[i]);
    tpo->cb = NULL; /* do not call twice */
  }
  GNUNET_JSON_parse_free (spec);
  return GNUNET_OK;
}


/**
 * Function called when we're done processing the
 * HTTP /track/transaction request.
 *
 * @param cls the `struct TALER_MERCHANT_TipPickupHandle`
 * @param response_code HTTP response code, 0 on error
 * @param response response body, NULL if not in JSON
 */
static void
handle_tip_pickup_finished (void *cls,
                            long response_code,
                            const void *response)
{
  struct TALER_MERCHANT_TipPickup2Handle *tpo = cls;
  const json_t *json = response;
  struct TALER_MERCHANT_HttpResponse hr = {
    .http_status = (unsigned int) response_code,
    .reply = json
  };

  tpo->job = NULL;
  switch (response_code)
  {
  case MHD_HTTP_OK:
    if (GNUNET_OK != check_ok (tpo,
                               json))
    {
      GNUNET_break_op (0);
      hr.http_status = 0;
      hr.ec = TALER_EC_GENERIC_INVALID_RESPONSE;
    }
    break;
  case MHD_HTTP_BAD_REQUEST:
    /* Can happen if we pickup an amount that exceeds the tip... */
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    GNUNET_break (TALER_EC_MERCHANT_TIP_PICKUP_AMOUNT_EXCEEDS_TIP_REMAINING ==
                  hr.ec);
    break;
  case MHD_HTTP_CONFLICT:
    /* legal, can happen if we pickup a tip twice... */
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    break;
  case MHD_HTTP_NOT_FOUND:
    /* legal, can happen if tip ID is unknown */
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    break;
  case MHD_HTTP_INTERNAL_SERVER_ERROR:
    /* Server had an internal issue; we should retry, but this API
       leaves this to the application */
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    break;
  default:
    /* unexpected response code */
    GNUNET_break_op (0);
    TALER_MERCHANT_parse_error_details_ (json,
                                         response_code,
                                         &hr);
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u/%d\n",
                (unsigned int) response_code,
                (int) hr.ec);
    break;
  }
  if (NULL != tpo->cb)
  {
    tpo->cb (tpo->cb_cls,
             &hr,
             0,
             NULL);
    tpo->cb = NULL;
  }
  TALER_MERCHANT_tip_pickup2_cancel (tpo);
}


struct TALER_MERCHANT_TipPickup2Handle *
TALER_MERCHANT_tip_pickup2 (struct GNUNET_CURL_Context *ctx,
                            const char *backend_url,
                            const struct GNUNET_HashCode *tip_id,
                            unsigned int num_planchets,
                            const struct TALER_PlanchetDetail planchets[],
                            TALER_MERCHANT_TipPickup2Callback pickup_cb,
                            void *pickup_cb_cls)
{
  struct TALER_MERCHANT_TipPickup2Handle *tpo;
  CURL *eh;
  json_t *pa;
  json_t *tp_obj;

  if (0 == num_planchets)
  {
    GNUNET_break (0);
    return NULL;
  }
  pa = json_array ();
  for (unsigned int i = 0; i<num_planchets; i++)
  {
    const struct TALER_PlanchetDetail *planchet = &planchets[i];
    json_t *p;

    p = GNUNET_JSON_PACK (
      GNUNET_JSON_pack_data_auto ("denom_pub_hash",
                                  &planchet->denom_pub_hash),
      GNUNET_JSON_pack_data_varsize ("coin_ev",
                                     planchet->coin_ev,
                                     planchet->coin_ev_size));
    if (0 !=
        json_array_append_new (pa,
                               p))
    {
      GNUNET_break (0);
      json_decref (pa);
      return NULL;
    }
  }
  tp_obj = GNUNET_JSON_PACK (
    GNUNET_JSON_pack_array_steal ("planchets",
                                  pa));
  tpo = GNUNET_new (struct TALER_MERCHANT_TipPickup2Handle);
  tpo->num_planchets = num_planchets;
  tpo->ctx = ctx;
  tpo->cb = pickup_cb;
  tpo->cb_cls = pickup_cb_cls;

  {
    char tip_str[sizeof (*tip_id) * 2];
    char arg_str[sizeof (tip_str) + 32];
    char *end;

    end = GNUNET_STRINGS_data_to_string (tip_id,
                                         sizeof (*tip_id),
                                         tip_str,
                                         sizeof (tip_str));
    *end = '\0';
    GNUNET_snprintf (arg_str,
                     sizeof (arg_str),
                     "tips/%s/pickup",
                     tip_str);
    tpo->url = TALER_url_join (backend_url,
                               arg_str,
                               NULL);
  }
  if (NULL == tpo->url)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Could not construct request URL.\n");
    json_decref (tp_obj);
    GNUNET_free (tpo);
    return NULL;
  }
  eh = TALER_MERCHANT_curl_easy_get_ (tpo->url);
  if (GNUNET_OK != TALER_curl_easy_post (&tpo->post_ctx,
                                         eh,
                                         tp_obj))
  {
    GNUNET_break (0);
    json_decref (tp_obj);
    GNUNET_free (tpo->url);
    GNUNET_free (tpo);
    return NULL;
  }
  json_decref (tp_obj);
  GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
              "Requesting URL '%s'\n",
              tpo->url);
  tpo->job = GNUNET_CURL_job_add2 (ctx,
                                   eh,
                                   tpo->post_ctx.headers,
                                   &handle_tip_pickup_finished,
                                   tpo);
  if (NULL == tpo->job)
  {
    TALER_MERCHANT_tip_pickup2_cancel (tpo);
    return NULL;
  }
  return tpo;
}


void
TALER_MERCHANT_tip_pickup2_cancel (
  struct TALER_MERCHANT_TipPickup2Handle *tpo)
{
  if (NULL != tpo->job)
  {
    GNUNET_CURL_job_cancel (tpo->job);
    tpo->job = NULL;
  }
  TALER_curl_easy_post_finished (&tpo->post_ctx);
  GNUNET_free (tpo->url);
  GNUNET_free (tpo);
}


/* end of merchant_api_tip_pickup2.c */
