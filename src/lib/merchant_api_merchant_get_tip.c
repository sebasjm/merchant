/*
  This file is part of TALER
  Copyright (C) 2020 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Lesser General Public License as published by the Free Software
  Foundation; either version 2.1, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License along with
  TALER; see the file COPYING.LGPL.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file merchant_api_merchant_get_tip.c
 * @brief Implementation of the GET /private/tips/$TIP_ID request of the merchant's HTTP API
 * @author Jonathan Buchanan
 */
#include "platform.h"
#include <curl/curl.h>
#include <jansson.h>
#include <microhttpd.h> /* just for HTTP status codes */
#include <gnunet/gnunet_util_lib.h>
#include <gnunet/gnunet_curl_lib.h>
#include "taler_merchant_service.h"
#include "merchant_api_curl_defaults.h"
#include <taler/taler_json_lib.h>
#include <taler/taler_signatures.h>


struct TALER_MERCHANT_TipMerchantGetHandle
{
  /**
   * The url for this request.
   */
  char *url;

  /**
   * Handle for the request.
   */
  struct GNUNET_CURL_Job *job;

  /**
   * Function to call with the result.
   */
  TALER_MERCHANT_TipMerchantGetCallback cb;

  /**
   * Closure for @a cb.
   */
  void *cb_cls;

  /**
   * Reference to the execution context.
   */
  struct GNUNET_CURL_Context *ctx;
};


static int
parse_pickups (const json_t *pa,
               const struct TALER_Amount *total_authorized,
               const struct TALER_Amount *total_picked_up,
               const char *reason,
               struct GNUNET_TIME_Timestamp expiration,
               const struct TALER_ReservePublicKeyP *reserve_pub,
               struct TALER_MERCHANT_TipMerchantGetHandle *tgh)
{
  unsigned int pa_len = json_array_size (pa);
  struct TALER_MERCHANT_PickupDetail pickups[pa_len];
  size_t index;
  json_t *value;
  int ret = GNUNET_OK;

  json_array_foreach (pa, index, value)
  {
    struct TALER_MERCHANT_PickupDetail *pickup = &pickups[index];
    struct GNUNET_JSON_Specification spec[] = {
      GNUNET_JSON_spec_fixed_auto ("pickup_id",
                                   &pickup->pickup_id),
      GNUNET_JSON_spec_uint64 ("num_planchets",
                               &pickup->num_planchets),
      TALER_JSON_spec_amount_any ("requested_amount",
                                  &pickup->requested_amount),
      GNUNET_JSON_spec_end ()
    };

    if (GNUNET_OK !=
        GNUNET_JSON_parse (value,
                           spec,
                           NULL,
                           NULL))
    {
      GNUNET_break_op (0);
      ret = GNUNET_SYSERR;
      break;
    }
  }
  if (GNUNET_OK == ret)
  {
    struct TALER_MERCHANT_HttpResponse hr = {
      .http_status = MHD_HTTP_OK
    };

    tgh->cb (tgh->cb_cls,
             &hr,
             total_authorized,
             total_picked_up,
             reason,
             expiration,
             reserve_pub,
             pa_len,
             pickups);
  }
  return ret;
}


/**
 * Function called when we're done processing the
 * GET /private/tips/$TIP_ID request.
 *
 * @param cls the `struct TALER_MERCHANT_TipMerchantGetHandle`
 * @param response_code HTTP response code, 0 on error
 * @param response response body, NULL if not in JSON
 */
static void
handle_merchant_tip_get_finished (void *cls,
                                  long response_code,
                                  const void *response)
{
  struct TALER_MERCHANT_TipMerchantGetHandle *tgh = cls;
  const json_t *json = response;
  struct TALER_MERCHANT_HttpResponse hr = {
    .http_status = (unsigned int) response_code,
    .reply = json
  };

  GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
              "Got /private/tips/$TIP_ID response with status code %u\n",
              (unsigned int) response_code);
  tgh->job = NULL;
  switch (response_code)
  {
  case MHD_HTTP_OK:
    {
      struct TALER_Amount total_authorized;
      struct TALER_Amount total_picked_up;
      const char *reason;
      struct GNUNET_TIME_Timestamp expiration;
      struct TALER_ReservePublicKeyP reserve_pub;
      struct GNUNET_JSON_Specification spec[] = {
        TALER_JSON_spec_amount_any ("total_authorized",
                                    &total_authorized),
        TALER_JSON_spec_amount_any ("total_picked_up",
                                    &total_picked_up),
        GNUNET_JSON_spec_string ("reason",
                                 &reason),
        GNUNET_JSON_spec_timestamp ("expiration",
                                    &expiration),
        GNUNET_JSON_spec_fixed_auto ("reserve_pub",
                                     &reserve_pub),
        GNUNET_JSON_spec_end ()
      };

      if (GNUNET_OK !=
          GNUNET_JSON_parse (json,
                             spec,
                             NULL, NULL))
      {
        hr.http_status = 0;
        hr.ec = TALER_EC_GENERIC_INVALID_RESPONSE;
      }
      else
      {
        json_t *pickups = json_object_get (json,
                                           "pickups");
        if (! json_is_array (pickups))
        {
          tgh->cb (tgh->cb_cls,
                   &hr,
                   &total_authorized,
                   &total_picked_up,
                   reason,
                   expiration,
                   &reserve_pub,
                   0,
                   NULL);
          TALER_MERCHANT_merchant_tip_get_cancel (tgh);
          return;
        }
        else if (GNUNET_OK == parse_pickups (pickups,
                                             &total_authorized,
                                             &total_picked_up,
                                             reason,
                                             expiration,
                                             &reserve_pub,
                                             tgh))
        {
          GNUNET_JSON_parse_free (spec);
          TALER_MERCHANT_merchant_tip_get_cancel (tgh);
          return;
        }
        else
        {
          hr.http_status = 0;
          hr.ec = TALER_EC_GENERIC_INVALID_RESPONSE;
        }
      }
      GNUNET_JSON_parse_free (spec);
      break;
    }
  case MHD_HTTP_UNAUTHORIZED:
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    /* Nothing really to verify, merchant says we need to authenticate. */
    break;
  case MHD_HTTP_NOT_FOUND:
    /* legal, can happen if instance or tip reserve is unknown */
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    break;
  case MHD_HTTP_INTERNAL_SERVER_ERROR:
    /* Server had an internal issue; we should retry, but this API
       leaves this to the application */
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    break;
  default:
    /* unexpected response code */
    GNUNET_break_op (0);
    TALER_MERCHANT_parse_error_details_ (json,
                                         response_code,
                                         &hr);
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u/%d\n",
                (unsigned int) response_code,
                (int) hr.ec);
    break;
  }
  tgh->cb (tgh->cb_cls,
           &hr,
           NULL,
           NULL,
           NULL,
           GNUNET_TIME_UNIT_ZERO_TS,
           NULL,
           0,
           NULL);
  TALER_MERCHANT_merchant_tip_get_cancel (tgh);
}


struct TALER_MERCHANT_TipMerchantGetHandle *
TALER_MERCHANT_merchant_tip_get (struct GNUNET_CURL_Context *ctx,
                                 const char *backend_url,
                                 const struct GNUNET_HashCode *tip_id,
                                 bool pickups,
                                 TALER_MERCHANT_TipMerchantGetCallback cb,
                                 void *cb_cls)
{
  struct TALER_MERCHANT_TipMerchantGetHandle *tgh;
  CURL *eh;

  GNUNET_assert (NULL != backend_url);
  tgh = GNUNET_new (struct TALER_MERCHANT_TipMerchantGetHandle);
  tgh->ctx = ctx;
  tgh->cb = cb;
  tgh->cb_cls = cb_cls;

  {
    char res_str[sizeof (*tip_id) * 2];
    char arg_str[sizeof (res_str) + 48];
    char *end;

    end = GNUNET_STRINGS_data_to_string (tip_id,
                                         sizeof (*tip_id),
                                         res_str,
                                         sizeof (res_str));
    *end = '\0';
    GNUNET_snprintf (arg_str,
                     sizeof (arg_str),
                     "private/tips/%s",
                     res_str);
    tgh->url = TALER_url_join (backend_url,
                               arg_str,
                               "pickups", pickups ? "yes" : NULL,
                               NULL);
  }
  if (NULL == tgh->url)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Could not construct request URL.\n");
    GNUNET_free (tgh);
    return NULL;
  }

  eh = TALER_MERCHANT_curl_easy_get_ (tgh->url);
  tgh->job = GNUNET_CURL_job_add (ctx,
                                  eh,
                                  &handle_merchant_tip_get_finished,
                                  tgh);
  return tgh;
}


void
TALER_MERCHANT_merchant_tip_get_cancel (struct
                                        TALER_MERCHANT_TipMerchantGetHandle *tgh)
{
  if (NULL != tgh->job)
  {
    GNUNET_CURL_job_cancel (tgh->job);
    tgh->job = NULL;
  }
  GNUNET_free (tgh->url);
  GNUNET_free (tgh);
}


/* end of merchant_api_merchant_get_tip.c */
