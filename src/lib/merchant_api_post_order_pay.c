/*
  This file is part of TALER
  Copyright (C) 2014-2021 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation; either version 2.1,
  or (at your option) any later version.

  TALER is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General
  Public License along with TALER; see the file COPYING.LGPL.
  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file merchant_api_post_order_pay.c
 * @brief Implementation of the POST /order/$ID/pay request
 *        of the merchant's HTTP API
 * @author Christian Grothoff
 * @author Marcello Stanisci
 */
#include "platform.h"
#include <curl/curl.h>
#include <jansson.h>
#include <microhttpd.h> /* just for HTTP status codes */
#include <gnunet/gnunet_util_lib.h>
#include <gnunet/gnunet_curl_lib.h>
#include "taler_merchant_service.h"
#include "merchant_api_curl_defaults.h"
#include <taler/taler_json_lib.h>
#include <taler/taler_signatures.h>
#include <taler/taler_exchange_service.h>
#include <taler/taler_curl_lib.h>


/**
 * @brief A Pay Handle
 */
struct TALER_MERCHANT_OrderPayHandle
{

  /**
   * The url for this request.
   */
  char *url;

  /**
   * Handle for the request.
   */
  struct GNUNET_CURL_Job *job;

  /**
   * Function to call with the result in "pay" @e mode.
   */
  TALER_MERCHANT_OrderPayCallback pay_cb;

  /**
   * Closure for @a pay_cb.
   */
  void *pay_cb_cls;

  /**
   * Reference to the execution context.
   */
  struct GNUNET_CURL_Context *ctx;

  /**
   * Minor context that holds body and headers.
   */
  struct TALER_CURL_PostContext post_ctx;

  /**
   * The coins we are paying with.
   */
  struct TALER_MERCHANT_PaidCoin *coins;

  /**
   * Hash of the contract we are paying, set
   * if @e am_wallet is true.
   */
  struct TALER_PrivateContractHash h_contract_terms;

  /**
   * Public key of the merchant (instance) being paid, set
   * if @e am_wallet is true.
   */
  struct TALER_MerchantPublicKeyP merchant_pub;

  /**
   * Number of @e coins we are paying with.
   */
  unsigned int num_coins;

  /**
   * Set to true if this is the wallet API and we have
   * initialized @e h_contract_terms and @e merchant_pub.
   */
  bool am_wallet;

};


/**
 * We got a 409 response back from the exchange (or the merchant).
 * Now we need to check the provided cryptograophic proof that the
 * coin was actually already spent!
 *
 * @param pc handle of the original coin we paid with
 * @param json cryptograophic proof of coin's transaction
 *        history as was returned by the exchange/merchant
 * @return #GNUNET_OK if proof checks out
 */
static int
check_coin_history (const struct TALER_MERCHANT_PaidCoin *pc,
                    json_t *json)
{
  struct TALER_Amount spent;
  struct TALER_Amount spent_plus_contrib;
  struct TALER_DenominationHash h_denom_pub;
  struct TALER_DenominationHash h_denom_pub_pc;

  if (GNUNET_OK !=
      TALER_EXCHANGE_verify_coin_history (NULL, /* do not verify fees */
                                          pc->amount_with_fee.currency,
                                          &pc->coin_pub,
                                          json,
                                          &h_denom_pub,
                                          &spent))
  {
    /* Exchange's history fails to verify */
    GNUNET_break_op (0);
    return GNUNET_SYSERR;
  }
  if (0 >
      TALER_amount_add (&spent_plus_contrib,
                        &spent,
                        &pc->amount_with_fee))
  {
    /* We got an integer overflow? Bad application! */
    GNUNET_break (0);
    return GNUNET_SYSERR;
  }
  TALER_denom_pub_hash (&pc->denom_pub,
                        &h_denom_pub_pc);
  if ( (-1 != TALER_amount_cmp (&pc->denom_value,
                                &spent_plus_contrib)) &&
       (0 != GNUNET_memcmp (&h_denom_pub,
                            &h_denom_pub_pc)) )
  {
    /* according to our calculations, the transaction should
       have still worked, AND we did not get any proof of
       coin public key re-use; hence: exchange error! */
    GNUNET_break_op (0);
    return GNUNET_SYSERR;
  }
  GNUNET_log (GNUNET_ERROR_TYPE_INFO,
              "Accepting proof of double-spending (or coin public key re-use)\n");
  return GNUNET_OK;
}


/**
 * We got a 409 response back from the exchange (or the merchant).
 * Now we need to check the provided cryptograophic proof that the
 * coin was actually already spent!
 *
 * @param oph handle of the original pay operation
 * @param json cryptograophic proof returned by the
 *        exchange/merchant
 * @return #GNUNET_OK if proof checks out
 */
static enum GNUNET_GenericReturnValue
check_conflict (struct TALER_MERCHANT_OrderPayHandle *oph,
                const json_t *json)
{
  json_t *history;
  json_t *ereply;
  struct TALER_CoinSpendPublicKeyP coin_pub;
  struct GNUNET_JSON_Specification spec[] = {
    GNUNET_JSON_spec_json ("exchange_reply", &ereply),
    GNUNET_JSON_spec_fixed_auto ("coin_pub", &coin_pub),
    GNUNET_JSON_spec_end ()
  };
  struct GNUNET_JSON_Specification hspec[] = {
    GNUNET_JSON_spec_json ("history", &history),
    GNUNET_JSON_spec_end ()
  };

  if (GNUNET_OK !=
      GNUNET_JSON_parse (json,
                         spec,
                         NULL, NULL))
  {
    GNUNET_break_op (0);
    return GNUNET_SYSERR;
  }
  if (GNUNET_OK !=
      GNUNET_JSON_parse (ereply,
                         hspec,
                         NULL, NULL))
  {
    GNUNET_break_op (0);
    GNUNET_JSON_parse_free (spec);
    return GNUNET_SYSERR;
  }
  GNUNET_JSON_parse_free (spec);

  for (unsigned int i = 0; i<oph->num_coins; i++)
  {
    if (0 == memcmp (&oph->coins[i].coin_pub,
                     &coin_pub,
                     sizeof (struct TALER_CoinSpendPublicKeyP)))
    {
      int ret;

      ret = check_coin_history (&oph->coins[i],
                                history);
      GNUNET_JSON_parse_free (hspec);
      return ret;
    }
  }
  GNUNET_break_op (0); /* complaint is not about any of the coins
                          that we actually paid with... */
  GNUNET_JSON_parse_free (hspec);
  return GNUNET_SYSERR;
}


/**
 * Function called when we're done processing the
 * HTTP /pay request.
 *
 * @param cls the `struct TALER_MERCHANT_Pay`
 * @param response_code HTTP response code, 0 on error
 * @param response response body, NULL if not in JSON
 */
static void
handle_pay_finished (void *cls,
                     long response_code,
                     const void *response)
{
  struct TALER_MERCHANT_OrderPayHandle *oph = cls;
  const json_t *json = response;
  struct TALER_MERCHANT_HttpResponse hr = {
    .http_status = (unsigned int) response_code,
    .reply = json
  };
  struct TALER_MerchantSignatureP *merchant_sigp = NULL;
  struct TALER_MerchantSignatureP merchant_sig;

  oph->job = NULL;
  GNUNET_log (GNUNET_ERROR_TYPE_INFO,
              "/pay completed with response code %u\n",
              (unsigned int) response_code);
  switch (response_code)
  {
  case 0:
    hr.ec = TALER_EC_GENERIC_INVALID_RESPONSE;
    break;
  case MHD_HTTP_OK:
    if (oph->am_wallet)
    {
      /* Here we can (and should) verify the merchant's signature */
      struct TALER_PaymentResponsePS pr = {
        .purpose.purpose = htonl (TALER_SIGNATURE_MERCHANT_PAYMENT_OK),
        .purpose.size = htonl (sizeof (pr)),
        .h_contract_terms = oph->h_contract_terms
      };
      struct GNUNET_JSON_Specification spec[] = {
        GNUNET_JSON_spec_fixed_auto ("sig",
                                     &merchant_sig),
        GNUNET_JSON_spec_end ()
      };

      if (GNUNET_OK !=
          GNUNET_JSON_parse (json,
                             spec,
                             NULL, NULL))
      {
        GNUNET_break_op (0);
        hr.ec = TALER_EC_GENERIC_INVALID_RESPONSE;
        hr.http_status = 0;
        hr.hint = "sig field missing in response";
        break;
      }

      if (GNUNET_OK !=
          GNUNET_CRYPTO_eddsa_verify (TALER_SIGNATURE_MERCHANT_PAYMENT_OK,
                                      &pr,
                                      &merchant_sig.eddsa_sig,
                                      &oph->merchant_pub.eddsa_pub))
      {
        GNUNET_break_op (0);
        hr.ec = TALER_EC_GENERIC_INVALID_RESPONSE;
        hr.http_status = 0;
        hr.hint = "signature invalid";
      }
      merchant_sigp = &merchant_sig;
    }
    break;
  /* Tolerating Not Acceptable because sometimes
   * - especially in tests - we might want to POST
   * coins one at a time.  */
  case MHD_HTTP_NOT_ACCEPTABLE:
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    break;
  case MHD_HTTP_BAD_REQUEST:
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    /* This should never happen, either us
     * or the merchant is buggy (or API version conflict);
     * just pass JSON reply to the application */
    break;
  case MHD_HTTP_PAYMENT_REQUIRED:
    /* was originally paid, but then refunded */
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    break;
  case MHD_HTTP_FORBIDDEN:
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    /* Nothing really to verify, merchant says we tried to abort the payment
     * after it was successful. We should pass the JSON reply to the
     * application */
    break;
  case MHD_HTTP_NOT_FOUND:
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    /* Nothing really to verify, this should never
       happen, we should pass the JSON reply to the
       application */
    break;
  case MHD_HTTP_PRECONDITION_FAILED:
    TALER_MERCHANT_parse_error_details_ (json,
                                         response_code,
                                         &hr);
    /* Nothing really to verify, the merchant is blaming us for failing to
       satisfy some constraint (likely it does not like our exchange because
       of some disagreement on the PKI).  We should pass the JSON reply to the
       application */
    break;
  case MHD_HTTP_REQUEST_TIMEOUT:
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    /* The merchant couldn't generate a timely response, likely because
       it itself waited too long on the exchange.
       Pass on to application. */
    break;
  case MHD_HTTP_CONFLICT:
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    if (GNUNET_OK != check_conflict (oph,
                                     json))
    {
      GNUNET_break_op (0);
      response_code = 0;
    }
    break;
  case MHD_HTTP_GONE:
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    /* The merchant says we are too late, the offer has expired or some
       denomination key of a coin involved has expired.
       Might be a disagreement in timestamps? Still, pass on to application. */
    break;
  case MHD_HTTP_INTERNAL_SERVER_ERROR:
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    /* Server had an internal issue; we should retry,
       but this API leaves this to the application */
    break;
  case MHD_HTTP_BAD_GATEWAY:
    /* Nothing really to verify, the merchant is blaming the exchange.
       We should pass the JSON reply to the application */
    TALER_MERCHANT_parse_error_details_ (json,
                                         response_code,
                                         &hr);
    break;
  case MHD_HTTP_SERVICE_UNAVAILABLE:
    TALER_MERCHANT_parse_error_details_ (json,
                                         response_code,
                                         &hr);
    /* Exchange couldn't respond properly; the retry is
       left to the application */
    break;
  case MHD_HTTP_GATEWAY_TIMEOUT:
    TALER_MERCHANT_parse_error_details_ (json,
                                         response_code,
                                         &hr);
    /* Exchange couldn't respond in a timely fashion;
       the retry is left to the application */
    break;
  default:
    TALER_MERCHANT_parse_error_details_ (json,
                                         response_code,
                                         &hr);
    /* unexpected response code */
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u/%d\n",
                (unsigned int) response_code,
                (int) hr.ec);
    GNUNET_break_op (0);
    break;
  }
  oph->pay_cb (oph->pay_cb_cls,
               &hr,
               merchant_sigp);
  TALER_MERCHANT_order_pay_cancel (oph);
}


struct TALER_MERCHANT_OrderPayHandle *
TALER_MERCHANT_order_pay_frontend (
  struct GNUNET_CURL_Context *ctx,
  const char *merchant_url,
  const char *order_id,
  const char *session_id,
  unsigned int num_coins,
  const struct TALER_MERCHANT_PaidCoin coins[],
  TALER_MERCHANT_OrderPayCallback pay_cb,
  void *pay_cb_cls)
{
  struct TALER_MERCHANT_OrderPayHandle *oph;
  json_t *pay_obj;
  json_t *j_coins;
  CURL *eh;
  struct TALER_Amount total_fee;
  struct TALER_Amount total_amount;

  if (0 == num_coins)
  {
    GNUNET_break (0);
    return NULL;
  }
  j_coins = json_array ();
  for (unsigned int i = 0; i<num_coins; i++)
  {
    json_t *j_coin;
    const struct TALER_MERCHANT_PaidCoin *pc = &coins[i];
    struct TALER_Amount fee;
    struct TALER_DenominationHash denom_hash;

    if (0 >
        TALER_amount_subtract (&fee,
                               &pc->amount_with_fee,
                               &pc->amount_without_fee))
    {
      /* Integer underflow, fee larger than total amount?
         This should not happen (client violated API!) */
      GNUNET_break (0);
      json_decref (j_coins);
      return NULL;
    }
    if (0 == i)
    {
      total_fee = fee;
      total_amount = pc->amount_with_fee;
    }
    else
    {
      if ( (0 >
            TALER_amount_add (&total_fee,
                              &total_fee,
                              &fee)) ||
           (0 >
            TALER_amount_add (&total_amount,
                              &total_amount,
                              &pc->amount_with_fee)) )
      {
        /* integer overflow */
        GNUNET_break (0);
        json_decref (j_coins);
        return NULL;
      }
    }

    TALER_denom_pub_hash (&pc->denom_pub,
                          &denom_hash);
    /* create JSON for this coin */
    j_coin = GNUNET_JSON_PACK (
      TALER_JSON_pack_amount ("contribution",
                              &pc->amount_with_fee),
      GNUNET_JSON_pack_data_auto ("coin_pub",
                                  &pc->coin_pub),
      GNUNET_JSON_pack_string ("exchange_url",
                               pc->exchange_url),
      GNUNET_JSON_pack_data_auto ("h_denom",
                                  &denom_hash),
      TALER_JSON_pack_denom_sig ("ub_sig",
                                 &pc->denom_sig),
      GNUNET_JSON_pack_data_auto ("coin_sig",
                                  &pc->coin_sig));
    if (0 !=
        json_array_append_new (j_coins,
                               j_coin))
    {
      GNUNET_break (0);
      json_decref (j_coins);
      return NULL;
    }
  }

  pay_obj = GNUNET_JSON_PACK (
    GNUNET_JSON_pack_array_steal ("coins",
                                  j_coins),
    GNUNET_JSON_pack_allow_null (
      GNUNET_JSON_pack_string ("session_id",
                               session_id)));

  oph = GNUNET_new (struct TALER_MERCHANT_OrderPayHandle);
  oph->ctx = ctx;
  oph->pay_cb = pay_cb;
  oph->pay_cb_cls = pay_cb_cls;
  {
    char *path;

    GNUNET_asprintf (&path,
                     "orders/%s/pay",
                     order_id);
    oph->url = TALER_url_join (merchant_url,
                               path,
                               NULL);
    GNUNET_free (path);
  }
  if (NULL == oph->url)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Could not construct request URL.\n");
    json_decref (pay_obj);
    GNUNET_free (oph);
    return NULL;
  }
  oph->num_coins = num_coins;
  oph->coins = GNUNET_new_array (num_coins,
                                 struct TALER_MERCHANT_PaidCoin);
  memcpy (oph->coins,
          coins,
          num_coins * sizeof (struct TALER_MERCHANT_PaidCoin));

  eh = TALER_MERCHANT_curl_easy_get_ (oph->url);
  if (GNUNET_OK !=
      TALER_curl_easy_post (&oph->post_ctx,
                            eh,
                            pay_obj))
  {
    GNUNET_break (0);
    curl_easy_cleanup (eh);
    json_decref (pay_obj);
    GNUNET_free (oph->url);
    GNUNET_free (oph);
    return NULL;
  }
  json_decref (pay_obj);
  oph->job = GNUNET_CURL_job_add2 (ctx,
                                   eh,
                                   oph->post_ctx.headers,
                                   &handle_pay_finished,
                                   oph);
  return oph;
}


struct TALER_MERCHANT_OrderPayHandle *
TALER_MERCHANT_order_pay (struct GNUNET_CURL_Context *ctx,
                          const char *merchant_url,
                          const char *session_id,
                          const struct
                          TALER_PrivateContractHash *h_contract_terms,
                          const struct TALER_Amount *amount,
                          const struct TALER_Amount *max_fee,
                          const struct TALER_MerchantPublicKeyP *merchant_pub,
                          const struct TALER_MerchantSignatureP *merchant_sig,
                          struct GNUNET_TIME_Timestamp timestamp,
                          struct GNUNET_TIME_Timestamp refund_deadline,
                          struct GNUNET_TIME_Timestamp pay_deadline,
                          const struct TALER_MerchantWireHash *h_wire,
                          const char *order_id,
                          unsigned int num_coins,
                          const struct TALER_MERCHANT_PayCoin coins[],
                          TALER_MERCHANT_OrderPayCallback pay_cb,
                          void *pay_cb_cls)
{
  if (GNUNET_YES !=
      TALER_amount_cmp_currency (amount,
                                 max_fee))
  {
    GNUNET_break (0);
    return NULL;
  }

  {
    struct TALER_MERCHANT_PaidCoin pc[num_coins];

    for (unsigned int i = 0; i<num_coins; i++)
    {
      const struct TALER_MERCHANT_PayCoin *coin = &coins[i]; // coin priv.
      struct TALER_MERCHANT_PaidCoin *p = &pc[i]; // coin pub.
      struct TALER_Amount fee;
      struct TALER_DenominationHash h_denom_pub;

      if (0 >
          TALER_amount_subtract (&fee,
                                 &coin->amount_with_fee,
                                 &coin->amount_without_fee))
      {
        /* Integer underflow, fee larger than total amount?
           This should not happen (client violated API!) */
        GNUNET_break (0);
        return NULL;
      }
      TALER_denom_pub_hash (&coin->denom_pub,
                            &h_denom_pub);
      TALER_wallet_deposit_sign (&coin->amount_with_fee,
                                 &fee,
                                 h_wire,
                                 h_contract_terms,
                                 NULL /* h_extensions! */,
                                 &h_denom_pub,
                                 timestamp,
                                 merchant_pub,
                                 refund_deadline,
                                 &coin->coin_priv,
                                 &p->coin_sig);
      p->denom_pub = coin->denom_pub;
      p->denom_sig = coin->denom_sig;
      p->denom_value = coin->denom_value;
      GNUNET_CRYPTO_eddsa_key_get_public (&coin->coin_priv.eddsa_priv,
                                          &p->coin_pub.eddsa_pub);
      p->amount_with_fee = coin->amount_with_fee;
      p->amount_without_fee = coin->amount_without_fee;
      p->exchange_url = coin->exchange_url;
    }
    {
      struct TALER_MERCHANT_OrderPayHandle *oph;

      oph = TALER_MERCHANT_order_pay_frontend (ctx,
                                               merchant_url,
                                               order_id,
                                               session_id,
                                               num_coins,
                                               pc,
                                               pay_cb,
                                               pay_cb_cls);
      if (NULL == oph)
        return NULL;
      oph->h_contract_terms = *h_contract_terms;
      oph->merchant_pub = *merchant_pub;
      oph->am_wallet = true;
      return oph;
    }
  }
}


void
TALER_MERCHANT_order_pay_cancel (struct TALER_MERCHANT_OrderPayHandle *oph)
{
  if (NULL != oph->job)
  {
    GNUNET_CURL_job_cancel (oph->job);
    oph->job = NULL;
  }
  TALER_curl_easy_post_finished (&oph->post_ctx);
  GNUNET_free (oph->coins);
  GNUNET_free (oph->url);
  GNUNET_free (oph);
}


/* end of merchant_api_post_order_pay.c */
