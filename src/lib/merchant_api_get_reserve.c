/*
  This file is part of TALER
  Copyright (C) 2014-2021 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Lesser General Public License as published by the Free Software
  Foundation; either version 2.1, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License along with
  TALER; see the file COPYING.LGPL.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file merchant_api_get_reserve.c
 * @brief Implementation of the GET /reserve request of the merchant's HTTP API
 * @author Marcello Stanisci
 * @author Christian Grothoff
 */
#include "platform.h"
#include <curl/curl.h>
#include <jansson.h>
#include <microhttpd.h> /* just for HTTP status codes */
#include <gnunet/gnunet_util_lib.h>
#include <gnunet/gnunet_curl_lib.h>
#include "taler_merchant_service.h"
#include "merchant_api_curl_defaults.h"
#include <taler/taler_json_lib.h>
#include <taler/taler_signatures.h>


/**
 * @brief A Handle for tracking wire reserve.
 */
struct TALER_MERCHANT_ReserveGetHandle
{

  /**
   * The url for this request.
   */
  char *url;

  /**
   * Handle for the request.
   */
  struct GNUNET_CURL_Job *job;

  /**
   * Function to call with the result.
   */
  TALER_MERCHANT_ReserveGetCallback cb;

  /**
   * Closure for @a cb.
   */
  void *cb_cls;

  /**
   * Reference to the execution context.
   */
  struct GNUNET_CURL_Context *ctx;
};


/**
 * Function called when we're done processing the
 * HTTP GET /reserve request.
 *
 * @param cls the `struct TALER_MERCHANT_ReserveGetHandle`
 * @param response_code HTTP response code, 0 on error
 * @param response response body, NULL if not in JSON
 */
static void
handle_reserve_get_finished (void *cls,
                             long response_code,
                             const void *response)
{
  struct TALER_MERCHANT_ReserveGetHandle *rgh = cls;
  const json_t *json = response;
  struct TALER_MERCHANT_HttpResponse hr = {
    .http_status = (unsigned int) response_code,
    .reply = json
  };
  bool active;

  rgh->job = NULL;
  switch (response_code)
  {
  case 0:
    hr.ec = TALER_EC_GENERIC_INVALID_RESPONSE;
    break;
  case MHD_HTTP_OK:
    {
      struct TALER_MERCHANT_ReserveSummary rs;
      const json_t *tips;
      const char *exchange_url = NULL;
      const char *payto_uri = NULL;
      struct GNUNET_JSON_Specification spec[] = {
        GNUNET_JSON_spec_timestamp ("creation_time",
                                       &rs.creation_time),
        GNUNET_JSON_spec_timestamp ("expiration_time",
                                       &rs.expiration_time),
        GNUNET_JSON_spec_bool ("active",
                               &active),
        GNUNET_JSON_spec_mark_optional (
          GNUNET_JSON_spec_string ("exchange_url",
                                   &exchange_url)),
        GNUNET_JSON_spec_mark_optional (
          GNUNET_JSON_spec_string ("payto_uri",
                                   &payto_uri)),
        TALER_JSON_spec_amount_any ("merchant_initial_amount",
                                    &rs.merchant_initial_amount),
        TALER_JSON_spec_amount_any ("exchange_initial_amount",
                                    &rs.exchange_initial_amount),
        TALER_JSON_spec_amount_any ("pickup_amount",
                                    &rs.pickup_amount),
        TALER_JSON_spec_amount_any ("committed_amount",
                                    &rs.committed_amount),
        GNUNET_JSON_spec_end ()
      };

      if (GNUNET_OK !=
          GNUNET_JSON_parse (json,
                             spec,
                             NULL, NULL))
      {
        GNUNET_break_op (0);
        hr.http_status = 0;
        hr.ec = TALER_EC_GENERIC_INVALID_RESPONSE;
        break;
      }

      tips = json_object_get (json,
                              "tips");
      if ((NULL == tips) ||
          json_is_null (tips))
      {
        rgh->cb (rgh->cb_cls,
                 &hr,
                 &rs,
                 false,
                 NULL,
                 NULL,
                 0,
                 NULL);
        TALER_MERCHANT_reserve_get_cancel (rgh);
        return;
      }
      if (! json_is_array (tips))
      {
        GNUNET_break_op (0);
        hr.http_status = 0;
        hr.ec = TALER_EC_GENERIC_INVALID_RESPONSE;
        break;
      }
      {
        size_t tds_length;
        json_t *tip;
        struct TALER_MERCHANT_TipDetails *tds;
        unsigned int i;
        bool ok;

        tds_length = json_array_size (tips);
        tds = GNUNET_new_array (tds_length,
                                struct TALER_MERCHANT_TipDetails);
        ok = true;
        json_array_foreach (tips, i, tip) {
          struct TALER_MERCHANT_TipDetails *td = &tds[i];
          struct GNUNET_JSON_Specification ispec[] = {
            GNUNET_JSON_spec_fixed_auto ("tip_id",
                                         &td->tip_id),
            TALER_JSON_spec_amount_any ("total_amount",
                                        &td->amount),
            GNUNET_JSON_spec_string ("reason",
                                     &td->reason),
            GNUNET_JSON_spec_end ()
          };

          if (GNUNET_OK !=
              GNUNET_JSON_parse (tip,
                                 ispec,
                                 NULL, NULL))
          {
            GNUNET_break_op (0);
            ok = false;
            break;
          }
        }

        if (! ok)
        {
          GNUNET_break_op (0);
          GNUNET_free (tds);
          hr.http_status = 0;
          hr.ec = TALER_EC_GENERIC_INVALID_RESPONSE;
          break;
        }
        rgh->cb (rgh->cb_cls,
                 &hr,
                 &rs,
                 active,
                 exchange_url,
                 payto_uri,
                 tds_length,
                 tds);
        GNUNET_free (tds);
        TALER_MERCHANT_reserve_get_cancel (rgh);
        return;
      }
    }
  case MHD_HTTP_UNAUTHORIZED:
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    /* Nothing really to verify, merchant says we need to authenticate. */
    break;
  case MHD_HTTP_INTERNAL_SERVER_ERROR:
    /* Server had an internal issue; we should retry, but this API
       leaves this to the application */
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    break;
  default:
    /* unexpected response code */
    GNUNET_break_op (0);
    TALER_MERCHANT_parse_error_details_ (json,
                                         response_code,
                                         &hr);
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u/%d\n",
                (unsigned int) response_code,
                (int) hr.ec);
    response_code = 0;
    break;
  }
  rgh->cb (rgh->cb_cls,
           &hr,
           NULL,
           false,
           NULL,
           NULL,
           0,
           NULL);
  TALER_MERCHANT_reserve_get_cancel (rgh);
}


struct TALER_MERCHANT_ReserveGetHandle *
TALER_MERCHANT_reserve_get (struct GNUNET_CURL_Context *ctx,
                            const char *backend_url,
                            const struct TALER_ReservePublicKeyP *reserve_pub,
                            bool fetch_tips,
                            TALER_MERCHANT_ReserveGetCallback cb,
                            void *cb_cls)
{
  struct TALER_MERCHANT_ReserveGetHandle *rgh;
  CURL *eh;

  rgh = GNUNET_new (struct TALER_MERCHANT_ReserveGetHandle);
  rgh->ctx = ctx;
  rgh->cb = cb;
  rgh->cb_cls = cb_cls;
  {
    char res_str[sizeof (*reserve_pub) * 2];
    char arg_str[sizeof (res_str) + 32];
    char *end;

    end = GNUNET_STRINGS_data_to_string (reserve_pub,
                                         sizeof (*reserve_pub),
                                         res_str,
                                         sizeof (res_str));
    *end = '\0';
    GNUNET_snprintf (arg_str,
                     sizeof (arg_str),
                     "private/reserves/%s",
                     res_str);
    rgh->url = TALER_url_join (backend_url,
                               arg_str,
                               "tips",
                               fetch_tips ? "yes" : "no",
                               NULL);
  }
  if (NULL == rgh->url)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Could not construct request URL.\n");
    GNUNET_free (rgh);
    return NULL;
  }
  eh = TALER_MERCHANT_curl_easy_get_ (rgh->url);
  rgh->job = GNUNET_CURL_job_add (ctx,
                                  eh,
                                  &handle_reserve_get_finished,
                                  rgh);
  return rgh;
}


void
TALER_MERCHANT_reserve_get_cancel (
  struct TALER_MERCHANT_ReserveGetHandle *rgh)
{
  if (NULL != rgh->job)
  {
    GNUNET_CURL_job_cancel (rgh->job);
    rgh->job = NULL;
  }
  GNUNET_free (rgh->url);
  GNUNET_free (rgh);
}


/* end of merchant_api_get_reserve.c */
