/*
  This file is part of TALER
  (C) 2018-2021 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3, or
  (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/

/**
 * @file taler_merchant_testing_lib.h
 * @brief API for writing an interpreter to test Taler components
 * @author Christian Grothoff <christian@grothoff.org>
 * @author Marcello Stanisci
 */
#ifndef TALER_MERCHANT_TESTING_LIB_H
#define TALER_MERCHANT_TESTING_LIB_H

#include <taler/taler_testing_lib.h>
#include "taler_merchant_service.h"

/* ********************* Helper functions ********************* */


#define MERCHANT_FAIL() \
  do {GNUNET_break (0); return NULL; } while (0)

/**
 * Prepare the merchant execution.  Create tables and check if
 * the port is available.
 *
 * @param config_filename configuration filename.
 * @return the base url, or NULL upon errors.  Must be freed
 *         by the caller.
 */
char *
TALER_TESTING_prepare_merchant (const char *config_filename);


/**
 * Start the merchant backend process.  Assume the port
 * is available and the database is clean.  Use the "prepare
 * merchant" function to do such tasks.
 *
 * @param config_filename configuration filename.
 * @param merchant_url merchant base URL, used to check
 *        if the merchant was started right.
 * @return the process, or NULL if the process could not
 *         be started.
 */
struct GNUNET_OS_Process *
TALER_TESTING_run_merchant (const char *config_filename,
                            const char *merchant_url);

/* ************** Specific interpreter commands ************ */


/**
 * Define a "config" CMD.
 *
 * @param label command label.
 * @param merchant_url base URL of the merchant serving the
 *        "config" request.
 * @param http_code expected HTTP response code.
 * @return the command.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_config (const char *label,
                          const char *merchant_url,
                          unsigned int http_code);


/**
 * Define a "GET /instances" CMD.
 *
 * @param label command label.
 * @param merchant_url base URL of the merchant serving the
 *        GET /instances request.
 * @param http_status expected HTTP response code.
 * @param ... NULL-terminated list of labels (const char *) of
 *        product (commands) we expect to be returned in the list
 *        (assuming @a http_code is #MHD_HTTP_OK)
 * @return the command.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_get_instances (const char *label,
                                          const char *merchant_url,
                                          unsigned int http_status,
                                          ...);


/**
 * Define a "POST /instances" CMD, simple version
 *
 * @param label command label.
 * @param merchant_url base URL of the merchant serving the
 *        POST /instances request.
 * @param instance_id the ID of the instance to create
 * @param payto_uri payment URI to use
 * @param currency currency to use for default fees
 * @param http_status expected HTTP response code.
 * @return the command.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_post_instances (const char *label,
                                           const char *merchant_url,
                                           const char *instance_id,
                                           const char *payto_uri,
                                           const char *currency,
                                           unsigned int http_status);


/**
 * Define a "POST /private/auth" CMD.
 *
 * @param label command label.
 * @param merchant_url base URL of the merchant serving the
 *        POST /instances request.
 * @param instance_id the ID of the instance, or NULL
 * @param auth_token new auth token to use, can be NULL for no auth
 * @param http_status expected HTTP response code.
 * @return the command.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_post_instance_auth (const char *label,
                                               const char *merchant_url,
                                               const char *instance_id,
                                               const char *auth_token,
                                               unsigned int http_status);


/**
 * Define a "POST /instances" CMD.  Comprehensive version.
 *
 * @param label command label.
 * @param merchant_url base URL of the merchant serving the
 *        POST /instances request.
 * @param instance_id the ID of the instance to query
 * @param accounts_length length of the @a accounts array
 * @param payto_uris URIs of the bank accounts of the merchant instance
 * @param name name of the merchant instance
 * @param address physical address of the merchant instance
 * @param jurisdiction jurisdiction of the merchant instance
 * @param default_max_wire_fee default maximum wire fee merchant is willing to fully pay
 * @param default_wire_fee_amortization default amortization factor for excess wire fees
 * @param default_max_deposit_fee default maximum deposit fee merchant is willing to pay
 * @param default_wire_transfer_delay default wire transfer delay merchant will ask for
 * @param default_pay_delay default validity period for offers merchant makes
 * @param auth_token authorization token needed to access the instance, can be NULL
 * @param http_status expected HTTP response code.
 * @return the command.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_post_instances2 (
  const char *label,
  const char *merchant_url,
  const char *instance_id,
  unsigned int accounts_length,
  const char *payto_uris[],
  const char *name,
  json_t *address,
  json_t *jurisdiction,
  const char *default_max_wire_fee,
  uint32_t default_wire_fee_amortization,
  const char *default_max_deposit_fee,
  struct GNUNET_TIME_Relative default_wire_transfer_delay,
  struct GNUNET_TIME_Relative default_pay_delay,
  const char *auth_token,
  unsigned int http_status);


/**
 * Define a "PATCH /instances/$ID" CMD.
 *
 * @param label command label.
 * @param merchant_url base URL of the merchant serving the
 *        PATCH /instance request.
 * @param instance_id the ID of the instance to query
 * @param payto_uris_length length of the @a accounts array
 * @param payto_uris URIs of the bank accounts of the merchant instance
 * @param name name of the merchant instance
 * @param address physical address of the merchant instance
 * @param jurisdiction jurisdiction of the merchant instance
 * @param default_max_wire_fee default maximum wire fee merchant is willing to fully pay
 * @param default_wire_fee_amortization default amortization factor for excess wire fees
 * @param default_max_deposit_fee default maximum deposit fee merchant is willing to pay
 * @param default_wire_transfer_delay default wire transfer delay merchant will ask for
 * @param default_pay_delay default validity period for offers merchant makes
 * @param http_status expected HTTP response code.
 * @return the command.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_patch_instance (
  const char *label,
  const char *merchant_url,
  const char *instance_id,
  unsigned int payto_uris_length,
  const char *payto_uris[],
  const char *name,
  json_t *address,
  json_t *jurisdiction,
  const char *default_max_wire_fee,
  uint32_t default_wire_fee_amortization,
  const char *default_max_deposit_fee,
  struct GNUNET_TIME_Relative default_wire_transfer_delay,
  struct GNUNET_TIME_Relative default_pay_delay,
  unsigned int http_status);


/**
 * Define a "GET instance" CMD.
 *
 * @param label command label.
 * @param merchant_url base URL of the merchant serving the
 *        GET /instances/$ID request.
 * @param instance_id the ID of the instance to query
 * @param http_status expected HTTP response code.
 * @param instance_reference reference to a "POST /instances" or "PATCH /instances/$ID" CMD
 *        that will provide what we expect the backend to return to us
 * @return the command.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_get_instance (const char *label,
                                         const char *merchant_url,
                                         const char *instance_id,
                                         unsigned int http_status,
                                         const char *instance_reference);


/**
 * Define a "GET instance" CMD that compares accounts returned.
 *
 * @param label command label.
 * @param merchant_url base URL of the merchant serving the
 *        GET /instances/$ID request.
 * @param instance_id the ID of the instance to query
 * @param http_status expected HTTP response code.
 * @param instance_reference reference to a "POST /instances" or "PATCH /instances/$ID" CMD
 *        that will provide what we expect the backend to return to us
 * @param active_accounts the accounts the merchant is actively using.
 * @param active_accounts_length length of @e active_accounts.
 * @param inactive_accounts the accounts the merchant is no longer using.
 * @param inactive_accounts_length length of @e inactive_accounts.
 * @return the command.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_get_instance2 (const char *label,
                                          const char *merchant_url,
                                          const char *instance_id,
                                          unsigned int http_status,
                                          const char *instance_reference,
                                          const char *active_accounts[],
                                          unsigned int active_accounts_length,
                                          const char *inactive_accounts[],
                                          unsigned int
                                          inactive_accounts_length);


/**
 * Define a "PURGE instance" CMD.
 *
 * @param label command label.
 * @param merchant_url base URL of the merchant serving the
 *        PURGE /instances/$ID request.
 * @param instance_id the ID of the instance to query
 * @param http_status expected HTTP response code.
 * @return the command.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_purge_instance (const char *label,
                                           const char *merchant_url,
                                           const char *instance_id,
                                           unsigned int http_status);


/**
 * Define a "DELETE instance" CMD.
 *
 * @param label command label.
 * @param merchant_url base URL of the merchant serving the
 *        DELETE /instances/$ID request.
 * @param instance_id the ID of the instance to query
 * @param http_status expected HTTP response code.
 * @return the command.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_delete_instance (const char *label,
                                            const char *merchant_url,
                                            const char *instance_id,
                                            unsigned int http_status);


/* ******************* /products **************** */


/**
 * Define a "POST /products" CMD.
 *
 * @param label command label.
 * @param merchant_url base URL of the merchant serving the
 *        POST /products request.
 * @param product_id the ID of the product to query
 * @param description description of the product
 * @param description_i18n Map from IETF BCP 47 language tags to localized descriptions
 * @param unit unit in which the product is measured (liters, kilograms, packages, etc.)
 * @param price the price for one @a unit of the product, zero is used to imply that
 *              this product is not sold separately or that the price is not fixed and
 *              must be supplied by the front-end.  If non-zero, price must include
 *              applicable taxes.
 * @param image base64-encoded product image
 * @param taxes list of taxes paid by the merchant
 * @param total_stock in @a units, -1 to indicate "infinite" (i.e. electronic books)
 * @param address where the product is in stock
 * @param next_restock when the next restocking is expected to happen, 0 for unknown,
 *                     #GNUNET_TIME_UNIT_FOREVER_ABS for 'never'.
 * @param http_status expected HTTP response code.
 * @return the command.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_post_products2 (
  const char *label,
  const char *merchant_url,
  const char *product_id,
  const char *description,
  json_t *description_i18n,
  const char *unit,
  const char *price,
  const char *image,
  json_t *taxes,
  int64_t total_stock,
  json_t *address,
  struct GNUNET_TIME_Timestamp next_restock,
  unsigned int http_status);


/**
 * Define a "POST /products" CMD, simple version
 *
 * @param label command label.
 * @param merchant_url base URL of the merchant serving the
 *        POST /products request.
 * @param product_id the ID of the product to create
 * @param description name of the product
 * @param price price of the product
 * @param http_status expected HTTP response code.
 * @return the command.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_post_products (const char *label,
                                          const char *merchant_url,
                                          const char *product_id,
                                          const char *description,
                                          const char *price,
                                          unsigned int http_status);


/**
 * Define a "PATCH /products/$ID" CMD.
 *
 * @param label command label.
 * @param merchant_url base URL of the merchant serving the
 *        PATCH /product request.
 * @param product_id the ID of the product to query
 * @param description description of the product
 * @param description_i18n Map from IETF BCP 47 language tags to localized descriptions
 * @param unit unit in which the product is measured (liters, kilograms, packages, etc.)
 * @param price the price for one @a unit of the product, zero is used to imply that
 *              this product is not sold separately or that the price is not fixed and
 *              must be supplied by the front-end.  If non-zero, price must include
 *              applicable taxes.
 * @param image base64-encoded product image
 * @param taxes list of taxes paid by the merchant
 * @param total_stock in @a units, -1 to indicate "infinite" (i.e. electronic books)
 * @param total_lost in @a units, must be larger than previous values, and may
 *               not exceed total_stock minus total_sold; if it does, the transaction
 *               will fail with a #MHD_HTTP_CONFLICT HTTP status code
 * @param address where the product is in stock
 * @param next_restock when the next restocking is expected to happen, 0 for unknown,
 *                     #GNUNET_TIME_UNIT_FOREVER_ABS for 'never'.
 * @param http_status expected HTTP response code.
 * @return the command.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_patch_product (
  const char *label,
  const char *merchant_url,
  const char *product_id,
  const char *description,
  json_t *description_i18n,
  const char *unit,
  const char *price,
  const char *image,
  json_t *taxes,
  int64_t total_stock,
  uint64_t total_lost,
  json_t *address,
  struct GNUNET_TIME_Timestamp next_restock,
  unsigned int http_status);


/**
 * Define a "GET /products" CMD.
 *
 * @param label command label.
 * @param merchant_url base URL of the merchant serving the
 *        GET /products request.
 * @param http_status expected HTTP response code.
 * @param ... NULL-terminated list of labels (const char *) of
 *        product (commands) we expect to be returned in the list
 *        (assuming @a http_code is #MHD_HTTP_OK)
 * @return the command.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_get_products (const char *label,
                                         const char *merchant_url,
                                         unsigned int http_status,
                                         ...);


/**
 * Define a "GET product" CMD.
 *
 * @param label command label.
 * @param merchant_url base URL of the merchant serving the
 *        GET /products/$ID request.
 * @param product_id the ID of the product to query
 * @param http_status expected HTTP response code.
 * @param product_reference reference to a "POST /products" or "PATCH /products/$ID" CMD
 *        that will provide what we expect the backend to return to us
 * @return the command.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_get_product (const char *label,
                                        const char *merchant_url,
                                        const char *product_id,
                                        unsigned int http_status,
                                        const char *product_reference);


/**
 * Define a "LOCK /products/$ID" CMD.
 *
 * @param label command label.
 * @param merchant_url base URL of the merchant serving the
 *        LOCK /product request.
 * @param product_id the ID of the product to lock.
 * @param duration how long the lock should last.
 * @param quantity how money units to lock.
 * @param http_status expected HTTP response code.
 * @return the command.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_lock_product (
  const char *label,
  const char *merchant_url,
  const char *product_id,
  struct GNUNET_TIME_Relative duration,
  uint32_t quantity,
  unsigned int http_status);


/**
 * Define a "DELETE product" CMD.
 *
 * @param label command label.
 * @param merchant_url base URL of the merchant serving the
 *        DELETE /products/$ID request.
 * @param product_id the ID of the product to query
 * @param http_status expected HTTP response code.
 * @return the command.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_delete_product (const char *label,
                                           const char *merchant_url,
                                           const char *product_id,
                                           unsigned int http_status);


/* ******************* /orders **************** */

/**
 * Make the "proposal" command.
 *
 * @param label command label
 * @param merchant_url base URL of the merchant serving
 *        the proposal request.
 * @param http_status expected HTTP status.
 * @param order_id the name of the order to add.
 * @param refund_deadline the deadline for refunds on this order.
 * @param pay_deadline the deadline for payment on this order.
 * @param amount the amount this order is for.
 * @return the command
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_post_orders (
  const char *label,
  const char *merchant_url,
  unsigned int http_status,
  const char *order_id,
  struct GNUNET_TIME_Timestamp refund_deadline,
  struct GNUNET_TIME_Timestamp pay_deadline,
  const char *amount);

/**
 * Make the "proposal" command AVOIDING claiming the order.
 *
 * @param label command label
 * @param merchant_url base URL of the merchant serving
 *        the proposal request.
 * @param http_status expected HTTP status.
 * @param order_id the name of the order to add.
 * @param refund_deadline the deadline for refunds on this order.
 * @param pay_deadline the deadline for payment on this order.
 * @param amount the amount this order is for.
 * @return the command
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_post_orders_no_claim (
  const char *label,
  const char *merchant_url,
  unsigned int http_status,
  const char *order_id,
  struct GNUNET_TIME_Timestamp refund_deadline,
  struct GNUNET_TIME_Timestamp pay_deadline,
  const char *amount);

/**
 * Make the "proposal" command.
 *
 * @param label command label
 * @param merchant_url base URL of the merchant serving
 *        the proposal request.
 * @param http_status expected HTTP status.
 * @param order_id the name of the order to add.
 * @param refund_deadline the deadline for refunds on this order.
 * @param pay_deadline the deadline for payment on this order.
 * @param claim_token whether to generate a claim token.
 * @param amount the amount this order is for.
 * @param payment_target payment target for the order.
 * @param products a string indicating the products this order will be
 *        purchasing. Should be formatted as
 *        "[product_id]/[quantity];...".
 * @param locks a string of references to lock product commands that should
 *        be formatted as "[lock_1];[lock_2];...".
 * @param duplicate_of if not NULL, a reference to a previous order command
 *        that should be duplicated and checked for an identical response.
 * @return the command
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_post_orders2 (
  const char *label,
  const char *merchant_url,
  unsigned int http_status,
  const char *order_id,
  struct GNUNET_TIME_Timestamp refund_deadline,
  struct GNUNET_TIME_Timestamp pay_deadline,
  bool claim_token,
  const char *amount,
  const char *payment_target,
  const char *products,
  const char *locks,
  const char *duplicate_of);


/**
 * Define a "GET /orders" CMD.
 *
 * @param label command label.
 * @param merchant_url base URL of the merchant serving the
 *        GET /orders request.
 * @param http_status expected HTTP response code.
 * @param ... NULL-terminated list of labels (const char *) of
 *        reserve (commands) we expect to be returned in the list
 *        (assuming @a http_code is #MHD_HTTP_OK)
 * @return the command.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_get_orders (const char *label,
                                       const char *merchant_url,
                                       unsigned int http_status,
                                       ...);


/**
 * Start a long poll for GET /private/orders.
 *
 * FIXME: needs additional arguments to specify range to long poll for!
 *
 * @param label the command label
 * @param merchant_url base URL of the merchant which will
 *        serve the request.
 * @param timeout how long to wait for the request to complete
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_poll_orders_start (const char *label,
                                     const char *merchant_url,
                                     struct GNUNET_TIME_Relative timeout);


/**
 * Complete a long poll for GET /private/orders.
 *
 * @param label the command label
 * @param http_status expected HTTP response code
 * @param poll_start_reference reference to the #TALER_TESTING_cmd_poll_orders_start command
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_poll_orders_conclude (const char *label,
                                        unsigned int http_status,
                                        const char *poll_start_reference);


/**
 * Define a GET /orders/$ORDER_ID CMD.
 *
 * @param label the command label
 * @param merchant_url base URL of the merchant which will
 *        serve the request.
 * @param order_reference reference to a command that created an order.
 * @param paid whether the order has been paid for or not.
 * @param refunded whether the order has been refunded.
 * @param refund_pending whether the order has refunds that haven't been obtained.
 * @param http_status expected HTTP response code for the request.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_wallet_get_order (const char *label,
                                    const char *merchant_url,
                                    const char *order_reference,
                                    bool paid,
                                    bool refunded,
                                    bool refund_pending,
                                    unsigned int http_status);


/**
 * Start a long poll for GET /orders/$ORDER_ID.
 *
 * @param label the command label
 * @param merchant_url base URL of the merchant which will
 *        serve the request.
 * @param order_ref reference to a command that created an order.
 * @param timeout how long to wait for the request to complete
 * @param await_refund NULL to not wait for refund, amount of value
 *         zero to wait for any refund amount, non-zero to poll
 *         for refund exceeding the given amount
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_wallet_poll_order_start (
  const char *label,
  const char *merchant_url,
  const char *order_ref,
  struct GNUNET_TIME_Relative timeout,
  const char *await_refund);


/**
 * Start a long poll for GET /orders/$ORDER_ID.
 *
 * @param label the command label
 * @param merchant_url base URL of the merchant which will
 *        serve the request.
 * @param order_ref reference to a command that created an order.
 * @param timeout how long to wait for the request to complete
 * @param await_refund NULL to not wait for refund, amount of value
 *         zero to wait for any refund amount, non-zero to poll
 *         for refund exceeding the given amount
 * @param session_id which session ID to use
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_wallet_poll_order_start2 (
  const char *label,
  const char *merchant_url,
  const char *order_ref,
  struct GNUNET_TIME_Relative timeout,
  const char *await_refund,
  const char *session_id);


/**
 * Complete a long poll for GET /orders/$ORDER_ID.
 *
 * @param label the command label
 * @param expected_http_status expected HTTP response code
 * @param expected_refund_amount refund expected, NULL for no refund expected
 * @param poll_start_reference reference to the #TALER_TESTING_cmd_wallet_poll_order_start command
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_wallet_poll_order_conclude (
  const char *label,
  unsigned int expected_http_status,
  const char *expected_refund_amount,
  const char *poll_start_reference);


/**
 * Complete a long poll for GET /orders/$ORDER_ID.
 *
 * @param label the command label
 * @param expected_http_status expected HTTP response code
 * @param expected_refund_amount refund expected, NULL for no refund expected
 * @param poll_start_reference reference to the #TALER_TESTING_cmd_wallet_poll_order_start command
 * @param already_paid_order_id ID of an already paid order we expect to get returned
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_wallet_poll_order_conclude2 (
  const char *label,
  unsigned int expected_http_status,
  const char *expected_refund_amount,
  const char *poll_start_reference,
  const char *already_paid_order_id);


/**
 * Define a GET /private/orders/$ORDER_ID CMD.
 *
 * @param label the command label
 * @param merchant_url base URL of the merchant which will
 *        serve the request.
 * @param order_reference reference to a command that created an order.
 * @param osc expected order status
 * @param refunded whether the order has been refunded.
 * @param http_status expected HTTP response code for the request.
 * @param ... NULL-terminated list of labels (const char *) of
 *        refunds (commands) we expect to be aggregated in the transfer
 *        (assuming @a http_code is #MHD_HTTP_OK). If @e refunded is false,
 *        this parameter is ignored.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_get_order (const char *label,
                                      const char *merchant_url,
                                      const char *order_reference,
                                      enum TALER_MERCHANT_OrderStatusCode osc,
                                      bool refunded,
                                      unsigned int http_status,
                                      ...);


/**
 * Define a GET /private/orders/$ORDER_ID CMD.
 *
 * @param label the command label
 * @param merchant_url base URL of the merchant which will
 *        serve the request.
 * @param order_reference reference to a command that created an order.
 * @param osc expected order status
 * @param wired whether the order has been wired or not.
 * @param transfers NULL-terminated list of labels (const char *) of
 *        wire transfers (commands) we expect to be aggregated in the order
 *        (assuming @a http_code is #MHD_HTTP_OK). If @e paid is fale, this
 *        parameter is ignored.
 * @param refunded whether the order has been refunded.
 * @param refunds NULL-terminated list of labels (const char *) of
 *        refunds (commands) we expect to be aggregated in the order
 *        (assuming @a http_code is #MHD_HTTP_OK). If @e refunded is false,
 *        this parameter is ignored.
 * @param forgets a NULL-terminated list of references to forget commands
 *        that apply to the order we are querying. If NULL, this is
 *        interpreted as no forgets are expected.
 * @param http_status expected HTTP response code for the request.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_get_order2 (const char *label,
                                       const char *merchant_url,
                                       const char *order_reference,
                                       enum TALER_MERCHANT_OrderStatusCode osc,
                                       bool wired,
                                       const char **transfers,
                                       bool refunded,
                                       const char **refunds,
                                       const char **forgets,
                                       unsigned int http_status);


/**
 * Start a long poll for GET /private/orders/$ORDER_ID.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_poll_order_start (const char *label,
                                    const char *merchant_url,
                                    const char *order_id,
                                    struct GNUNET_TIME_Relative timeout);


/**
 * Complete a long poll for GET /private/orders/$ORDER_ID.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_poll_order_conclude (const char *label,
                                       unsigned int http_status,
                                       const char *poll_start_reference);

/**
 * Make a "claim order" command.
 *
 * @param label command label.
 * @param merchant_url base URL of the merchant backend
 *        serving the proposal lookup request.
 * @param http_status expected HTTP response code.
 * @param order_reference reference to a POST order CMD, can be NULL if @a order_id given
 * @param order_id order id to lookup, can be NULL (then we use @a order_reference)
 * @return the command.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_claim_order (const char *label,
                                        const char *merchant_url,
                                        unsigned int http_status,
                                        const char *order_reference,
                                        const char *order_id);


/**
 * Make a "pay" test command.
 *
 * @param label command label.
 * @param merchant_url merchant base url
 * @param http_status expected HTTP response code.
 * @param proposal_reference the proposal whose payment status
 *        is going to be checked.
 * @param coin_reference reference to any command which is able
 *        to provide coins to use for paying.
 * @param amount_with_fee amount to pay, including the deposit
 *        fee
 * @param amount_without_fee amount to pay, no fees included.
 * @param session_id the session id to use for the payment (can be NULL).
 * @return the command
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_pay_order (const char *label,
                                      const char *merchant_url,
                                      unsigned int http_status,
                                      const char *proposal_reference,
                                      const char *coin_reference,
                                      const char *amount_with_fee,
                                      const char *amount_without_fee,
                                      const char *session_id);


/**
 * Make an "order paid" test command.
 *
 * @param label command label
 * @param merchant_url merchant base URL
 * @param pay_reference reference to the payment to verify
 * @param session_id the session to use for the verification.
 * @param http_status expected HTTP response code
 * @return the command
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_post_orders_paid (const char *label,
                                             const char *merchant_url,
                                             const char *pay_reference,
                                             const char *session_id,
                                             unsigned int http_status);


/**
 * Make an "abort" test command.
 *
 * @param label command label
 * @param merchant_url merchant base URL
 * @param pay_reference reference to the payment to abort
 * @param http_status expected HTTP response code
 * @return the command
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_order_abort (const char *label,
                                        const char *merchant_url,
                                        const char *pay_reference,
                                        unsigned int http_status);


/**
 * Make a "order forget" command.
 *
 * @param label command label.
 * @param merchant_url base URL of the merchant backend
 *        serving the order claim request.
 * @param http_status expected HTTP response code.
 * @param order_reference reference to a POST order CMD, can be NULL if @a order_id given
 * @param order_id order id to forget for, can be NULL (then we use @a order_reference)
 * @param ... NULL-terminated list of paths (const char *) to forget in
 *        the contract terms.
 * @return the command.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_forget_order (
  const char *label,
  const char *merchant_url,
  unsigned int http_status,
  const char *order_reference,
  const char *order_id,
  ...);


/**
 * Define a "refund" order CMD.
 *
 * @param label command label.
 * @param merchant_url base URL of the backend serving the
 *        "refund increase" request.
 * @param reason refund justification, human-readable.
 * @param order_id order id of the contract to refund.
 * @param refund_amount amount to be refund-increased.
 * @param http_code expected HTTP response code.
 * @return the command.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_order_refund (const char *label,
                                         const char *merchant_url,
                                         const char *reason,
                                         const char *order_id,
                                         const char *refund_amount,
                                         unsigned int http_code);


/**
 * Define a "refund order" CMD.
 *
 * @param label command label.
 * @param merchant_url base URL of the backend serving the
 *        "refund increase" request.
 * @param order_ref order id of the contract to refund.
 * @param http_code expected HTTP response code.
 * @param ... NULL-terminated list of labels (const char *) of
 *        refunds (commands) we expect to be aggregated in the transfer
 *        (assuming @a http_code is #MHD_HTTP_OK). If @e refunded is false,
 *        this parameter is ignored.
 * @return the command.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_wallet_order_refund (const char *label,
                                       const char *merchant_url,
                                       const char *order_ref,
                                       unsigned int http_code,
                                       ...);


/**
 * Define a "DELETE order" CMD.
 *
 * @param label command label.
 * @param merchant_url base URL of the merchant serving the
 *        DELETE /instances/$ID request.
 * @param order_id the ID of the instance to query
 * @param http_status expected HTTP response code.
 * @return the command.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_delete_order (const char *label,
                                         const char *merchant_url,
                                         const char *order_id,
                                         unsigned int http_status);


/* ******************* /transfers *************** */


/**
 * Define a POST /transfers CMD.  Details like the WTID and
 * other required parameters will be extracted from the bank
 * history, using the latest transfer of the specified
 * @a credit_amount to the @a merchant_url.
 *
 * @param label command label.
 * @param auth credentials to access the exchange's bank account
 * @param payto_uri URL of the exchange's bank account
 * @param merchant_url base URL of the backend serving the
 *        "refund increase" request.
 * @param credit_amount amount credited
 * @param http_code expected HTTP response code
 * @param ... NULL-terminated list of labels (const char *) of
 *        deposit (commands) we expect to be aggregated in the transfer
 *        (assuming @a http_code is #MHD_HTTP_OK)
 * @return the command.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_post_transfer (
  const char *label,
  const struct TALER_BANK_AuthenticationData *auth,
  const char *payto_uri,
  const char *merchant_url,
  const char *credit_amount,
  unsigned int http_code,
  ...);


/**
 * Define a POST /transfers CMD.  Details like the WTID and
 * other required parameters are explicitly provided.
 *
 * @param label command label.
 * @param merchant_url base URL of the backend
 * @param payto_uri URL of the exchange's bank account
 * @param credit_amount amount credited
 * @param wtid wire transfer ID to use, NULL to pick one at random
 * @param exchange_url URL of the exchange to use
 * @param http_code expected HTTP response code
 * @return the command.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_post_transfer2 (
  const char *label,
  const char *merchant_url,
  const char *payto_uri,
  const char *credit_amount,
  const char *wtid,
  const char *exchange_url,
  unsigned int http_code);


/**
 * We have discovered the @a serial for a POST transfer
 * command. Store it (for the bank row trait).
 *
 * @param cmd command to update
 * @param serial serial number to store (identifies the row)
 */
void
TALER_TESTING_cmd_merchant_post_transfer_set_serial (
  struct TALER_TESTING_Command *cmd,
  uint64_t serial);


/**
 * Define a GET /transfers CMD.
 *
 * @param label command label.
 * @param merchant_url base URL of the backend serving the
 *        "refund increase" request.
 * @param payto_uri payto URI to filter by, NULL for no filter
 * @param http_code expected HTTP response code
 * @param ... NULL-terminated list of labels (const char *) of
 *        transfer (commands) we expect to be returned in the list
 *        (assuming @a http_code is #MHD_HTTP_OK)
 * @return the command.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_get_transfers (const char *label,
                                          const char *merchant_url,
                                          const char *payto_uri,
                                          unsigned int http_code,
                                          ...);


/**
 * Define a "DELETE transfer" CMD.
 *
 * @param label command label.
 * @param merchant_url base URL of the merchant for the
 *        DELETE /transfers/$ID request.
 * @param transfer_ref reference to a command that
 *             yields the transfer ID to delete
 * @param http_status expected HTTP response code.
 * @return the command.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_delete_transfer (const char *label,
                                            const char *merchant_url,
                                            const char *transfer_ref,
                                            unsigned int http_status);


/* ******************* /reserves *************** */


/**
 * Define a "POST /reserves" CMD
 *
 * @param label command label.
 * @param merchant_url url to the murchant.
 * @param initial_balance initial amount in the reserve.
 * @param exchange_url url to the exchange
 * @param wire_method wire transfer method to use for this reserve
 * @param http_status expected HTTP response code.
 * @return the command.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_post_reserves (const char *label,
                                          const char *merchant_url,
                                          const char *initial_balance,
                                          const char *exchange_url,
                                          const char *wire_method,
                                          unsigned int http_status);


/**
 * This commands does not query the backend at all,
 * but just makes up a fake reserve.
 *
 * @param label command label.
 * @return the command.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_post_reserves_fake (const char *label);


/**
 * Define a "GET reserve" CMD.
 *
 * @param label command label.
 * @param merchant_url base URL of the merchant serving the request.
 * @param http_status expected HTTP response code.
 * @param reserve_reference reference to a "POST /reserves" that provides the
 *        information we are expecting.
 * @return the command.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_get_reserve (const char *label,
                                        const char *merchant_url,
                                        unsigned int http_status,
                                        const char *reserve_reference);


/**
 * Define a "GET reserve" CMD.
 *
 * @param label command label.
 * @param merchant_url base URL of the merchant serving the request.
 * @param http_status expected HTTP response code.
 * @param reserve_reference reference to a "POST /reserves" that provides the
 *        information we are expecting.
 * @param ... NULL-terminated list of labels (const char *) of
 *        tip (commands) we expect to be returned in the list
 *        (assuming @a http_code is #MHD_HTTP_OK)
 * @return the command.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_get_reserve_with_tips (const char *label,
                                                  const char *merchant_url,
                                                  unsigned int http_status,
                                                  const char *reserve_reference,
                                                  ...);


/**
 * Define a "GET /reserves" CMD
 *
 * @param label command label.
 * @param merchant_url url to the merchant.
 * @param http_status expected HTTP response code.
 * @param ... NULL-terminated list of labels (const char *) of
 *        reserve (commands) we expect to be returned in the list
 *        (assuming @a http_code is #MHD_HTTP_OK)
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_get_reserves (const char *label,
                                         const char *merchant_url,
                                         unsigned int http_status,
                                         ...);


/**
 * Define a "DELETE reserve" CMD.
 *
 * @param label command label.
 * @param merchant_url base URL of the merchant serving the
 *        DELETE /reserves/$RESERVE_PUB request.
 * @param reserve_reference command label of a command providing a reserve
 * @param http_status expected HTTP response code.
 * @return the command.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_delete_reserve (const char *label,
                                           const char *merchant_url,
                                           const char *reserve_reference,
                                           unsigned int http_status);


/**
 * Define a "PURGE reserve" CMD.
 *
 * @param label command label.
 * @param merchant_url base URL of the merchant serving the
 *        DELETE /reserves/$RESERVE_PUB request.
 * @param reserve_reference command label of a command providing a reserve
 * @param http_status expected HTTP response code.
 * @return the command.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_purge_reserve (const char *label,
                                          const char *merchant_url,
                                          const char *reserve_reference,
                                          unsigned int http_status);


/**
 * Define a get tips CMD.
 *
 * @param label the command label
 * @param merchant_url base URL of the merchant which will
 *        server the /tip-query request.
 * @param http_status expected HTTP response code for the
 *        /tip-query request.
 * @param ... NULL-terminated list of labels (const char *) of
 *        tip (commands) we expect to be returned in the list
 *        (assuming @a http_code is #MHD_HTTP_OK)
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_get_tips (const char *label,
                            const char *merchant_url,
                            unsigned int http_status,
                            ...);


/**
 * Define a get tips CMD.
 *
 * @param label the command label
 * @param merchant_url base URL of the merchant which will
 *        server the /tip-query request.
 * @param http_status expected HTTP response code for the
 *        /tip-query request.
 * @param offset row to start querying the database from.
 * @param limit how many rows to return (with direction).
 * @param ... NULL-terminated list of labels (const char *) of
 *        tip (commands) we expect to be returned in the list
 *        (assuming @a http_code is #MHD_HTTP_OK)
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_get_tips2 (const char *label,
                             const char *merchant_url,
                             uint64_t offset,
                             int64_t limit,
                             unsigned int http_status,
                             ...);


/**
 * Define a GET /private/tips/$TIP_ID CMD.
 *
 * @param label the command label
 * @param merchant_url base URL of the merchant which will
 *        serve the request.
 * @param tip_reference reference to a command that created a tip.
 * @param http_status expected HTTP response code for the request.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_get_tip (const char *label,
                                    const char *merchant_url,
                                    const char *tip_reference,
                                    unsigned int http_status);


/**
 * Define a GET /private/tips/$TIP_ID CMD.
 *
 * @param label the command label
 * @param merchant_url base URL of the merchant which will
 *        serve the request.
 * @param tip_reference reference to a command that created a tip.
 * @param http_status expected HTTP response code for the request.
 * @param ... NULL-terminated list of labels (const char *) of
 *        pickup (commands) we expect to be returned in the list
 *        (assuming @a http_code is #MHD_HTTP_OK)
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_get_tip_with_pickups (const char *label,
                                                 const char *merchant_url,
                                                 const char *tip_reference,
                                                 unsigned int http_status,
                                                 ...);

/**
 * Define a GET /tips/$TIP_ID CMD.
 *
 * @param label the command label
 * @param merchant_url base URL of the merchant which will
 *        serve the request.
 * @param tip_reference reference to a command that created a tip.
 * @param http_status expected HTTP response code for the request.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_wallet_get_tip (const char *label,
                                  const char *merchant_url,
                                  const char *tip_reference,
                                  unsigned int http_status);


/**
 * Define a GET /tips/$TIP_ID CMD.
 *
 * @param label the command label
 * @param merchant_url base URL of the merchant which will
 *        serve the request.
 * @param tip_reference reference to a command that created a tip.
 * @param amount_remaining the balance remaining after pickups.
 * @param http_status expected HTTP response code for the request.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_wallet_get_tip2 (const char *label,
                                   const char *merchant_url,
                                   const char *tip_reference,
                                   const char *amount_remaining,
                                   unsigned int http_status);


/**
 * Create a /tip-authorize CMD.
 *
 * @param label this command label
 * @param merchant_url the base URL of the merchant that will
 *        serve the /tip-authorize request.
 * @param exchange_url the base URL of the exchange that owns
 *        the reserve from which the tip is going to be gotten.
 * @param http_status the HTTP response code which is expected
 *        for this operation.
 * @param justification human-readable justification for this
 *        tip authorization.
 * @param amount the amount to authorize for tipping.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_tip_authorize (const char *label,
                                 const char *merchant_url,
                                 const char *exchange_url,
                                 unsigned int http_status,
                                 const char *justification,
                                 const char *amount);


/**
 * Create a /tip-authorize CMD.
 *
 * @param label this command label
 * @param merchant_url the base URL of the merchant that will
 *        serve the /tip-authorize request.
 * @param exchange_url the base URL of the exchange that owns
 *        the reserve from which the tip is going to be gotten.
 * @param reserve_reference reference to a command that created
 *        a reserve.
 * @param http_status the HTTP response code which is expected
 *        for this operation.
 * @param justification human-readable justification for this
 *        tip authorization.
 * @param amount the amount to authorize for tipping.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_tip_authorize_from_reserve (const char *label,
                                              const char *merchant_url,
                                              const char *exchange_url,
                                              const char *reserve_reference,
                                              unsigned int http_status,
                                              const char *justification,
                                              const char *amount);


/**
 * Create a /tip-authorize CMD, specifying the Taler error code
 * that is expected to be returned by the backend.
 *
 * @param label this command label
 * @param merchant_url the base URL of the merchant that will
 *        serve the /tip-authorize request.
 * @param exchange_url the base URL of the exchange that owns
 *        the reserve from which the tip is going to be gotten.
 * @param http_status the HTTP response code which is expected
 *        for this operation.
 * @param justification human-readable justification for this
 *        tip authorization.
 * @param amount the amount to authorize for tipping.
 * @param ec expected Taler-defined error code.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_tip_authorize_with_ec (const char *label,
                                         const char *merchant_url,
                                         const char *exchange_url,
                                         unsigned int http_status,
                                         const char *justification,
                                         const char *amount,
                                         enum TALER_ErrorCode ec);


/**
 * Create a /tip-authorize CMD, specifying the Taler error code
 * that is expected to be returned by the backend.
 *
 * @param label this command label
 * @param merchant_url the base URL of the merchant that will
 *        serve the /tip-authorize request.
 * @param exchange_url the base URL of the exchange that owns
 *        the reserve from which the tip is going to be gotten.
 * @param reserve_reference reference to a command that created
 *        a reserve.
 * @param http_status the HTTP response code which is expected
 *        for this operation.
 * @param justification human-readable justification for this
 *        tip authorization.
 * @param amount the amount to authorize for tipping.
 * @param ec expected Taler-defined error code.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_tip_authorize_from_reserve_with_ec (
  const char *label,
  const char *merchant_url,
  const char *exchange_url,
  const char *reserve_reference,
  unsigned int http_status,
  const char *justification,
  const char *amount,
  enum TALER_ErrorCode ec);


/**
 * This commands does not query the backend at all,
 * but just makes up a fake authorization id that will
 * be subsequently used by the "pick up" CMD in order
 * to test against such a case.
 *
 * @param label command label.
 * @return the command.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_tip_authorize_fake (const char *label);


/**
 * Define a /tip-pickup CMD, equipped with the expected error
 * code.
 *
 * @param label the command label
 * @param merchant_url base URL of the backend which will serve
 *        the /tip-pickup request.
 * @param http_status expected HTTP response code.
 * @param authorize_reference reference to a /tip-autorize CMD
 *        that offers a tip id to pick up.
 * @param amounts array of string-defined amounts that specifies
 *        which denominations will be accepted for tipping.
 * @param ec expected Taler error code.
 * @return the command
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_tip_pickup_with_ec (const char *label,
                                      const char *merchant_url,
                                      unsigned int http_status,
                                      const char *authorize_reference,
                                      const char **amounts,
                                      enum TALER_ErrorCode ec);

/**
 * Define a /tip-pickup CMD.
 *
 * @param label the command label
 * @param merchant_url base URL of the backend which will serve
 *        the /tip-pickup request.
 * @param http_status expected HTTP response code.
 * @param authorize_reference reference to a /tip-autorize CMD
 *        that offers a tip id to pick up.
 * @param amounts array of string-defined amounts that specifies
 *        which denominations will be accepted for tipping.
 * @return the command
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_tip_pickup (const char *label,
                              const char *merchant_url,
                              unsigned int http_status,
                              const char *authorize_reference,
                              const char **amounts);


/**
 * Run a command to fetch the KYC status of a merchant.
 *
 * @param label the command label
 * @param merchant_url base URL of the merchant
 * @param instance_id instance to use, NULL if instance is part of @a merchant_url
 * @param h_wire_ref label of command with a merchant wire hash trait
 *        of the bank account to check KYC for; NULL to check all accounts
 * @param exchange_url base URL of the exchange to check KYC status for
 * @param expected_http_status expected HTTP status
 * @return the command
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_kyc_get (const char *label,
                                    const char *merchant_url,
                                    const char *instance_id,
                                    const char *h_wire_ref,
                                    const char *exchange_url,
                                    unsigned int expected_http_status);


/* ****** Specific traits supported by this component ******* */


/**
 * Call #op on all simple traits.
 */
// FIXME: rename: refund_entry->refund_detail
#define TALER_MERCHANT_TESTING_SIMPLE_TRAITS(op) \
  op (claim_nonce, const struct GNUNET_CRYPTO_EddsaPublicKey) \
  op (tip_id, const struct GNUNET_HashCode) \
  op (instance_name, const char *) \
  op (instance_id, const char *) \
  op (address, const json_t) \
  op (product_description, const char *) \
  op (product_image, const char *) \
  op (product_stock, const int64_t) \
  op (product_unit, const char *) \
  op (product_id, const char *) \
  op (reason, const char *) \
  op (lock_uuid, const char *) \
  op (auth_token, const char *) \
  op (paths_length, const uint32_t) \
  op (payto_length, const uint32_t) \
  op (num_planchets, const uint32_t) \
  op (i18n_description, const json_t) \
  op (taxes, const json_t)      \
  op (fee, const struct TALER_Amount) \
  op (max_wire_fee, const struct TALER_Amount) \
  op (max_deposit_fee, const struct TALER_Amount) \
  op (wire_fee_amortization, const uint32_t) \
  op (jurisdiction, const json_t) \
  op (wire_delay, const struct GNUNET_TIME_Relative) \
  op (pay_delay, const struct GNUNET_TIME_Relative) \
  op (refund_entry, const struct TALER_MERCHANT_RefundDetail) \
  op (order_terms, const json_t) \
  op (h_contract_terms, const struct TALER_PrivateContractHash) \
  op (h_wire, const struct TALER_MerchantWireHash) \
  op (proposal_reference, const char *)


/**
 * Call #op on all indexed traits.
 */
#define TALER_MERCHANT_TESTING_INDEXED_TRAITS(op) \
  op (coin_reference, const char *) \
  op (paths, const char *) \
  op (payto_uris, const char *) \
  op (amounts, const struct TALER_Amount) \
  op (planchet_secrets, const struct TALER_PlanchetSecretsP)


TALER_MERCHANT_TESTING_SIMPLE_TRAITS (TALER_TESTING_MAKE_DECL_SIMPLE_TRAIT)
TALER_MERCHANT_TESTING_INDEXED_TRAITS (TALER_TESTING_MAKE_DECL_INDEXED_TRAIT)


#endif
