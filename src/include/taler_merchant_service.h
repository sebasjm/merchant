/*
  This file is part of TALER
  Copyright (C) 2014-2021 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License along with
  TALER; see the file COPYING.LIB.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file taler_merchant_service.h
 * @brief C interface of libtalermerchant, a C library to use merchant's HTTP API
 * @author Christian Grothoff
 * @author Marcello Stanisci
 */
#ifndef _TALER_MERCHANT_SERVICE_H
#define _TALER_MERCHANT_SERVICE_H

#include <taler/taler_util.h>
#include <taler/taler_error_codes.h>
#include <taler/taler_exchange_service.h>
#include <gnunet/gnunet_curl_lib.h>
#include <jansson.h>


/**
 * General information about the HTTP response we obtained
 * from the merchant for a request.
 */
struct TALER_MERCHANT_HttpResponse
{

  /**
   * The complete JSON reply. NULL if we failed to parse the
   * reply (too big, invalid JSON).
   */
  const json_t *reply;

  /**
   * The complete JSON reply from the exchange, if we generated an error in
   * response to an exchange error.  Usually set if @e http_status is
   * #MHD_HTTP_FAILED_DEPENDENCY or #MHD_HTTP_SERVICE_UNAVAILABLE. NULL if we
   * failed to obtain a JSON reply from the exchange or if we did not receive
   * an error from the exchange.
   */
  const json_t *exchange_reply;

  /**
   * Set to the human-readable 'hint' that is optionally
   * provided by the exchange together with errors. NULL
   * if no hint was provided or if there was no error.
   */
  const char *hint;

  /**
   * The error hint from the exchange, if we generated an error in
   * response to an exchange error.  Usually set if @e http_status is
   * #MHD_HTTP_FAILED_DEPENDENCY or #MHD_HTTP_SERVICE_UNAVAILABLE. NULL if we
   * failed to obtain a hint from the exchange or if we did not receive
   * an error from the exchange.
   */
  const char *exchange_hint;

  /**
   * HTTP status code for the response.  0 if the
   * HTTP request failed and we did not get any answer, or
   * if the answer was invalid and we set @a ec to a
   * client-side error code.
   */
  unsigned int http_status;

  /**
   * The HTTP status code from the exchange, if we generated an error in
   * response to an exchange error.  Usually set if @e http_status is
   * #MHD_HTTP_FAILED_DEPENDENCY or #MHD_HTTP_SERVICE_UNAVAILABLE. 0 if we
   * failed to obtain a JSON reply from the exchange or if we did not receive
   * an error from the exchange.
   */
  unsigned int exchange_http_status;

  /**
   * Taler error code.  #TALER_EC_NONE if everything was
   * OK.  Usually set to the "code" field of an error
   * response, but may be set to values created at the
   * client side, for example when the response was
   * not in JSON format or was otherwise ill-formed.
   */
  enum TALER_ErrorCode ec;

  /**
   * The error code from the reply from the exchange, if we generated an error in
   * response to an exchange error.  Usually set if @e http_status is
   * #MHD_HTTP_FAILED_DEPENDENCY or #MHD_HTTP_SERVICE_UNAVAILABLE. NULL if we
   * failed to obtain a error code from the exchange or if we did not receive
   * an error from the exchange.
   */
  enum TALER_ErrorCode exchange_code;

};


/**
 * Take a @a response from the merchant API that (presumably) contains
 * error details and setup the corresponding @a hr structure.  Internally
 * used to convert merchant's responses in to @a hr.
 *
 * @param response if NULL we will report #TALER_EC_GENERIC_INVALID_RESPONSE in `ec`
 * @param http_status http status to use
 * @param[out] hr response object to initialize, fields will
 *        only be valid as long as @a response is valid as well
 */
void
TALER_MERCHANT_parse_error_details_ (const json_t *response,
                                     unsigned int http_status,
                                     struct TALER_MERCHANT_HttpResponse *hr);


/**
 * Construct a new base URL using the existing @a base_url
 * and the given @a instance_id.  The result WILL end with
 * '/'.
 *
 * @param base_url a merchant base URL without "/instances/" in it,
 *         must not be the empty string; MAY end with '/'.
 * @param instance_id ID of an instance
 * @return "${base_url}/instances/${instance_id}/"
 */
char *
TALER_MERCHANT_baseurl_add_instance (const char *base_url,
                                     const char *instance_id);


/**
 * Contains information gathered from parsing a taler://pay URI.
 */
struct TALER_MERCHANT_PayUriData
{
  /**
   * Hostname (and possibly port) of the merchant.
   */
  char *merchant_host;

  /**
   * Prefix to the base url of the merchant backend. May be NULL.
   */
  char *merchant_prefix_path;

  /**
   * The id of the order to pay.
   */
  char *order_id;

  /**
   * Session id to use when paying the order. May be NULL.
   */
  char *session_id;

  /**
   * Claim token to use when claiming the order. May be NULL.
   */
  struct TALER_ClaimTokenP *claim_token;

  /**
   * A WLAN SSID that the wallet can use to connect to the internet in order to
   * to pay. May be NULL.
   */
  char *ssid;

  /**
   * true if the URI used taler+http.
   */
  bool use_http;
};


/**
 * Extracts information from a taler://pay URI.
 *
 * @param pay_uri the URI to parse.
 * @param[out] parse_data data extracted from the URI. Must be free'd.
 * @return #GNUNET_SYSERR if @e pay_uri is malformed, #GNUNET_OK otherwise.
 */
int
TALER_MERCHANT_parse_pay_uri (const char *pay_uri,
                              struct TALER_MERCHANT_PayUriData *parse_data);


/**
 * Frees data contained in the result of parsing a taler://pay URI.
 *
 * @param[in] parse_data the data to free.
 */
void
TALER_MERCHANT_parse_pay_uri_free (
  struct TALER_MERCHANT_PayUriData *parse_data);


/**
 * Contains information gathered from parsing a taler://refund URI.
 */
struct TALER_MERCHANT_RefundUriData
{
  /**
   * Hostname (and possibly port) of the merchant.
   */
  char *merchant_host;

  /**
   * Prefix to the base url of the merchant backend. May be NULL.
   */
  char *merchant_prefix_path;

  /**
   * The id of the order to pay.
   */
  char *order_id;

  /**
   * A WLAN SSID that the wallet can use to connect to the internet in order to
   * to pay. May be NULL.
   */
  char *ssid;

  /**
   * true if the URI used taler+http.
   */
  bool use_http;
};


/**
 * Extracts information from a taler://refund URI.
 *
 * @param refund_uri the URI to parse.
 * @param[out] parse_data data extracted from the URI. Must be free'd.
 * @return #GNUNET_SYSERR if @e refund_uri is malformed, #GNUNET_OK otherwise.
 */
int
TALER_MERCHANT_parse_refund_uri (
  const char *refund_uri,
  struct TALER_MERCHANT_RefundUriData *parse_data);


/**
 * Frees data contained in the result of parsing a taler://refund URI.
 *
 * @param parse_data the data to free.
 */
void
TALER_MERCHANT_parse_refund_uri_free (
  struct TALER_MERCHANT_RefundUriData *parse_data);


/* ********************* /public/config ****************** */


/**
 * How compatible are the protocol version of the auditor and this
 * client?  The bits (1,2,4) can be used to test if the auditor's
 * version is incompatible, older or newer respectively.
 */
enum TALER_MERCHANT_VersionCompatibility
{

  /**
   * The auditor runs exactly the same protocol version.
   */
  TALER_MERCHANT_VC_MATCH = 0,

  /**
   * The auditor is too old or too new to be compatible with this
   * implementation (bit)
   */
  TALER_MERCHANT_VC_INCOMPATIBLE = 1,

  /**
   * The auditor is older than this implementation (bit)
   */
  TALER_MERCHANT_VC_OLDER = 2,

  /**
   * The auditor is too old to be compatible with
   * this implementation.
   */
  TALER_MERCHANT_VC_INCOMPATIBLE_OUTDATED
    = TALER_MERCHANT_VC_INCOMPATIBLE
      | TALER_MERCHANT_VC_OLDER,

  /**
   * The auditor is more recent than this implementation (bit).
   */
  TALER_MERCHANT_VC_NEWER = 4,

  /**
   * The auditor is too recent for this implementation.
   */
  TALER_MERCHANT_VC_INCOMPATIBLE_NEWER
    = TALER_MERCHANT_VC_INCOMPATIBLE
      | TALER_MERCHANT_VC_NEWER,

  /**
   * We could not even parse the version data.
   */
  TALER_MERCHANT_VC_PROTOCOL_ERROR = 8

};


/**
 * @brief Config information we get from the backend.
 */
struct TALER_MERCHANT_ConfigInformation
{
  /**
   * Currency used/supported by the merchant.
   */
  const char *currency;

  /**
   * Supported Taler protocol version by the merchant.
   * String in the format current:revision:age using the
   * semantics of GNU libtool.  See
   * https://www.gnu.org/software/libtool/manual/html_node/Versioning.html#Versioning
   */
  const char *version;

};


/**
 * Function called with information about the merchant.
 *
 * @param cls closure
 * @param hr HTTP response data
 * @param ci basic information about the merchant
 * @param compat protocol compatibility information
 */
typedef void
(*TALER_MERCHANT_ConfigCallback) (
  void *cls,
  const struct TALER_MERCHANT_HttpResponse *hr,
  const struct TALER_MERCHANT_ConfigInformation *ci,
  enum TALER_MERCHANT_VersionCompatibility compat);


/**
 * Handle for a #TALER_MERCHANT_config_get() operation.
 */
struct TALER_MERCHANT_ConfigGetHandle;


/**
 * Get the config data of a merchant. Will connect to the merchant backend
 * and obtain information about the backend.  The respective information will
 * be passed to the @a config_cb once available.
 *
 * @param ctx the context
 * @param backend_url HTTP base URL for the backend
 * @param config_cb function to call with the
 *        backend's config information
 * @param config_cb_cls closure for @a config_cb
 * @return the config check handle; NULL upon error
 */
struct TALER_MERCHANT_ConfigGetHandle *
TALER_MERCHANT_config_get (struct GNUNET_CURL_Context *ctx,
                           const char *backend_url,
                           TALER_MERCHANT_ConfigCallback config_cb,
                           void *config_cb_cls);


/**
 * Cancel /config request.  Must not be called by clients after
 * the callback was invoked.
 *
 * @param vgh request to cancel.
 */
void
TALER_MERCHANT_config_get_cancel (struct TALER_MERCHANT_ConfigGetHandle *vgh);


/* ********************* /instances *********************** */


/**
 * @brief Information about a merchant instance.
 */
struct TALER_MERCHANT_InstanceInformation
{
  /**
   * Id of this instance.  This $ID can be used to construct the URL of the
   * instance, by combining it using "$MERCHANT_BASEURL/instances/$ID/".
   */
  const char *id;

  /**
   * Legal name of the merchant/instance.
   */
  const char *name;

  /**
   * Public key of the instance.
   */
  struct TALER_MerchantPublicKeyP merchant_pub;

  /**
   * JSON array of payment targets (strings) supported by this backend
   * instance.
   */
  json_t *payment_targets;

};


/**
 * Handle for a GET /instances operation.
 */
struct TALER_MERCHANT_InstancesGetHandle;


/**
 * Function called with the result of the GET /instances operation.
 *
 * @param cls closure
 * @param hr HTTP response data
 * @param iis_length length of the @a iis array
 * @param iis array with instance information of length @a iis_length
 */
typedef void
(*TALER_MERCHANT_InstancesGetCallback)(
  void *cls,
  const struct TALER_MERCHANT_HttpResponse *hr,
  unsigned int iis_length,
  const struct TALER_MERCHANT_InstanceInformation iis[]);


/**
 * Get the instance data of a backend. Will connect to the merchant backend
 * and obtain information about the instances.  The respective information will
 * be passed to the @a instances_cb once available.
 *
 * @param ctx the context
 * @param backend_url HTTP base URL for the backend
 * @param instances_cb function to call with the
 *        backend's instances information
 * @param instances_cb_cls closure for @a config_cb
 * @return the instances handle; NULL upon error
 */
struct TALER_MERCHANT_InstancesGetHandle *
TALER_MERCHANT_instances_get (struct GNUNET_CURL_Context *ctx,
                              const char *backend_url,
                              TALER_MERCHANT_InstancesGetCallback instances_cb,
                              void *instances_cb_cls);


/**
 * Cancel /instances request.  Must not be called by clients after
 * the callback was invoked.
 *
 * @param igh request to cancel.
 */
void
TALER_MERCHANT_instances_get_cancel (
  struct TALER_MERCHANT_InstancesGetHandle *igh);


/**
 * Handle for a POST /instances/$ID operation.
 */
struct TALER_MERCHANT_InstancesPostHandle;


/**
 * Function called with the result of the GET /instances/$ID operation.
 *
 * @param cls closure
 * @param hr HTTP response data
 */
typedef void
(*TALER_MERCHANT_InstancesPostCallback)(
  void *cls,
  const struct TALER_MERCHANT_HttpResponse *hr);


/**
 * Setup an new instance in the backend.
 *
 * @param ctx the context
 * @param backend_url HTTP base URL for the backend
 * @param instance_id identity of the instance to get information about
 * @param accounts_length how many bank accounts this instance has
 * @param payto_uris URIs of the bank accounts of the merchant instance
 * @param name name of the merchant instance
 * @param address physical address of the merchant instance
 * @param jurisdiction jurisdiction of the merchant instance
 * @param default_max_wire_fee default maximum wire fee merchant is willing to fully pay
 * @param default_wire_fee_amortization default amortization factor for excess wire fees
 * @param default_max_deposit_fee default maximum deposit fee merchant is willing to pay
 * @param default_wire_transfer_delay default wire transfer delay merchant will ask for
 * @param default_pay_delay default validity period for offers merchant makes
 * @param auth_token authentication token to use for access control, NULL for external auth; MUST follow RFC 8959
 * @param cb function to call with the
 *        backend's instances information
 * @param cb_cls closure for @a config_cb
 * @return the instances handle; NULL upon error
 */
struct TALER_MERCHANT_InstancesPostHandle *
TALER_MERCHANT_instances_post (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  const char *instance_id,
  unsigned int accounts_length,
  const char *payto_uris[],
  const char *name,
  const json_t *address,
  const json_t *jurisdiction,
  const struct TALER_Amount *default_max_wire_fee,
  uint32_t default_wire_fee_amortization,
  const struct TALER_Amount *default_max_deposit_fee,
  struct GNUNET_TIME_Relative default_wire_transfer_delay,
  struct GNUNET_TIME_Relative default_pay_delay,
  const char *auth_token,
  TALER_MERCHANT_InstancesPostCallback cb,
  void *cb_cls);


/**
 * Cancel /instances request.  Must not be called by clients after
 * the callback was invoked.
 *
 * @param iph request to cancel.
 */
void
TALER_MERCHANT_instances_post_cancel (
  struct TALER_MERCHANT_InstancesPostHandle *iph);


/**
 * Handle for a PATCH /instances/$ID operation.
 */
struct TALER_MERCHANT_InstancePatchHandle;


/**
 * Function called with the result of the GET /instances/$ID operation.
 *
 * @param cls closure
 * @param hr HTTP response data
 */
typedef void
(*TALER_MERCHANT_InstancePatchCallback)(
  void *cls,
  const struct TALER_MERCHANT_HttpResponse *hr);


/**
 * Modify an existing instance in the backend.
 *
 * @param ctx the context
 * @param backend_url HTTP base URL for the backend (top-level "default" instance
 *                    or base URL of an instance if @a instance_id is NULL)
 * @param instance_id identity of the instance to modify information about; NULL
 *                    if the instance is identified as part of the @a backend_url
 * @param accounts_length length of the @a accounts array
 * @param payto_uris URIs of the bank accounts of the merchant instance
 * @param name name of the merchant instance
 * @param address physical address of the merchant instance
 * @param jurisdiction jurisdiction of the merchant instance
 * @param default_max_wire_fee default maximum wire fee merchant is willing to fully pay
 * @param default_wire_fee_amortization default amortization factor for excess wire fees
 * @param default_max_deposit_fee default maximum deposit fee merchant is willing to pay
 * @param default_wire_transfer_delay default wire transfer delay merchant will ask for
 * @param default_pay_delay default validity period for offers merchant makes
 * @param cb function to call with the
 *        backend's instances information
 * @param cb_cls closure for @a config_cb
 * @return the instances handle; NULL upon error
 */
struct TALER_MERCHANT_InstancePatchHandle *
TALER_MERCHANT_instance_patch (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  const char *instance_id,
  unsigned int accounts_length,
  const char *payto_uris[],
  const char *name,
  const json_t *address,
  const json_t *jurisdiction,
  const struct TALER_Amount *default_max_wire_fee,
  uint32_t default_wire_fee_amortization,
  const struct TALER_Amount *default_max_deposit_fee,
  struct GNUNET_TIME_Relative default_wire_transfer_delay,
  struct GNUNET_TIME_Relative default_pay_delay,
  TALER_MERCHANT_InstancePatchCallback cb,
  void *cb_cls);


/**
 * Cancel /instances request.  Must not be called by clients after
 * the callback was invoked.
 *
 * @param iph request to cancel.
 */
void
TALER_MERCHANT_instance_patch_cancel (
  struct TALER_MERCHANT_InstancePatchHandle *iph);


/**
 * Handle for an operation to modify authentication settings.
 */
struct TALER_MERCHANT_InstanceAuthPostHande;


/**
 * Function called with the result of the GET /instances/$ID operation.
 *
 * @param cls closure
 * @param hr HTTP response data
 */
typedef void
(*TALER_MERCHANT_InstanceAuthPostCallback)(
  void *cls,
  const struct TALER_MERCHANT_HttpResponse *hr);


/**
 * Modify authentication for an existing instance in the backend.
 *
 * @param ctx the context
 * @param backend_url HTTP base URL for the backend (top-level "default" instance
 *                    or base URL of an instance if @a instance_id is NULL)
 * @param instance_id identity of the instance to patch the authentication for; NULL
 *                    if the instance is identified as part of the @a backend_url
 * @param auth_token authorization token needed to access the instance, can be NULL
 *                   to switch to no (or external) authentication; MUST follow RFC 8959
 * @param cb function to call with the backend's response
 * @param cb_cls closure for @a config_cb
 * @return the instances handle; NULL upon error
 */
struct TALER_MERCHANT_InstanceAuthPostHandle *
TALER_MERCHANT_instance_auth_post (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  const char *instance_id,
  const char *auth_token,
  TALER_MERCHANT_InstanceAuthPostCallback cb,
  void *cb_cls);


/**
 * Cancel /private/auth request.  Must not be called by clients after
 * the callback was invoked.  Afterwards, the authentication may or
 * may not have been updated.
 *
 * @param iaph request to cancel.
 */
void
TALER_MERCHANT_instance_auth_post_cancel (
  struct TALER_MERCHANT_InstanceAuthPostHandle *iaph);


/**
 * Handle for a GET /instances/$ID operation.
 */
struct TALER_MERCHANT_InstanceGetHandle;


/**
 * Details about a merchant's bank account.
 */
struct TALER_MERCHANT_Account
{
  /**
   * salt used to compute h_wire
   */
  struct TALER_WireSalt salt;

  /**
   * payto:// URI of the account.
   */
  const char *payto_uri;

  /**
   * Hash of @e payto_uri and @e salt.
   */
  struct TALER_MerchantWireHash h_wire;

  /**
   * true if the account is active,
   * false if it is historic.
   */
  bool active;
};


/**
 * Details about an instance.
 */
struct TALER_MERCHANT_InstanceDetails
{
  /**
   * Name of the merchant instance
   */
  const char *name;

  /**
   * public key of the merchant instance
   */
  const struct TALER_MerchantPublicKeyP *merchant_pub;

  /**
   * physical address of the merchant instance
   */
  const json_t *address;

  /**
   * jurisdiction of the merchant instance
   */
  const json_t *jurisdiction;

  /**
   * default maximum wire fee merchant is willing to fully pay
   */
  const struct TALER_Amount *default_max_wire_fee;

  /**
   * default amortization factor for excess wire fees
   */
  uint32_t default_wire_fee_amortization;

  /**
   * default maximum deposit fee merchant is willing to pay
   */
  const struct TALER_Amount *default_max_deposit_fee;

  /**
   * default wire transfer delay merchant will ask for
   */
  struct GNUNET_TIME_Relative default_wire_transfer_delay;

  /**
   * default validity period for offers merchant makes
   */
  struct GNUNET_TIME_Relative default_pay_delay;
};


/**
 * Function called with the result of the GET /instances/$ID operation.
 *
 * @param cls closure
 * @param hr HTTP response data
 * @param accounts_length length of the @a accounts array
 * @param accounts bank accounts of the merchant instance
 * @param details details about the instance configuration
 */
typedef void
(*TALER_MERCHANT_InstanceGetCallback)(
  void *cls,
  const struct TALER_MERCHANT_HttpResponse *hr,
  unsigned int accounts_length,
  const struct TALER_MERCHANT_Account accounts[],
  const struct TALER_MERCHANT_InstanceDetails *details);


/**
 * Get the details on one of the instances of a backend. Will connect to the
 * merchant backend and obtain information about the instance.  The respective
 * information will be passed to the @a cb once available.
 *
 * @param ctx the context
 * @param backend_url HTTP base URL for the backend
 * @param instance_id identity of the instance to get information about
 * @param cb function to call with the
 *        backend's instances information
 * @param cb_cls closure for @a config_cb
 * @return the instances handle; NULL upon error
 */
struct TALER_MERCHANT_InstanceGetHandle *
TALER_MERCHANT_instance_get (struct GNUNET_CURL_Context *ctx,
                             const char *backend_url,
                             const char *instance_id,
                             TALER_MERCHANT_InstanceGetCallback cb,
                             void *cb_cls);


/**
 * Cancel /instances request.  Must not be called by clients after
 * the callback was invoked.
 *
 * @param igh request to cancel.
 */
void
TALER_MERCHANT_instance_get_cancel (
  struct TALER_MERCHANT_InstanceGetHandle *igh);


/**
 * Handle for a DELETE /instances operation.
 */
struct TALER_MERCHANT_InstanceDeleteHandle;


/**
 * Function called with the result of the DELETE /instances operation.
 *
 * @param cls closure
 * @param hr HTTP response data
 */
typedef void
(*TALER_MERCHANT_InstanceDeleteCallback)(
  void *cls,
  const struct TALER_MERCHANT_HttpResponse *hr);


/**
 * Delete the private key of an instance of a backend, thereby disabling the
 * instance for future requests.  Will preserve the other instance data
 * (i.e. for taxation).
 *
 * @param ctx the context
 * @param backend_url HTTP base URL for the backend (top-level "default" instance
 *                    or base URL of an instance if @a instance_id is NULL)
 * @param instance_id identity of the instance to modify information about; NULL
 *                    if the instance is identified as part of the @a backend_url
 * @param instances_cb function to call with the
 *        backend's return
 * @param instances_cb_cls closure for @a config_cb
 * @return the instances handle; NULL upon error
 */
struct TALER_MERCHANT_InstanceDeleteHandle *
TALER_MERCHANT_instance_delete (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  const char *instance_id,
  TALER_MERCHANT_InstanceDeleteCallback instances_cb,
  void *instances_cb_cls);


/**
 * Purge all data associated with an instance. Use with
 * extreme caution.
 *
 * @param ctx the context
 * @param backend_url HTTP base URL for the backend
 * @param instance_id which instance should be deleted
 * @param instances_cb function to call with the
 *        backend's return
 * @param instances_cb_cls closure for @a config_cb
 * @return the instances handle; NULL upon error
 */
struct TALER_MERCHANT_InstanceDeleteHandle *
TALER_MERCHANT_instance_purge (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  const char *instance_id,
  TALER_MERCHANT_InstanceDeleteCallback instances_cb,
  void *instances_cb_cls);


/**
 * Cancel /instances DELETE request.  Must not be called by clients after
 * the callback was invoked.
 *
 * @param idh request to cancel.
 */
void
TALER_MERCHANT_instance_delete_cancel (
  struct TALER_MERCHANT_InstanceDeleteHandle *idh);


/**
 * Cancel /instances DELETE request.  Must not be called by clients after
 * the callback was invoked.
 *
 * @param arg request to cancel.
 */
#define TALER_MERCHANT_instance_purge_cancel(arg) \
  TALER_MERCHANT_instance_delete_cancel (arg)


/* ********************* /products *********************** */


/**
 * Handle for a GET /products operation.
 */
struct TALER_MERCHANT_ProductsGetHandle;

/**
 * Individual product from the inventory (minimal information
 * returned via GET /products).
 */
struct TALER_MERCHANT_InventoryEntry
{
  /**
   * Product identifier.
   */
  const char *product_id;

};


/**
 * Function called with the result of the GET /products operation.
 *
 * @param cls closure
 * @param hr HTTP response details
 * @param products_length length of the @a products array
 * @param products array of products the requested instance offers
 */
typedef void
(*TALER_MERCHANT_ProductsGetCallback)(
  void *cls,
  const struct TALER_MERCHANT_HttpResponse *hr,
  unsigned int products_length,
  const struct TALER_MERCHANT_InventoryEntry products[]);


/**
 * Make a GET /products request.
 *
 * @param ctx the context
 * @param backend_url HTTP base URL for the backend
 * @param cb function to call with the backend's inventory information
 * @param cb_cls closure for @a cb
 * @return the request handle; NULL upon error
 */
struct TALER_MERCHANT_ProductsGetHandle *
TALER_MERCHANT_products_get (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  TALER_MERCHANT_ProductsGetCallback cb,
  void *cb_cls);


/**
 * Cancel GET /products operation.
 *
 * @param pgh operation to cancel
 */
void
TALER_MERCHANT_products_get_cancel (
  struct TALER_MERCHANT_ProductsGetHandle *pgh);


/**
 * Handle for a GET /product/$ID operation. Gets details
 * about a single product. Do not confused with a
 * `struct TALER_MERCHANT_ProductsGetHandle`, which
 * obtains a list of all products.
 */
struct TALER_MERCHANT_ProductGetHandle;


/**
 * Function called with the result of the GET /products operation.
 *
 * @param cls closure
 * @param hr HTTP response details
 * @param description description of the product
 * @param description_i18n Map from IETF BCP 47 language tags to localized descriptions
 * @param unit unit in which the product is measured (liters, kilograms, packages, etc.)
 * @param price the price for one @a unit of the product, zero is used to imply that
 *              this product is not sold separately or that the price is not fixed and
 *              must be supplied by the front-end.  If non-zero, price must include
 *              applicable taxes.
 * @param image base64-encoded product image
 * @param taxes list of taxes paid by the merchant
 * @param total_stock in @a units, -1 to indicate "infinite" (i.e. electronic books),
 *                does NOT indicate remaining stocks, to get remaining stocks,
 *                subtract @a total_sold and @a total_lost. Note that this still
 *                does not then say how many of the remaining inventory are locked.
 * @param total_sold in @a units, total number of @a unit of product sold
 * @param total_lost in @a units, total number of @a unit of product lost from inventory
 * @param location where the product is in stock
 * @param next_restock when the next restocking is expected to happen, 0 for unknown,
 *                     #GNUNET_TIME_UNIT_FOREVER_ABS for 'never'.
 */
typedef void
(*TALER_MERCHANT_ProductGetCallback)(
  void *cls,
  const struct TALER_MERCHANT_HttpResponse *hr,
  const char *description,
  const json_t *description_i18n,
  const char *unit,
  const struct TALER_Amount *price,
  const char *image,
  const json_t *taxes,
  int64_t total_stock,
  uint64_t total_sold,
  uint64_t total_lost,
  const json_t *location,
  struct GNUNET_TIME_Timestamp next_restock);


/**
 * Make a GET /product/$ID request to get details about an
 * individual product.
 *
 * @param ctx the context
 * @param backend_url HTTP base URL for the backend
 * @param product_id identifier of the product to inquire about
 * @param cb function to call with the backend's product information
 * @param cb_cls closure for @a cb
 * @return the request handle; NULL upon error
 */
struct TALER_MERCHANT_ProductGetHandle *
TALER_MERCHANT_product_get (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  const char *product_id,
  TALER_MERCHANT_ProductGetCallback cb,
  void *cb_cls);


/**
 * Cancel GET /products/$ID operation.
 *
 * @param pgh operation to cancel
 */
void
TALER_MERCHANT_product_get_cancel (
  struct TALER_MERCHANT_ProductGetHandle *pgh);


/**
 * Handle for a POST /products operation.
 */
struct TALER_MERCHANT_ProductsPostHandle;


/**
 * Function called with the result of the POST /products operation.
 *
 * @param cls closure
 * @param hr HTTP response details
 */
typedef void
(*TALER_MERCHANT_ProductsPostCallback)(
  void *cls,
  const struct TALER_MERCHANT_HttpResponse *hr);


/**
 * Make a POST /products request to add a product to the
 * inventory.
 *
 * @param ctx the context
 * @param backend_url HTTP base URL for the backend
 * @param product_id identifier to use for the product
 * @param description description of the product
 * @param description_i18n Map from IETF BCP 47 language tags to localized descriptions
 * @param unit unit in which the product is measured (liters, kilograms, packages, etc.)
 * @param price the price for one @a unit of the product, zero is used to imply that
 *              this product is not sold separately or that the price is not fixed and
 *              must be supplied by the front-end.  If non-zero, price must include
 *              applicable taxes.
 * @param image base64-encoded product image
 * @param taxes list of taxes paid by the merchant
 * @param total_stock in @a units, -1 to indicate "infinite" (i.e. electronic books)
 * @param address where the product is in stock
 * @param next_restock when the next restocking is expected to happen, 0 for unknown,
 *                     #GNUNET_TIME_UNIT_FOREVER_ABS for 'never'.
 * @param cb function to call with the backend's result
 * @param cb_cls closure for @a cb
 * @return the request handle; NULL upon error
 */
struct TALER_MERCHANT_ProductsPostHandle *
TALER_MERCHANT_products_post (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  const char *product_id,
  const char *description,
  const json_t *description_i18n,
  const char *unit,
  const struct TALER_Amount *price,
  const char *image,
  const json_t *taxes,
  int64_t total_stock,
  const json_t *address,
  struct GNUNET_TIME_Timestamp next_restock,
  TALER_MERCHANT_ProductsPostCallback cb,
  void *cb_cls);


/**
 * Cancel POST /products operation.
 *
 * @param pph operation to cancel
 */
void
TALER_MERCHANT_products_post_cancel (
  struct TALER_MERCHANT_ProductsPostHandle *pph);


/**
 * Handle for a PATCH /products operation.
 */
struct TALER_MERCHANT_ProductPatchHandle;


/**
 * Function called with the result of the PATCH /products operation.
 *
 * @param cls closure
 * @param hr HTTP response details
 */
typedef void
(*TALER_MERCHANT_ProductPatchCallback)(
  void *cls,
  const struct TALER_MERCHANT_HttpResponse *hr);


/**
 * Make a PATCH /products request to update product details in the
 * inventory.
 *
 * @param ctx the context
 * @param backend_url HTTP base URL for the backend
 * @param product_id identifier to use for the product; the product must exist,
 *                    or the transaction will fail with a #MHD_HTTP_NOT_FOUND
 *                    HTTP status code
 * @param description description of the product
 * @param description_i18n Map from IETF BCP 47 language tags to localized descriptions
 * @param unit unit in which the product is measured (liters, kilograms, packages, etc.)
 * @param price the price for one @a unit of the product, zero is used to imply that
 *              this product is not sold separately or that the price is not fixed and
 *              must be supplied by the front-end.  If non-zero, price must include
 *              applicable taxes.
 * @param image base64-encoded product image
 * @param taxes list of taxes paid by the merchant
 * @param total_stock in @a units, -1 to indicate "infinite" (i.e. electronic books),
 *               must be larger than previous values
 * @param total_lost in @a units, must be larger than previous values, and may
 *               not exceed total_stock minus total_sold; if it does, the transaction
 *               will fail with a #MHD_HTTP_CONFLICT HTTP status code
 * @param address where the product is in stock
 * @param next_restock when the next restocking is expected to happen
 * @param cb function to call with the backend's result
 * @param cb_cls closure for @a cb
 * @return the request handle; NULL upon error
 */
struct TALER_MERCHANT_ProductPatchHandle *
TALER_MERCHANT_product_patch (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  const char *product_id,
  const char *description,
  const json_t *description_i18n,
  const char *unit,
  const struct TALER_Amount *price,
  const char *image,
  const json_t *taxes,
  int64_t total_stock,
  uint64_t total_lost,
  const json_t *address,
  struct GNUNET_TIME_Timestamp next_restock,
  TALER_MERCHANT_ProductPatchCallback cb,
  void *cb_cls);


/**
 * Cancel PATCH /products operation.
 *
 * @param pph operation to cancel
 */
void
TALER_MERCHANT_product_patch_cancel (
  struct TALER_MERCHANT_ProductPatchHandle *pph);


/**
 * Handle for a POST /products/$ID/lock operation.
 */
struct TALER_MERCHANT_ProductLockHandle;


/**
 * Function called with the result of the POST /product/$ID/lock operation.
 *
 * @param cls closure
 * @param hr HTTP response details
 */
typedef void
(*TALER_MERCHANT_ProductLockCallback)(
  void *cls,
  const struct TALER_MERCHANT_HttpResponse *hr);


/**
 * Make a POST /products/$ID/lock request to reserve a certain
 * amount of product in inventory to a reservation UUID.
 *
 * @param ctx the context
 * @param backend_url HTTP base URL for the backend
 * @param product_id identifier of the product
 * @param uuid UUID that identifies the client holding the lock
 * @param duration how long should the lock be held
 * @param quantity how much product should be locked
 * @param cb function to call with the backend's lock status
 * @param cb_cls closure for @a cb
 * @return the request handle; NULL upon error
 */
struct TALER_MERCHANT_ProductLockHandle *
TALER_MERCHANT_product_lock (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  const char *product_id,
  const char *uuid,
  struct GNUNET_TIME_Relative duration,
  uint32_t quantity,
  TALER_MERCHANT_ProductLockCallback cb,
  void *cb_cls);


/**
 * Cancel POST /products/$ID/lock operation. Note that the
 * lock may or may not be acquired despite the cancellation.
 *
 * @param plh operation to cancel
 */
void
TALER_MERCHANT_product_lock_cancel (
  struct TALER_MERCHANT_ProductLockHandle *plh);


/**
 * Handle for a DELETE /products/$ID operation.
 */
struct TALER_MERCHANT_ProductDeleteHandle;


/**
 * Function called with the result of the DELETE /product/$ID operation.
 *
 * @param cls closure
 * @param hr HTTP response details
 */
typedef void
(*TALER_MERCHANT_ProductDeleteCallback)(
  void *cls,
  const struct TALER_MERCHANT_HttpResponse *hr);


/**
 * Make a DELETE /products/$ID request to delete a product from our
 * inventory.
 *
 * @param ctx the context
 * @param backend_url HTTP base URL for the backend
 * @param product_id identifier of the product
 * @param cb function to call with the backend's deletion status
 * @param cb_cls closure for @a cb
 * @return the request handle; NULL upon error
 */
struct TALER_MERCHANT_ProductDeleteHandle *
TALER_MERCHANT_product_delete (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  const char *product_id,
  TALER_MERCHANT_ProductDeleteCallback cb,
  void *cb_cls);


/**
 * Cancel DELETE /products/$ID operation.
 *
 * @param pdh operation to cancel
 */
void
TALER_MERCHANT_product_delete_cancel (
  struct TALER_MERCHANT_ProductDeleteHandle *pdh);


/* ********************* /orders ************************** */


/**
 * Handle to a POST /private/orders operation
 */
struct TALER_MERCHANT_PostOrdersHandle;


/**
 * Possible details from a reply to POST /private/orders.
 */
struct TALER_MERCHANT_PostOrdersReply
{

  /**
   * HTTP response details. HTTP status code
   * determines what parts of @e details are valid.
   */
  struct TALER_MERCHANT_HttpResponse hr;

  /**
   * Details of the reply, depending on the HTTP
   * status code.
   */
  union
  {

    /**
     * Details provided if the @e hr.http_status is
     * #MHD_HTTP_OK and the order was created.
     */
    struct
    {

      /**
       * order id of the newly created order
       */
      const char *order_id;

      /**
       * the claim token generated by the merchant,
       * (NULL if it was NOT generated).
       */
      const struct TALER_ClaimTokenP *token;

    } ok;

    /**
     * Details provided if the @e hr.http_status is
     * #MHD_HTTP_GONE because a product was out of stock.
     */
    struct
    {
      /**
       * ID of the product of the order that is out of
       * stock.
       */
      const char *product_id;

      /**
       * How many units were requested by the order.
       */
      uint64_t requested_quantity;

      /**
       * How many units are actually still in stock.
       */
      uint64_t available_quantity;

      /**
       * When does the backend expect the stock to be
       * restocked? 0 for unknown.
       */
      struct GNUNET_TIME_Timestamp restock_expected;

    } gone;

  } details;

};


/**
 * Callbacks of this type are used to serve the result of submitting a
 * POST /orders request to a merchant.
 *
 * @param cls closure
 * @param por response details
 */
typedef void
(*TALER_MERCHANT_PostOrdersCallback) (
  void *cls,
  const struct TALER_MERCHANT_PostOrdersReply *por);


/**
 * POST to /orders at the backend to setup an order and obtain
 * the order ID (which may have been set by the front-end).
 *
 * @param ctx execution context
 * @param backend_url URL of the backend
 * @param order basic information about this purchase, to be extended by the backend
 * @param refund_delay how long can refunds happen for this order; 0 to use
 *             absolute value from contract (or not allow refunds).
 * @param cb the callback to call when a reply for this request is available
 * @param cb_cls closure for @a cb
 * @return a handle for this request, NULL on error
 */
struct TALER_MERCHANT_PostOrdersHandle *
TALER_MERCHANT_orders_post (struct GNUNET_CURL_Context *ctx,
                            const char *backend_url,
                            const json_t *order,
                            struct GNUNET_TIME_Relative refund_delay,
                            TALER_MERCHANT_PostOrdersCallback cb,
                            void *cb_cls);

/**
 * Information needed per product for constructing orders from
 * the inventory.
 */
struct TALER_MERCHANT_InventoryProduct
{

  /**
   * Identifier of the product.
   */
  char *product_id;

  /**
   * How many units of this product should be ordered.
   */
  unsigned int quantity;
};


/**
 * POST to /orders at the backend to setup an order and obtain
 * the order ID (which may have been set by the front-end).
 *
 * @param ctx execution context
 * @param backend_url URL of the backend
 * @param order basic information about this purchase, to be extended by the backend
 * @param refund_delay how long can refunds happen for this order; 0 to use
 *             absolute value from contract (or not allow refunds).
 * @param payment_target desired payment target identifier (to select merchant bank details)
 * @param inventory_products_length length of the @a inventory_products array
 * @param inventory_products products to add to the order from the inventory
 * @param uuids_length length of the @a uuids array
 * @param uuids array of UUIDs with locks on @a inventory_products
 * @param create_token whether to create a claim token
 * @param cb the callback to call when a reply for this request is available
 * @param cb_cls closure for @a cb
 * @return a handle for this request, NULL on error
 */
struct TALER_MERCHANT_PostOrdersHandle *
TALER_MERCHANT_orders_post2 (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  const json_t *order,
  struct GNUNET_TIME_Relative refund_delay,
  const char *payment_target,
  unsigned int inventory_products_length,
  const struct TALER_MERCHANT_InventoryProduct inventory_products[],
  unsigned int uuids_length,
  const char *uuids[],
  bool create_token,
  TALER_MERCHANT_PostOrdersCallback cb,
  void *cb_cls);


/**
 * Cancel a POST /orders request.  This function cannot be used
 * on a request handle if a response is already served for it.
 *
 * @param po the proposal operation request handle
 */
void
TALER_MERCHANT_orders_post_cancel (
  struct TALER_MERCHANT_PostOrdersHandle *po);


/**
 * Handle for a GET /orders operation.
 */
struct TALER_MERCHANT_OrdersGetHandle;

/**
 * Individual order (minimal information returned via GET /orders).
 */
struct TALER_MERCHANT_OrderEntry
{
  /**
   * Order identifier.
   */
  const char *order_id;

  /**
   * Time when the order was created. Useful for filtering by
   * 'date' (in #TALER_MERCHANT_orders_get2()).
   */
  struct GNUNET_TIME_Timestamp timestamp;

  /**
   * Serial ID of the order. Useful for filtering by 'start_row'
   * (in #TALER_MERCHANT_orders_get2()).
   */
  uint64_t order_serial;

  /**
   * The amount of the order.
   */
  struct TALER_Amount amount;

  /**
   * The summary of the order.
   */
  const char *summary;

  /**
   * Whether it is possible to refund a part of the order still.
   */
  bool refundable;

  /**
   * Whether the order has been paid.
   */
  bool paid;

};


/**
 * Function called with the result of the GET /orders operation.
 *
 * @param cls closure
 * @param hr HTTP response details
 * @param orders_length length of the @a orders array
 * @param orders array of orders the requested instance has made
 */
typedef void
(*TALER_MERCHANT_OrdersGetCallback)(
  void *cls,
  const struct TALER_MERCHANT_HttpResponse *hr,
  unsigned int orders_length,
  const struct TALER_MERCHANT_OrderEntry orders[]);


/**
 * Make a GET /orders request.
 *
 * @param ctx the context
 * @param backend_url HTTP base URL for the backend
 * @param cb function to call with the backend's inventory information
 * @param cb_cls closure for @a cb
 * @return the request handle; NULL upon error
 */
struct TALER_MERCHANT_OrdersGetHandle *
TALER_MERCHANT_orders_get (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  TALER_MERCHANT_OrdersGetCallback cb,
  void *cb_cls);


/**
 * Make a GET /orders request with filters.
 *
 * @param ctx the context
 * @param backend_url HTTP base URL for the backend
 * @param paid filter on payment status
 * @param refunded filter on refund status
 * @param wired filter on wire transfer status
 * @param date range limit by date
 * @param start_row range limit by order table row
 * @param delta range from which @a date and @a start_row apply, positive
 *              to return delta items after the given limit(s), negative to
 *              return delta items before the given limit(s)
 * @param timeout how long to wait (long polling) of zero results match the query
 * @param cb function to call with the backend's inventory information
 * @param cb_cls closure for @a cb
 * @return the request handle; NULL upon error
 */
struct TALER_MERCHANT_OrdersGetHandle *
TALER_MERCHANT_orders_get2 (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  enum TALER_EXCHANGE_YesNoAll paid,
  enum TALER_EXCHANGE_YesNoAll refunded,
  enum TALER_EXCHANGE_YesNoAll wired,
  struct GNUNET_TIME_Timestamp date,
  uint64_t start_row,
  int64_t delta,
  struct GNUNET_TIME_Relative timeout,
  TALER_MERCHANT_OrdersGetCallback cb,
  void *cb_cls);


/**
 * Cancel GET /orders operation.
 *
 * @param pgh operation to cancel
 */
void
TALER_MERCHANT_orders_get_cancel (
  struct TALER_MERCHANT_OrdersGetHandle *pgh);


/**
 * Handle for a GET /orders/$ID operation. (Wallet's public endpoint,
 * not to be confused with the private endpoint for the merchant.)
 */
struct TALER_MERCHANT_OrderWalletGetHandle;


/**
 * Callback to process a GET /orders/$ID response
 *
 * @param cls closure
 * @param hr HTTP response details
 * @param paid #GNUNET_YES if the payment is settled, #GNUNET_NO if not
 *        settled, #GNUNET_SYSERR on error
 *        (note that refunded payments are returned as paid!)
 * @param refunded #GNUNET_YES if there is at least on refund on this payment,
 *        #GNUNET_NO if refunded, #GNUNET_SYSERR or error
 * @param refund_pending #GNUNET_YES if there are refunds waiting to be
 *        obtained, #GNUNET_NO if all refunds have been obtained, #GNUNET_SYSERR
 *        on error.
 * @param refund_amount amount that was refunded, NULL if there
 *        was no refund
 * @param taler_pay_uri the URI that instructs the wallets to process
 *                      the payment
 * @param already_paid_order_id equivalent order that this customer
 *                      paid already, or NULL for none
 */
typedef void
(*TALER_MERCHANT_OrderWalletGetCallback) (
  void *cls,
  const struct TALER_MERCHANT_HttpResponse *hr,
  enum GNUNET_GenericReturnValue paid,
  enum GNUNET_GenericReturnValue refunded,
  enum GNUNET_GenericReturnValue refund_pending,
  struct TALER_Amount *refund_amount,
  const char *taler_pay_uri,
  const char *already_paid_order_id);


/**
 * Checks the status of a payment.  Issue a GET /orders/$ID request to the
 * backend.  The @a h_contract serves as identification of the wallet and is
 * used to authorize the request.
 *
 * @param ctx execution context
 * @param backend_url base URL of the merchant backend
 * @param order_id order id to identify the payment
 * @param h_contract hash of the contract to authenticate the wallet
 * @param timeout timeout to use in long polling (how long may the server wait to reply
 *        before generating an unpaid response). Note that this is just provided to
 *        the server, we as client will block until the response comes back or until
 *        #TALER_MERCHANT_wallet_order_get_cancel() is called.
 * @param session_id for which session should the payment status be checked. Use
 *        NULL to disregard sessions.
 * @param min_refund long poll for the service to approve a refund exceeding this value;
 *        use NULL to not wait for any refund (only for payment). Only makes sense
 *        with a non-zero @a timeout. Can be NULL.
 * @param await_refund_obtained long poll for the order's refunds to be
 *        picked up by the wallet.
 * @param cb callback which will work the response gotten from the backend
 * @param cb_cls closure to pass to @a cb
 * @return handle for this operation, NULL upon errors
 */
struct TALER_MERCHANT_OrderWalletGetHandle *
TALER_MERCHANT_wallet_order_get (struct GNUNET_CURL_Context *ctx,
                                 const char *backend_url,
                                 const char *order_id,
                                 const struct
                                 TALER_PrivateContractHash *h_contract,
                                 struct GNUNET_TIME_Relative timeout,
                                 const char *session_id,
                                 const struct TALER_Amount *min_refund,
                                 bool await_refund_obtained,
                                 TALER_MERCHANT_OrderWalletGetCallback cb,
                                 void *cb_cls);

/**
 * Cancel a GET /orders/$ID request.
 *
 * @param owgh handle to the request to be canceled
 */
void
TALER_MERCHANT_wallet_order_get_cancel (
  struct TALER_MERCHANT_OrderWalletGetHandle *owgh);


/**
 * Handle for a GET /private/orders/$ID operation (merchant's internal
 * API, not to be confused with the endpoint for wallets).
 */
struct TALER_MERCHANT_OrderMerchantGetHandle;


/**
 * Details about a wire transfer to the merchant related to the order.
 */
struct TALER_MERCHANT_WireTransfer
{
  /**
   * Base URL of the exchange that made the transfer.
   */
  const char *exchange_url;

  /**
   * When the transfer took place (note: may not be exact,
   * as the time at which the exchange initiated the transfer
   * may differ from the time the bank or the merchant observed).
   */
  struct GNUNET_TIME_Timestamp execution_time;

  /**
   * Wire transfer subject.
   */
  struct TALER_WireTransferIdentifierRawP wtid;

  /**
   * Total amount that was transferred.
   */
  struct TALER_Amount total_amount;

  /**
   * Whether this transfer was confirmed by the merchant using
   * the POST /transfers API, or whether it was merely claimed
   * by the exchange.
   */
  bool confirmed;
};


/**
 * Details about a failures returned by the exchange when
 * tracking wire transfers.
 */
struct TALER_MERCHANT_WireReport
{

  /**
   * Error code explaining the nature of the problem.
   */
  enum TALER_ErrorCode code;

  /**
   * Human-readable error description.
   */
  const char *hint;

  /**
   * Public key of the coin involved.
   */
  struct TALER_CoinSpendPublicKeyP coin_pub;

  /**
   * HTTP response data from the exchange (fields are MAY BE NULL or 0 if not
   * available).
   */
  struct TALER_EXCHANGE_HttpResponse hr;

};


/**
 * Detail returned by the merchant backend about refunds.
 */
struct TALER_MERCHANT_RefundOrderDetail
{

  /**
   * Human-readable reason given for the refund.
   */
  const char *reason;

  /**
   * Time when the refund was granted.
   */
  struct GNUNET_TIME_Timestamp refund_time;

  /**
   * Total amount that was refunded.
   */
  struct TALER_Amount refund_amount;

};


/**
 * Status of an order.
 */
enum TALER_MERCHANT_OrderStatusCode
{
  /**
   * The order was paid.
   */
  TALER_MERCHANT_OSC_PAID = 1,

  /**
  * The order was claimed but not yet paid.
  */
  TALER_MERCHANT_OSC_CLAIMED = 2,

  /**
   * The order was never paid or claimed.
   */
  TALER_MERCHANT_OSC_UNPAID = 3
};


/**
 * Details about the status of an order.
 */
struct TALER_MERCHANT_OrderStatusResponse
{

  /**
   * Status of the order.
   */
  enum TALER_MERCHANT_OrderStatusCode status;

  /**
   * Details depending on the payment status given in @e status.
   */
  union
  {

    /**
     * Details provided if @e status is #TALER_MERCHANT_OSC_PAID.
     */
    struct
    {
      /**
       * Amount that was refunded.
       */
      struct TALER_Amount refund_amount;

      /**
       * Amount that was deposited into our bank account,
       * excluding fees.
       */
      struct TALER_Amount deposit_total;

      /**
       * The full contract terms of the order.
       */
      const json_t *contract_terms;

      /**
       * Array of wire transfers made for this payment to the
       * merchant by the exchange. Of length @e wts_len.
       */
      struct TALER_MERCHANT_WireTransfer *wts;

      /**
       * Length of the @e wts array.
       */
      unsigned int wts_len;

      /**
       * Array of wire reports about problems tracking wire transfers.
       * Of length @e wrs_len.
       */
      struct TALER_MERCHANT_WireReport *wrs;

      /**
       * Length of the @e wrs array.
       */
      unsigned int wrs_len;

      /**
       * Details returned by the merchant backend about refunds.
       * Of length @e refunds_len.
       */
      struct TALER_MERCHANT_RefundOrderDetail *refunds;

      /**
       * Length of the @e refunds array.
       */
      unsigned int refunds_len;

      /**
       * Error code encountered trying to contact the exchange
       * about the wire tracking, 0 for none.
       */
      enum TALER_ErrorCode exchange_ec;

      /**
       * HTTP status code encountered trying to contact the exchange
       * about the wire tracking, 0 for no error.
       */
      unsigned int exchange_hc;

      /**
       * true if there is at least on refund on this payment,
       * false if there are no refunds.
       */
      bool refunded;

      /**
       * true if refunds were approved that have not yet been obtained
       * by the wallet.
       */
      bool refund_pending;

      /**
       * true if the exchange paid the merchant for this order,
       * false if not.
       */
      bool wired;

    } paid;

    /**
     * Details provided if @e status is #TALER_MERCHANT_OSC_CLAIMED.
     */
    struct
    {

      /**
       * The full contract terms of the claimed order (including client nonce from claiming).
       */
      const json_t *contract_terms;

    } claimed;

    /**
     * Details provided if @e status is #TALER_MERCHANT_OSC_UNPAID.
     */
    struct
    {

      /**
       * URI that should be shown to the wallet to trigger a payment.
       */
      const char *taler_pay_uri;

      /**
       * Alternative order ID which was paid for already in the same session.
       * Only given if the same product was purchased before in the same session.
       * Otherwise NULL.
       */
      const char *already_paid_order_id;

      /**
       * Order summary.
       */
      const char *summary;

      /**
       * Time when the order was created.
       */
      struct GNUNET_TIME_Timestamp creation_time;

      /**
       * Total amount the order is about (amount to be paid by customer).
       */
      struct TALER_Amount contract_amount;

    } unpaid;

  } details;
};


/**
 * Callback to process a GET /orders/$ID request
 *
 * @param cls closure
 * @param hr HTTP response details
 * @param osr order status response details (on success)
 */
typedef void
(*TALER_MERCHANT_OrderMerchantGetCallback) (
  void *cls,
  const struct TALER_MERCHANT_HttpResponse *hr,
  const struct TALER_MERCHANT_OrderStatusResponse *osr);


/**
 * Checks the status of a payment.  Issue a GET /private/orders/$ID request to
 * the backend.
 *
 * @param ctx execution context
 * @param backend_url base URL of the merchant backend
 * @param order_id order id to identify the payment
 * @param session_id sesion id for the payment (or NULL if the check is not
 *                   bound to a session)
 * @param transfer if true, obtain the wire transfer status from the exhcange.
 *               Otherwise, the wire transfer status MAY be returned if it is available.
 * @param timeout timeout to use in long polling (how long may the server wait to reply
 *        before generating an unpaid response). Note that this is just provided to
 *        the server, we as client will block until the response comes back or until
 *        #TALER_MERCHANT_merchant_order_get_cancel() is called.
 * @param cb callback which will work the response gotten from the backend
 * @param cb_cls closure to pass to @a cb
 * @return handle for this operation, NULL upon errors
 */
struct TALER_MERCHANT_OrderMerchantGetHandle *
TALER_MERCHANT_merchant_order_get (struct GNUNET_CURL_Context *ctx,
                                   const char *backend_url,
                                   const char *order_id,
                                   const char *session_id,
                                   bool transfer,
                                   struct GNUNET_TIME_Relative timeout,
                                   TALER_MERCHANT_OrderMerchantGetCallback cb,
                                   void *cb_cls);


/**
 * Cancel a GET /private/orders/$ID request.
 *
 * @param omgh handle to the request to be canceled
 */
void
TALER_MERCHANT_merchant_order_get_cancel (
  struct TALER_MERCHANT_OrderMerchantGetHandle *omgh);


/**
 * Handle for a DELETE /orders/$ID operation.
 */
struct TALER_MERCHANT_OrderDeleteHandle;


/**
 * Function called with the result of the DELETE /orders/$ID operation.
 *
 * @param cls closure
 * @param hr HTTP response details
 */
typedef void
(*TALER_MERCHANT_OrderDeleteCallback)(
  void *cls,
  const struct TALER_MERCHANT_HttpResponse *hr);


/**
 * Make a DELETE /orders/$ID request to delete a order from our
 * inventory.
 *
 * @param ctx the context
 * @param backend_url HTTP base URL for the backend
 * @param order_id identifier of the order
 * @param cb function to call with the backend's deletion status
 * @param cb_cls closure for @a cb
 * @return the request handle; NULL upon error
 */
struct TALER_MERCHANT_OrderDeleteHandle *
TALER_MERCHANT_order_delete (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  const char *order_id,
  TALER_MERCHANT_OrderDeleteCallback cb,
  void *cb_cls);


/**
 * Cancel DELETE /orders/$ID operation.
 *
 * @param odh operation to cancel
 */
void
TALER_MERCHANT_order_delete_cancel (
  struct TALER_MERCHANT_OrderDeleteHandle *odh);


/**
 * Handle to a POST /orders/$ID/claim handle
 */
struct TALER_MERCHANT_OrderClaimHandle;


/**
 * Callback called to process a POST /orders/$ID/claim response.
 *
 * @param cls closure
 * @param hr HTTP response details
 * @param contract_terms the details of the contract
 * @param sig merchant's signature over @a contract_terms (already verified)
 * @param h_contract_terms hash over @a contract_terms (computed
 *        client-side to verify @a sig)
 */
typedef void
(*TALER_MERCHANT_OrderClaimCallback) (
  void *cls,
  const struct TALER_MERCHANT_HttpResponse *hr,
  const json_t *contract_terms,
  const struct TALER_MerchantSignatureP *sig,
  const struct TALER_PrivateContractHash *h_contract_terms);


/**
 * Calls the POST /orders/$ID/claim API at the backend.  That is,
 * retrieve the final contract terms including the client nonce.
 *
 * This is a PUBLIC API for wallets.
 *
 * @param ctx execution context
 * @param backend_url base URL of the merchant backend
 * @param order_id order id used to perform the lookup
 * @param nonce nonce to use to claim the proposal
 * @param claim_token the token used to verify the claim (NULL for none)
 * @param cb callback which will work the response gotten from the backend
 * @param cb_cls closure to pass to @a cb
 * @return handle for this handle, NULL upon errors
 */
struct TALER_MERCHANT_OrderClaimHandle *
TALER_MERCHANT_order_claim (struct GNUNET_CURL_Context *ctx,
                            const char *backend_url,
                            const char *order_id,
                            const struct GNUNET_CRYPTO_EddsaPublicKey *nonce,
                            const struct TALER_ClaimTokenP *claim_token,
                            TALER_MERCHANT_OrderClaimCallback cb,
                            void *cb_cls);


/**
 * Cancel a POST /order/$ID/claim request.
 *
 * @param och handle to the request to be canceled
 */
void
TALER_MERCHANT_order_claim_cancel (struct TALER_MERCHANT_OrderClaimHandle *och);


/**
 * @brief Handle to a POST /orders/$ID/pay operation at a merchant.  Note that
 * we use the same handle for interactions with frontends (API for wallets) or
 * backends (API for frontends).  The difference is that for the frontend API,
 * we need the private keys of the coins, while for the backend API we need
 * the public keys and signatures received from the wallet.
 */
struct TALER_MERCHANT_OrderPayHandle;


/**
 * Callbacks of this type are used to serve the result of submitting a
 * POST /orders/$ID/pay request to a merchant.
 *
 * @param cls closure
 * @param hr HTTP response details
 * @param merchant_sig signature from the merchant
 *        affirming payment, or NULL on errors
 */
typedef void
(*TALER_MERCHANT_OrderPayCallback) (
  void *cls,
  const struct TALER_MERCHANT_HttpResponse *hr,
  const struct TALER_MerchantSignatureP *merchant_sig);


/**
 * Information we need from the frontend (ok, the frontend sends just JSON)
 * when forwarding a payment to the backend.
 */
struct TALER_MERCHANT_PaidCoin
{
  /**
   * Denomination key with which the coin is signed
   */
  struct TALER_DenominationPublicKey denom_pub;

  /**
   * Exchange’s unblinded signature of the coin
   */
  struct TALER_DenominationSignature denom_sig;

  /**
   * Overall value that coins of this @e denom_pub have.
   */
  struct TALER_Amount denom_value;

  /**
   * Coin's public key.
   */
  struct TALER_CoinSpendPublicKeyP coin_pub;

  /**
   * Coin's signature key.
   */
  struct TALER_CoinSpendSignatureP coin_sig;

  /**
   * Amount this coin contributes to (including fee).
   */
  struct TALER_Amount amount_with_fee;

  /**
   * Amount this coin contributes to (without fee).
   */
  struct TALER_Amount amount_without_fee;

  /**
   * What is the URL of the exchange that issued @a coin_pub?
   */
  const char *exchange_url;

};


/**
 * Pay a merchant.  API for frontends talking to backends. Here,
 * the frontend does not have the coin's private keys, but just
 * the public keys and signatures.
 *
 * This is a PUBLIC API, albeit in this form useful for the frontend,
 * in case the frontend is proxying the request.
 *
 * @param ctx execution context
 * @param merchant_url base URL of the merchant
 * @param order_id which order should be paid
 * @param session_id session to pay for, or NULL for none
 * @param num_coins length of the @a coins array
 * @param coins array of coins to pay with
 * @param pay_cb the callback to call when a reply for this request is available
 * @param pay_cb_cls closure for @a pay_cb
 * @return a handle for this request
 */
struct TALER_MERCHANT_OrderPayHandle *
TALER_MERCHANT_order_pay_frontend (
  struct GNUNET_CURL_Context *ctx,
  const char *merchant_url,
  const char *order_id,
  const char *session_id,
  unsigned int num_coins,
  const struct TALER_MERCHANT_PaidCoin coins[],
  TALER_MERCHANT_OrderPayCallback pay_cb,
  void *pay_cb_cls);


/**
 * Information we need from the wallet for each coin when doing the
 * payment.
 */
struct TALER_MERCHANT_PayCoin
{
  /**
   * Denomination key with which the coin is signed
   */
  struct TALER_DenominationPublicKey denom_pub;

  /**
   * Exchange’s unblinded signature of the coin
   */
  struct TALER_DenominationSignature denom_sig;

  /**
   * Overall value that coins of this @e denom_pub have.
   */
  struct TALER_Amount denom_value;

  /**
   * Coin's private key.
   */
  struct TALER_CoinSpendPrivateKeyP coin_priv;

  /**
   * Amount this coin contributes to (including fee).
   */
  struct TALER_Amount amount_with_fee;

  /**
   * Amount this coin contributes to (without fee).
   */
  struct TALER_Amount amount_without_fee;

  /**
   * URL of the exchange that issued @e coin_priv.
   */
  const char *exchange_url;

};


/**
 * Pay a merchant.  API for wallets that have the coin's private keys.
 *
 * This is a PUBLIC API for wallets.
 *
 * @param ctx execution context
 * @param merchant_url base URL of the merchant
 * @param session_id session to pay for, or NULL for none
 * @param h_contract hash of the contact of the merchant with the customer
 * @param amount total value of the contract to be paid to the merchant
 * @param max_fee maximum fee covered by the merchant (according to the contract)
 * @param merchant_pub the public key of the merchant (used to identify the merchant for refund requests)
 * @param merchant_sig signature from the merchant over the original contract
 * @param timestamp timestamp when the contract was finalized, must match approximately the current time of the merchant
 * @param refund_deadline date until which the merchant can issue a refund to the customer via the merchant (can be zero if refunds are not allowed)
 * @param pay_deadline maximum time limit to pay for this contract
 * @param h_wire hash of the merchant’s account details
 * @param order_id order id
 * @param num_coins number of coins used to pay
 * @param coins array of coins we use to pay
 * @param pay_cb the callback to call when a reply for this request is available
 * @param pay_cb_cls closure for @a pay_cb
 * @return a handle for this request
 */
struct TALER_MERCHANT_OrderPayHandle *
TALER_MERCHANT_order_pay (struct GNUNET_CURL_Context *ctx,
                          const char *merchant_url,
                          const char *session_id,
                          const struct TALER_PrivateContractHash *h_contract,
                          const struct TALER_Amount *amount,
                          const struct TALER_Amount *max_fee,
                          const struct TALER_MerchantPublicKeyP *merchant_pub,
                          const struct TALER_MerchantSignatureP *merchant_sig,
                          struct GNUNET_TIME_Timestamp timestamp,
                          struct GNUNET_TIME_Timestamp refund_deadline,
                          struct GNUNET_TIME_Timestamp pay_deadline,
                          const struct TALER_MerchantWireHash *h_wire,
                          const char *order_id,
                          unsigned int num_coins,
                          const struct TALER_MERCHANT_PayCoin coins[],
                          TALER_MERCHANT_OrderPayCallback pay_cb,
                          void *pay_cb_cls);


/**
 * Cancel a POST /orders/$ID/pay request.  Note that if you cancel a request
 * like this, you have no assurance that the request has not yet been
 * forwarded to the merchant. Thus, the payment may still succeed or fail.
 * Re-issue the original /pay request to resume/retry and obtain a definitive
 * result, or refresh the coins involved to ensure that the merchant can no
 * longer complete the payment.
 *
 * @param oph the payment request handle
 */
void
TALER_MERCHANT_order_pay_cancel (struct TALER_MERCHANT_OrderPayHandle *oph);


/**
 * @brief Handle to a POST /orders/$ID/paid operation at a merchant.
 */
struct TALER_MERCHANT_OrderPaidHandle;


/**
 * Callbacks of this type are used to serve the result of submitting a
 * POST /orders/$ID/paid request to a merchant.
 *
 * @param cls closure
 * @param hr HTTP response details
 */
typedef void
(*TALER_MERCHANT_OrderPaidCallback) (
  void *cls,
  const struct TALER_MERCHANT_HttpResponse *hr);


/**
 * Send proof of payment to a merchant.
 *
 * This is a PUBLIC API, albeit in this form useful for the frontend,
 * in case the frontend is proxying the request.
 *
 * @param ctx execution context
 * @param merchant_url base URL of the merchant
 * @param order_id which order should be paid
 * @param session_id session to pay for, or NULL for none
 * @param h_contract_terms hash of the contract terms
 * @param merchant_sig signature from the merchant
 *        affirming payment, or NULL on errors
 * @param paid_cb the callback to call when a reply for this request is available
 * @param paid_cb_cls closure for @a paid_cb
 * @return a handle for this request
 */
struct TALER_MERCHANT_OrderPaidHandle *
TALER_MERCHANT_order_paid (
  struct GNUNET_CURL_Context *ctx,
  const char *merchant_url,
  const char *order_id,
  const char *session_id,
  const struct TALER_PrivateContractHash *h_contract_terms,
  const struct TALER_MerchantSignatureP *merchant_sig,
  TALER_MERCHANT_OrderPaidCallback paid_cb,
  void *paid_cb_cls);


/**
 * Cancel POST /orders/$ID/paid operation.
 *
 * @param oph operation to cancel
 */
void
TALER_MERCHANT_order_paid_cancel (
  struct TALER_MERCHANT_OrderPaidHandle *oph);


/**
 * Handle for an POST /orders/$ID/abort operation.
 */
struct TALER_MERCHANT_OrderAbortHandle;


/**
 * Entry in the array of refunded coins.
 */
struct TALER_MERCHANT_AbortedCoin
{
  /**
   * Exchange signature affirming the refund.
   */
  struct TALER_ExchangeSignatureP exchange_sig;

  /**
   * Exchange public key affirming the refund.
   */
  struct TALER_ExchangePublicKeyP exchange_pub;
};


/**
 * Callbacks of this type are used to serve the result of submitting a
 * /orders/$ID/abort request to a merchant.
 *
 * @param cls closure
 * @param hr HTTP response details
 * @param merchant_pub public key of the merchant
 * @param num_aborts size of the @a res array, 0 on errors
 * @param aborts merchant signatures refunding coins, NULL on errors
 */
typedef void
(*TALER_MERCHANT_AbortCallback) (
  void *cls,
  const struct TALER_MERCHANT_HttpResponse *hr,
  const struct TALER_MerchantPublicKeyP *merchant_pub,
  unsigned int num_aborts,
  const struct TALER_MERCHANT_AbortedCoin aborts[]);


/**
 * Information we need from the wallet for each coin when aborting.
 */
struct TALER_MERCHANT_AbortCoin
{

  /**
   * Coin's public key.
   */
  struct TALER_CoinSpendPublicKeyP coin_pub;

  /**
   * Amount this coin contributes to (including fee).
   */
  struct TALER_Amount amount_with_fee;

  /**
   * URL of the exchange that issued @e coin_priv.
   */
  const char *exchange_url;

};

/**
 * Run a payment abort operation, asking for the payment to be aborted,
 * yieldingrefunds for coins that were previously spend on a payment that
 * failed to go through.
 *
 * This is a PUBLIC API for wallets.
 *
 * @param ctx execution context
 * @param merchant_url base URL of the merchant
 * @param order_id order to abort
 * @param h_contract hash of the contact of the merchant with the customer
 * @param merchant_pub the public key of the merchant (used to identify the merchant for refund requests)
 * @param num_coins number of coins used to pay
 * @param coins array of coins we use to pay
 * @param cb the callback to call when a reply for this request is available
 * @param cb_cls closure for @a cb
 * @return a handle for this request
 */
struct TALER_MERCHANT_OrderAbortHandle *
TALER_MERCHANT_order_abort (struct GNUNET_CURL_Context *ctx,
                            const char *merchant_url,
                            const char *order_id,
                            const struct TALER_MerchantPublicKeyP *merchant_pub,
                            const struct TALER_PrivateContractHash *h_contract,
                            unsigned int num_coins,
                            const struct TALER_MERCHANT_AbortCoin coins[],
                            TALER_MERCHANT_AbortCallback cb,
                            void *cb_cls);


/**
 * Cancel a POST /orders/$ID/abort request.  Note that if you cancel a request
 * like this, you have no assurance that the request has not yet been
 * forwarded to the merchant.
 *
 * @param oah the abort request handle
 */
void
TALER_MERCHANT_order_abort_cancel (struct TALER_MERCHANT_OrderAbortHandle *oah);


/**
 * Handle for an PATCH /orders/$ID/forget operation.
 */
struct TALER_MERCHANT_OrderForgetHandle;


/**
 * Callbacks of this type are used to serve the result of submitting a
 * /orders/$ID/forget request to a merchant.
 *
 * @param cls closure
 * @param hr HTTP response details
 */
typedef void
(*TALER_MERCHANT_ForgetCallback) (
  void *cls,
  const struct TALER_MERCHANT_HttpResponse *hr);


/**
 * Run an order forget operation, which forgets certain fields in an order's
 * contract terms without changing the hash of the contract terms.
 *
 * @param ctx execution context.
 * @param merchant_url base URL of the merchant.
 * @param order_id order to forget.
 * @param fields_length length of @e fields.
 * @param fields the fields in the contract terms to forget.
 * @param cb the callback to call when a reply for this request is available.
 * @param cb_cls closure for @a cb.
 * @return a handle for this request.
 */
struct TALER_MERCHANT_OrderForgetHandle *
TALER_MERCHANT_order_forget (struct GNUNET_CURL_Context *ctx,
                             const char *merchant_url,
                             const char *order_id,
                             unsigned int fields_length,
                             const char *fields[],
                             TALER_MERCHANT_ForgetCallback cb,
                             void *cb_cls);


/**
 * Cancel a PATCH /orders/$ID/forget request.  Note that if you cancel a request
 * like this, you have no assurance that the request has not yet been
 * forwarded to the merchant.
 *
 * @param ofh the forget request handle
 */
void
TALER_MERCHANT_order_forget_cancel (struct
                                    TALER_MERCHANT_OrderForgetHandle *ofh);


/**
 * Handle for a POST /orders/ID/refund operation.
 */
struct TALER_MERCHANT_OrderRefundHandle;


/**
 * Callback to process a POST /orders/ID/refund request
 *
 * @param cls closure
 * @param hr HTTP response details this request
 * @param taler_refund_uri the refund uri offered to the wallet
 * @param h_contract hash of the contract a Browser may need to authorize
 *        obtaining the HTTP response.
 */
typedef void
(*TALER_MERCHANT_RefundCallback) (
  void *cls,
  const struct TALER_MERCHANT_HttpResponse *hr,
  const char *taler_refund_uri,
  const struct TALER_PrivateContractHash *h_contract);


/**
 * Increase the refund granted for an order
 *
 * @param ctx the CURL context used to connect to the backend
 * @param backend_url backend's base URL, including final "/"
 * @param order_id id of the order whose refund is to be increased
 * @param refund amount to which increase the refund
 * @param reason human-readable reason justifying the refund
 * @param cb callback processing the response from /refund
 * @param cb_cls closure for cb
 */
struct TALER_MERCHANT_OrderRefundHandle *
TALER_MERCHANT_post_order_refund (struct GNUNET_CURL_Context *ctx,
                                  const char *backend_url,
                                  const char *order_id,
                                  const struct TALER_Amount *refund,
                                  const char *reason,
                                  TALER_MERCHANT_RefundCallback cb,
                                  void *cb_cls);

/**
 * Cancel a POST /refund request.
 *
 * @param orh the refund increasing operation to cancel
 */
void
TALER_MERCHANT_post_order_refund_cancel (
  struct TALER_MERCHANT_OrderRefundHandle *orh);


/**
 * Handle for a (public) POST /orders/ID/refund operation.
 */
struct TALER_MERCHANT_WalletOrderRefundHandle;


/**
 * Detail about a refund lookup result.
 */
struct TALER_MERCHANT_RefundDetail
{

  /**
   * Exchange response details.  Full details are only included
   * upon failure (HTTP status is not #MHD_HTTP_OK).
   */
  struct TALER_EXCHANGE_HttpResponse hr;

  /**
   * Coin this detail is about.
   */
  struct TALER_CoinSpendPublicKeyP coin_pub;

  /**
   * Refund transaction ID used.
   */
  uint64_t rtransaction_id;

  /**
   * Amount to be refunded for this coin.
   */
  struct TALER_Amount refund_amount;

  /**
   * Public key of the exchange affirming the refund,
   * only valid if the @e hr http_status is #MHD_HTTP_OK.
   */
  struct TALER_ExchangePublicKeyP exchange_pub;

  /**
   * Signature of the exchange affirming the refund,
   * only valid if the @e hr http_status is #MHD_HTTP_OK.
   */
  struct TALER_ExchangeSignatureP exchange_sig;

};


/**
 * Callback to process a (public) POST /orders/ID/refund request
 *
 * @param cls closure
 * @param hr HTTP response details
 * @param refund_amount what is the total amount of the refund that was granted
 * @param merchant_pub public key of the merchant signing the @a refunds
 * @param refunds array with details about the refunds obtained
 * @param refunds_length length of the @a refunds array
 */
typedef void
(*TALER_MERCHANT_WalletRefundCallback) (
  void *cls,
  const struct TALER_MERCHANT_HttpResponse *hr,
  const struct TALER_Amount *refund_amount,
  const struct TALER_MerchantPublicKeyP *merchant_pub,
  struct TALER_MERCHANT_RefundDetail refunds[],
  unsigned int refunds_length);


/**
 * Obtain the refunds that have been granted for an order.
 *
 * @param ctx the CURL context used to connect to the backend
 * @param backend_url backend's base URL, including final "/"
 * @param order_id id of the order whose refund is to be increased
 * @param h_contract_terms hash of the contract terms of the order
 * @param cb callback processing the response from /refund
 * @param cb_cls closure for cb
 */
struct TALER_MERCHANT_WalletOrderRefundHandle *
TALER_MERCHANT_wallet_post_order_refund (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  const char *order_id,
  const struct TALER_PrivateContractHash *h_contract_terms,
  TALER_MERCHANT_WalletRefundCallback cb,
  void *cb_cls);

/**
 * Cancel a (public) POST /refund request.
 *
 * @param orh the refund operation to cancel
 */
void
TALER_MERCHANT_wallet_post_order_refund_cancel (
  struct TALER_MERCHANT_WalletOrderRefundHandle *orh);


/* ********************* /transfers *********************** */

/**
 * @brief Handle to a POST /transfers operation at a merchant's backend.
 */
struct TALER_MERCHANT_PostTransfersHandle;

/**
 * Information about the _total_ amount that was paid back
 * by the exchange for a given h_contract_terms, by _one_ wire
 * transfer.
 */
struct TALER_MERCHANT_TrackTransferDetail
{

  /**
   * Total amount paid back by the exchange.
   */
  struct TALER_Amount deposit_value;

  /**
   * Total amount of deposit fees.
   */
  struct TALER_Amount deposit_fee;

  /**
   * Order ID associated whit this payment.
   */
  const char *order_id;

};

/**
 * Callbacks of this type are used to work the result of submitting a
 * POST /transfers request to a merchant
 *
 * @param cls closure
 * @param hr HTTP response details
 * @param execution_time when did the transfer happen (according to the exchange),
 *          #GNUNET_TIME_UNIT_FOREVER_ABS if the transfer did not yet happen or if
 *          we have no data from the exchange about it
 * @param total_amount total amount of the wire transfer, or NULL if the exchange did
 *             not provide any details
 * @param wire_fee how much did the exchange charge in terms of wire fees, or NULL
 *             if the exchange did not provide any details
 * @param details_length length of the @a details array
 * @param details array with details about the combined transactions
 */
typedef void
(*TALER_MERCHANT_PostTransfersCallback) (
  void *cls,
  const struct TALER_MERCHANT_HttpResponse *hr,
  struct GNUNET_TIME_Timestamp execution_time,
  const struct TALER_Amount *total_amount,
  const struct TALER_Amount *wire_fee,
  unsigned int details_length,
  const struct TALER_MERCHANT_TrackTransferDetail details[]);


/**
 * Request backend to remember that we received the given
 * wire transfer and to request details about the aggregated
 * transactions from the exchange.
 *
 * @param ctx execution context
 * @param backend_url base URL of the backend
 * @param credit_amount how much money did we receive (without wire fee)
 * @param wtid base32 string indicating a wtid
 * @param payto_uri which account was credited by the wire transfer
 * @param exchange_url what is the URL of the exchange that made the transfer
 * @param cb the callback to call when a reply for this request is available
 * @param cls closure for @a cb
 * @return a handle for this request
 */
struct TALER_MERCHANT_PostTransfersHandle *
TALER_MERCHANT_transfers_post (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  const struct TALER_Amount *credit_amount,
  const struct TALER_WireTransferIdentifierRawP *wtid,
  const char *payto_uri,
  const char *exchange_url,
  TALER_MERCHANT_PostTransfersCallback cb,
  void *cls);


/**
 * Cancel a POST /transfers request.  This function cannot be used
 * on a request handle if a response is already served for it.
 *
 * @param pth the operation to cancel
 */
void
TALER_MERCHANT_transfers_post_cancel (
  struct TALER_MERCHANT_PostTransfersHandle *pth);


/**
 * Handle for a DELETE /transfers/ID operation.
 */
struct TALER_MERCHANT_TransferDeleteHandle;


/**
 * Function called with the result of the DELETE /transfers/$ID operation.
 *
 * @param cls closure
 * @param hr HTTP response details
 */
typedef void
(*TALER_MERCHANT_TransferDeleteCallback)(
  void *cls,
  const struct TALER_MERCHANT_HttpResponse *hr);


/**
 * Delete an incoming wire transfer at an instance of a backend. This
 * will only succeed if the exchange did not respond to the inquiry
 * about the transfer.
 *
 * @param ctx the context
 * @param backend_url HTTP base URL for the backend (top-level "default" instance
 *                    or base URL of an instance if @a instance_id is NULL)
 * @param wire_transfer_serial unique number that identifies the transfer
 * @param td_cb function to call with the backend's result
 * @param td_cb_cls closure for @a td_cb
 * @return the request handle; NULL upon error
 */
struct TALER_MERCHANT_TransferDeleteHandle *
TALER_MERCHANT_transfer_delete (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  uint64_t wire_transfer_serial,
  TALER_MERCHANT_TransferDeleteCallback td_cb,
  void *td_cb_cls);


/**
 * Cancel deletion operation. Note that the operation may still
 * succeed.
 *
 * @param tdh operation to cancel
 */
void
TALER_MERCHANT_transfer_delete_cancel (
  struct TALER_MERCHANT_TransferDeleteHandle *tdh);


/**
 * @brief Handle to a GET /transfers operation at a merchant's backend.
 */
struct TALER_MERCHANT_GetTransfersHandle;

/**
 * Information about the _total_ amount that was paid back
 * by the exchange for a given h_contract_terms, by _one_ wire
 * transfer.
 */
struct TALER_MERCHANT_TransferData
{

  /**
   * Total amount wired by the exchange (without wire fee)
   */
  struct TALER_Amount credit_amount;

  /**
   * Wire transfer identifier used.
   */
  struct TALER_WireTransferIdentifierRawP wtid;

  /**
   * URI of the target account.
   */
  const char *payto_uri;

  /**
   * URL of the exchange that made the transfer.
   */
  const char *exchange_url;

  /**
   * Serial number of the credit operation in the merchant backend.
   * Needed, for example, to delete the transfer using
   * #TALER_MERCHANT_transfer_delete().
   */
  uint64_t credit_serial;

  /**
   * Time of the wire transfer, according to the exchange.
   * 0 for not provided by the exchange.
   */
  struct GNUNET_TIME_Timestamp execution_time;

  /**
   * Did we check the exchange's answer and are happy about it?  False if we
   * did not check or are unhappy with the answer.
   */
  bool verified;

  /**
   * Did we confirm the wire transfer happened (via
   * #TALER_MERCHANT_transfers_post())?
   */
  bool confirmed;

};

/**
 * Callbacks of this type are used to work the result of submitting a
 * GET /transfers request to a merchant
 *
 * @param cls closure
 * @param hr HTTP response details
 * @param transfers_length length of the @a transfers array
 * @param transfers array with details about the transfers we received
 */
typedef void
(*TALER_MERCHANT_GetTransfersCallback) (
  void *cls,
  const struct TALER_MERCHANT_HttpResponse *hr,
  unsigned int transfers_length,
  const struct TALER_MERCHANT_TransferData transfers[]);


/**
 * Request backend to return list of all wire transfers that
 * we received (or that the exchange claims we should have received).
 *
 * Note that when filtering by timestamp (using “before” and/or “after”), we
 * use the time reported by the exchange and thus will ONLY return results for
 * which we already have a response from the exchange. This should be
 * virtually all transfers, however it is conceivable that for some transfer
 * the exchange responded with a temporary error (i.e. HTTP status 500+) and
 * then we do not yet have an execution time to filter by. Thus, IF timestamp
 * filters are given, transfers for which we have no response from the
 * exchange yet are automatically excluded.
 *
 * @param ctx execution context
 * @param backend_url base URL of the backend
 * @param payto_uri filter by this credit account of the merchant
 * @param before limit to transactions before this timestamp, use
 *                 #GNUNET_TIME_UNIT_FOREVER_ABS to not filter by @a before
 * @param after limit to transactions after this timestamp, use
 *                 #GNUNET_TIME_UNIT_ZERO_ABS to not filter by @a after
 * @param limit return at most this number of results; negative to descend in execution time
 * @param offset start at this "credit serial" number (exclusive)
 * @param verified filter results by verification status
 * @param cb the callback to call when a reply for this request is available
 * @param cb_cls closure for @a cb
 * @return a handle for this request
 */
struct TALER_MERCHANT_GetTransfersHandle *
TALER_MERCHANT_transfers_get (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  const char *payto_uri,
  const struct GNUNET_TIME_Timestamp before,
  const struct GNUNET_TIME_Timestamp after,
  int64_t limit,
  uint64_t offset,
  enum TALER_EXCHANGE_YesNoAll verified,
  TALER_MERCHANT_GetTransfersCallback cb,
  void *cb_cls);


/**
 * Cancel a POST /transfers request.  This function cannot be used
 * on a request handle if a response is already served for it.
 *
 * @param gth the operation to cancel
 */
void
TALER_MERCHANT_transfers_get_cancel (
  struct TALER_MERCHANT_GetTransfersHandle *gth);


/* ******************* /reserves *************** */


/**
 * @brief Handle to a POST /reserves operation at a merchant's backend.
 */
struct TALER_MERCHANT_PostReservesHandle;


/**
 * Callbacks of this type are used to work the result of submitting a
 * POST /reserves request to a merchant
 *
 * @param cls closure
 * @param hr HTTP response details
 * @param reserve_pub public key of the created reserve, NULL on error
 * @param payto_uri where to make the payment to for filling the reserve, NULL on error
 */
typedef void
(*TALER_MERCHANT_PostReservesCallback) (
  void *cls,
  const struct TALER_MERCHANT_HttpResponse *hr,
  const struct TALER_ReservePublicKeyP *reserve_pub,
  const char *payto_uri);


/**
 * Request backend to create a reserve.
 *
 * @param ctx execution context
 * @param backend_url base URL of the backend
 * @param initial_balance desired initial balance for the reserve
 * @param exchange_url what is the URL of the exchange where the reserve should be set up
 * @param wire_method desired wire method, for example "iban" or "x-taler-bank"
 * @param cb the callback to call when a reply for this request is available
 * @param cb_cls closure for @a cb
 * @return a handle for this request
 */
struct TALER_MERCHANT_PostReservesHandle *
TALER_MERCHANT_reserves_post (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  const struct TALER_Amount *initial_balance,
  const char *exchange_url,
  const char *wire_method,
  TALER_MERCHANT_PostReservesCallback cb,
  void *cb_cls);


/**
 * Cancel a POST /reserves request.  This function cannot be used
 * on a request handle if a response is already served for it.
 *
 * @param prh the operation to cancel
 */
void
TALER_MERCHANT_reserves_post_cancel (
  struct TALER_MERCHANT_PostReservesHandle *prh);


/**
 * Handle for a GET /reserves operation.
 */
struct TALER_MERCHANT_ReservesGetHandle;


/**
 * Information about a reserve.
 */
struct TALER_MERCHANT_ReserveSummary
{
  /**
   * Public key of the reserve
   */
  struct TALER_ReservePublicKeyP reserve_pub;

  /**
   * Timestamp when it was established
   */
  struct GNUNET_TIME_Timestamp creation_time;

  /**
   * Timestamp when it expires
   */
  struct GNUNET_TIME_Timestamp expiration_time;

  /**
   * Initial amount as per reserve creation call
   */
  struct TALER_Amount merchant_initial_amount;

  /**
   * Initial amount as per exchange, 0 if exchange did
   * not confirm reserve creation yet.
   */
  struct TALER_Amount exchange_initial_amount;

  /**
   * Amount picked up so far.
   */
  struct TALER_Amount pickup_amount;

  /**
   * Amount approved for tips that exceeds the pickup_amount.
   */
  struct TALER_Amount committed_amount;

  /**
   * Is this reserve active (false if it was deleted but not purged)
   */
  bool active;
};


/**
 * Callback to process a GET /reserves request
 *
 * @param cls closure
 * @param hr HTTP response details
 * @param reserves_length length of the @a reserves array
 * @param reserves array with details about the reserves, NULL on error
 */
typedef void
(*TALER_MERCHANT_ReservesGetCallback) (
  void *cls,
  const struct TALER_MERCHANT_HttpResponse *hr,
  unsigned int reserves_length,
  const struct TALER_MERCHANT_ReserveSummary reserves[]);


/**
 * Issue a GET /reserves request to the backend.  Informs the backend
 * that a customer wants to pick up a reserves.
 *
 * @param ctx execution context
 * @param backend_url base URL of the merchant backend
 * @param after filter for reserves created after this date, use 0 for no filtering
 * @param active filter for reserves that are active
 * @param failures filter for reserves where we disagree about the balance with
 *        the exchange
 * @param cb function to call with the result(s)
 * @param cb_cls closure for @a cb
 * @return handle for this operation, NULL upon errors
 */
struct TALER_MERCHANT_ReservesGetHandle *
TALER_MERCHANT_reserves_get (struct GNUNET_CURL_Context *ctx,
                             const char *backend_url,
                             struct GNUNET_TIME_Timestamp after,
                             enum TALER_EXCHANGE_YesNoAll active,
                             enum TALER_EXCHANGE_YesNoAll failures,
                             TALER_MERCHANT_ReservesGetCallback cb,
                             void *cb_cls);


/**
 * Cancel a GET /reserves request.
 *
 * @param rgh handle to the request to be canceled
 */
void
TALER_MERCHANT_reserves_get_cancel (
  struct TALER_MERCHANT_ReservesGetHandle *rgh);


/**
 * Handle for a request to obtain details on a specific
 * (tipping) reserve.
 */
struct TALER_MERCHANT_ReserveGetHandle;


/**
 * Details about a tip granted by the merchant.
 */
struct TALER_MERCHANT_TipDetails
{
  /**
   * Identifier for the tip.
   */
  struct GNUNET_HashCode tip_id;

  /**
   * Total value of the tip (including fees).
   */
  struct TALER_Amount amount;

  /**
   * Human-readable reason for why the tip was granted.
   */
  const char *reason;

};


/**
 * Callback to process a GET /reserve/$RESERVE_PUB request
 *
 * @param cls closure
 * @param hr HTTP response details
 * @param rs reserve summary for the reserve, NULL on error
 * @param active is this reserve active (false if it was deleted but not purged)
 * @param exchange_url URL of the exchange hosting the reserve, NULL if not @a active
 * @param payto_uri URI to fill the reserve, NULL if not @a active or already filled
 * @param tips_length length of the @a reserves array
 * @param tips array with details about the tips granted, NULL on error
 */
typedef void
(*TALER_MERCHANT_ReserveGetCallback) (
  void *cls,
  const struct TALER_MERCHANT_HttpResponse *hr,
  const struct TALER_MERCHANT_ReserveSummary *rs,
  bool active,
  const char *exchange_url,
  const char *payto_uri,
  unsigned int tips_length,
  const struct TALER_MERCHANT_TipDetails tips[]);


/**
 * Issue a GET /reserve/$RESERVE_ID request to the backend.  Queries the backend
 * about the status of a reserve.
 *
 * @param ctx execution context
 * @param backend_url base URL of the merchant backend
 * @param reserve_pub which reserve should be queried
 * @param fetch_tips should we return details about the tips issued from the reserve
 * @param cb function to call with the result(s)
 * @param cb_cls closure for @a cb
 * @return handle for this operation, NULL upon errors
 */
struct TALER_MERCHANT_ReserveGetHandle *
TALER_MERCHANT_reserve_get (struct GNUNET_CURL_Context *ctx,
                            const char *backend_url,
                            const struct TALER_ReservePublicKeyP *reserve_pub,
                            bool fetch_tips,
                            TALER_MERCHANT_ReserveGetCallback cb,
                            void *cb_cls);


/**
 * Cancel a GET /reserve/$RESERVE_ID request.
 *
 * @param rgh handle to the request to be canceled
 */
void
TALER_MERCHANT_reserve_get_cancel (
  struct TALER_MERCHANT_ReserveGetHandle *rgh);


/**
 * Handle for a /tip-authorize operation.
 */
struct TALER_MERCHANT_TipAuthorizeHandle;


/**
 * Callback for a /reserves/$RESERVE_PUB/tip-authorize request.  Returns the result of
 * the operation.
 *
 * @param cls closure
 * @param hr HTTP response details
 * @param tip_id which tip ID should be used to pickup the tip
 * @param tip_uri URI for the tip
 * @param tip_expiration when does the tip expire
 */
typedef void
(*TALER_MERCHANT_TipAuthorizeCallback) (
  void *cls,
  const struct TALER_MERCHANT_HttpResponse *hr,
  struct GNUNET_HashCode *tip_id,
  const char *tip_uri,
  struct GNUNET_TIME_Timestamp tip_expiration);


/**
 * Issue a /tip-authorize request to the backend.  Informs the backend
 * that a tip should be created.
 *
 * @param ctx execution context
 * @param backend_url base URL of the merchant backend
 * @param reserve_pub public key of the reserve
 * @param next_url where the browser should proceed after picking up the tip
 * @param amount amount to be handed out as a tip
 * @param justification which justification should be stored (human-readable reason for the tip)
 * @param authorize_cb callback which will work the response gotten from the backend
 * @param authorize_cb_cls closure to pass to @a authorize_cb
 * @return handle for this operation, NULL upon errors
 */
struct TALER_MERCHANT_TipAuthorizeHandle *
TALER_MERCHANT_tip_authorize2 (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  const struct TALER_ReservePublicKeyP *reserve_pub,
  const char *next_url,
  const struct TALER_Amount *amount,
  const char *justification,
  TALER_MERCHANT_TipAuthorizeCallback authorize_cb,
  void *authorize_cb_cls);


/**
 * Issue a POST /tips request to the backend.  Informs the backend that a tip
 * should be created. In contrast to #TALER_MERCHANT_tip_authorize2(), the
 * backend gets to pick the reserve with this API.
 *
 * @param ctx execution context
 * @param backend_url base URL of the merchant backend
 * @param next_url where the browser should proceed after picking up the tip
 * @param amount amount to be handed out as a tip
 * @param justification which justification should be stored (human-readable reason for the tip)
 * @param authorize_cb callback which will work the response gotten from the backend
 * @param authorize_cb_cls closure to pass to @a authorize_cb
 * @return handle for this operation, NULL upon errors
 */
struct TALER_MERCHANT_TipAuthorizeHandle *
TALER_MERCHANT_tip_authorize (struct GNUNET_CURL_Context *ctx,
                              const char *backend_url,
                              const char *next_url,
                              const struct TALER_Amount *amount,
                              const char *justification,
                              TALER_MERCHANT_TipAuthorizeCallback authorize_cb,
                              void *authorize_cb_cls);


/**
 * Cancel a pending /tip-authorize request
 *
 * @param ta handle from the operation to cancel
 */
void
TALER_MERCHANT_tip_authorize_cancel (
  struct TALER_MERCHANT_TipAuthorizeHandle *ta);


/**
 * Handle for a request to delete or purge a specific reserve.
 */
struct TALER_MERCHANT_ReserveDeleteHandle;


/**
 * Callback to process a DELETE /reserve/$RESERVE_PUB request
 *
 * @param cls closure
 * @param hr HTTP response details
 */
typedef void
(*TALER_MERCHANT_ReserveDeleteCallback) (
  void *cls,
  const struct TALER_MERCHANT_HttpResponse *hr);


/**
 * Issue a DELETE /reserve/$RESERVE_ID request to the backend.  Only
 * deletes the private key of the reserve, preserves tipping data.
 *
 * @param ctx execution context
 * @param backend_url base URL of the merchant backend
 * @param reserve_pub which reserve should be queried
 * @param cb function to call with the result
 * @param cb_cls closure for @a cb
 * @return handle for this operation, NULL upon errors
 */
struct TALER_MERCHANT_ReserveDeleteHandle *
TALER_MERCHANT_reserve_delete (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  const struct TALER_ReservePublicKeyP *reserve_pub,
  TALER_MERCHANT_ReserveDeleteCallback cb,
  void *cb_cls);


/**
 * Issue a DELETE /reserve/$RESERVE_ID request to the backend.
 * Purges the reserve, deleting all associated data. DANGEROUS.
 *
 * @param ctx execution context
 * @param backend_url base URL of the merchant backend
 * @param reserve_pub which reserve should be queried
 * @param cb function to call with the result
 * @param cb_cls closure for @a cb
 * @return handle for this operation, NULL upon errors
 */
struct TALER_MERCHANT_ReserveDeleteHandle *
TALER_MERCHANT_reserve_purge (struct GNUNET_CURL_Context *ctx,
                              const char *backend_url,
                              const struct TALER_ReservePublicKeyP *reserve_pub,
                              TALER_MERCHANT_ReserveDeleteCallback cb,
                              void *cb_cls);


/**
 * Cancel a DELETE (or purge) /reserve/$RESERVE_ID request.
 *
 * @param rdh handle to the request to be canceled
 */
void
TALER_MERCHANT_reserve_delete_cancel (
  struct TALER_MERCHANT_ReserveDeleteHandle *rdh);


/* ********************* /tips ************************** */


/**
 * Handle for a GET /tips/$TIP_ID (public variant) operation.
 */
struct TALER_MERCHANT_TipWalletGetHandle;


/**
 * Callback to process a GET /tips/$TIP_ID request
 *
 * @param cls closure
 * @param hr HTTP response details
 * @param expiration when the tip will expire
 * @param exchange_url exchange from which the coins should be withdrawn
 * @param amount_remaining total amount still available for the tip
 */
typedef void
(*TALER_MERCHANT_TipWalletGetCallback) (
  void *cls,
  const struct TALER_MERCHANT_HttpResponse *hr,
  struct GNUNET_TIME_Timestamp expiration,
  const char *exchange_url,
  const struct TALER_Amount *amount_remaining);


/**
 * Issue a GET /tips/$TIP_ID (public variant) request to the backend.  Returns
 * information needed to pick up a tip.
 *
 * @param ctx execution context
 * @param backend_url base URL of the merchant backend
 * @param tip_id which tip should we query
 * @param cb function to call with the result
 * @param cb_cls closure for @a cb
 * @return handle for this operation, NULL upon errors
 */
struct TALER_MERCHANT_TipWalletGetHandle *
TALER_MERCHANT_wallet_tip_get (struct GNUNET_CURL_Context *ctx,
                               const char *backend_url,
                               const struct GNUNET_HashCode *tip_id,
                               TALER_MERCHANT_TipWalletGetCallback cb,
                               void *cb_cls);


/**
 * Cancel a GET /tips/$TIP_ID request.
 *
 * @param tgh handle to the request to be canceled
 */
void
TALER_MERCHANT_wallet_tip_get_cancel (struct
                                      TALER_MERCHANT_TipWalletGetHandle *tgh);


/**
 * Handle for a GET /private/tips/$TIP_ID (private variant) operation.
 */
struct TALER_MERCHANT_TipMerchantGetHandle;


/**
 * Summary information for a tip pickup.
 */
struct TALER_MERCHANT_PickupDetail
{
  /**
   * Identifier of the pickup.
   */
  struct GNUNET_HashCode pickup_id;

  /**
   * Number of planchets involved.
   */
  uint64_t num_planchets;

  /**
   * Total amount requested for this pickup.
   */
  struct TALER_Amount requested_amount;
};

/**
 * Callback to process a GET /private/tips/$TIP_ID request
 *
 * @param cls closure
 * @param hr HTTP response details
 * @param total_authorized how many tips were authorized under this tip
 * @param total_picked_up how many tips have been picked up
 * @param reason what was the reason given for the tip
 * @param expiration when the tip will expire
 * @param reserve_pub which reserve is funding this tip
 * @param pickups_length length of the @a pickups array
 * @param pickups array of pickup operations performed for this tip
 */
typedef void
(*TALER_MERCHANT_TipMerchantGetCallback) (
  void *cls,
  const struct TALER_MERCHANT_HttpResponse *hr,
  const struct TALER_Amount *total_authorized,
  const struct TALER_Amount *total_picked_up,
  const char *reason,
  struct GNUNET_TIME_Timestamp expiration,
  const struct TALER_ReservePublicKeyP *reserve_pub,
  unsigned int pickups_length,
  const struct TALER_MERCHANT_PickupDetail pickups[]);


/**
 * Issue a GET /private/tips/$TIP_ID (private variant) request to the backend.
 * Returns information needed to pick up a tip.
 *
 * @param ctx execution context
 * @param backend_url base URL of the merchant backend
 * @param tip_id which tip should we query
 * @param pickups whether to fetch associated pickups
 * @param cb function to call with the result
 * @param cb_cls closure for @a cb
 * @return handle for this operation, NULL upon errors
 */
struct TALER_MERCHANT_TipMerchantGetHandle *
TALER_MERCHANT_merchant_tip_get (struct GNUNET_CURL_Context *ctx,
                                 const char *backend_url,
                                 const struct GNUNET_HashCode *tip_id,
                                 bool pickups,
                                 TALER_MERCHANT_TipMerchantGetCallback cb,
                                 void *cb_cls);


/**
 * Cancel a GET /private/tips/$TIP_ID request.
 *
 * @param tgh handle to the request to be canceled
 */
void
TALER_MERCHANT_merchant_tip_get_cancel (
  struct TALER_MERCHANT_TipMerchantGetHandle *tgh);


/**
 * Handle for a GET /private/tips request.
 */
struct TALER_MERCHANT_TipsGetHandle;


/**
 * Database entry information of a tip.
 */
struct TALER_MERCHANT_TipEntry
{
  /**
   * Row number of the tip in the database.
   */
  uint64_t row_id;

  /**
   * Identifier for the tip.
   */
  struct GNUNET_HashCode tip_id;

  /**
   * Total value of the tip (including fees).
   */
  struct TALER_Amount tip_amount;

};


/**
 * Callback to process a GET /private/tips request.
 *
 * @param cls closure
 * @param hr HTTP response details
 * @param tips_length length of the @a tips array
 * @param tips the array of tips, NULL on error
 */
typedef void
(*TALER_MERCHANT_TipsGetCallback) (
  void *cls,
  const struct TALER_MERCHANT_HttpResponse *hr,
  unsigned int tips_length,
  const struct TALER_MERCHANT_TipEntry tips[]);


/**
 * Issue a GET /private/tips request to the backend.
 *
 * @param ctx execution context
 * @param backend_url base URL of the merchant backend
 * @param cb function to call with the result
 * @param cb_cls closure for @a cb
 * @return handle for this operation, NULL upon errors
 */
struct TALER_MERCHANT_TipsGetHandle *
TALER_MERCHANT_tips_get (struct GNUNET_CURL_Context *ctx,
                         const char *backend_url,
                         TALER_MERCHANT_TipsGetCallback cb,
                         void *cb_cls);


/**
 * Issue a GET /private/tips request with filters to the backend.
 *
 * @param ctx execution context
 * @param backend_url base URL of the merchant backend
 * @param expired yes for expired tips, no for unexpired tips, all for all tips
 * @param limit number of results to return, negative for descending row id, positive for ascending
 * @param offset row id to start returning results from
 * @param cb function to call with the result
 * @param cb_cls closure for @a cb
 * @return handle for this operation, NULL upon errors
 */
struct TALER_MERCHANT_TipsGetHandle *
TALER_MERCHANT_tips_get2 (struct GNUNET_CURL_Context *ctx,
                          const char *backend_url,
                          enum TALER_EXCHANGE_YesNoAll expired,
                          int64_t limit,
                          uint64_t offset,
                          TALER_MERCHANT_TipsGetCallback cb,
                          void *cb_cls);


/**
 * Cancel a GET /private/tips request.
 *
 * @param tgh the operation to cancel
 */
void
TALER_MERCHANT_tips_get_cancel (struct TALER_MERCHANT_TipsGetHandle *tgh);


/**
 * Handle for a POST /tips/$TIP_ID/pickup operation.
 */
struct TALER_MERCHANT_TipPickupHandle;


/**
 * Callback for a POST /tips/$TIP_ID/pickup request.  Returns the result of
 * the operation.
 *
 * @param cls closure
 * @param hr HTTP response details
 * @param num_sigs length of the @a reserve_sigs array, 0 on error
 * @param sigs array of signatures over the coins, NULL on error
 */
typedef void
(*TALER_MERCHANT_TipPickupCallback) (
  void *cls,
  const struct TALER_MERCHANT_HttpResponse *hr,
  unsigned int num_sigs,
  const struct TALER_DenominationSignature sigs[]);


/**
 * Information per planchet.
 */
struct TALER_MERCHANT_PlanchetData
{
  /**
   * Planchet secrets.
   */
  struct TALER_PlanchetSecretsP ps;

  /**
   * Denomination key desired.
   */
  const struct TALER_EXCHANGE_DenomPublicKey *pk;

};

/**
 * Issue a POST /tips/$TIP_ID/pickup request to the backend.  Informs the
 * backend that a customer wants to pick up a tip.
 *
 * @param ctx execution context
 * @param backend_url base URL of the merchant backend
 * @param tip_id unique identifier for the tip
 * @param num_planchets number of planchets provided in @a pds
 * @param planchets array of planchet secrets to be signed into existence for the tip
 * @param pickup_cb callback which will work the response gotten from the backend
 * @param pickup_cb_cls closure to pass to @a pickup_cb
 * @return handle for this operation, NULL upon errors
 */
struct TALER_MERCHANT_TipPickupHandle *
TALER_MERCHANT_tip_pickup (struct GNUNET_CURL_Context *ctx,
                           const char *backend_url,
                           const struct GNUNET_HashCode *tip_id,
                           unsigned int num_planchets,
                           const struct TALER_MERCHANT_PlanchetData planchets[],
                           TALER_MERCHANT_TipPickupCallback pickup_cb,
                           void *pickup_cb_cls);


/**
 * Cancel a pending /tips/$TIP_ID/pickup request
 *
 * @param tph handle from the operation to cancel
 */
void
TALER_MERCHANT_tip_pickup_cancel (struct TALER_MERCHANT_TipPickupHandle *tph);


/**
 * Handle for a low-level /tip-pickup operation (without unblinding).
 */
struct TALER_MERCHANT_TipPickup2Handle;


/**
 * Callback for a POST /tips/$TIP_ID/pickup request.  Returns the result of
 * the operation.  Note that the client MUST still do the unblinding of the @a
 * blind_sigs.
 *
 * @param cls closure
 * @param hr HTTP response details
 * @param num_blind_sigs length of the @a blind_sigs array, 0 on error
 * @param blind_sigs array of blind signatures over the planchets, NULL on error
 */
typedef void
(*TALER_MERCHANT_TipPickup2Callback) (
  void *cls,
  const struct TALER_MERCHANT_HttpResponse *hr,
  unsigned int num_blind_sigs,
  const struct TALER_BlindedDenominationSignature blind_sigs[]);


/**
 * Issue a POST /tips/$TIP_ID/pickup request to the backend.  Informs the
 * backend that a customer wants to pick up a tip.
 *
 * @param ctx execution context
 * @param backend_url base URL of the merchant backend
 * @param tip_id unique identifier for the tip
 * @param num_planchets number of planchets provided in @a planchets
 * @param planchets array of planchets to be signed into existence for the tip
 * @param pickup_cb callback which will work the response gotten from the backend
 * @param pickup_cb_cls closure to pass to @a pickup_cb
 * @return handle for this operation, NULL upon errors
 */
struct TALER_MERCHANT_TipPickup2Handle *
TALER_MERCHANT_tip_pickup2 (struct GNUNET_CURL_Context *ctx,
                            const char *backend_url,
                            const struct GNUNET_HashCode *tip_id,
                            unsigned int num_planchets,
                            const struct TALER_PlanchetDetail planchets[],
                            TALER_MERCHANT_TipPickup2Callback pickup_cb,
                            void *pickup_cb_cls);


/**
 * Cancel a pending /tip-pickup request.
 *
 * @param tp handle from the operation to cancel
 */
void
TALER_MERCHANT_tip_pickup2_cancel (
  struct TALER_MERCHANT_TipPickup2Handle *tp);


/* ********************* /kyc ************************** */

/**
 * Handle for GETing the KYC status of instance(s).
 */
struct TALER_MERCHANT_KycGetHandle;


/**
 * Information about KYC actions the merchant still must perform.
 */
struct TALER_MERCHANT_AccountKycRedirectDetail
{

  /**
   * URL that the user should open in a browser to
   * proceed with the KYC process (as returned
   * by the exchange's /kyc-check/ endpoint).
   */
  const char *kyc_url;

  /**
   * Base URL of the exchange this is about.
   */
  const char *exchange_url;

  /**
   * Our bank wire account this is about.
   */
  const char *payto_uri;
};


/**
 * Information about KYC status failures at the exchange.
 */
struct TALER_MERCHANT_ExchangeKycFailureDetail
{
  /**
   * Base URL of the exchange this is about.
   */
  const char *exchange_url;

  /**
   * Error code indicating errors the exchange
   * returned, or #TALER_EC_INVALID for none.
   */
  enum TALER_ErrorCode exchange_code;

  /**
   * HTTP status code returned by the exchange when we asked for
   * information about the KYC status.
   * 0 if there was no response at all.
   */
  unsigned int exchange_http_status;
};


/**
 * Details in a response to a GET /kyc request.
 */
struct TALER_MERCHANT_KycResponse
{
  struct TALER_MERCHANT_HttpResponse hr;

  /**
   * Response details.
   */
  union
  {
    /**
     * Information returned if the status was #MHD_HTTP_ACCEPTED,
     * #MHD_HTTP_BAD_GATEWAY or #MHD_HTTP_GATEWAY_TIMEOUT.
     */
    struct
    {

      /**
       * Array with information about KYC actions the merchant still must perform.
       */
      struct TALER_MERCHANT_AccountKycRedirectDetail *pending_kycs;

      /**
       * Array with information about KYC failures at the exchange.
       */
      struct TALER_MERCHANT_ExchangeKycFailureDetail *timeout_kycs;

      /**
       * Length of the @e pending_kycs array.
       */
      unsigned int pending_kycs_length;

      /**
       * Length of the @e timeout_kycs array.
       */
      unsigned int timeout_kycs_length;
    } kyc_status;

  } details;

};


/**
 * Callback to with a response from a GET [/private]/kyc request
 *
 * @param cls closure
 * @param kr response details
 */
typedef void
(*TALER_MERCHANT_KycGetCallback) (
  void *cls,
  const struct TALER_MERCHANT_KycResponse *kr);


/**
 * Issue a GET /private/kycs/$KYC_ID (private variant) request to the backend.
 * Returns KYC status of bank accounts.
 *
 * @param ctx execution context
 * @param backend_url base URL of the merchant backend
 * @param h_wire which bank account to query, NULL for all
 * @param exchange_url which exchange to query, NULL for all
 * @param timeout how long to wait for a (positive) reply
 * @param cb function to call with the result
 * @param cb_cls closure for @a cb
 * @return handle for this operation, NULL upon errors
 */
struct TALER_MERCHANT_KycGetHandle *
TALER_MERCHANT_kyc_get (struct GNUNET_CURL_Context *ctx,
                        const char *backend_url,
                        const struct TALER_MerchantWireHash *h_wire,
                        const char *exchange_url,
                        struct GNUNET_TIME_Relative timeout,
                        TALER_MERCHANT_KycGetCallback cb,
                        void *cb_cls);


/**
 * Issue a GET /management/instances/$INSTANCE/kyc request to the backend.
 * Returns KYC status of bank accounts.
 *
 * @param ctx execution context
 * @param backend_url base URL of the merchant backend
 * @param instance_id specific instance to query
 * @param h_wire which bank account to query, NULL for all
 * @param exchange_url which exchange to query, NULL for all
 * @param timeout how long to wait for a (positive) reply
 * @param cb function to call with the result
 * @param cb_cls closure for @a cb
 * @return handle for this operation, NULL upon errors
 */
struct TALER_MERCHANT_KycGetHandle *
TALER_MERCHANT_management_kyc_get (struct GNUNET_CURL_Context *ctx,
                                   const char *backend_url,
                                   const char *instance_id,
                                   const struct TALER_MerchantWireHash *h_wire,
                                   const char *exchange_url,
                                   struct GNUNET_TIME_Relative timeout,
                                   TALER_MERCHANT_KycGetCallback cb,
                                   void *cb_cls);


/**
 * Cancel a GET [/private]/kyc/$KYC_ID request.
 *
 * @param kyc handle to the request to be canceled
 */
void
TALER_MERCHANT_kyc_get_cancel (
  struct TALER_MERCHANT_KycGetHandle *kyc);


#endif  /* _TALER_MERCHANT_SERVICE_H */
