/*
  This file is part of TALER
  Copyright (C) 2014-2021 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Lesser General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.GPL.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file taler_merchantdb_plugin.h
 * @brief database access for the merchant
 * @author Florian Dold
 * @author Christian Grothoff
 */
#ifndef TALER_MERCHANTDB_PLUGIN_H
#define TALER_MERCHANTDB_PLUGIN_H

#include <gnunet/gnunet_util_lib.h>
#include <gnunet/gnunet_db_lib.h>
#include <taler/taler_exchange_service.h>
#include <jansson.h>

/**
 * Handle to interact with the database.
 */
struct TALER_MERCHANTDB_Plugin;


/**
 * Details about a wire account of the merchant.
 */
struct TALER_MERCHANTDB_AccountDetails
{
  /**
   * Hash of the wire details (@e payto_uri and @e salt).
   */
  struct TALER_MerchantWireHash h_wire;

  /**
   * Salt value used for hashing @e payto_uri.
   */
  struct TALER_WireSalt salt;

  /**
   * Actual account address as a payto://-URI.
   */
  const char *payto_uri;

  /**
   * Is the account set for active use in new contracts?
   */
  bool active;

};

/**
 * Authentication settings for an instance.
 */
struct TALER_MERCHANTDB_InstanceAuthSettings
{
  /**
   * Hash used for authentication.  All zero if authentication is off.
   */
  struct GNUNET_HashCode auth_hash;

  /**
   * Salt used to hash the "Authentication" header, the result must then
   * match the @e auth_hash.
   */
  struct GNUNET_ShortHashCode auth_salt;
};

/**
 * General settings for an instance.
 */
struct TALER_MERCHANTDB_InstanceSettings
{
  /**
   * prefix for the instance under "/instances/"
   */
  char *id;

  /**
   * legal name of the instance
   */
  char *name;

  /**
   * Address of the business
   */
  json_t *address;

  /**
   * jurisdiction of the business
   */
  json_t *jurisdiction;

  /**
   * Default max deposit fee that the merchant is willing to
   * pay; if deposit costs more, then the customer will cover
   * the difference.
   */
  struct TALER_Amount default_max_deposit_fee;

  /**
   * Default maximum wire fee to assume, unless stated differently in the
   * proposal already.
   */
  struct TALER_Amount default_max_wire_fee;

  /**
   * Default factor for wire fee amortization.
   */
  uint32_t default_wire_fee_amortization;

  /**
   * If the frontend does NOT specify an execution date, how long should
   * we tell the exchange to wait to aggregate transactions before
   * executing the wire transfer?  This delay is added to the current
   * time when we generate the advisory execution time for the exchange.
   */
  struct GNUNET_TIME_Relative default_wire_transfer_delay;

  /**
   * If the frontend does NOT specify a payment deadline, how long should
   * offers we make be valid by default?
   */
  struct GNUNET_TIME_Relative default_pay_delay;

};


/**
 * Typically called by `lookup_instances`.
 *
 * @param cls closure
 * @param merchant_pub public key of the instance
 * @param merchant_priv private key of the instance, NULL if not available
 * @param is general instance settings
 * @param ias instance authentication settings
 * @param accounts_length length of the @a accounts array
 * @param accounts list of accounts of the merchant
 */
typedef void
(*TALER_MERCHANTDB_InstanceCallback)(
  void *cls,
  const struct TALER_MerchantPublicKeyP *merchant_pub,
  const struct TALER_MerchantPrivateKeyP *merchant_priv,
  const struct TALER_MERCHANTDB_InstanceSettings *is,
  const struct TALER_MERCHANTDB_InstanceAuthSettings *ias,
  unsigned int accounts_length,
  const struct TALER_MERCHANTDB_AccountDetails accounts[]);


/**
 * Typically called by `lookup_products`.
 *
 * @param cls a `json_t *` JSON array to build
 * @param product_id ID of the product
 */
typedef void
(*TALER_MERCHANTDB_ProductsCallback)(void *cls,
                                     const char *product_id);


/**
 * Details about a product.
 */
struct TALER_MERCHANTDB_ProductDetails
{
  /**
   * Description of the product.
   */
  char *description;

  /**
   * Internationalized description.
   */
  json_t *description_i18n;

  /**
   * Unit in which the product is sold.
   */
  char *unit;

  /**
   * Price per unit of the product.  Zero to imply that the
   * product is not sold separately or that the price is not fixed.
   */
  struct TALER_Amount price;

  /**
   * Base64-encoded product image, or an empty string.
   */
  char *image;

  /**
   * List of taxes the merchant pays for this product. Never NULL,
   * but can be an empty array.
   */
  json_t *taxes;

  /**
   * Number of units of the product in stock in sum in total, including all
   * existing sales and lost product, in product-specific units. UINT64_MAX
   * indicates "infinite".
   */
  uint64_t total_stock;

  /**
   * Number of units of the product in sold, in product-specific units.
   */
  uint64_t total_sold;

  /**
   * Number of units of stock lost.
   */
  uint64_t total_lost;

  /**
   * Identifies where the product is in stock, possibly an empty map.
   */
  json_t *address;

  /**
   * Identifies when the product will be restocked. 0 for unknown,
   * #GNUNET_TIME_UNIT_FOREVER_ABS for never.
   */
  struct GNUNET_TIME_Timestamp next_restock;
};


/**
 * Filter preferences.
 */
struct TALER_MERCHANTDB_OrderFilter
{
  /**
   * Filter by payment status.
   */
  enum TALER_EXCHANGE_YesNoAll paid;

  /**
   * Filter by refund status.
   */
  enum TALER_EXCHANGE_YesNoAll refunded;

  /**
   * Filter by wire transfer status.
   */
  enum TALER_EXCHANGE_YesNoAll wired;

  /**
   * Filter orders by date, exact meaning depends on @e delta.
   */
  struct GNUNET_TIME_Timestamp date;

  /**
   * Filter orders by order serial number, exact meaning depends on @e delta.
   */
  uint64_t start_row;

  /**
   * takes value of the form N (-N), so that at most N values strictly older
   * (younger) than start and date are returned.
   */
  int64_t delta;

  /**
   * Timeout for long-polling.
   */
  struct GNUNET_TIME_Relative timeout;

};


/**
 * Typically called by `lookup_orders`.
 *
 * @param cls a `json_t *` JSON array to build
 * @param order_id ID of the order
 * @param order_serial row of the order in the database
 * @param timestamp creation time of the order in the database
 */
typedef void
(*TALER_MERCHANTDB_OrdersCallback)(void *cls,
                                   const char *order_id,
                                   uint64_t order_serial,
                                   struct GNUNET_TIME_Timestamp timestamp);


/**
 * Function called with information about a coin that was deposited.
 *
 * @param cls closure
 * @param exchange_url exchange where @a coin_pub was deposited
 * @param coin_pub public key of the coin
 * @param amount_with_fee amount the exchange will deposit for this coin
 * @param deposit_fee fee the exchange will charge for this coin
 * @param refund_fee fee the exchange will charge for refunding this coin
 * @param wire_fee wire fee the exchange charges
 */
typedef void
(*TALER_MERCHANTDB_DepositsCallback)(
  void *cls,
  const char *exchange_url,
  const struct TALER_CoinSpendPublicKeyP *coin_pub,
  const struct TALER_Amount *amount_with_fee,
  const struct TALER_Amount *deposit_fee,
  const struct TALER_Amount *refund_fee,
  const struct TALER_Amount *wire_fee);


/**
 * Function called with information about a refund.
 *
 * @param cls closure
 * @param coin_pub public coin from which the refund comes from
 * @param refund_amount refund amount which is being taken from @a coin_pub
 */
typedef void
(*TALER_MERCHANTDB_RefundCallback)(
  void *cls,
  const struct TALER_CoinSpendPublicKeyP *coin_pub,
  const struct TALER_Amount *refund_amount);


/**
 * Typically called by `lookup_transfer_details_by_order`.
 *
 * @param cls closure
 * @param wtid wire transfer subject of the wire transfer for the coin
 * @param exchange_url base URL of the exchange that made the payment
 * @param execution_time when was the payment made
 * @param deposit_value contribution of the coin to the total wire transfer value
 * @param deposit_fee deposit fee charged by the exchange for the coin
 * @param transfer_confirmed did the merchant confirm that a wire transfer with
 *        @a wtid over the total amount happened?
 */
typedef void
(*TALER_MERCHANTDB_OrderTransferDetailsCallback)(
  void *cls,
  const struct TALER_WireTransferIdentifierRawP *wtid,
  const char *exchange_url,
  struct GNUNET_TIME_Timestamp execution_time,
  const struct TALER_Amount *deposit_value,
  const struct TALER_Amount *deposit_fee,
  bool transfer_confirmed);


/**
 * Function called with detailed information about a refund.
 *
 * @param cls closure
 * @param refund_serial unique serial number of the refund
 * @param timestamp time of the refund (for grouping of refunds in the wallet UI)
 * @param coin_pub public coin from which the refund comes from
 * @param exchange_url URL of the exchange that issued @a coin_pub
 * @param rtransaction_id identificator of the refund
 * @param reason human-readable explanation of the refund
 * @param refund_amount refund amount which is being taken from @a coin_pub
 * @param pending true if the this refund was not yet processed by the wallet/exchange
 */
typedef void
(*TALER_MERCHANTDB_RefundDetailCallback)(
  void *cls,
  uint64_t refund_serial,
  struct GNUNET_TIME_Timestamp timestamp,
  const struct TALER_CoinSpendPublicKeyP *coin_pub,
  const char *exchange_url,
  uint64_t rtransaction_id,
  const char *reason,
  const struct TALER_Amount *refund_amount,
  bool pending);


/**
 * Function called from ``account_kyc_get_status``
 * with KYC status information for this merchant.
 *
 * @param cls closure
 * @param h_wire hash of the wire account
 * @param exchange_kyc_serial serial number for the KYC process at the exchange, 0 if unknown
 * @param payto_uri payto:// URI of the merchant's bank account
 * @param exchange_url base URL of the exchange for which this is a status
 * @param last_check when did we last get an update on our KYC status from the exchange
 * @param kyc_ok true if we satisfied the KYC requirements
 */
typedef void
(*TALER_MERCHANTDB_KycCallback)(
  void *cls,
  const struct TALER_MerchantWireHash *h_wire,
  uint64_t exchange_kyc_serial,
  const char *payto_uri,
  const char *exchange_url,
  struct GNUNET_TIME_Timestamp last_check,
  bool kyc_ok);


/**
 * Results from trying to increase a refund.
 */
enum TALER_MERCHANTDB_RefundStatus
{

  /**
   * Refund amount exceeds original payment.
   */
  TALER_MERCHANTDB_RS_TOO_HIGH = -3,

  /**
   * Hard database failure.
   */
  TALER_MERCHANTDB_RS_HARD_ERROR = -2,

  /**
   * Soft database failure.
   */
  TALER_MERCHANTDB_RS_SOFT_ERROR = -1,

  /**
   * Order not found.
   */
  TALER_MERCHANTDB_RS_NO_SUCH_ORDER = 0,

  /**
   * Refund is now at or above the requested amount.
   */
  TALER_MERCHANTDB_RS_SUCCESS = 1

};


/**
 * Function called with information about a wire transfer identifier.
 *
 * @param cls closure
 * @param order_id the order to which the deposits belong
 * @param deposit_value the amount deposited under @a order_id
 * @param deposit_fee the fee charged for @a deposit_value
 */
typedef void
(*TALER_MERCHANTDB_TransferSummaryCallback)(
  void *cls,
  const char *order_id,
  const struct TALER_Amount *deposit_value,
  const struct TALER_Amount *deposit_fee);


/**
 * Function called with detailed information about a wire transfer and
 * the underlying deposits that are being aggregated.
 *
 * @param cls closure
 * @param current_offset offset in the exchange reply we are at
 * @param ttd details about the transfer at @a current_offset
 */
typedef void
(*TALER_MERCHANTDB_TransferDetailsCallback)(
  void *cls,
  unsigned int current_offset,
  const struct TALER_TrackTransferDetails *ttd);


/**
 * Function called with information about a wire transfer.
 *
 * @param cls closure with a `json_t *` array to build up the response
 * @param credit_amount how much was wired to the merchant (minus fees)
 * @param wtid wire transfer identifier
 * @param payto_uri target account that received the wire transfer
 * @param exchange_url base URL of the exchange that made the wire transfer
 * @param transfer_serial_id serial number identifying the transfer in the backend
 * @param execution_time when did the exchange make the transfer, #GNUNET_TIME_UNIT_FOREVER_ABS
 *           if it did not yet happen
 * @param verified true if we checked the exchange's answer and liked it,
 *                 false there is a problem (verification failed or did not yet happen)
 * @param confirmed true if the merchant confirmed this wire transfer
 *                 false if it is so far only claimed to have been made by the exchange
 */
typedef void
(*TALER_MERCHANTDB_TransferCallback)(
  void *cls,
  const struct TALER_Amount *credit_amount,
  const struct TALER_WireTransferIdentifierRawP *wtid,
  const char *payto_uri,
  const char *exchange_url,
  uint64_t transfer_serial_id,
  struct GNUNET_TIME_Timestamp execution_time,
  bool verified,
  bool confirmed);


/**
 * Callback with reserve details.
 *
 * @param cls closure
 * @param reserve_pub public key of the reserve
 * @param creation_time time when the reserve was setup
 * @param expiration_time time when the reserve will be closed by the exchange
 * @param merchant_initial_amount initial amount that the merchant claims to have filled the
 *           reserve with
 * @param exchange_initial_amount initial amount that the exchange claims to have received
 * @param pickup_amount total of tips that were picked up from this reserve
 * @param committed_amount total of tips that the merchant committed to, but that were not
 *           picked up yet
 * @param active true if the reserve is still active (we have the private key)
 */
typedef void
(*TALER_MERCHANTDB_ReservesCallback)(
  void *cls,
  const struct TALER_ReservePublicKeyP *reserve_pub,
  struct GNUNET_TIME_Timestamp creation_time,
  struct GNUNET_TIME_Timestamp expiration_time,
  const struct TALER_Amount *merchant_initial_amount,
  const struct TALER_Amount *exchange_initial_amount,
  const struct TALER_Amount *pickup_amount,
  const struct TALER_Amount *committed_amount,
  bool active);


/**
 * Callback with details about a reserve pending exchange confirmation.
 *
 * @param cls closure
 * @param instance_id for which instance is this reserve
 * @param exchange_url base URL of the exchange
 * @param reserve_pub public key of the reserve
 * @param expected_amount how much do we expect to see in the reserve
 */
typedef void
(*TALER_MERCHANTDB_PendingReservesCallback)(
  void *cls,
  const char *instance_id,
  const char *exchange_url,
  const struct TALER_ReservePublicKeyP *reserve_pub,
  const struct TALER_Amount *expected_amount);


/**
 * Details about a tip.
 */
struct TALER_MERCHANTDB_TipDetails
{
  /**
   * ID of the tip.
   */
  struct GNUNET_HashCode tip_id;

  /**
   * Total amount of the tip.
   */
  struct TALER_Amount total_amount;

  /**
   * Reason given for granting the tip.
   */
  char *reason;
};


/**
 * Function called with information about a coin that was deposited.
 *
 * @param cls closure
 * @param deposit_serial which deposit operation is this about
 * @param exchange_url URL of the exchange that issued the coin
 * @param amount_with_fee amount the exchange will deposit for this coin
 * @param deposit_fee fee the exchange will charge for this coin
 * @param h_wire hash of merchant's wire details
 * @param coin_pub public key of the coin
 */
typedef void
(*TALER_MERCHANTDB_DepositedCoinsCallback)(
  void *cls,
  uint64_t deposit_serial,
  const char *exchange_url,
  const struct TALER_MerchantWireHash *h_wire,
  const struct TALER_Amount *amount_with_fee,
  const struct TALER_Amount *deposit_fee,
  const struct TALER_CoinSpendPublicKeyP *coin_pub);


/**
 * Callback with reserve details.
 *
 * @param cls closure
 * @param creation_time time when the reserve was setup
 * @param expiration_time time when the reserve will be closed by the exchange
 * @param merchant_initial_amount initial amount that the merchant claims to have filled the
 *           reserve with
 * @param exchange_initial_amount initial amount that the exchange claims to have received
 * @param picked_up_amount total of tips that were picked up from this reserve
 * @param committed_amount total of tips that the merchant committed to, but that were not
 *           picked up yet
 * @param active true if the reserve is still active (we have the private key)
 * @param exchange_url base URL of the exchange hosting the reserve, NULL if not @a active
 * @param payto_uri URI to use to fund the reserve, NULL if not @a active
 * @param tips_length length of the @a tips array
 * @param tips information about the tips created by this reserve
 */
typedef void
(*TALER_MERCHANTDB_ReserveDetailsCallback)(
  void *cls,
  struct GNUNET_TIME_Timestamp creation_time,
  struct GNUNET_TIME_Timestamp expiration_time,
  const struct TALER_Amount *merchant_initial_amount,
  const struct TALER_Amount *exchange_initial_amount,
  const struct TALER_Amount *picked_up_amount,
  const struct TALER_Amount *committed_amount,
  bool active,
  const char *exchange_url,
  const char *payto_uri,
  unsigned int tips_length,
  const struct TALER_MERCHANTDB_TipDetails *tips);


/**
 * Typically called by `lookup_tips`.
 *
 * @param cls closure
 * @param row_id row of the tip in the database
 * @param tip_id id of the tip
 * @param amount amount of the tip
 */
typedef void
(*TALER_MERCHANTDB_TipsCallback)(void *cls,
                                 uint64_t row_id,
                                 struct GNUNET_HashCode tip_id,
                                 struct TALER_Amount amount);


/**
 * Function called with information about a coin that was deposited.
 *
 * @param cls closure
 * @param exchange_url URL of the exchange that issued the coin
 * @param amount_with_fee amount the exchange will deposit for this coin
 * @param deposit_fee fee the exchange will charge for this coin
 * @param refund_fee fee the exchange will charge for refunding this coin
 * @param wire_fee wire fee the exchange charges
 * @param h_wire hash of merchant's wire details
 * @param deposit_timestamp when did the exchange receive the deposit
 * @param refund_deadline until when are refunds allowed
 * @param exchange_sig signature by the exchange
 * @param exchange_pub exchange signing key used for @a exchange_sig
 */
typedef void
(*TALER_MERCHANTDB_CoinDepositCallback)(
  void *cls,
  const char *exchange_url,
  const struct TALER_Amount *amount_with_fee,
  const struct TALER_Amount *deposit_fee,
  const struct TALER_Amount *refund_fee,
  const struct TALER_Amount *wire_fee,
  const struct TALER_MerchantWireHash *h_wire,
  struct GNUNET_TIME_Timestamp deposit_timestamp,
  struct GNUNET_TIME_Timestamp refund_deadline,
  const struct TALER_ExchangeSignatureP *exchange_sig,
  const struct TALER_ExchangePublicKeyP *exchange_pub);


/**
 * Details about a pickup operation executed by the merchant.
 */
struct TALER_MERCHANTDB_PickupDetails
{
  /**
   * Identifier for the pickup operation.
   */
  struct GNUNET_HashCode pickup_id;

  /**
   * Total amount requested for this @e pickup_id.
   */
  struct TALER_Amount requested_amount;

  /**
   * Number of planchets involved in the request.
   */
  unsigned int num_planchets;

};


/**
 * Handle to interact with the database.
 *
 * Functions ending with "_TR" run their OWN transaction scope
 * and MUST NOT be called from within a transaction setup by the
 * caller.  Functions ending with "_NT" require the caller to
 * setup a transaction scope.  Functions without a suffix are
 * simple, single SQL queries that MAY be used either way.
 */
struct TALER_MERCHANTDB_Plugin
{

  /**
   * Closure for all callbacks.
   */
  void *cls;

  /**
   * Name of the library which generated this plugin.  Set by the
   * plugin loader.
   */
  char *library_name;

  /**
   * Connect to the database.
   *
   * @param cls closure
   */
  enum GNUNET_GenericReturnValue
  (*connect)(void *cls);

  /**
   * Drop merchant tables. Used for testcases and to reset the DB.
   *
   * @param cls closure
   * @return #GNUNET_OK upon success; #GNUNET_SYSERR upon failure
   */
  enum GNUNET_GenericReturnValue
  (*drop_tables)(void *cls);

  /**
   * Initialize merchant tables
   *
   * @param cls closure
   * @return #GNUNET_OK upon success; #GNUNET_SYSERR upon failure
   */
  enum GNUNET_GenericReturnValue
  (*create_tables)(void *cls);

  /**
   * Register callback to be invoked on events of type @a es.
   *
   * @param cls database context to use
   * @param es specification of the event to listen for
   * @param timeout how long to wait for the event
   * @param cb function to call when the event happens, possibly
   *         multiple times (until cancel is invoked)
   * @param cb_cls closure for @a cb
   * @return handle useful to cancel the listener
   */
  struct GNUNET_DB_EventHandler *
  (*event_listen)(void *cls,
                  const struct GNUNET_DB_EventHeaderP *es,
                  struct GNUNET_TIME_Relative timeout,
                  GNUNET_DB_EventCallback cb,
                  void *cb_cls);

  /**
   * Stop notifications.
   *
   * @param eh handle to unregister.
   */
  void
  (*event_listen_cancel)(struct GNUNET_DB_EventHandler *eh);


  /**
   * Notify all that listen on @a es of an event.
   *
   * @param cls database context to use
   * @param es specification of the event to generate
   * @param extra additional event data provided
   * @param extra_size number of bytes in @a extra
   */
  void
  (*event_notify)(void *cls,
                  const struct GNUNET_DB_EventHeaderP *es,
                  const void *extra,
                  size_t extra_size);


  /**
   * Do a pre-flight check that we are not in an uncommitted transaction.  If
   * we are, die.  Does not return anything, as we will continue regardless of
   * the outcome.
   *
   * @param cls the `struct PostgresClosure` with the plugin-specific state
   */
  void
  (*preflight) (void *cls);

  /**
   * Start a transaction.
   *
   * @param cls the `struct PostgresClosure` with the plugin-specific state
   * @param name unique name identifying the transaction (for debugging),
   *             must point to a constant
   * @return #GNUNET_OK on success
   */
  enum GNUNET_GenericReturnValue
  (*start)(void *cls,
           const char *name);

  /**
   * Start a transaction with isolation level 'read committed'.
   *
   * @param cls the `struct PostgresClosure` with the plugin-specific state
   * @param name unique name identifying the transaction (for debugging),
   *             must point to a constant
   * @return #GNUNET_OK on success
   */
  enum GNUNET_GenericReturnValue
  (*start_read_committed)(void *cls,
                          const char *name);

  /**
   * Roll back the current transaction of a database connection.
   *
   * @param cls the `struct PostgresClosure` with the plugin-specific state
   * @return #GNUNET_OK on success
   */
  void
  (*rollback) (void *cls);

  /**
   * Commit the current transaction of a database connection.
   *
   * @param cls the `struct PostgresClosure` with the plugin-specific state
   * @return transaction status code
   */
  enum GNUNET_DB_QueryStatus
  (*commit)(void *cls);

  /**
   * Lookup all of the instances this backend has configured.
   *
   * @param cls closure
   * @param active_only only find 'active' instances
   * @param cb function to call on all instances found
   * @param cb_cls closure for @a cb
   */
  enum GNUNET_DB_QueryStatus
  (*lookup_instances)(void *cls,
                      bool active_only,
                      TALER_MERCHANTDB_InstanceCallback cb,
                      void *cb_cls);

  /**
   * Lookup one of the instances this backend has configured.
   *
   * @param cls closure
   * @param id ID of instance to look up
   * @param active_only only find 'active' instances
   * @param cb function to call on all instances found
   * @param cb_cls closure for @a cb
   */
  enum GNUNET_DB_QueryStatus
  (*lookup_instance)(void *cls,
                     const char *id,
                     bool active_only,
                     TALER_MERCHANTDB_InstanceCallback cb,
                     void *cb_cls);

  /**
   * Lookup authentication data of an instance.
   *
   * @param cls closure
   * @param instance_id instance to query
   * @param[out] ias where to store the auth data
   */
  enum GNUNET_DB_QueryStatus
  (*lookup_instance_auth)(void *cls,
                          const char *instance_id,
                          struct TALER_MERCHANTDB_InstanceAuthSettings *ias);


  /**
   * Insert information about an instance into our database.
   *
   * @param cls closure
   * @param merchant_pub public key of the instance
   * @param merchant_priv private key of the instance
   * @param is details about the instance
   * @return database result code
   */
  enum GNUNET_DB_QueryStatus
  (*insert_instance)(void *cls,
                     const struct TALER_MerchantPublicKeyP *merchant_pub,
                     const struct TALER_MerchantPrivateKeyP *merchant_priv,
                     const struct TALER_MERCHANTDB_InstanceSettings *is,
                     const struct TALER_MERCHANTDB_InstanceAuthSettings *ias);

  /**
   * Insert information about an instance's account into our database.
   *
   * @param cls closure
   * @param id identifier of the instance
   * @param account_details details about the account
   * @return database result code
   */
  enum GNUNET_DB_QueryStatus
  (*insert_account)(
    void *cls,
    const char *id,
    const struct TALER_MERCHANTDB_AccountDetails *account_details);

  /**
   * Delete private key of an instance from our database.
   *
   * @param cls closure
   * @param merchant_id identifier of the instance
   * @return database result code
   */
  enum GNUNET_DB_QueryStatus
  (*delete_instance_private_key)(
    void *cls,
    const char *merchant_id);

  /**
   * Purge an instance and all associated information from our database.
   * Highly likely to cause undesired data loss. Use with caution.
   *
   * @param cls closure
   * @param merchant_id identifier of the instance
   * @return database result code
   */
  enum GNUNET_DB_QueryStatus
  (*purge_instance)(void *cls,
                    const char *merchant_id);

  /**
   * Update information about an instance into our database.
   *
   * @param cls closure
   * @param is details about the instance
   * @return database result code
   */
  enum GNUNET_DB_QueryStatus
  (*update_instance)(void *cls,
                     const struct TALER_MERCHANTDB_InstanceSettings *is);

  /**
   * Update information about an instance's authentication settings
   * into our database.
   *
   * @param cls closure
   * @param merchant_id merchant backend instance ID
   * @param ias instance auth settings
   * @return database result code
   */
  enum GNUNET_DB_QueryStatus
  (*update_instance_auth)(
    void *cls,
    const char *merchant_id,
    const struct TALER_MERCHANTDB_InstanceAuthSettings *ias);

  /**
   * Set an instance's account in our database to "inactive".
   *
   * @param cls closure
   * @param merchant_id merchant backend instance ID
   * @param h_wire hash of the wire account to set to inactive
   * @return database result code
   */
  enum GNUNET_DB_QueryStatus
  (*inactivate_account)(void *cls,
                        const char *merchant_id,
                        const struct TALER_MerchantWireHash *h_wire);


  /**
   * Set an instance's account in our database to "active".
   *
   * @param cls closure
   * @param merchant_id merchant backend instance ID
   * @param h_wire hash of the wire account to set to active
   * @return database result code
   */
  enum GNUNET_DB_QueryStatus
  (*activate_account)(void *cls,
                      const char *merchant_id,
                      const struct TALER_MerchantWireHash *h_wire);


  /**
   * Check an instance's account's KYC status.
   *
   * @param cls closure
   * @param merchant_id merchant backend instance ID
   * @param h_wire hash of the wire account to check,
   *        NULL to check all accounts of the merchant
   * @param exchange_url base URL of the exchange to check,
   *        NULL to check all exchanges
   * @param kyc_cb KYC status callback to invoke
   * @param kyc_cb_cls closure for @a kyc_cb
   * @return database result code
   */
  enum GNUNET_DB_QueryStatus
  (*account_kyc_get_status)(void *cls,
                            const char *merchant_id,
                            const struct TALER_MerchantWireHash *h_wire,
                            const char *exchange_url,
                            TALER_MERCHANTDB_KycCallback kyc_cb,
                            void *kyc_cb_cls);

  /**
   * Update an instance's account's KYC status.
   *
   * @param cls closure
   * @param merchant_id merchant backend instance ID
   * @param h_wire hash of the wire account to check
   * @param exchange_url base URL of the exchange to check
   * @param exchange_kyc_serial serial number for our account at the exchange (0 if unknown)
   * @param exchange_sig signature of the exchange, or NULL for none
   * @param exchange_pub public key of the exchange, or NULL for none
   * @param timestamp timestamp to store
   * @param kyc_ok current KYC status (true for satisfied)
   * @return database result code
   */
  enum GNUNET_DB_QueryStatus
  (*account_kyc_set_status)(void *cls,
                            const char *merchant_id,
                            const struct TALER_MerchantWireHash *h_wire,
                            const char *exchange_url,
                            uint64_t exchange_kyc_serial,
                            const struct TALER_ExchangeSignatureP *exchange_sig,
                            const struct TALER_ExchangePublicKeyP *exchange_pub,
                            struct GNUNET_TIME_Timestamp timestamp,
                            bool kyc_ok);


  /**
   * Lookup all of the products the given instance has configured.
   *
   * @param cls closure
   * @param instance_id instance to lookup products for
   * @param cb function to call on all products found
   * @param cb_cls closure for @a cb
   * @return database result code
   */
  enum GNUNET_DB_QueryStatus
  (*lookup_products)(void *cls,
                     const char *instance_id,
                     TALER_MERCHANTDB_ProductsCallback cb,
                     void *cb_cls);

  /**
   * Lookup details about a particular product.
   *
   * @param cls closure
   * @param instance_id instance to lookup products for
   * @param product_id product to lookup
   * @param[out] pd set to the product details on success, can be NULL
   *             (in that case we only want to check if the product exists)
   * @return database result code
   */
  enum GNUNET_DB_QueryStatus
  (*lookup_product)(void *cls,
                    const char *instance_id,
                    const char *product_id,
                    struct TALER_MERCHANTDB_ProductDetails *pd);

  /**
   * Delete information about a product. Note that the transaction must
   * enforce that no stocks are currently locked.
   *
   * @param cls closure
   * @param instance_id instance to delete product of
   * @param product_id product to delete
   * @return DB status code, #GNUNET_DB_STATUS_SUCCESS_NO_RESULTS
   *           if locks prevent deletion OR product unknown
   */
  enum GNUNET_DB_QueryStatus
  (*delete_product)(void *cls,
                    const char *instance_id,
                    const char *product_id);

  /**
   * Insert details about a particular product.
   *
   * @param cls closure
   * @param instance_id instance to insert product for
   * @param product_id product identifier of product to insert
   * @param pd the product details to insert
   * @return database result code
   */
  enum GNUNET_DB_QueryStatus
  (*insert_product)(void *cls,
                    const char *instance_id,
                    const char *product_id,
                    const struct TALER_MERCHANTDB_ProductDetails *pd);

  /**
   * Update details about a particular product. Note that the
   * transaction must enforce that the sold/stocked/lost counters
   * are not reduced (i.e. by expanding the WHERE clause on the existing
   * values).
   *
   * @param cls closure
   * @param instance_id instance to lookup products for
   * @param product_id product to lookup
   * @param pd set to the product details on success, can be NULL
   *             (in that case we only want to check if the product exists);
   *             total_sold in @a pd is ignored (!), total_lost must not
   *             exceed total_stock minus the existing total_sold;
   *             total_sold and total_stock must be larger or equal to
   *             the existing value;
   * @return database result code, #GNUNET_DB_STATUS_SUCCESS_NO_RESULTS if the
   *         non-decreasing constraints are not met *or* if the product
   *         does not yet exist.
   */
  enum GNUNET_DB_QueryStatus
  (*update_product)(void *cls,
                    const char *instance_id,
                    const char *product_id,
                    const struct TALER_MERCHANTDB_ProductDetails *pd);

  /**
   * Lock stocks of a particular product. Note that the transaction must
   * enforce that the "stocked-sold-lost >= locked" constraint holds.
   *
   * @param cls closure
   * @param instance_id instance to lookup products for
   * @param product_id product to lookup
   * @param uuid the UUID that holds the lock
   * @param quantity how many units should be locked
   * @param expiration_time when should the lock expire
   * @return database result code, #GNUNET_DB_STATUS_SUCCESS_NO_RESULTS if the
   *         product is unknown OR if there insufficient stocks remaining
   */
  enum GNUNET_DB_QueryStatus
  (*lock_product)(void *cls,
                  const char *instance_id,
                  const char *product_id,
                  const struct GNUNET_Uuid *uuid,
                  uint64_t quantity,
                  struct GNUNET_TIME_Timestamp expiration_time);


  /**
   * Release all expired product locks, including
   * those from expired offers -- across all
   * instances.
   *
   * @param cls closure
   */
  void
  (*expire_locks)(void *cls);


  /**
   * Delete information about an order. Note that the transaction must
   * enforce that the order is not awaiting payment anymore.
   *
   * @param cls closure
   * @param instance_id instance to delete order of
   * @param order_id order to delete
   * @return DB status code, #GNUNET_DB_STATUS_SUCCESS_NO_RESULTS
   *           if locks prevent deletion OR order unknown
   */
  enum GNUNET_DB_QueryStatus
  (*delete_order)(void *cls,
                  const char *instance_id,
                  const char *order_id);


  /**
   * Retrieve order given its @a order_id and the @a instance_id.
   *
   * @param cls closure
   * @param instance_id instance to obtain order of
   * @param order_id order id used to perform the lookup
   * @param[out] claim_token the claim token generated for the order,
   *             NULL to only test if the order exists
   * @param[out] h_post_data set to the hash of the POST data that created the order
   * @param[out] contract_terms where to store the retrieved contract terms,
   *             NULL to only test if the order exists
   * @return transaction status
   */
  enum GNUNET_DB_QueryStatus
  (*lookup_order)(void *cls,
                  const char *instance_id,
                  const char *order_id,
                  struct TALER_ClaimTokenP *claim_token,
                  struct GNUNET_HashCode *h_post_data,
                  json_t **contract_terms);


  /**
   * Retrieve order summary given its @a order_id and the @a instance_id.
   *
   * @param cls closure
   * @param instance_id instance to obtain order of
   * @param order_id order id used to perform the lookup
   * @param[out] timestamp when was the order created
   * @param[out] order_serial under which serial do we keep this order
   * @return transaction status
   */
  enum GNUNET_DB_QueryStatus
  (*lookup_order_summary)(void *cls,
                          const char *instance_id,
                          const char *order_id,
                          struct GNUNET_TIME_Timestamp *timestamp,
                          uint64_t *order_serial);


  /**
   * Retrieve orders given the @a instance_id.
   *
   * @param cls closure
   * @param instance_id instance to obtain order of
   * @param of filter to apply when looking up orders
   * @param[out] contract_terms where to store the retrieved contract terms,
   *             NULL to only test if the order exists
   * @return transaction status
   */
  enum GNUNET_DB_QueryStatus
  (*lookup_orders)(void *cls,
                   const char *instance_id,
                   const struct TALER_MERCHANTDB_OrderFilter *of,
                   TALER_MERCHANTDB_OrdersCallback cb,
                   void *cb_cls);


  /**
   * Insert order into db.
   *
   * @param cls closure
   * @param instance_id identifies the instance responsible for the order
   * @param order_id alphanumeric string that uniquely identifies the order
   * @param h_post_data hash of the POST data for idempotency checks
   * @param pay_deadline how long does the customer have to pay for the order
   * @param claim_token token to use for access control
   * @param contract_terms proposal data to store
   * @return transaction status
   */
  enum GNUNET_DB_QueryStatus
  (*insert_order)(void *cls,
                  const char *instance_id,
                  const char *order_id,
                  const struct GNUNET_HashCode *h_post_data,
                  struct GNUNET_TIME_Timestamp pay_deadline,
                  const struct TALER_ClaimTokenP *claim_token,
                  const json_t *contract_terms);


  /**
   * Release an inventory lock by UUID. Releases ALL stocks locked under
   * the given UUID.
   *
   * @param cls closure
   * @param uuid the UUID to release locks for
   * @return transaction status,
   *   #GNUNET_DB_STATUS_SUCCESS_NO_RESULTS means there are no locks under @a uuid
   *   #GNUNET_DB_STATUS_SUCCESS_ONE_RESULT indicates success
   */
  enum GNUNET_DB_QueryStatus
  (*unlock_inventory)(void *cls,
                      const struct GNUNET_Uuid *uuid);


  /**
   * Lock inventory stock to a particular order.
   *
   * @param cls closure
   * @param instance_id identifies the instance responsible for the order
   * @param order_id alphanumeric string that uniquely identifies the order
   * @param product_id uniquely identifies the product to be locked
   * @param quantity how many units should be locked to the @a order_id
   * @return transaction status,
   *   #GNUNET_DB_STATUS_SUCCESS_NO_RESULTS means there are insufficient stocks
   *   #GNUNET_DB_STATUS_SUCCESS_ONE_RESULT indicates success
   */
  enum GNUNET_DB_QueryStatus
  (*insert_order_lock)(void *cls,
                       const char *instance_id,
                       const char *order_id,
                       const char *product_id,
                       uint64_t quantity);


  /**
   * Retrieve contract terms given its @a order_id
   *
   * @param cls closure
   * @param instance_id instance's identifier
   * @param order_id order_id used to lookup.
   * @param[out] contract_terms where to store the result, NULL to only check for existence
   * @param[out] order_serial set to the order's serial number
   * @param[out] claim_token set to the claim token, NULL to only check for existence
   * @return transaction status
   */
  enum GNUNET_DB_QueryStatus
  (*lookup_contract_terms)(void *cls,
                           const char *instance_id,
                           const char *order_id,
                           json_t **contract_terms,
                           uint64_t *order_serial,
                           struct TALER_ClaimTokenP *claim_token);


  /**
   * Store contract terms given its @a order_id. Note that some attributes are
   * expected to be calculated inside of the function, like the hash of the
   * contract terms (to be hashed), the creation_time and pay_deadline (to be
   * obtained from the merchant_orders table). The "session_id" should be
   * initially set to the empty string.  The "fulfillment_url" and "refund_deadline"
   * must be extracted from @a contract_terms.
   *
   * @param cls closure
   * @param instance_id instance's identifier
   * @param order_id order_id used to store
   * @param claim_token the token belonging to the order
   * @return transaction status, #GNUNET_DB_STATUS_HARD_ERROR if @a contract_terms
   *          is malformed
   */
  enum GNUNET_DB_QueryStatus
  (*insert_contract_terms)(void *cls,
                           const char *instance_id,
                           const char *order_id,
                           json_t *contract_terms);


  /**
   * Update the contract terms stored for @a order_id. Note that some attributes are
   * expected to be calculated inside of the function, like the hash of the
   * contract terms (to be hashed), the creation_time and pay_deadline (to be
   * obtained from the merchant_orders table). The "session_id" should be
   * initially set to the empty string.  The "fulfillment_url" and "refund_deadline"
   * must be extracted from @a contract_terms.
   *
   * @param cls closure
   * @param instance_id instance's identifier
   * @param order_id order_id used to store
   * @param contract_terms contract to store
   * @return transaction status, #GNUNET_DB_STATUS_HARD_ERROR if @a contract_terms
   *          is malformed
   */
  enum GNUNET_DB_QueryStatus
  (*update_contract_terms)(void *cls,
                           const char *instance_id,
                           const char *order_id,
                           json_t *contract_terms);


  /**
   * Delete information about a contract. Note that the transaction must
   * enforce that the contract is not awaiting payment anymore AND was not
   * paid, or is past the legal expiration.
   *
   * @param cls closure
   * @param instance_id instance to delete order of
   * @param order_id order to delete
   * @param legal_expiration how long do we need to keep (paid) contracts on
   *          file for legal reasons (i.e. taxation)
   * @return DB status code, #GNUNET_DB_STATUS_SUCCESS_NO_RESULTS
   *           if locks prevent deletion OR order unknown
   */
  enum GNUNET_DB_QueryStatus
  (*delete_contract_terms)(void *cls,
                           const char *instance_id,
                           const char *order_id,
                           struct GNUNET_TIME_Relative legal_expiration);


  /**
   * Lookup information about coins that were successfully deposited for a
   * given contract.
   *
   * @param cls closure
   * @param instance_id instance to lookup deposits for
   * @param h_contract_terms proposal data's hashcode
   * @param cb function to call with payment data
   * @param cb_cls closure for @a cb
   * @return transaction status
   */
  enum GNUNET_DB_QueryStatus
  (*lookup_deposits)(void *cls,
                     const char *instance_id,
                     const struct TALER_PrivateContractHash *h_contract_terms,
                     TALER_MERCHANTDB_DepositsCallback cb,
                     void *cb_cls);


  /**
   * Insert an exchange signing key into our database.
   *
   * @param cls closure
   * @param master_pub exchange master public key used for @a master_sig
   * @param exchange_pub exchange signing key to insert
   * @param start_date when does the signing key become valid
   * @param expire_date when does the signing key stop being used
   * @param end_date when does the signing key become void as proof
   * @param master_sig signature of @a master_pub over the @a exchange_pub and the dates
   */
  enum GNUNET_DB_QueryStatus
  (*insert_exchange_signkey)(
    void *cls,
    const struct TALER_MasterPublicKeyP *master_pub,
    const struct TALER_ExchangePublicKeyP *exchange_pub,
    struct GNUNET_TIME_Timestamp start_date,
    struct GNUNET_TIME_Timestamp expire_date,
    struct GNUNET_TIME_Timestamp end_date,
    const struct TALER_MasterSignatureP *master_sig);


  /**
   * Insert payment confirmation from the exchange into the database.
   *
   * @param cls closure
   * @param instance_id instance to lookup deposits for
   * @param deposit_timestamp time when the exchange generated the deposit confirmation
   * @param h_contract_terms proposal data's hashcode
   * @param coin_pub public key of the coin
   * @param exchange_url URL of the exchange that issued @a coin_pub
   * @param amount_with_fee amount the exchange will deposit for this coin
   * @param deposit_fee fee the exchange will charge for this coin
   * @param wire_fee wire fee the exchange charges
   * @param h_wire hash of the wire details of the target account of the merchant
   * @param exchange_sig signature from exchange that coin was accepted
   * @param exchange_pub signing key that was used for @a exchange_sig
   * @return transaction status
   */
  enum GNUNET_DB_QueryStatus
  (*insert_deposit)(void *cls,
                    const char *instance_id,
                    struct GNUNET_TIME_Timestamp deposit_timestamp,
                    const struct TALER_PrivateContractHash *h_contract_terms,
                    const struct TALER_CoinSpendPublicKeyP *coin_pub,
                    const char *exchange_url,
                    const struct TALER_Amount *amount_with_fee,
                    const struct TALER_Amount *deposit_fee,
                    const struct TALER_Amount *refund_fee,
                    const struct TALER_Amount *wire_fee,
                    const struct TALER_MerchantWireHash *h_wire,
                    const struct TALER_ExchangeSignatureP *exchange_sig,
                    const struct TALER_ExchangePublicKeyP *exchange_pub);


  /**
   * Obtain refunds associated with a contract.
   *
   * @param cls closure, typically a connection to the db
   * @param instance_id instance to lookup refunds for
   * @param h_contract_terms hash code of the contract
   * @param rc function to call for each coin on which there is a refund
   * @param rc_cls closure for @a rc
   * @return transaction status
   */
  enum GNUNET_DB_QueryStatus
  (*lookup_refunds)(void *cls,
                    const char *instance_id,
                    const struct TALER_PrivateContractHash *h_contract_terms,
                    TALER_MERCHANTDB_RefundCallback rc,
                    void *rc_cls);


  /**
   * Mark contract as paid and store the current @a session_id
   * for which the contract was paid. Deletes the underlying order
   * and marks the locked stocks of the order as sold.
   *
   * @param cls closure
   * @param instance_id instance to mark contract as paid for
   * @param h_contract_terms hash of the contract that is now paid
   * @param session_id the session that paid the contract
   * @return transaction status
   */
  enum GNUNET_DB_QueryStatus
  (*mark_contract_paid)(void *cls,
                        const char *instance_id,
                        const struct
                        TALER_PrivateContractHash *h_contract_terms,
                        const char *session_id);


  /**
   * Function called during aborts to refund a coin. Marks the
   * respective coin as refunded.
   *
   * @param cls closure
   * @param instance_id instance to refund payment for
   * @param h_contract_terms hash of the contract to refund coin for
   * @param refund_timestamp timestamp of when the coin was refunded
   * @param coin_pub public key of the coin to refund (fully)
   * @param reason text justifying the refund
   * @return transaction status
   *        #GNUNET_DB_STATUS_SUCCESS_NO_RESULTS if @a coin_pub is unknown to us;
   *        #GNUNET_DB_STATUS_SUCCESS_ONE_RESULT if the request is valid,
   *        regardless of whether it actually increased the refund
   */
  enum GNUNET_DB_QueryStatus
  (*refund_coin)(void *cls,
                 const char *instance_id,
                 const struct TALER_PrivateContractHash *h_contract_terms,
                 struct GNUNET_TIME_Timestamp refund_timestamp,
                 const struct TALER_CoinSpendPublicKeyP *coin_pub,
                 const char *reason);


  /**
   * Retrieve contract terms given its @a order_id
   *
   * @param cls closure
   * @param instance_id instance's identifier
   * @param order_id order to lookup contract for
   * @param[out] h_contract_terms set to the hash of the contract.
   * @param[out] paid set to the payment status of the contract
   * @return transaction status
   */
  enum GNUNET_DB_QueryStatus
  (*lookup_order_status)(void *cls,
                         const char *instance_id,
                         const char *order_id,
                         struct TALER_PrivateContractHash *h_contract_terms,
                         bool *paid);

  /**
   * Retrieve contract terms given its @a order_serial
   *
   * @param cls closure
   * @param instance_id instance's identifier
   * @param order_serial serial ID of the order to look up
   * @param[out] order_id set to ID of the order
   * @param[out] h_contract_terms set to the hash of the contract.
   * @param[out] paid set to the payment status of the contract
   * @return transaction status
   */
  enum GNUNET_DB_QueryStatus
  (*lookup_order_status_by_serial)(void *cls,
                                   const char *instance_id,
                                   uint64_t order_serial,
                                   char **order_id,
                                   struct TALER_PrivateContractHash *
                                   h_contract_terms,
                                   bool *paid);


  /**
   * Retrieve payment and wire status for a given @a order_serial and
   * session ID.
   *
   * @param cls closure
   * @param order_serial identifies the order
   * @param session_id session for which to check the payment status, NULL for any
   * @param[out] paid set to the payment status of the contract
   * @param[out] wired set to the wire transfer status of the exchange payment
   * @return transaction status
   */
  enum GNUNET_DB_QueryStatus
  (*lookup_payment_status)(void *cls,
                           uint64_t order_serial,
                           const char *session_id,
                           bool *paid,
                           bool *wired);


  /**
   * Retrieve details about coins that were deposited for an order.
   *
   * @param cls closure
   * @param order_serial identifies the order
   * @param cb function to call for each deposited coin
   * @param cb_cls closure for @a cb
   * @return transaction status
   */
  enum GNUNET_DB_QueryStatus
  (*lookup_deposits_by_order)(void *cls,
                              uint64_t order_serial,
                              TALER_MERCHANTDB_DepositedCoinsCallback cb,
                              void *cb_cls);


  /**
   * Retrieve wire transfer details for all deposits associated with
   * a given @a order_serial.
   *
   * @param cls closure
   * @param order_serial identifies the order
   * @param cb function called with the wire transfer details
   * @param cb_cls closure for @a cb
   * @return transaction status
   */
  enum GNUNET_DB_QueryStatus
  (*lookup_transfer_details_by_order)(
    void *cls,
    uint64_t order_serial,
    TALER_MERCHANTDB_OrderTransferDetailsCallback cb,
    void *cb_cls);


  /**
   * Insert wire transfer details for a deposit.
   *
   * @param cls closure
   * @param deposit_serial serial number of the deposit
   * @param dd deposit transfer data from the exchange to store
   * @return transaction status
   */
  enum GNUNET_DB_QueryStatus
  (*insert_deposit_to_transfer)(void *cls,
                                uint64_t deposit_serial,
                                const struct TALER_EXCHANGE_DepositData *dd);


  /**
   * Set 'wired' status for an order to 'true'.
   *
   * @param cls closure
   * @param order_serial serial number of the order
   * @return transaction status
   */
  enum GNUNET_DB_QueryStatus
  (*mark_order_wired)(void *cls,
                      uint64_t order_serial);


  /**
   * Function called when some backoffice staff decides to award or
   * increase the refund on an existing contract.  This function
   * MUST be called from within a transaction scope setup by the
   * caller as it executes multiple SQL statements.
   *
   * @param cls closure
   * @param instance_id instance identifier
   * @param order_id the order to increase the refund for
   * @param refund maximum refund to return to the customer for this contract
   * @param reason 0-terminated UTF-8 string giving the reason why the customer
   *               got a refund (free form, business-specific)
   * @return transaction status
   *        #GNUNET_DB_STATUS_SUCCESS_NO_RESULTS if @a refund is ABOVE the amount we
   *        were originally paid and thus the transaction failed;
   *        #GNUNET_DB_STATUS_SUCCESS_ONE_RESULT if the request is valid,
   *        regardless of whether it actually increased the refund beyond
   *        what was already refunded (idempotency!)
   */
  enum TALER_MERCHANTDB_RefundStatus
  (*increase_refund)(void *cls,
                     const char *instance_id,
                     const char *order_id,
                     const struct TALER_Amount *refund,
                     const char *reason);


  /**
   * Obtain detailed refund data associated with a contract.
   *
   * @param cls closure, typically a connection to the db
   * @param instance_id instance to lookup refunds for
   * @param h_contract_terms hash code of the contract
   * @param rc function to call for each coin on which there is a refund
   * @param rc_cls closure for @a rc
   * @return transaction status
   */
  enum GNUNET_DB_QueryStatus
  (*lookup_refunds_detailed)(void *cls,
                             const char *instance_id,
                             const struct
                             TALER_PrivateContractHash *h_contract_terms,
                             TALER_MERCHANTDB_RefundDetailCallback rc,
                             void *rc_cls);

  /**
   * Insert refund proof data from the exchange into the database.
   *
   * @param cls closure
   * @param refund_serial serial number of the refund
   * @param exchange_sig signature from exchange that coin was refunded
   * @param exchange_pub signing key that was used for @a exchange_sig
   * @return transaction status
   */
  enum GNUNET_DB_QueryStatus
  (*insert_refund_proof)(void *cls,
                         uint64_t refund_serial,
                         const struct TALER_ExchangeSignatureP *exchange_sig,
                         const struct TALER_ExchangePublicKeyP *exchange_pub);


  /**
   * Lookup refund proof data.
   *
   * @param cls closure
   * @param refund_serial serial number of the refund
   * @param[out] exchange_sig set to signature from exchange
   * @param[out] exchange_pub signing key that was used for @a exchange_sig
   * @return transaction status
   */
  enum GNUNET_DB_QueryStatus
  (*lookup_refund_proof)(void *cls,
                         uint64_t refund_serial,
                         struct TALER_ExchangeSignatureP *exchange_sig,
                         struct TALER_ExchangePublicKeyP *exchange_pub);


  /**
   * Retrieve the order ID that was used to pay for a resource within a session.
   *
   * @param cls closure
   * @param instance_id instance to lookup the order from
   * @param fulfillment_url URL that canonically identifies the resource
   *        being paid for
   * @param session_id session id
   * @param[out] order_id location to store the order ID that was used when
   *             paying for the resource URL
   * @return transaction status
   */
  enum GNUNET_DB_QueryStatus
  (*lookup_order_by_fulfillment)(void *cls,
                                 const char *instance_id,
                                 const char *fulfillment_url,
                                 const char *session_id,
                                 char **order_id);

  /**
   * Insert information about a wire transfer the merchant has received.
   *
   * @param cls closure
   * @param instance_id instance to lookup the order from
   * @param exchange_url which exchange made the transfer
   * @param wtid identifier of the wire transfer
   * @param credit_amount how much did we receive
   * @param payto_uri what is the merchant's bank account that received the transfer
   * @param confirmed whether the transfer was confirmed by the merchant or
   *                  was merely claimed by the exchange at this point
   * @return transaction status
   */
  enum GNUNET_DB_QueryStatus
  (*insert_transfer)(
    void *cls,
    const char *instance_id,
    const char *exchange_url,
    const struct TALER_WireTransferIdentifierRawP *wtid,
    const struct TALER_Amount *credit_amount,
    const char *payto_uri,
    bool confirmed);


  /**
   * Delete information about a transfer. Note that transfers
   * confirmed by the exchange cannot be deleted anymore.
   *
   * @param cls closure
   * @param instance_id instance to delete transfer of
   * @param transfer_serial_id transfer to delete
   * @return DB status code, #GNUNET_DB_STATUS_SUCCESS_NO_RESULTS
   *           if deletion is prohibited OR transfer is unknown
   */
  enum GNUNET_DB_QueryStatus
  (*delete_transfer)(void *cls,
                     const char *instance_id,
                     uint64_t transfer_serial_id);


  /**
   * Check if information about a transfer exists with the
   * backend.  Returns no data, only the query status.
   *
   * @param cls closure
   * @param instance_id instance to delete transfer of
   * @param transfer_serial_id transfer to delete
   * @return DB status code, #GNUNET_DB_STATUS_SUCCESS_ONE_RESULT
   *           if the transfer record exists
   */
  enum GNUNET_DB_QueryStatus
  (*check_transfer_exists)(void *cls,
                           const char *instance_id,
                           uint64_t transfer_serial_id);


  /**
   * Lookup account serial by payto URI.
   *
   * @param cls closure
   * @param instance_id instance to lookup the account from
   * @param payto_uri what is the merchant's bank account to lookup
   * @param[out] account_serial serial number of the account
   * @return transaction status
   */
  enum GNUNET_DB_QueryStatus
  (*lookup_account)(void *cls,
                    const char *instance_id,
                    const char *payto_uri,
                    uint64_t *account_serial);


  /**
   * Insert information about a wire transfer the merchant has received.
   *
   * @param cls closure
   * @param instance_id instance to provide transfer details for
   * @param exchange_url which exchange made the transfer
   * @param payto_uri what is the merchant's bank account that received the transfer
   * @param wtid identifier of the wire transfer
   * @param td transfer details to store
   * @return transaction status,
   *   #GNUNET_DB_STATUS_SUCCESS_NO_RESULTS if the @a wtid and @a exchange_uri are not known for this @a instance_id
   *   #GNUNET_DB_STATUS_SUCCESS_ONE_RESULT on success
   */
  enum GNUNET_DB_QueryStatus
  (*insert_transfer_details)(
    void *cls,
    const char *instance_id,
    const char *exchange_url,
    const char *payto_uri,
    const struct TALER_WireTransferIdentifierRawP *wtid,
    const struct TALER_EXCHANGE_TransferData *td);


  /**
   * Obtain information about wire fees charged by an exchange,
   * including signature (so we have proof).
   *
   * @param cls closure
   * @param master_pub master public key of the exchange
   * @param h_wire_method hash of wire method
   * @param contract_date date of the contract to use for the lookup
   * @param[out] wire_fee wire fee charged
   * @param[out] closing_fee closing fee charged (irrelevant for us,
   *              but needed to check signature)
   * @param[out] start_date start of fee being used
   * @param[out] end_date end of fee being used
   * @param[out] master_sig signature of exchange over fee structure
   * @return transaction status code
   */
  enum GNUNET_DB_QueryStatus
  (*lookup_wire_fee)(void *cls,
                     const struct TALER_MasterPublicKeyP *master_pub,
                     const char *wire_method,
                     struct GNUNET_TIME_Timestamp contract_date,
                     struct TALER_Amount *wire_fee,
                     struct TALER_Amount *closing_fee,
                     struct GNUNET_TIME_Timestamp *start_date,
                     struct GNUNET_TIME_Timestamp *end_date,
                     struct TALER_MasterSignatureP *master_sig);


  /**
   * Lookup information about coin payments by @a h_contract_terms and
   * @a coin_pub.
   *
   * @param cls closure
   * @param instance_id instance to lookup payments for
   * @param h_contract_terms proposal data's hashcode
   * @param coin_pub public key to use for the search
   * @param cb function to call with payment data
   * @param cb_cls closure for @a cb
   * @return transaction status
   */
  enum GNUNET_DB_QueryStatus
  (*lookup_deposits_by_contract_and_coin)(
    void *cls,
    const char *instance_id,
    const struct TALER_PrivateContractHash *h_contract_terms,
    const struct TALER_CoinSpendPublicKeyP *coin_pub,
    TALER_MERCHANTDB_CoinDepositCallback cb,
    void *cb_cls);


  /**
   * Lookup transfer status.
   *
   * @param cls closure
   * @param instance_id the instance to look up details at
   * @param exchange_url the exchange that made the transfer
   * @param payto_uri account that received the transfer
   * @param wtid wire transfer subject
   * @param[out] total_amount amount that was debited from our
   *             aggregate balance at the exchange (in total, sum of
   *             the wire transfer amount and the @a wire_fee)
   * @param[out] wire_fee the wire fee the exchange charged (only set if @a have_exchange_sig is true)
   * @param[out] exchange_amount the amount the exchange claims was transferred (only set if @a have_exchange_sig is true)
   * @param[out] execution_time when the transfer was executed by the exchange (only set if @a have_exchange_sig is true)
   * @param[out] have_exchange_sig do we have a response from the exchange about this transfer
   * @param[out] verified did we confirm the transfer was OK
   * @return transaction status
   */
  enum GNUNET_DB_QueryStatus
  (*lookup_transfer)(
    void *cls,
    const char *instance_id,
    const char *exchange_url,
    const struct TALER_WireTransferIdentifierRawP *wtid,
    struct TALER_Amount *total_amount,
    struct TALER_Amount *wire_fee,
    struct TALER_Amount *exchange_amount,
    struct GNUNET_TIME_Timestamp *execution_time,
    bool *have_exchange_sig,
    bool *verified);


  /**
   * Set transfer status to verified.
   *
   * @param cls closure
   * @param instance_id instance to lookup payments for
   * @param exchange_url the exchange that made the transfer
   * @param payto_uri account that received the transfer
   * @param wtid wire transfer subject
   * @return transaction status
   */
  enum GNUNET_DB_QueryStatus
  (*set_transfer_status_to_verified)(
    void *cls,
    const char *exchange_url,
    const struct TALER_WireTransferIdentifierRawP *wtid);


  /**
   * Lookup transfer summary (used if we already verified the details).
   *
   * @param cls closure
   * @param instance_id instance to lookup payments for
   * @param exchange_url the exchange that made the transfer
   * @param payto_uri account that received the transfer
   * @param wtid wire transfer subject
   * @param cb function to call with detailed transfer data
   * @param cb_cls closure for @a cb
   * @return transaction status
   */
  enum GNUNET_DB_QueryStatus
  (*lookup_transfer_summary)(
    void *cls,
    const char *exchange_url,
    const struct TALER_WireTransferIdentifierRawP *wtid,
    TALER_MERCHANTDB_TransferSummaryCallback cb,
    void *cb_cls);


  /**
   * Lookup transfer details. Used if we still need to verify the details.
   *
   * @param cls closure
   * @param instance_id instance to lookup payments for
   * @param exchange_url the exchange that made the transfer
   * @param payto_uri account that received the transfer
   * @param wtid wire transfer subject
   * @param cb function to call with detailed transfer data
   * @param cb_cls closure for @a cb
   * @return transaction status
   */
  enum GNUNET_DB_QueryStatus
  (*lookup_transfer_details)(
    void *cls,
    const char *exchange_url,
    const struct TALER_WireTransferIdentifierRawP *wtid,
    TALER_MERCHANTDB_TransferDetailsCallback cb,
    void *cb_cls);


  /**
   * Lookup transfers.
   *
   * @param cls closure
   * @param instance_id instance to lookup payments for
   * @param payto_uri account that we are interested in transfers to
   * @param before timestamp for the earliest transfer we care about
   * @param after timestamp for the last transfer we care about
   * @param limit number of entries to return, negative for descending in execution time,
   *                positive for ascending in execution time
   * @param offset transfer_serial number of the transfer we want to offset from
   * @param verified filter transfers by verification status
   * @param cb function to call with detailed transfer data
   * @param cb_cls closure for @a cb
   * @return transaction status
   */
  enum GNUNET_DB_QueryStatus
  (*lookup_transfers)(void *cls,
                      const char *instance_id,
                      const char *payto_uri,
                      struct GNUNET_TIME_Timestamp before,
                      struct GNUNET_TIME_Timestamp after,
                      int64_t limit,
                      uint64_t offset,
                      enum TALER_EXCHANGE_YesNoAll yna,
                      TALER_MERCHANTDB_TransferCallback cb,
                      void *cb_cls);


  /**
   * Store information about wire fees charged by an exchange,
   * including signature (so we have proof).
   *
   * @param cls closure
   * @param exchange_pub public key of the exchange
   * @param h_wire_method hash of wire method
   * @param wire_fee wire fee charged
   * @param closing_fee closing fee charged (irrelevant for us,
   *              but needed to check signature)
   * @param start_date start of fee being used
   * @param end_date end of fee being used
   * @param exchange_sig signature of exchange over fee structure
   * @return transaction status code
   */
  enum GNUNET_DB_QueryStatus
  (*store_wire_fee_by_exchange)(
    void *cls,
    const struct TALER_MasterPublicKeyP *exchange_pub,
    const struct GNUNET_HashCode *h_wire_method,
    const struct TALER_Amount *wire_fee,
    const struct TALER_Amount *closing_fee,
    struct GNUNET_TIME_Timestamp start_date,
    struct GNUNET_TIME_Timestamp end_date,
    const struct TALER_MasterSignatureP *exchange_sig);


  /**
   * Add @a credit to a reserve to be used for tipping.  Note that
   * this function does not actually perform any wire transfers to
   * credit the reserve, it merely tells the merchant backend that
   * a reserve now exists.  This has to happen before tips can be
   * authorized.
   *
   * @param cls closure, typically a connection to the db
   * @param instance_id which instance is the reserve tied to
   * @param reserve_priv which reserve is topped up or created
   * @param reserve_pub which reserve is topped up or created
   * @param exchange_url what URL is the exchange reachable at where the reserve is located
   * @param payto_uri URI to fund the reserve
   * @param initial_balance how much money will be added to the reserve
   * @param expiration when does the reserve expire?
   * @return transaction status, usually
   *      #GNUNET_DB_STATUS_SUCCESS_ONE_RESULT for success
   */
  enum TALER_ErrorCode
  (*insert_reserve)(void *cls,
                    const char *instance_id,
                    const struct TALER_ReservePrivateKeyP *reserve_priv,
                    const struct TALER_ReservePublicKeyP *reserve_pub,
                    const char *exchange_url,
                    const char *payto_uri,
                    const struct TALER_Amount *initial_balance,
                    struct GNUNET_TIME_Timestamp expiration);


  /**
   * Confirms @a credit as the amount the exchange claims to have received and
   * thus really 'activates' the reserve.  This has to happen before tips can
   * be authorized.
   *
   * @param cls closure, typically a connection to the db
   * @param instance_id which instance is the reserve tied to
   * @param reserve_pub which reserve is topped up or created
   * @param initial_exchange_balance how much money was be added to the reserve
   *           according to the exchange
   * @return transaction status, usually
   *      #GNUNET_DB_STATUS_SUCCESS_ONE_RESULT for success
   */
  enum GNUNET_DB_QueryStatus
  (*activate_reserve)(void *cls,
                      const char *instance_id,
                      const struct TALER_ReservePublicKeyP *reserve_pub,
                      const struct TALER_Amount *initial_exchange_balance);


  /**
   * Lookup reserves.
   *
   * @param cls closure
   * @param instance_id instance to lookup payments for
   * @param created_after filter by reserves created after this date
   * @param active filter by active reserves
   * @param failures filter by reserves with a disagreement on the initial balance
   * @param cb function to call with reserve summary data
   * @param cb_cls closure for @a cb
   * @return transaction status
   */
  enum GNUNET_DB_QueryStatus
  (*lookup_reserves)(void *cls,
                     const char *instance_id,
                     struct GNUNET_TIME_Timestamp created_after,
                     enum TALER_EXCHANGE_YesNoAll active,
                     enum TALER_EXCHANGE_YesNoAll failures,
                     TALER_MERCHANTDB_ReservesCallback cb,
                     void *cb_cls);


  /**
   * Lookup reserves pending activation across all instances.
   *
   * @param cls closure
   * @param cb function to call with reserve data
   * @param cb_cls closure for @a cb
   * @return transaction status
   */
  enum GNUNET_DB_QueryStatus
  (*lookup_pending_reserves)(void *cls,
                             TALER_MERCHANTDB_PendingReservesCallback cb,
                             void *cb_cls);


  /**
   * Lookup reserve details.
   *
   * @param cls closure
   * @param instance_id instance to lookup payments for
   * @param reserve_pub public key of the reserve to inspect
   * @param fetch_tips if true, also return information about tips
   * @param cb function to call with reserve summary data
   * @param cb_cls closure for @a cb
   * @return transaction status
   */
  enum GNUNET_DB_QueryStatus
  (*lookup_reserve)(void *cls,
                    const char *instance_id,
                    const struct TALER_ReservePublicKeyP *reserve_pub,
                    bool fetch_tips,
                    TALER_MERCHANTDB_ReserveDetailsCallback cb,
                    void *cb_cls);


  /**
   * Delete private key of a reserve.
   *
   * @param cls closure
   * @param instance_id instance to lookup payments for
   * @param reserve_pub public key of the reserve to delete
   * @return transaction status
   */
  enum GNUNET_DB_QueryStatus
  (*delete_reserve)(void *cls,
                    const char *instance_id,
                    const struct TALER_ReservePublicKeyP *reserve_pub);

  /**
   * Purge all information about a reserve (including tips from it).
   *
   * @param cls closure
   * @param instance_id instance to lookup payments for
   * @param reserve_pub public key of the reserve to purge
   * @return transaction status
   */
  enum GNUNET_DB_QueryStatus
  (*purge_reserve)(void *cls,
                   const char *instance_id,
                   const struct TALER_ReservePublicKeyP *reserve_pub);


  /**
   * Authorize a tip over @a amount from reserve @a reserve_pub.  Remember
   * the authorization under @a tip_id for later, together with the
   * @a justification.
   *
   * @param cls closure, typically a connection to the db
   * @param instance_id which instance should generate the tip
   * @param reserve_pub which reserve is debited, NULL to pick one in the DB
   * @param amount how high is the tip (with fees)
   * @param justification why was the tip approved
   * @param next_url where to send the URL post tip pickup
   * @param[out] tip_id set to the unique ID for the tip
   * @param[out] expiration set to when the tip expires
   * @return transaction status,
   *      #TALER_EC_MERCHANT_PRIVATE_POST_TIP_AUTHORIZE_RESERVE_EXPIRED if the reserve is known but has expired
   *      #TALER_EC_MERCHANT_PRIVATE_POST_TIP_AUTHORIZE_RESERVE_NOT_FOUND if the reserve is not known
   *      #TALER_EC_MERCHANT_PRIVATE_POST_TIP_AUTHORIZE_INSUFFICIENT_FUNDS if the reserve has insufficient funds left
   *      #TALER_EC_GENERIC_DB_START_FAILED on hard DB errors
   *      #TALER_EC_GENERIC_DB_FETCH_FAILED on hard DB errors
   *      #TALER_EC_GENERIC_DB_STORE_FAILED on hard DB errors
   *      #TALER_EC_GENERIC_DB_INVARIANT_FAILURE on hard DB errors
   *      #TALER_EC_GENERIC_DB_SOFT_FAILURE on soft DB errors (client should retry)
   *      #TALER_EC_NONE upon success
   */
  enum TALER_ErrorCode
  (*authorize_tip)(void *cls,
                   const char *instance_id,
                   const struct TALER_ReservePublicKeyP *reserve_pub,
                   const struct TALER_Amount *amount,
                   const char *justification,
                   const char *next_url,
                   struct GNUNET_HashCode *tip_id,
                   struct GNUNET_TIME_Timestamp *expiration);


  /**
   * Lookup pickup details for pickup @a pickup_id.
   *
   * @param cls closure, typically a connection to the db
   * @param instance_id which instance should we lookup tip details for
   * @param tip_id which tip should we lookup details on
   * @param pickup_id which pickup should we lookup details on
   * @param[out] exchange_url which exchange is the tip withdrawn from
   * @param[out] reserve_priv private key the tip is withdrawn from (set if still available!)
   * @param sigs_length length of the @a sigs array
   * @param[out] sigs set to the (blind) signatures we have for this @a pickup_id,
   *              those that are unavailable are left at NULL
   * @return transaction status
   */
  enum GNUNET_DB_QueryStatus
  (*lookup_pickup)(void *cls,
                   const char *instance_id,
                   const struct GNUNET_HashCode *tip_id,
                   const struct GNUNET_HashCode *pickup_id,
                   char **exchange_url,
                   struct TALER_ReservePrivateKeyP *reserve_priv,
                   unsigned int sigs_length,
                   struct TALER_BlindedDenominationSignature sigs[]);


  /**
   * Lookup tip details for tip @a tip_id.
   *
   * @param cls closure, typically a connection to the db
   * @param instance_id which instance should we lookup tip details for
   * @param tip_id which tip should we lookup details on
   * @param[out] total_authorized amount how high is the tip (with fees)
   * @param[out] total_picked_up how much of the tip was so far picked up (with fees)
   * @param[out] expiration set to when the tip expires
   * @param[out] exchange_url set to the exchange URL where the reserve is
   * @param[out] reserve_priv set to private key of reserve to be debited
   * @return transaction status
   */
  enum GNUNET_DB_QueryStatus
  (*lookup_tip)(void *cls,
                const char *instance_id,
                const struct GNUNET_HashCode *tip_id,
                struct TALER_Amount *total_authorized,
                struct TALER_Amount *total_picked_up,
                struct GNUNET_TIME_Timestamp *expiration,
                char **exchange_url,
                struct TALER_ReservePrivateKeyP *reserve_priv);


  /**
   * Lookup tips
   *
   * @param cls closure, typically a connection to the db
   * @param instance_id which instance should we lookup tips for
   * @param expired should we include expired tips?
   * @param limit maximum number of results to return, positive for
   *   ascending row id, negative for descending
   * @param offset row id to start returning results from
   * @param cb function to call with tip data
   * @param cb_cls closure for @a cb
   * @return transaction status
   */
  enum GNUNET_DB_QueryStatus
  (*lookup_tips)(void *cls,
                 const char *instance_id,
                 enum TALER_EXCHANGE_YesNoAll expired,
                 int64_t limit,
                 uint64_t offset,
                 TALER_MERCHANTDB_TipsCallback cb,
                 void *cb_cls);


  /**
   * Lookup tip details for tip @a tip_id.
   *
   * @param cls closure, typically a connection to the db
   * @param instance_id which instance should we lookup tip details for
   * @param tip_id which tip should we lookup details on
   * @param fpu should we fetch details about individual pickups
   * @param[out] total_authorized amount how high is the tip (with fees)
   * @param[out] total_picked_up how much of the tip was so far picked up (with fees)
   * @param[out] justification why was the tip approved
   * @param[out] expiration set to when the tip expires
   * @param[out] reserve_pub set to which reserve is debited
   * @param[out] pickups_length set to the length of @e pickups
   * @param[out] pickups if @a fpu is true, set to details about the pickup operations
   * @return transaction status
   */
  enum GNUNET_DB_QueryStatus
  (*lookup_tip_details)(void *cls,
                        const char *instance_id,
                        const struct GNUNET_HashCode *tip_id,
                        bool fpu,
                        struct TALER_Amount *total_authorized,
                        struct TALER_Amount *total_picked_up,
                        char **justification,
                        struct GNUNET_TIME_Timestamp *expiration,
                        struct TALER_ReservePublicKeyP *reserve_pub,
                        unsigned int *pickups_length,
                        struct TALER_MERCHANTDB_PickupDetails **pickups);


  /**
   * Insert details about a tip pickup operation.  The @a total_picked_up
   * UPDATES the total amount under the @a tip_id, while the @a total_requested
   * is the amount to be associated with this @a pickup_id.
   * While there is usually only one pickup event that picks up the entire
   * amount, our schema allows for wallets to pick up the amount incrementally
   * over multiple pick up operations.
   *
   * @param cls closure, typically a connection to the db
   * @param tip_id the unique ID for the tip
   * @param total_picked_up how much was picked up overall at this
   *          point (includes @a total_requested)
   * @param pickup_id unique ID for the operation
   * @param total_requested how much is being picked up in this operation
   * @return transaction status, usually
   *      #GNUNET_DB_STATUS_SUCCESS_ONE_RESULT for success
   *      #GNUNET_DB_STATUS_SUCCESS_NO_RESULTS if @a credit_uuid already known
   */
  enum GNUNET_DB_QueryStatus
  (*insert_pickup)(void *cls,
                   const char *instance_id,
                   const struct GNUNET_HashCode *tip_id,
                   const struct TALER_Amount *total_picked_up,
                   const struct GNUNET_HashCode *pickup_id,
                   const struct TALER_Amount *total_requested);


  /**
   * Insert blind signature obtained from the exchange during a
   * tip pickup operation.
   *
   * @param cls closure, typically a connection to the db
   * @param pickup_id unique ID for the operation
   * @param offset offset of the blind signature for the pickup
   * @param blind_sig the blind signature
   * @return transaction status, usually
   *      #GNUNET_DB_STATUS_SUCCESS_ONE_RESULT for success
   *      #GNUNET_DB_STATUS_SUCCESS_NO_RESULTS if @a credit_uuid already known
   */
  enum GNUNET_DB_QueryStatus
  (*insert_pickup_blind_signature)(
    void *cls,
    const struct GNUNET_HashCode *pickup_id,
    uint32_t offset,
    const struct TALER_BlindedDenominationSignature *blind_sig);

};

#endif
