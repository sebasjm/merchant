#!/bin/bash
# This file is in the public domain.

. initialize_taler_system.sh

echo -n "First, prepare wallet with coins..."
rm $WALLET_DB
taler-wallet-cli --no-throttle --wallet-db=$WALLET_DB api 'withdrawTestBalance' \
  "$(jq -n '
    {
        amount: "TESTKUDOS:99",
        bankBaseUrl: $BANK_URL,
        exchangeBaseUrl: $EXCHANGE_URL
    }' \
    --arg BANK_URL "$BANK_URL" \
    --arg EXCHANGE_URL "$EXCHANGE_URL"
  )" 2>wallet-withdraw-1.err >wallet-withdraw-1.out
taler-wallet-cli --wallet-db=$WALLET_DB run-until-done 2>wallet-withdraw-finish-1.err >wallet-withdraw-finish-1.out
echo " OK"

#
# CREATE INSTANCE FOR TESTING
#

echo -n "Configuring merchant instance ..."

# create with 2 address
STATUS=$(curl -H "Content-Type: application/json" -X POST \
    -H 'Authorization: Bearer secret-token:super_secret' \
    http://localhost:9966/management/instances \
    -d '{"auth":{"method":"external"},"payto_uris":["payto://x-taler-bank/localhost:8082/43","payto://x-taler-bank/localhost:8082/44"],"id":"default","name":"default","address":{},"jurisdiction":{},"default_max_wire_fee":"TESTKUDOS:1", "default_max_deposit_fee":"TESTKUDOS:1","default_wire_fee_amortization":1,"default_wire_transfer_delay":{"d_ms" : 50000},"default_pay_delay":{"d_ms": 60000}}' \
    -w "%{http_code}" -s -o /dev/null)

if [ "$STATUS" != "204" ]
then
    echo 'should respond ok, instance created. got:' $STATUS
    exit 1
fi

# remove one account address
STATUS=$(curl -H "Content-Type: application/json" -X PATCH \
    -H 'Authorization: Bearer secret-token:super_secret' \
    http://localhost:9966/instances/default/private/ \
    -d '{"auth":{"method":"external"},"payto_uris":["payto://x-taler-bank/localhost:8082/43"],"id":"default","name":"default","address":{},"jurisdiction":{},"default_max_wire_fee":"TESTKUDOS:1", "default_max_deposit_fee":"TESTKUDOS:1","default_wire_fee_amortization":1,"default_wire_transfer_delay":{"d_ms" : 50000},"default_pay_delay":{"d_ms": 60000}}' \
    -w "%{http_code}" -s -o /dev/null)

if [ "$STATUS" != "204" ]
then
    echo 'should respond ok, instance updated. got:' $STATUS
    exit 1
fi

echo OK

NOW=`date +%s`
IN_TEN_SECS=`echo $(( $NOW + 5 ))`

echo -n "Creating order to be claimed..."
STATUS=$(curl 'http://localhost:9966/instances/default/private/orders' \
    -d '{"order":{"amount":"TESTKUDOS:1","summary":"payme","pay_deadline": {"t_ms":'$NOW'000}}}' \
    -w "%{http_code}" -s -o $LAST_RESPONSE)

if [ "$STATUS" != "200" ]
then
    echo 'should response ok, order created. got:' $STATUS `cat $LAST_RESPONSE`
    exit 1
fi

ORDER_ID=`jq -e -r .order_id < $LAST_RESPONSE`
TOKEN=`jq -e -r .token < $LAST_RESPONSE`

STATUS=$(curl "http://localhost:9966/instances/default/private/orders/${ORDER_ID}" \
    -w "%{http_code}" -s -o $LAST_RESPONSE)

if [ "$STATUS" != "200" ]
then
    echo 'should response ok, getting order info before claming it. got:' $STATUS `cat $LAST_RESPONSE`
    exit 1
fi

PAY_URL=`jq -e -r .taler_pay_uri < $LAST_RESPONSE`

echo "Ok (order $ORDER_ID)"

NOW=`date +%s`

echo -n "Claim the order but do not pay it ..."
taler-wallet-cli --no-throttle --wallet-db=$WALLET_DB advanced pay-prepare "${PAY_URL}" 2> wallet-pay1.err > wallet-pay1.log

STATUS=$(curl "http://localhost:9966/instances/default/private/orders/${ORDER_ID}" \
    -w "%{http_code}" -s -o $LAST_RESPONSE)

if [ "$STATUS" != "200" ]
then
    echo 'should response ok, after pay. got:' $STATUS `cat $LAST_RESPONSE`
    exit 1
fi

ORDER_STATUS=`jq -r .order_status < $LAST_RESPONSE`

if [ "$ORDER_STATUS" != "claimed" ]
then
    echo 'order should be paid. got:' $ORDER_STATUS `cat $LAST_RESPONSE`
    exit 1
fi
NOW2=`date +%s`
echo " OK (took $( echo -n $(($NOW2 - $NOW)) ) secs )"

echo 'wait 8 secs '
sleep 8

echo -n "Trying to get the list of orders..."
STATUS=$(curl "http://localhost:9966/instances/default/private/orders" \
    -w "%{http_code}" -s -o $LAST_RESPONSE)

if [ "$STATUS" != "200" ]
then
    echo 'should response ok. got:' $STATUS `cat $LAST_RESPONSE`
    exit 1
fi
echo "ok"

echo -n "Creating a new order that will trigger the db cleanup..."
STATUS=$(curl 'http://localhost:9966/instances/default/private/orders' \
    -d '{"order":{"amount":"TESTKUDOS:1","summary":"payme"}}' \
    -w "%{http_code}" -s -o $LAST_RESPONSE)

if [ "$STATUS" != "200" ]
then
    echo 'should response ok, order created. got:' $STATUS `cat $LAST_RESPONSE`
    exit 1
fi

ORDER_ID=`jq -e -r .order_id < $LAST_RESPONSE`
TOKEN=`jq -e -r .token < $LAST_RESPONSE`

STATUS=$(curl "http://localhost:9966/instances/default/private/orders/${ORDER_ID}" \
    -w "%{http_code}" -s -o $LAST_RESPONSE)

if [ "$STATUS" != "200" ]
then
    echo 'should response ok, getting order info before claming it. got:' $STATUS `cat $LAST_RESPONSE`
    exit 1
fi

PAY_URL=`jq -e -r .taler_pay_uri < $LAST_RESPONSE`

echo "Ok (order $ORDER_ID)"

echo -n "Trying to get the list of orders (issue #7025)..."
STATUS=$(curl "http://localhost:9966/instances/default/private/orders" \
    -w "%{http_code}" -s -o $LAST_RESPONSE)

if [ "$STATUS" != "200" ]
then
    echo 'should response ok. got:' $STATUS `cat $LAST_RESPONSE`
    exit 1
fi
echo ok

# set -x

echo -n "Now paying this order..."
taler-wallet-cli --no-throttle --wallet-db=$WALLET_DB handle-uri "${PAY_URL}" -y 2> wallet-pay1.err > wallet-pay1.log

STATUS=$(curl "http://localhost:9966/instances/default/private/orders/${ORDER_ID}" \
    -w "%{http_code}" -s -o $LAST_RESPONSE)

if [ "$STATUS" != "200" ]
then
    echo 'should response ok, after pay. got:' $STATUS `cat $LAST_RESPONSE`
    exit 1
fi

ORDER_STATUS=`jq -r .order_status < $LAST_RESPONSE`

if [ "$ORDER_STATUS" != "paid" ]
then
    echo 'order should be paid. got:' $ORDER_STATUS `cat $LAST_RESPONSE`
    exit 1
fi
NOW2=`date +%s`
echo " OK (took $( echo -n $(($NOW2 - $NOW)) ) secs )"


echo -n "Trying to get the list of orders..."
STATUS=$(curl "http://localhost:9966/instances/default/private/orders" \
    -w "%{http_code}" -s -o $LAST_RESPONSE)

if [ "$STATUS" != "200" ]
then
    echo 'should response ok. got:' $STATUS `cat $LAST_RESPONSE`
    exit 1
fi
echo ok


echo -n "Finally, create another order..."
STATUS=$(curl 'http://localhost:9966/instances/default/private/orders' \
    -d '{"order":{"amount":"TESTKUDOS:1","summary":"payme"}}' \
    -w "%{http_code}" -s -o $LAST_RESPONSE)

if [ "$STATUS" != "200" ]
then
    echo 'should response ok, order created. got:' $STATUS `cat $LAST_RESPONSE`
    exit 1
fi

ORDER_ID=`jq -e -r .order_id < $LAST_RESPONSE`
TOKEN=`jq -e -r .token < $LAST_RESPONSE`

STATUS=$(curl "http://localhost:9966/instances/default/private/orders/${ORDER_ID}" \
    -w "%{http_code}" -s -o $LAST_RESPONSE)

if [ "$STATUS" != "200" ]
then
    echo 'should response ok, getting order info before claming it. got:' $STATUS `cat $LAST_RESPONSE`
    exit 1
fi

PAY_URL=`jq -e -r .taler_pay_uri < $LAST_RESPONSE`

echo "Ok (order $ORDER_ID)"

echo -n "Now paying this order..."
taler-wallet-cli --no-throttle --wallet-db=$WALLET_DB handle-uri "${PAY_URL}" -y 2> wallet-pay1.err > wallet-pay1.log

STATUS=$(curl "http://localhost:9966/instances/default/private/orders/${ORDER_ID}" \
    -w "%{http_code}" -s -o $LAST_RESPONSE)

if [ "$STATUS" != "200" ]
then
    echo 'should response ok, after pay. got:' $STATUS `cat $LAST_RESPONSE`
    exit 1
fi

ORDER_STATUS=`jq -r .order_status < $LAST_RESPONSE`

if [ "$ORDER_STATUS" != "paid" ]
then
    echo 'order should be paid. got:' $ORDER_STATUS `cat $LAST_RESPONSE`
    exit 1
fi
NOW2=`date +%s`
echo " OK (took $( echo -n $(($NOW2 - $NOW)) ) secs )"

echo -n "Trying to get the list of orders..."
STATUS=$(curl "http://localhost:9966/instances/default/private/orders" \
    -w "%{http_code}" -s -o $LAST_RESPONSE)

if [ "$STATUS" != "200" ]
then
    echo 'should response ok. got:' $STATUS `cat $LAST_RESPONSE`
    exit 1
fi
echo ok

exit 0
