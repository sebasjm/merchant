/*
  This file is part of TALER
  Copyright (C) 2020 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3, or
  (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file testing_api_cmd_get_tips.c
 * @brief command to test GET /private/tips
 * @author Jonathan Buchanan
 */
#include "platform.h"
#include <taler/taler_exchange_service.h>
#include <taler/taler_testing_lib.h>
#include "taler_merchant_service.h"
#include "taler_merchant_testing_lib.h"


/**
 * State of a "GET tips" CMD.
 */
struct GetTipsState
{

  /**
   * Handle for a "GET tips" request.
   */
  struct TALER_MERCHANT_TipsGetHandle *tgh;

  /**
   * The interpreter state.
   */
  struct TALER_TESTING_Interpreter *is;

  /**
   * Base URL of the merchant serving the request.
   */
  const char *merchant_url;

  /**
   * Row to start querying the database from.
   */
  uint64_t offset;

  /**
   * How many rows to return (with direction).
   */
  int64_t limit;

  /**
   * Expected HTTP response code.
   */
  unsigned int http_status;

  /**
   * Length of @e tips.
   */
  unsigned int tips_length;

  /**
   * References to tips that we expect to be found.
   */
  const char **tips;

};

/**
 * Callback for a GET /private/tips operation.
 *
 * @param cls closure for this function
 * @param hr HTTP response details
 * @param tips_length length of the @a tips array
 * @param tips array of tips
 */
static void
get_tips_cb (void *cls,
             const struct TALER_MERCHANT_HttpResponse *hr,
             unsigned int tips_length,
             const struct TALER_MERCHANT_TipEntry tips[])
{
  struct GetTipsState *gts = cls;

  gts->tgh = NULL;
  if (gts->http_status != hr->http_status)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u (%d) to command %s\n",
                hr->http_status,
                (int) hr->ec,
                TALER_TESTING_interpreter_get_current_label (gts->is));
    TALER_TESTING_interpreter_fail (gts->is);
    return;
  }
  switch (hr->http_status)
  {
  case MHD_HTTP_OK:
    if (tips_length != gts->tips_length)
    {
      GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                  "Tips length does not match\n");
      TALER_TESTING_interpreter_fail (gts->is);
      return;
    }
    for (unsigned int i = 0; i < tips_length; ++i)
    {
      const struct TALER_TESTING_Command *tip_cmd;

      tip_cmd = TALER_TESTING_interpreter_lookup_command (
        gts->is,
        gts->tips[i]);
      {
        const struct GNUNET_HashCode *tip_id;

        if (GNUNET_OK !=
            TALER_TESTING_get_trait_tip_id (tip_cmd,
                                            &tip_id))
        {
          GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                      "Could not fetch tip id\n");
          TALER_TESTING_interpreter_fail (gts->is);
          return;
        }
        if (0 != GNUNET_memcmp (tip_id,
                                &tips[i].tip_id))
        {
          GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                      "Tip id does not match\n");
          TALER_TESTING_interpreter_fail (gts->is);
          return;
        }
      }
      {
        const struct TALER_Amount *tip_amount;

        if (GNUNET_OK !=
            TALER_TESTING_get_trait_amount (tip_cmd,
                                            &tip_amount))
        {
          GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                      "Could not fetch tip amount\n");
          TALER_TESTING_interpreter_fail (gts->is);
          return;
        }
        if ((GNUNET_OK != TALER_amount_cmp_currency (tip_amount,
                                                     &tips[i].tip_amount)) ||
            (0 != TALER_amount_cmp (tip_amount,
                                    &tips[i].tip_amount)))
        {
          GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                      "Tip amount does not match\n");
          TALER_TESTING_interpreter_fail (gts->is);
          return;
        }
      }
    }
    break;
  default:
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "Unhandled HTTP status.\n");
  }
  TALER_TESTING_interpreter_next (gts->is);
}


/**
 * Run the "GET /private/tips" CMD.
 *
 * @param cls closure.
 * @param cmd command being run now.
 * @param is interpreter state.
 */
static void
get_tips_run (void *cls,
              const struct TALER_TESTING_Command *cmd,
              struct TALER_TESTING_Interpreter *is)
{
  struct GetTipsState *gts = cls;

  gts->is = is;
  gts->tgh = TALER_MERCHANT_tips_get2 (is->ctx,
                                       gts->merchant_url,
                                       TALER_EXCHANGE_YNA_NO,
                                       gts->limit,
                                       gts->offset,
                                       &get_tips_cb,
                                       gts);

  GNUNET_assert (NULL != gts->tgh);
}


/**
 * Free the state of a "GET tips" CMD, and possibly
 * cancel a pending operation thereof.
 *
 * @param cls closure.
 * @param cmd command being run.
 */
static void
get_tips_cleanup (void *cls,
                  const struct TALER_TESTING_Command *cmd)
{
  struct GetTipsState *gts = cls;

  if (NULL != gts->tgh)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "GET /private/tips operation did not complete\n");
    TALER_MERCHANT_tips_get_cancel (gts->tgh);
  }
  GNUNET_array_grow (gts->tips,
                     gts->tips_length,
                     0);
  GNUNET_free (gts);
}


struct TALER_TESTING_Command
TALER_TESTING_cmd_get_tips (const char *label,
                            const char *merchant_url,
                            unsigned int http_status,
                            ...)
{
  struct GetTipsState *gts;

  gts = GNUNET_new (struct GetTipsState);
  gts->merchant_url = merchant_url;
  gts->offset = INT64_MAX;
  gts->limit = -20;
  gts->http_status = http_status;
  {
    const char *clabel;
    va_list ap;

    va_start (ap, http_status);
    while (NULL != (clabel = va_arg (ap, const char *)))
    {
      GNUNET_array_append (gts->tips,
                           gts->tips_length,
                           clabel);
    }
    va_end (ap);
  }
  {
    struct TALER_TESTING_Command cmd = {
      .cls = gts,
      .label = label,
      .run = &get_tips_run,
      .cleanup = &get_tips_cleanup
    };

    return cmd;
  }
}


struct TALER_TESTING_Command
TALER_TESTING_cmd_get_tips2 (const char *label,
                             const char *merchant_url,
                             uint64_t offset,
                             int64_t limit,
                             unsigned int http_status,
                             ...)
{
  struct GetTipsState *gts;

  gts = GNUNET_new (struct GetTipsState);
  gts->merchant_url = merchant_url;
  gts->offset = offset;
  gts->limit = limit;
  gts->http_status = http_status;
  {
    const char *clabel;
    va_list ap;

    va_start (ap, http_status);
    while (NULL != (clabel = va_arg (ap, const char *)))
    {
      GNUNET_array_append (gts->tips,
                           gts->tips_length,
                           clabel);
    }
    va_end (ap);
  }
  {
    struct TALER_TESTING_Command cmd = {
      .cls = gts,
      .label = label,
      .run = &get_tips_run,
      .cleanup = &get_tips_cleanup
    };

    return cmd;
  }
}


/* end of testing_api_cmd_get_tips.c */
