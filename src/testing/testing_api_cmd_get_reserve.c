/*
  This file is part of TALER
  Copyright (C) 2020 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3, or
  (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file testing_api_cmd_get_reserve.c
 * @brief command to test GET /private/reserves/$RESERVE_PUB
 * @author Jonathan Buchanan
 */
#include "platform.h"
#include <taler/taler_exchange_service.h>
#include <taler/taler_testing_lib.h>
#include "taler_merchant_service.h"
#include "taler_merchant_testing_lib.h"


struct GetReserveState
{

  /**
   * Handle for a "GET reserve" request.
   */
  struct TALER_MERCHANT_ReserveGetHandle *rgh;

  /**
   * The interpreter state.
   */
  struct TALER_TESTING_Interpreter *is;

  /**
   * Base URL of the merchant serving the request.
   */
  const char *merchant_url;

  /**
   * Label for a command that created a reserve.
   */
  const char *reserve_reference;

  /**
   * Expected HTTP response code.
   */
  unsigned int http_status;

  /**
   * Fetch tips
   */
  bool fetch_tips;

  /**
   * Length of @e tips.
   */
  unsigned int tips_length;

  /**
   * The list of references to tips.
   */
  const char **tips;
};


static void
get_reserve_cb (void *cls,
                const struct TALER_MERCHANT_HttpResponse *hr,
                const struct TALER_MERCHANT_ReserveSummary *rs,
                bool active,
                const char *exchange_url,
                const char *payto_uri,
                unsigned int tips_length,
                const struct TALER_MERCHANT_TipDetails tips[])
{
  /* FIXME, deeper checks should be implemented here. */
  struct GetReserveState *grs = cls;
  const struct TALER_TESTING_Command *reserve_cmd;

  reserve_cmd = TALER_TESTING_interpreter_lookup_command (
    grs->is,
    grs->reserve_reference);

  grs->rgh = NULL;
  if (grs->http_status != hr->http_status)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u (%d) to command %s\n",
                hr->http_status,
                (int) hr->ec,
                TALER_TESTING_interpreter_get_current_label (grs->is));
    TALER_TESTING_interpreter_fail (grs->is);
    return;
  }
  switch (hr->http_status)
  {
  case MHD_HTTP_OK:
    // FIXME: use grs->reserve_reference here to
    // check if the data returned matches that from the POST / PATCH
    {
      const struct TALER_Amount *initial_amount;
      if (GNUNET_OK !=
          TALER_TESTING_get_trait_amount (reserve_cmd,
                                          &initial_amount))
        TALER_TESTING_interpreter_fail (grs->is);
      if ((GNUNET_OK !=
           TALER_amount_cmp_currency (&rs->merchant_initial_amount,
                                      initial_amount)) ||
          (0 != TALER_amount_cmp (&rs->merchant_initial_amount,
                                  initial_amount)))
      {
        GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                    "Reserve initial amount does not match\n");
        TALER_TESTING_interpreter_fail (grs->is);
        return;
      }
    }
    if (tips_length != grs->tips_length)
    {
      GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                  "Number of tips authorized does not match\n");
      TALER_TESTING_interpreter_fail (grs->is);
      return;
    }
    for (unsigned int i = 0; i < tips_length; ++i)
    {
      const struct TALER_TESTING_Command *tip_cmd;

      tip_cmd = TALER_TESTING_interpreter_lookup_command (grs->is,
                                                          grs->tips[i]);
      {
        const struct GNUNET_HashCode *tip_id;

        if (GNUNET_OK !=
            TALER_TESTING_get_trait_tip_id (tip_cmd,
                                            &tip_id))
          TALER_TESTING_interpreter_fail (grs->is);

        if (0 != GNUNET_memcmp (&tips[i].tip_id,
                                tip_id))
        {
          GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                      "Reserve tip id does not match\n");
          TALER_TESTING_interpreter_fail (grs->is);
          return;
        }
      }
      {
        const struct TALER_Amount *total_amount;

        if (GNUNET_OK !=
            TALER_TESTING_get_trait_amount (tip_cmd,
                                            &total_amount))
          TALER_TESTING_interpreter_fail (grs->is);

        if ((GNUNET_OK !=
             TALER_amount_cmp_currency (&tips[i].amount,
                                        total_amount)) ||
            (0 != TALER_amount_cmp (&tips[i].amount,
                                    total_amount)))
        {
          GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                      "Reserve tip amount does not match\n");
          TALER_TESTING_interpreter_fail (grs->is);
          return;
        }
      }
      {
        const char **reason;

        if (GNUNET_OK !=
            TALER_TESTING_get_trait_reason (tip_cmd,
                                            &reason))
          TALER_TESTING_interpreter_fail (grs->is);

        if (0 != strcmp (tips[i].reason,
                         *reason))
        {
          GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                      "Reserve tip reason does not match\n");
          TALER_TESTING_interpreter_fail (grs->is);
          return;
        }
      }
    }
    break;
  default:
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "Unhandled HTTP status.\n");
  }
  TALER_TESTING_interpreter_next (grs->is);
}


/**
 * Run the "GET /private/reserves/$RESERVE_PUB" CMD.
 *
 * @param cls closure.
 * @param cmd command being run now.
 * @param is interpreter state.
 */
static void
get_reserve_run (void *cls,
                 const struct TALER_TESTING_Command *cmd,
                 struct TALER_TESTING_Interpreter *is)
{
  struct GetReserveState *grs = cls;
  const struct TALER_TESTING_Command *reserve_cmd;
  const struct TALER_ReservePublicKeyP *reserve_pub;

  reserve_cmd = TALER_TESTING_interpreter_lookup_command (
    is,
    grs->reserve_reference);
  if (GNUNET_OK !=
      TALER_TESTING_get_trait_reserve_pub (reserve_cmd,
                                           &reserve_pub))
    TALER_TESTING_FAIL (is);

  grs->is = is;
  grs->rgh = TALER_MERCHANT_reserve_get (is->ctx,
                                         grs->merchant_url,
                                         reserve_pub,
                                         grs->fetch_tips,
                                         &get_reserve_cb,
                                         grs);

  GNUNET_assert (NULL != grs->rgh);
}


/**
 * Free the state of a "GET reserve" CMD, and possibly
 * cancel a pending operation thereof.
 *
 * @param cls closure.
 * @param cmd command being run.
 */
static void
get_reserve_cleanup (void *cls,
                     const struct TALER_TESTING_Command *cmd)
{
  struct GetReserveState *grs = cls;

  if (NULL != grs->rgh)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "GET /private/reserve/$RESERVE_PUB operation did not complete\n");
    TALER_MERCHANT_reserve_get_cancel (grs->rgh);
  }
  GNUNET_array_grow (grs->tips,
                     grs->tips_length,
                     0);
  GNUNET_free (grs);
}


struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_get_reserve (const char *label,
                                        const char *merchant_url,
                                        unsigned int http_status,
                                        const char *reserve_reference)
{
  struct GetReserveState *grs;

  grs = GNUNET_new (struct GetReserveState);
  grs->merchant_url = merchant_url;
  grs->http_status = http_status;
  grs->reserve_reference = reserve_reference;
  grs->fetch_tips = false;
  {
    struct TALER_TESTING_Command cmd = {
      .cls = grs,
      .label = label,
      .run = &get_reserve_run,
      .cleanup = &get_reserve_cleanup
    };

    return cmd;
  }
}


struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_get_reserve_with_tips (const char *label,
                                                  const char *merchant_url,
                                                  unsigned int http_status,
                                                  const char *reserve_reference,
                                                  ...)
{
  struct GetReserveState *grs;

  grs = GNUNET_new (struct GetReserveState);
  grs->merchant_url = merchant_url;
  grs->http_status = http_status;
  grs->reserve_reference = reserve_reference;
  grs->fetch_tips = true;
  {
    const char *clabel;
    va_list ap;

    va_start (ap, reserve_reference);
    while (NULL != (clabel = va_arg (ap, const char *)))
    {
      GNUNET_array_append (grs->tips,
                           grs->tips_length,
                           clabel);
    }
    va_end (ap);
  }
  {
    struct TALER_TESTING_Command cmd = {
      .cls = grs,
      .label = label,
      .run = &get_reserve_run,
      .cleanup = &get_reserve_cleanup
    };

    return cmd;
  }
}


/* end of testing_api_cmd_get_reserve.c */
