/*
  This file is part of TALER
  Copyright (C) 2014-2021 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3, or
  (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file test_merchant_api.c
 * @brief testcase to test exchange's HTTP API interface
 * @author Sree Harsha Totakura <sreeharsha@totakura.in>
 * @author Christian Grothoff
 * @author Marcello Stanisci
 */
#include "platform.h"
#include <taler/taler_util.h>
#include <taler/taler_signatures.h>
#include <taler/taler_exchange_service.h>
#include <taler/taler_json_lib.h>
#include <gnunet/gnunet_util_lib.h>
#include <microhttpd.h>
#include <taler/taler_bank_service.h>
#include <taler/taler_fakebank_lib.h>
#include <taler/taler_testing_lib.h>
#include <taler/taler_error_codes.h>
#include "taler_merchant_testing_lib.h"


/**
 * Configuration file we use.  One (big) configuration is used
 * for the various components for this test.
 */
#define CONFIG_FILE "test_kyc_api.conf"

#define PAYTO_I1 "payto://x-taler-bank/localhost/3"

/**
 * Exchange base URL.  Could also be taken from config.
 */
#define EXCHANGE_URL "http://localhost:8081/"

/**
 * Payto URI of the customer (payer).
 */
static char *payer_payto;

/**
 * Payto URI of the exchange (escrow account).
 */
static char *exchange_payto;

/**
 * Payto URI of the merchant (receiver).
 */
static char *merchant_payto;

/**
 * Configuration of the bank.
 */
static struct TALER_TESTING_BankConfiguration bc;

/**
 * Configuration of the exchange.
 */
static struct TALER_TESTING_ExchangeConfiguration ec;

/**
 * Merchant base URL.
 */
static char *merchant_url;

/**
 * Merchant instance "i1a" base URL.
 */
static char *merchant_url_i1a;

/**
 * Merchant process.
 */
static struct GNUNET_OS_Process *merchantd;

/**
 * Account number of the exchange at the bank.
 */
#define EXCHANGE_ACCOUNT_NAME "2"

/**
 * Account number of some user.
 */
#define USER_ACCOUNT_NAME "62"

/**
 * Account number of some other user.
 */
#define USER_ACCOUNT_NAME2 "63"

/**
 * Account number used by the merchant
 */
#define MERCHANT_ACCOUNT_NAME "3"


/**
 * Execute the taler-exchange-aggregator, closer and transfer commands with
 * our configuration file.
 *
 * @param label label to use for the command.
 */
#define CMD_EXEC_AGGREGATOR(label) \
  TALER_TESTING_cmd_exec_aggregator_with_kyc (label "-aggregator", CONFIG_FILE), \
  TALER_TESTING_cmd_exec_transfer (label "-transfer", CONFIG_FILE)


/**
 * Run wire transfer of funds from some user's account to the
 * exchange.
 *
 * @param label label to use for the command.
 * @param amount amount to transfer, i.e. "EUR:1"
 * @param url exchange_url
 */
static struct TALER_TESTING_Command
cmd_transfer_to_exchange (const char *label,
                          const char *amount)
{
  return TALER_TESTING_cmd_admin_add_incoming (label,
                                               amount,
                                               &bc.exchange_auth,
                                               payer_payto);
}


/**
 * Main function that will tell the interpreter what commands to
 * run.
 *
 * @param cls closure
 */
static void
run (void *cls,
     struct TALER_TESTING_Interpreter *is)
{
  struct TALER_TESTING_Command pay[] = {
    /**
     * Move money to the exchange's bank account.
     */
    cmd_transfer_to_exchange ("create-reserve-1",
                              "EUR:10.02"),
    /**
     * Make a reserve exist, according to the previous transfer.
     */
    TALER_TESTING_cmd_exec_wirewatch ("wirewatch-1",
                                      CONFIG_FILE),
    TALER_TESTING_cmd_check_bank_admin_transfer ("check_bank_transfer-2",
                                                 "EUR:10.02",
                                                 payer_payto,
                                                 exchange_payto,
                                                 "create-reserve-1"),
    TALER_TESTING_cmd_withdraw_amount ("withdraw-coin-1",
                                       "create-reserve-1",
                                       "EUR:5",
                                       MHD_HTTP_OK),
    TALER_TESTING_cmd_withdraw_amount ("withdraw-coin-2",
                                       "create-reserve-1",
                                       "EUR:5",
                                       MHD_HTTP_OK),
    TALER_TESTING_cmd_merchant_get_orders ("get-orders-empty",
                                           merchant_url,
                                           MHD_HTTP_OK,
                                           NULL),
    /**
     * Check the reserve is depleted.
     */
    TALER_TESTING_cmd_status ("withdraw-status-1",
                              "create-reserve-1",
                              "EUR:0",
                              MHD_HTTP_OK),
    TALER_TESTING_cmd_merchant_post_orders2 ("create-proposal-1",
                                             merchant_url,
                                             MHD_HTTP_OK,
                                             "1", /* order ID */
                                             GNUNET_TIME_UNIT_ZERO_TS,
                                             GNUNET_TIME_UNIT_FOREVER_TS,
                                             true,
                                             "EUR:5.0",
                                             "x-taler-bank",
                                             "",
                                             "",
                                             NULL),
    TALER_TESTING_cmd_merchant_claim_order ("reclaim-1",
                                            merchant_url,
                                            MHD_HTTP_OK,
                                            "create-proposal-1",
                                            NULL),
    TALER_TESTING_cmd_merchant_pay_order ("deposit-simple",
                                          merchant_url,
                                          MHD_HTTP_OK,
                                          "create-proposal-1",
                                          "withdraw-coin-1",
                                          "EUR:5",
                                          "EUR:4.99",
                                          "session-0"),
    TALER_TESTING_cmd_merchant_post_orders_paid ("verify-order-1-paid",
                                                 merchant_url,
                                                 "deposit-simple",
                                                 "session-1",
                                                 MHD_HTTP_NO_CONTENT),
    TALER_TESTING_cmd_check_bank_empty ("check_bank_empty-1"),
    CMD_EXEC_AGGREGATOR ("run-aggregator"),
    /* KYC: hence nothing happened at the bank yet: */
    TALER_TESTING_cmd_check_bank_empty ("check_bank_empty-2"),
    /* FIXME-#7052: this should ideally not be needed... */
    TALER_TESTING_cmd_merchant_get_order ("get-order",
                                          merchant_url,
                                          "create-proposal-1",
                                          TALER_MERCHANT_OSC_PAID,
                                          false,
                                          MHD_HTTP_OK),
    TALER_TESTING_cmd_merchant_kyc_get ("kyc-pending",
                                        merchant_url,
                                        NULL,
                                        NULL,
                                        EXCHANGE_URL,
                                        MHD_HTTP_ACCEPTED),
    TALER_TESTING_cmd_proof_kyc ("kyc-do",
                                 "kyc-pending",
                                 "pass",
                                 "state",
                                 MHD_HTTP_SEE_OTHER),
    CMD_EXEC_AGGREGATOR ("run-aggregator"),
    TALER_TESTING_cmd_check_bank_transfer ("check_bank_transfer-498c",
                                           EXCHANGE_URL,
                                           "EUR:4.98",
                                           exchange_payto,
                                           merchant_payto),
    TALER_TESTING_cmd_merchant_post_transfer ("post-transfer-1",
                                              &bc.exchange_auth,
                                              PAYTO_I1,
                                              merchant_url,
                                              "EUR:4.98",
                                              MHD_HTTP_OK,
                                              "deposit-simple",
                                              NULL),
    TALER_TESTING_cmd_merchant_get_transfers ("get-transfers-1",
                                              merchant_url,
                                              PAYTO_I1,
                                              MHD_HTTP_OK,
                                              "post-transfer-1",
                                              NULL),
    TALER_TESTING_cmd_check_bank_empty ("check_bank_empty-3"),
    TALER_TESTING_cmd_end ()
  };

  struct TALER_TESTING_Command commands[] = {
    /* general setup */
    TALER_TESTING_cmd_oauth ("start-oauth-service",
                             6666),
    TALER_TESTING_cmd_auditor_add ("add-auditor-OK",
                                   MHD_HTTP_NO_CONTENT,
                                   false),
    TALER_TESTING_cmd_wire_add ("add-wire-account",
                                "payto://x-taler-bank/localhost/2",
                                MHD_HTTP_NO_CONTENT,
                                false),
    TALER_TESTING_cmd_exec_offline_sign_keys ("offline-sign-future-keys",
                                              CONFIG_FILE),
    TALER_TESTING_cmd_exec_offline_sign_fees ("offline-sign-fees",
                                              CONFIG_FILE,
                                              "EUR:0.01",
                                              "EUR:0.01"),
    TALER_TESTING_cmd_check_keys_pull_all_keys ("refetch /keys",
                                                1),
    TALER_TESTING_cmd_merchant_post_instances ("instance-create-default-setup",
                                               merchant_url,
                                               "default",
                                               PAYTO_I1,
                                               "EUR",
                                               MHD_HTTP_NO_CONTENT),
    TALER_TESTING_cmd_batch ("pay",
                             pay),
    TALER_TESTING_cmd_end ()
  };

  TALER_TESTING_run_with_fakebank (is,
                                   commands,
                                   bc.exchange_auth.wire_gateway_url);
}


int
main (int argc,
      char *const *argv)
{
  unsigned int ret;

  /* These environment variables get in the way... */
  unsetenv ("XDG_DATA_HOME");
  unsetenv ("XDG_CONFIG_HOME");
  GNUNET_log_setup ("test-kyc-api",
                    "DEBUG",
                    NULL);
  if (GNUNET_OK !=
      TALER_TESTING_prepare_fakebank (CONFIG_FILE,
                                      "exchange-account-exchange",
                                      &bc))
    return 77;

  payer_payto = ("payto://x-taler-bank/localhost/" USER_ACCOUNT_NAME);
  exchange_payto = ("payto://x-taler-bank/localhost/" EXCHANGE_ACCOUNT_NAME);
  merchant_payto = ("payto://x-taler-bank/localhost/" MERCHANT_ACCOUNT_NAME);

  if (NULL ==
      (merchant_url = TALER_TESTING_prepare_merchant (CONFIG_FILE)))
    return 77;

  GNUNET_asprintf (&merchant_url_i1a,
                   "%sinstances/i1a/",
                   merchant_url);
  TALER_TESTING_cleanup_files (CONFIG_FILE);

  switch (TALER_TESTING_prepare_exchange (CONFIG_FILE,
                                          GNUNET_YES,
                                          &ec))
  {
  case GNUNET_SYSERR:
    GNUNET_break (0);
    return 1;
  case GNUNET_NO:
    return 77;

  case GNUNET_OK:

    if (NULL == (merchantd =
                   TALER_TESTING_run_merchant (CONFIG_FILE,
                                               merchant_url)))
      return 1;

    ret = TALER_TESTING_setup_with_exchange (&run,
                                             NULL,
                                             CONFIG_FILE);

    GNUNET_OS_process_kill (merchantd, SIGTERM);
    GNUNET_OS_process_wait (merchantd);
    GNUNET_OS_process_destroy (merchantd);
    GNUNET_free (merchant_url);

    if (GNUNET_OK != ret)
      return 1;
    break;
  default:
    GNUNET_break (0);
    return 1;
  }
  return 0;
}


/* end of test_merchant_api.c */
