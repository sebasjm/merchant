/*
  This file is part of TALER
  Copyright (C) 2014-2020 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3, or
  (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file testing_api_cmd_wallet_get_tip.c
 * @brief command to test the tipping.
 * @author Marcello Stanisci
 */
#include "platform.h"
#include <taler/taler_exchange_service.h>
#include <taler/taler_testing_lib.h>
#include "taler_merchant_service.h"
#include "taler_merchant_testing_lib.h"


/**
 * State for a GET /tips/$TIP_ID CMD.
 */
struct WalletTipGetState
{

  /**
   * The merchant base URL.
   */
  const char *merchant_url;

  /**
   * Expected HTTP response code for this CMD.
   */
  unsigned int http_status;

  /**
   * Whether to compare amounts or not.
   */
  bool cmp_amounts;

  /**
   * The expected amount remaining.
   */
  struct TALER_Amount amount_remaining;

  /**
   * The handle to the current GET /tips/$TIP_ID request.
   */
  struct TALER_MERCHANT_TipWalletGetHandle *tgh;

  /**
   * The interpreter state.
   */
  struct TALER_TESTING_Interpreter *is;

  /**
   * Reference to a command that created a tip.
   */
  const char *tip_reference;
};


/**
 * Callback to process a GET /tips/$TIP_ID request, it mainly
 * checks that what the backend returned matches the command's
 * expectations.
 *
 * @param cls closure
 * @param hr HTTP response
 * @param reserve_expiration when the tip reserve will expire
 * @param exchange_url from where to pick up the tip
 * @param amount_remaining how much is remaining
 */
static void
wallet_tip_get_cb (void *cls,
                   const struct TALER_MERCHANT_HttpResponse *hr,
                   struct GNUNET_TIME_Timestamp reserve_expiration,
                   const char *exchange_url,
                   const struct TALER_Amount *amount_remaining)
{
  /* FIXME, deeper checks should be implemented here. */
  struct WalletTipGetState *gts = cls;
  const struct TALER_TESTING_Command *tip_cmd;

  tip_cmd = TALER_TESTING_interpreter_lookup_command (
    gts->is,
    gts->tip_reference);

  gts->tgh = NULL;
  if (gts->http_status != hr->http_status)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u (%d) to command %s\n",
                hr->http_status,
                (int) hr->ec,
                TALER_TESTING_interpreter_get_current_label (gts->is));
    TALER_TESTING_interpreter_fail (gts->is);
    return;
  }
  switch (hr->http_status)
  {
  case MHD_HTTP_OK:
    // FIXME: use gts->tip_reference here to
    // check if the data returned matches that from the POST / PATCH
    if (gts->cmp_amounts)
    {
      if ((GNUNET_OK != TALER_amount_cmp_currency (&gts->amount_remaining,
                                                   amount_remaining)) ||
          (0 != TALER_amount_cmp (&gts->amount_remaining,
                                  amount_remaining)))
      {
        GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                    "Amount remaining on tip does not match\n");
        TALER_TESTING_interpreter_fail (gts->is);
        return;
      }
    }
    {
      const struct GNUNET_TIME_Timestamp *expiration;

      if (GNUNET_OK !=
          TALER_TESTING_get_trait_timestamp (tip_cmd,
                                             0,
                                             &expiration))
        TALER_TESTING_interpreter_fail (gts->is);
      if (GNUNET_TIME_timestamp_cmp (*expiration,
                                     !=,
                                     reserve_expiration))
      {
        GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                    "Tip expiration does not match\n");
        TALER_TESTING_interpreter_fail (gts->is);
        return;
      }
    }
    break;
  default:
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "Unhandled HTTP status.\n");
  }
  TALER_TESTING_interpreter_next (gts->is);
}


/**
 * Run the "GET tip" CMD.
 *
 * @param cls closure.
 * @param cmd command being run now.
 * @param is interpreter state.
 */
static void
wallet_get_tip_run (void *cls,
                    const struct TALER_TESTING_Command *cmd,
                    struct TALER_TESTING_Interpreter *is)
{
  struct WalletTipGetState *tgs = cls;
  const struct TALER_TESTING_Command *tip_cmd;
  const struct GNUNET_HashCode *tip_id;

  tip_cmd = TALER_TESTING_interpreter_lookup_command (is,
                                                      tgs->tip_reference);

  if (GNUNET_OK !=
      TALER_TESTING_get_trait_tip_id (tip_cmd,
                                      &tip_id))
    TALER_TESTING_FAIL (is);

  tgs->is = is;
  tgs->tgh = TALER_MERCHANT_wallet_tip_get (is->ctx,
                                            tgs->merchant_url,
                                            tip_id,
                                            &wallet_tip_get_cb,
                                            tgs);
}


/**
 * Free the state of a "GET tip" CMD, and possibly
 * cancel a pending operation thereof.
 *
 * @param cls closure.
 * @param cmd command being run.
 */
static void
wallet_get_tip_cleanup (void *cls,
                        const struct TALER_TESTING_Command *cmd)
{
  struct WalletTipGetState *tgs = cls;

  if (NULL != tgs->tgh)
  {
    TALER_LOG_WARNING ("Get tip operation did not complete\n");
    TALER_MERCHANT_wallet_tip_get_cancel (tgs->tgh);
  }
  GNUNET_free (tgs);
}


struct TALER_TESTING_Command
TALER_TESTING_cmd_wallet_get_tip (const char *label,
                                  const char *merchant_url,
                                  const char *tip_reference,
                                  unsigned int http_status)
{
  struct WalletTipGetState *tgs;

  tgs = GNUNET_new (struct WalletTipGetState);
  tgs->merchant_url = merchant_url;
  tgs->tip_reference = tip_reference;
  tgs->http_status = http_status;
  {
    struct TALER_TESTING_Command cmd = {
      .cls = tgs,
      .label = label,
      .run = &wallet_get_tip_run,
      .cleanup = &wallet_get_tip_cleanup
    };

    return cmd;
  }
}


struct TALER_TESTING_Command
TALER_TESTING_cmd_wallet_get_tip2 (const char *label,
                                   const char *merchant_url,
                                   const char *tip_reference,
                                   const char *amount_remaining,
                                   unsigned int http_status)
{
  struct WalletTipGetState *tgs;

  tgs = GNUNET_new (struct WalletTipGetState);
  tgs->merchant_url = merchant_url;
  tgs->tip_reference = tip_reference;
  tgs->cmp_amounts = true;
  GNUNET_assert (GNUNET_OK == TALER_string_to_amount (amount_remaining,
                                                      &tgs->amount_remaining));
  tgs->http_status = http_status;
  {
    struct TALER_TESTING_Command cmd = {
      .cls = tgs,
      .label = label,
      .run = &wallet_get_tip_run,
      .cleanup = &wallet_get_tip_cleanup
    };

    return cmd;
  }
}


/* end of testing_api_cmd_wallet_get_tip.c */
