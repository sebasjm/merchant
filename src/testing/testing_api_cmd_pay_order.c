/*
  This file is part of TALER
  Copyright (C) 2014-2018, 2020 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3, or
  (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file testing_api_cmd_pay_order.c
 * @brief command to test the /orders/ID/pay feature.
 * @author Marcello Stanisci
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_exchange_service.h>
#include <taler/taler_testing_lib.h>
#include <taler/taler_signatures.h>
#include "taler_merchant_service.h"
#include "taler_merchant_testing_lib.h"


/**
 * State for a /pay CMD.
 */
struct PayState
{
  /**
   * Contract terms hash code.
   */
  struct TALER_PrivateContractHash h_contract_terms;

  /**
   * The interpreter state.
   */
  struct TALER_TESTING_Interpreter *is;

  /**
   * Expected HTTP response status code.
   */
  unsigned int http_status;

  /**
   * Reference to a command that can provide a order id,
   * typically a /proposal test command.
   */
  const char *proposal_reference;

  /**
   * Reference to a command that can provide a coin, so
   * we can pay here.
   */
  const char *coin_reference;

  /**
   * The merchant base URL.
   */
  const char *merchant_url;

  /**
   * Amount to be paid, plus the deposit fee.
   */
  const char *amount_with_fee;

  /**
   * Amount to be paid, including NO fees.
   */
  const char *amount_without_fee;

  /**
   * Handle to the pay operation.
   */
  struct TALER_MERCHANT_OrderPayHandle *oph;

  /**
   * Signature from the merchant, set on success.
   */
  struct TALER_MerchantSignatureP merchant_sig;

  /**
   * The session for which the payment is made.
   */
  const char *session_id;
};


/**
 * Parse the @a coins specification and grow the @a pc
 * array with the coins found, updating @a npc.
 *
 * @param[in,out] pc pointer to array of coins found
 * @param[in,out] npc length of array at @a pc
 * @param[in] coins string specifying coins to add to @a pc,
 *            clobbered in the process
 * @param is interpreter state
 * @param amount_with_fee total amount to be paid for a contract.
 * @param amount_without_fee to be removed, there is no
 *        per-contract fee, only per-coin exists.
 * @return #GNUNET_OK on success
 */
static enum GNUNET_GenericReturnValue
build_coins (struct TALER_MERCHANT_PayCoin **pc,
             unsigned int *npc,
             char *coins,
             struct TALER_TESTING_Interpreter *is,
             const char *amount_with_fee,
             const char *amount_without_fee)
{
  char *token;

  for (token = strtok (coins, ";");
       NULL != token;
       token = strtok (NULL, ";"))
  {
    const struct TALER_TESTING_Command *coin_cmd;
    char *ctok;
    unsigned int ci;
    struct TALER_MERCHANT_PayCoin *icoin;
    const struct TALER_EXCHANGE_DenomPublicKey *dpk;
    const char **exchange_url;

    /* Token syntax is "LABEL[/NUMBER]" */
    ctok = strchr (token, '/');
    ci = 0;
    if (NULL != ctok)
    {
      *ctok = '\0';
      ctok++;
      if (1 != sscanf (ctok,
                       "%u",
                       &ci))
      {
        GNUNET_break (0);
        return GNUNET_SYSERR;
      }
    }

    coin_cmd = TALER_TESTING_interpreter_lookup_command
                 (is, token);

    if (NULL == coin_cmd)
    {
      GNUNET_break (0);
      return GNUNET_SYSERR;
    }

    GNUNET_array_grow (*pc,
                       *npc,
                       (*npc) + 1);

    icoin = &((*pc)[(*npc) - 1]);

    {
      const struct TALER_CoinSpendPrivateKeyP *coin_priv;
      const struct TALER_DenominationSignature *denom_sig;
      const struct TALER_Amount *denom_value;
      const struct TALER_EXCHANGE_DenomPublicKey *denom_pub;

      GNUNET_assert (GNUNET_OK ==
                     TALER_TESTING_get_trait_coin_priv (coin_cmd,
                                                        0,
                                                        &coin_priv));
      GNUNET_assert (GNUNET_OK ==
                     TALER_TESTING_get_trait_denom_pub (coin_cmd,
                                                        0,
                                                        &denom_pub));
      GNUNET_assert (GNUNET_OK ==
                     TALER_TESTING_get_trait_denom_sig (coin_cmd,
                                                        0,
                                                        &denom_sig));
      GNUNET_assert (GNUNET_OK ==
                     TALER_TESTING_get_trait_amount (coin_cmd,
                                                     &denom_value));
      icoin->coin_priv = *coin_priv;
      icoin->denom_pub = denom_pub->key;
      icoin->denom_sig = *denom_sig;
      icoin->denom_value = *denom_value;
      icoin->amount_with_fee = *denom_value;
    }
    GNUNET_assert (NULL != (dpk =
                              TALER_TESTING_find_pk (is->keys,
                                                     &icoin->denom_value)));

    GNUNET_assert (0 <=
                   TALER_amount_subtract (&icoin->amount_without_fee,
                                          &icoin->denom_value,
                                          &dpk->fee_deposit));
    GNUNET_assert (GNUNET_OK ==
                   TALER_TESTING_get_trait_exchange_url (coin_cmd,
                                                         &exchange_url));
    icoin->exchange_url = *exchange_url;
  }

  return GNUNET_OK;
}


/**
 * Function called with the result of a /pay operation.
 * Checks whether the merchant signature is valid and the
 * HTTP response code matches our expectation.
 *
 * @param cls closure with the interpreter state
 * @param hr HTTP response
 * @param merchant_sig signature affirming payment,
 *        NULL on errors
 */
static void
pay_cb (void *cls,
        const struct TALER_MERCHANT_HttpResponse *hr,
        const struct TALER_MerchantSignatureP *merchant_sig)
{
  struct PayState *ps = cls;

  ps->oph = NULL;
  if (ps->http_status != hr->http_status)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u (%d) to command %s\n",
                hr->http_status,
                (int) hr->ec,
                TALER_TESTING_interpreter_get_current_label (ps->is));
    TALER_TESTING_FAIL (ps->is);
  }
  if (MHD_HTTP_OK == hr->http_status)
  {
    ps->merchant_sig = *merchant_sig;
  }
  TALER_TESTING_interpreter_next (ps->is);
}


/**
 * Run a "pay" CMD.
 *
 * @param cls closure.
 * @param cmd current CMD being run.
 * @param is interpreter state.
 */
static void
pay_run (void *cls,
         const struct TALER_TESTING_Command *cmd,
         struct TALER_TESTING_Interpreter *is)
{
  struct PayState *ps = cls;
  const struct TALER_TESTING_Command *proposal_cmd;
  const json_t *contract_terms;
  const char *order_id;
  struct GNUNET_TIME_Timestamp refund_deadline;
  struct GNUNET_TIME_Timestamp pay_deadline;
  struct GNUNET_TIME_Timestamp timestamp;
  struct TALER_MerchantPublicKeyP merchant_pub;
  struct TALER_MerchantWireHash h_wire;
  const struct TALER_PrivateContractHash *h_proposal;
  struct TALER_Amount total_amount;
  struct TALER_Amount max_fee;
  const char *error_name;
  unsigned int error_line;
  struct TALER_MERCHANT_PayCoin *pay_coins;
  unsigned int npay_coins;
  const struct TALER_MerchantSignatureP *merchant_sig;

  ps->is = is;
  proposal_cmd = TALER_TESTING_interpreter_lookup_command (
    is,
    ps->proposal_reference);

  if (NULL == proposal_cmd)
    TALER_TESTING_FAIL (is);

  if (GNUNET_OK !=
      TALER_TESTING_get_trait_contract_terms (proposal_cmd,
                                              &contract_terms))
    TALER_TESTING_FAIL (is);
  {
    /* Get information that needs to be put verbatim in the
     * deposit permission */
    struct GNUNET_JSON_Specification spec[] = {
      GNUNET_JSON_spec_string ("order_id",
                               &order_id),
      GNUNET_JSON_spec_timestamp ("refund_deadline",
                                  &refund_deadline),
      GNUNET_JSON_spec_timestamp ("pay_deadline",
                                  &pay_deadline),
      GNUNET_JSON_spec_timestamp ("timestamp",
                                  &timestamp),
      GNUNET_JSON_spec_fixed_auto ("merchant_pub",
                                   &merchant_pub),
      GNUNET_JSON_spec_fixed_auto ("h_wire",
                                   &h_wire),
      TALER_JSON_spec_amount_any ("amount",
                                  &total_amount),
      TALER_JSON_spec_amount_any ("max_fee",
                                  &max_fee),
      GNUNET_JSON_spec_end ()
    };

    if (GNUNET_OK !=
        GNUNET_JSON_parse (contract_terms,
                           spec,
                           &error_name,
                           &error_line))
    {
      char *js;

      js = json_dumps (contract_terms,
                       JSON_INDENT (1));
      GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                  "Parser failed on %s:%u for input `%s'\n",
                  error_name,
                  error_line,
                  js);
      free (js);
      TALER_TESTING_FAIL (is);
    }
  }

  {
    char *cr;

    cr = GNUNET_strdup (ps->coin_reference);
    pay_coins = NULL;
    npay_coins = 0;
    if (GNUNET_OK !=
        build_coins (&pay_coins,
                     &npay_coins,
                     cr,
                     is,
                     ps->amount_with_fee,
                     ps->amount_without_fee))
    {
      GNUNET_array_grow (pay_coins,
                         npay_coins,
                         0);
      GNUNET_free (cr);
      TALER_TESTING_FAIL (is);
    }
    GNUNET_free (cr);
  }
  if (GNUNET_OK !=
      TALER_TESTING_get_trait_merchant_sig (proposal_cmd,
                                            &merchant_sig))
    TALER_TESTING_FAIL (is);

  if (GNUNET_OK !=
      TALER_TESTING_get_trait_h_contract_terms (proposal_cmd,
                                                &h_proposal))
    TALER_TESTING_FAIL (is);
  ps->h_contract_terms = *h_proposal;
  ps->oph = TALER_MERCHANT_order_pay (is->ctx,
                                      ps->merchant_url,
                                      ps->session_id,
                                      h_proposal,
                                      &total_amount,
                                      &max_fee,
                                      &merchant_pub,
                                      merchant_sig,
                                      timestamp,
                                      refund_deadline,
                                      pay_deadline,
                                      &h_wire,
                                      order_id,
                                      npay_coins,
                                      pay_coins,
                                      &pay_cb,
                                      ps);
  GNUNET_array_grow (pay_coins,
                     npay_coins,
                     0);
  if (NULL == ps->oph)
    TALER_TESTING_FAIL (is);
}


/**
 * Free a "pay" CMD, and cancel it if need be.
 *
 * @param cls closure.
 * @param cmd command currently being freed.
 */
static void
pay_cleanup (void *cls,
             const struct TALER_TESTING_Command *cmd)
{
  struct PayState *ps = cls;

  if (NULL != ps->oph)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "Command `%s' did not complete.\n",
                TALER_TESTING_interpreter_get_current_label (
                  ps->is));
    TALER_MERCHANT_order_pay_cancel (ps->oph);
  }

  GNUNET_free (ps);
}


/**
 * Offer internal data useful to other commands.
 *
 * @param cls closure
 * @param[out] ret result
 * @param trait name of the trait
 * @param index index number of the object to extract.
 * @return #GNUNET_OK on success
 */
static enum GNUNET_GenericReturnValue
pay_traits (void *cls,
            const void **ret,
            const char *trait,
            unsigned int index)
{

  struct PayState *ps = cls;
  const char **order_id;
  const struct TALER_TESTING_Command *proposal_cmd;
  const struct TALER_MerchantPublicKeyP *merchant_pub;

  if (NULL ==
      (proposal_cmd =
         TALER_TESTING_interpreter_lookup_command (ps->is,
                                                   ps->proposal_reference)))
  {
    GNUNET_break (0);
    return GNUNET_SYSERR;
  }

  if (GNUNET_OK !=
      TALER_TESTING_get_trait_order_id (proposal_cmd,
                                        &order_id))
  {
    GNUNET_break (0);
    return GNUNET_SYSERR;
  }

  if (GNUNET_OK !=
      TALER_TESTING_get_trait_merchant_pub (proposal_cmd,
                                            &merchant_pub))
  {
    GNUNET_break (0);
    return GNUNET_SYSERR;
  }
  {
    struct TALER_Amount amount_with_fee;

    GNUNET_assert (GNUNET_OK ==
                   TALER_string_to_amount (ps->amount_with_fee,
                                           &amount_with_fee));
    {
      struct TALER_TESTING_Trait traits[] = {
        TALER_TESTING_make_trait_proposal_reference (&ps->proposal_reference),
        TALER_TESTING_make_trait_coin_reference (0,
                                                 &ps->coin_reference),
        TALER_TESTING_make_trait_order_id (order_id),
        TALER_TESTING_make_trait_merchant_pub (merchant_pub),
        TALER_TESTING_make_trait_merchant_sig (&ps->merchant_sig),
        TALER_TESTING_make_trait_amount (&amount_with_fee),
        TALER_TESTING_trait_end ()
      };

      return TALER_TESTING_get_trait (traits,
                                      ret,
                                      trait,
                                      index);
    }
  }
}


struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_pay_order (const char *label,
                                      const char *merchant_url,
                                      unsigned int http_status,
                                      const char *proposal_reference,
                                      const char *coin_reference,
                                      const char *amount_with_fee,
                                      const char *amount_without_fee,
                                      const char *session_id)
{
  struct PayState *ps;

  ps = GNUNET_new (struct PayState);
  ps->http_status = http_status;
  ps->proposal_reference = proposal_reference;
  ps->coin_reference = coin_reference;
  ps->merchant_url = merchant_url;
  ps->amount_with_fee = amount_with_fee;
  ps->amount_without_fee = amount_without_fee;
  ps->session_id = session_id;
  {
    struct TALER_TESTING_Command cmd = {
      .cls = ps,
      .label = label,
      .run = &pay_run,
      .cleanup = &pay_cleanup,
      .traits = &pay_traits
    };

    return cmd;
  }
}


/* end of testing_api_cmd_pay_order.c */
