# This file is part of TALER
# Copyright (C) 2014-2021 Taler Systems SA
#
# TALER is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3, or
# (at your option) any later version.
#
# TALER is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with TALER; see the file COPYING.  If not, see
# <http://www.gnu.org/licenses/>
#
## Coloring style Text shell script
COLOR='\033[0;35m'
NOCOLOR='\033[0m'

set -eu

# Exit, with status code "skip" (no 'real' failure)
function exit_skip() {
    echo " SKIP: $1"
    exit 77
}

# Exit, with error message (hard failure)
function exit_fail() {
    echo " FAIL: $1"
    exit 1
}

# Cleanup to run whenever we exit
function cleanup()
{
    # kill main HTTP servers first
    kill ${MERCHANT_HTTPD_PID:-X} &> /dev/null || true
    kill ${EXCHANGE_HTTPD_PID:-X} &> /dev/null || true
    for n in `jobs -p`
    do
        kill $n 2> /dev/null || true
    done
    rm -rf $CONF $WALLET_DB $TMP_DIR $LAST_RESPONSE
    wait
}

# Exchange configuration file will be edited, so we create one
# from the template.
CONF=`mktemp test_template.conf-XXXXXX`
cp test_template.conf $CONF

TMP_DIR=`mktemp -d keys-tmp-XXXXXX`
WALLET_DB=`mktemp test_wallet.json-XXXXXX`
LAST_RESPONSE=`mktemp test_response.conf-XXXXXX`

# Install cleanup handler (except for kill -9)
trap cleanup EXIT

# Check we can actually run
echo -n "Testing for jq"
jq -h > /dev/null || exit_skip "jq required"
echo " FOUND"

echo -n "Testing for taler"
taler-exchange-httpd -h > /dev/null || exit_skip " taler-exchange required"
taler-merchant-httpd -h > /dev/null || exit_skip " taler-merchant required"
echo " FOUND"

echo -n "Testing for taler-bank-manage"
taler-bank-manage --help >/dev/null </dev/null || exit_skip " MISSING"
echo " FOUND"
echo -n "Testing for taler-wallet-cli"
taler-wallet-cli -v >/dev/null </dev/null 2> /dev/null || exit_skip " MISSING"
echo " FOUND"

echo -n "Generating Taler auditor, exchange and merchant configurations ..."

DATA_DIR=`taler-config -f -c $CONF -s PATHS -o TALER_HOME`
rm -rf $DATA_DIR

# obtain key configuration data
MASTER_PRIV_FILE=`taler-config -f -c ${CONF} -s "EXCHANGE-OFFLINE" -o "MASTER_PRIV_FILE"`
MASTER_PRIV_DIR=`dirname $MASTER_PRIV_FILE`
mkdir -p $MASTER_PRIV_DIR
gnunet-ecc -g1 $MASTER_PRIV_FILE > /dev/null 2> /dev/null
MASTER_PUB=`gnunet-ecc -p ${MASTER_PRIV_FILE}`
EXCHANGE_URL=`taler-config -c $CONF -s EXCHANGE -o BASE_URL`
MERCHANT_PORT=`taler-config -c $CONF -s MERCHANT -o PORT`
MERCHANT_URL=http://localhost:${MERCHANT_PORT}/
BANK_PORT=`taler-config -c $CONF -s BANK -o HTTP_PORT`
BANK_URL=http://localhost:${BANK_PORT}/
AUDITOR_URL=http://localhost:8083/
AUDITOR_PRIV_FILE=`taler-config -f -c $CONF -s AUDITOR -o AUDITOR_PRIV_FILE`
AUDITOR_PRIV_DIR=`dirname $AUDITOR_PRIV_FILE`
mkdir -p $AUDITOR_PRIV_DIR
gnunet-ecc -g1 $AUDITOR_PRIV_FILE > /dev/null 2> /dev/null
AUDITOR_PUB=`gnunet-ecc -p $AUDITOR_PRIV_FILE`

# patch configuration
TALER_DB=talercheck
taler-config -c $CONF -s exchange -o MASTER_PUBLIC_KEY -V $MASTER_PUB
taler-config -c $CONF -s merchant-exchange-default -o MASTER_KEY -V $MASTER_PUB
taler-config -c $CONF -s exchangedb-postgres -o CONFIG -V postgres:///$TALER_DB
taler-config -c $CONF -s auditordb-postgres -o CONFIG -V postgres:///$TALER_DB
taler-config -c $CONF -s merchantdb-postgres -o CONFIG -V postgres:///$TALER_DB
taler-config -c $CONF -s bank -o database -V postgres:///$TALER_DB
taler-config -c $CONF -s exchange -o KEYDIR -V "${TMP_DIR}/keydir/"
taler-config -c $CONF -s exchange -o REVOCATION_DIR -V "${TMP_DIR}/revdir/"

echo " OK"

echo -n "Setting up exchange ..."

# reset database
dropdb $TALER_DB >/dev/null 2>/dev/null || true
createdb $TALER_DB || exit_skip "Could not create database $TALER_DB"
taler-exchange-dbinit -c $CONF
taler-merchant-dbinit -c $CONF
taler-auditor-dbinit -c $CONF
taler-auditor-exchange -c $CONF -m $MASTER_PUB -u $EXCHANGE_URL

echo " OK"

# Launch services
echo -n "Launching taler services ..."
taler-bank-manage-testing $CONF postgres:///$TALER_DB serve > taler-bank.log 2> taler-bank.err &
taler-exchange-secmod-eddsa -c $CONF -L DEBUG 2> taler-exchange-secmod-eddsa.log &
taler-exchange-secmod-rsa -c $CONF -L DEBUG 2> taler-exchange-secmod-rsa.log &
taler-exchange-httpd -c $CONF -L DEBUG 2> taler-exchange-httpd.log &
EXCHANGE_HTTPD_PID=$!
taler-merchant-httpd -c $CONF -L DEBUG 2> taler-merchant-httpd.log &
MERCHANT_HTTPD_PID=$!
taler-exchange-wirewatch -c $CONF 2> taler-exchange-wirewatch.log &
taler-auditor-httpd -L INFO -c $CONF 2> taler-auditor-httpd.log &

echo " OK"

echo -n "Waiting for the bank"
# Wait for bank to be available (usually the slowest)
for n in `seq 1 30`
do
    echo -n "."
    sleep 1
    OK=0
    # bank
    wget --waitretry=0 --timeout=1 http://localhost:8082/ -o /dev/null -O /dev/null >/dev/null || continue
    OK=1
    break
done

if [ 1 != $OK ]
then
    exit_skip "Failed to launch services (bank)"
fi
echo " OK"

echo -n "Waiting for taler services "
# Wait for all other taler services to be available
for n in `seq 1 20`
do
    echo -n "."
    sleep 1
    OK=0
    # exchange
    wget --tries=1 --timeout=1 http://localhost:8081/seed -o /dev/null -O /dev/null >/dev/null || continue
    # merchant
    wget --tries=1 --timeout=1 http://localhost:9966/ -o /dev/null -O /dev/null >/dev/null || continue
    # auditor
    wget --tries=1 --timeout=1 http://localhost:8083/ -o /dev/null -O /dev/null >/dev/null || continue
    OK=1
    break
done

if [ 1 != $OK ]
then
    exit_skip "Failed to launch taler services"
fi

echo "OK"

set +e
echo -n 'Wait the exchange for gather its keys '
for n in `seq 1 50`
do
    echo -n "."
    sleep 1
    OK=0
    # exchange
    wget --tries=3 --waitretry=0 --timeout=1 http://localhost:8081/management/keys -o /dev/null -O $LAST_RESPONSE >/dev/null
    DENOMS_COUNT=`jq '.future_denoms|length' < $LAST_RESPONSE`
    SIGNKEYS_COUNT=`jq '.future_signkeys|length' < $LAST_RESPONSE`
    [[ -z "$SIGNKEYS_COUNT" || "$SIGNKEYS_COUNT" == "0" || -z "$DENOMS_COUNT" || "$DENOMS_COUNT" == "0" ]] && continue
    OK=1
    break;
done
set -e

if [ 1 != $OK ]
then
    exit_skip "Failed to setup exchange keys, check secmod logs"
fi

echo " OK"

echo -n "Setting up keys ..."
taler-exchange-offline -c $CONF \
  download \
  sign \
  enable-account payto://x-taler-bank/localhost/Exchange \
  enable-auditor $AUDITOR_PUB $AUDITOR_URL "TESTKUDOS Auditor" \
  wire-fee now x-taler-bank TESTKUDOS:0.01 TESTKUDOS:0.01 \
  upload &> taler-exchange-offline.log

echo -n "."

for n in `seq 1 3`
do
    echo -n "."
    OK=0
    wget --tries=1 --timeout=1 http://localhost:8081/keys -o /dev/null -O /dev/null >/dev/null || continue
    OK=1
    break
done

if [ 1 != $OK ]
then
    exit_skip "Failed to setup keys"
fi

echo " OK"

echo -n "Setting up auditor signatures ..."
taler-auditor-offline -c $CONF \
  download sign upload &> taler-auditor-offline.log
echo " OK"
