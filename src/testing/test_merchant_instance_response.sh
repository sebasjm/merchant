#!/bin/bash
# This file is part of TALER
# Copyright (C) 2014-2021 Taler Systems SA
#
# TALER is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3, or
# (at your option) any later version.
#
# TALER is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with TALER; see the file COPYING.  If not, see
# <http://www.gnu.org/licenses/>
#

. initialize_taler_system.sh

# echo -n "Configuring merchant instance ..."

STATUS=$(curl -H "Content-Type: application/json" -X OPTIONS \
    http://localhost:9966/instances/default/private/products \
    -w "%{http_code}" -s -o /dev/null)

if [ "$STATUS" != "204" ]
then
    echo 'options should return 204 when default instance doest not exist yet. got:' $STATUS
    exit 1
fi

STATUS=$(curl -H "Content-Type: application/json" -X GET \
    -H 'Authorization: Bearer secret-token:super_secret' \
    http://localhost:9966/instances/default/private/products \
    -w "%{http_code}" -s -o /dev/null)

if [ "$STATUS" != "404" ]
then
    echo 'backend should respond 404 when the default instance is not yet created. got:' $STATUS
    exit 1
fi

STATUS=$(curl -H "Content-Type: application/json" -X POST \
    -H 'Authorization: Bearer secret-token:super_secret' \
    http://localhost:9966/management/instances \
    -d '{"auth":{"method":"token","token":"secret-token:other_secret"},"payto_uris":["payto://x-taler-bank/localhost/43"],"id":"default","name":"default","address":{},"jurisdiction":{},"default_max_wire_fee":"TESTKUDOS:1", "default_max_deposit_fee":"TESTKUDOS:1","default_wire_fee_amortization":1,"default_wire_transfer_delay":{"d_ms" : 3600000},"default_pay_delay":{"d_ms": 3600000}}' \
    -w "%{http_code}" -s -o /dev/null)

if [ "$STATUS" != "204" ]
then
    echo 'should respond ok, instance created. got:' $STATUS
    exit 1
fi

STATUS=$(curl -H "Content-Type: application/json" -X GET \
    http://localhost:9966/instances/default/private/products \
    -w "%{http_code}" -s -o /dev/null)

if [ "$STATUS" != "401" ]
then
    echo 'should respond unauthorized without the token for the list of product when the default instance was created. got:' $STATUS
    exit 1
fi

STATUS=$(curl -H "Content-Type: application/json" -X GET \
    -H 'Authorization: Bearer secret-token:other_secret' \
    http://localhost:9966/instances/default/private/products \
    -w "%{http_code}" -s -o /dev/null)

if [ "$STATUS" != "200" ]
then
    echo 'should respond ok for the list of product when the default instance was created. got:' $STATUS
    exit 1
fi

STATUS=$(curl -H "Content-Type: application/json" -X POST \
    -H 'Authorization: Bearer secret-token:other_secret' \
    http://localhost:9966/instances/default/private/auth \
    -d '{"method":"token","token":"secret-token:zxc"}' \
    -w "%{http_code}" -s -o /dev/null)

if [ "$STATUS" != "204" ]
then
    echo 'should respond ok, instance auth token changed. got:' $STATUS
    exit 1
fi


STATUS=$(curl -H "Content-Type: application/json" -X DELETE \
    "http://localhost:9966/instances/default/private" \
    -w "%{http_code}" -s -o /dev/null)


if [ "$STATUS" != "401" ]
then
    echo 'should respond unauthorized without the token, when purging the instance. got:' $STATUS
    exit 1
fi

STATUS=$(curl -H "Content-Type: application/json" -X DELETE \
    -H 'Authorization: Bearer secret-token:other_secret' \
    "http://localhost:9966/instances/default/private" \
    -w "%{http_code}" -s -o /dev/null)

if [ "$STATUS" != "401" ]
then
    echo 'should respond unauthorized using old token, when purging the instance. got:' $STATUS
    exit 1
fi

STATUS=$(curl -H "Content-Type: application/json" -X DELETE \
    -H 'Authorization: Bearer secret-token:zxc' \
    "http://localhost:9966/instances/default/private" \
    -w "%{http_code}" -s -o /dev/null)

if [ "$STATUS" != "204" ]
then
    echo 'should respond unauthorized without the token, when purging the instance. got:' $STATUS
    exit 1
fi

STATUS=$(curl -H "Content-Type: application/json" -X GET \
    -H 'Authorization: Bearer secret-token:zxc' \
    http://localhost:9966/instances/default/private/products \
    -w "%{http_code}" -s -o /dev/null)

if [ "$STATUS" != "404" ]
then
    echo 'should respond not found when trying to list the product and the default instance was deleted. got:' $STATUS
    exit 1
fi

echo "OK"

exit 0
