#!/bin/bash
# This file is part of TALER
# Copyright (C) 2014-2021 Taler Systems SA
#
# TALER is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3, or
# (at your option) any later version.
#
# TALER is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with TALER; see the file COPYING.  If not, see
# <http://www.gnu.org/licenses/>
#

. initialize_taler_system.sh

echo -n "Configuring merchant instance ..."

# create instance
STATUS=$(curl -H "Content-Type: application/json" -X POST \
    -H 'Authorization: Bearer secret-token:super_secret' \
    http://localhost:9966/management/instances \
    -d '{"auth":{"method":"external"},"payto_uris":["payto://x-taler-bank/'$BANK_URL'/43"],"id":"default","name":"default","address":{},"jurisdiction":{},"default_max_wire_fee":"TESTKUDOS:1", "default_max_deposit_fee":"TESTKUDOS:1","default_wire_fee_amortization":1,"default_wire_transfer_delay":{"d_ms" : 50000},"default_pay_delay":{"d_ms": 60000}}' \
    -w "%{http_code}" -s -o /dev/null)

if [ "$STATUS" != "204" ]
then
    echo 'should respond ok, instance created. got:' $STATUS
    exit 1
fi

echo OK

echo -n "creating reserve ..."

STATUS=$(curl 'http://localhost:9966/instances/default/private/reserves' \
    -d '{"initial_balance":"TESTKUDOS:2","exchange_url":"'$EXCHANGE_URL'","wire_method":"x-taler-bank"}' \
    -w "%{http_code}" -s -o $LAST_RESPONSE)


if [ "$STATUS" != "200" ]
then
    echo 'should respond ok, reserve created. got:' $STATUS
    exit 1
fi

echo OK

RESERVE_PUB=`jq -r .reserve_pub < $LAST_RESPONSE`

STATUS=$(curl 'http://localhost:9966/instances/default/private/reserves/'$RESERVE_PUB \
    -w "%{http_code}" -s -o $LAST_RESPONSE)

FUNDED=`jq -r '.merchant_initial_amount == .exchange_initial_amount' < $LAST_RESPONSE`

if [ "$FUNDED" != "false" ]
then
    echo 'should not be funded if we just created. got:' $STATUS 'is founded: ' $FUNDED
    cat $LAST_RESPONSE
    exit 1
fi


echo -n Wire transferring...
STATUS=$(curl http://Exchange:x@localhost:$BANK_PORT/taler-wire-gateway/Exchange/admin/add-incoming \
    -d '{"reserve_pub":"'$RESERVE_PUB'","debit_account":"payto://x-taler-bank/localhost:'$BANK_PORT'/43","amount":"TESTKUDOS:2"}' \
    -w "%{http_code}" -s -o $LAST_RESPONSE)

if [ "$STATUS" != "200" ]
then
    echo 'should respond ok, wire transfer executed. got:' $STATUS
    exit 1
fi

echo OK

taler-exchange-wirewatch -c $CONF -t -L INFO

#there seems to be a race condition here so we wait
sleep 1

STATUS=$(curl 'http://localhost:9966/instances/default/private/reserves/'$RESERVE_PUB \
    -w "%{http_code}" -s -o $LAST_RESPONSE)

FUNDED=`jq -r '.merchant_initial_amount == .exchange_initial_amount' < $LAST_RESPONSE`

if [ "$FUNDED" != "true" ]
then
    echo 'should be funded. got:' $STATUS 'is founded: ' $FUNDED
    cat $LAST_RESPONSE
    exit 1
fi


echo -n "authorizing tip ..."

STATUS=$(curl 'http://localhost:9966/instances/default/private/reserves/'$RESERVE_PUB'/authorize-tip' \
    -d '{"amount":"TESTKUDOS:1","justification":"off course","next_url":"https://taler.net/"}' \
    -w "%{http_code}" -s -o $LAST_RESPONSE)

if [ "$STATUS" != "200" ]
then
    echo 'should respond failed, we did not fund yet. got:' $STATUS
    exit 1
fi

echo OK

echo -n Checking tip ....
STATUS=$(curl 'http://localhost:9966/instances/default/private/reserves/'$RESERVE_PUB'?tips=yes' \
    -w "%{http_code}" -s -o $LAST_RESPONSE)

TIPS_SIZE=`jq -r ".tips | length"  < $LAST_RESPONSE`

if [ "$TIPS_SIZE" != "1" ]
then
    echo 'should respond 1, just 1 tip. got:' $TIPS_SIZE
    cat $LAST_RESPONSE
    exit 1
fi

TIP_ID=`jq -r .tips[0].tip_id < $LAST_RESPONSE`

echo found

echo -n Checking tip status ....

STATUS=$(curl 'http://localhost:9966/instances/default/private/tips/'$TIP_ID \
    -w "%{http_code}" -s -o $LAST_RESPONSE)

if [ "$STATUS" != "200" ]
then
    echo 'should respond ok, tip found. got:' $STATUS
    cat $LAST_RESPONSE
    exit 1
fi

echo -n " ... "

STATUS=$(curl 'http://localhost:9966/instances/default/private/tips/'$TIP_ID'?pickups=yes' \
    -w "%{http_code}" -s -o $LAST_RESPONSE)

if [ "$STATUS" != "200" ]
then
    echo 'should respond ok, tip found. got:' $STATUS
    cat $LAST_RESPONSE
    exit 1
fi

echo OK

echo -n "trying to create invalid reserve ..."

STATUS=$(curl 'http://localhost:9966/instances/default/private/reserves' \
    -d '{"initial_balance":"INVALID:2","exchange_url":"'$EXCHANGE_URL'","wire_method":"x-taler-bank"}' \
    -w "%{http_code}" -s -o $LAST_RESPONSE)

if [ "$STATUS" != "400" ]
then
    echo 'should respond invalid, bad currency. got:' $STATUS
    exit 1
fi

echo "FAILED (which is ok)"


exit 0
