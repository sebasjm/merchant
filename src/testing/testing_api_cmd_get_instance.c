/*
  This file is part of TALER
  Copyright (C) 2020 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3, or
  (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file testing_api_cmd_get_instance.c
 * @brief command to test GET /instance/$ID
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_exchange_service.h>
#include <taler/taler_testing_lib.h>
#include "taler_merchant_service.h"
#include "taler_merchant_testing_lib.h"


/**
 * State of a "GET instance" CMD.
 */
struct GetInstanceState
{

  /**
   * Handle for a "GET instance" request.
   */
  struct TALER_MERCHANT_InstanceGetHandle *igh;

  /**
   * The interpreter state.
   */
  struct TALER_TESTING_Interpreter *is;

  /**
   * Base URL of the merchant serving the request.
   */
  const char *merchant_url;

  /**
   * ID of the instance to run GET for.
   */
  const char *instance_id;

  /**
   * Reference for a POST or PATCH /instances CMD (optional).
   */
  const char *instance_reference;

  /**
   * Whether we should check the instance's accounts or not.
   */
  bool cmp_accounts;

  /**
   * The accounts of the merchant we expect to be active.
   */
  const char **active_accounts;

  /**
   * The length of @e active_accounts.
   */
  unsigned int active_accounts_length;

  /**
   * The accounts of the merchant we expect to be inactive.
   */
  const char **inactive_accounts;

  /**
   * The length of @e inactive_accounts.
   */
  unsigned int inactive_accounts_length;

  /**
   * Expected HTTP response code.
   */
  unsigned int http_status;

};


/**
 * Callback for a /get/instance/$ID operation.
 *
 * @param cls closure for this function
 * @param hr HTTP response
 * @param accounts_length how many bank accounts the instance has
 * @param accounts the list of the instance's bank accounts
 * @param details all the details related to this particular instance
 */
static void
get_instance_cb (void *cls,
                 const struct TALER_MERCHANT_HttpResponse *hr,
                 unsigned int accounts_length,
                 const struct TALER_MERCHANT_Account accounts[],
                 const struct TALER_MERCHANT_InstanceDetails *details)
{
  /* FIXME, deeper checks should be implemented here (for accounts). */
  struct GetInstanceState *gis = cls;
  const struct TALER_TESTING_Command *instance_cmd;

  instance_cmd = TALER_TESTING_interpreter_lookup_command (
    gis->is,
    gis->instance_reference);

  gis->igh = NULL;
  if (gis->http_status != hr->http_status)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u (%d) to command %s\n",
                hr->http_status,
                (int) hr->ec,
                TALER_TESTING_interpreter_get_current_label (gis->is));
    TALER_TESTING_interpreter_fail (gis->is);
    return;
  }
  switch (hr->http_status)
  {
  case MHD_HTTP_OK:
    {
      const char **name;

      if (GNUNET_OK !=
          TALER_TESTING_get_trait_instance_name (instance_cmd,
                                                 &name))
        TALER_TESTING_interpreter_fail (gis->is);
      if (0 != strcmp (details->name,
                       *name))
      {
        GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                    "Instance name does not match: Got `%s', wanted `%s'\n",
                    details->name,
                    *name);
        TALER_TESTING_interpreter_fail (gis->is);
        return;
      }
    }
    {
      const json_t *address;

      if (GNUNET_OK !=
          TALER_TESTING_get_trait_address (instance_cmd,
                                           &address))
        TALER_TESTING_interpreter_fail (gis->is);
      if (1 != json_equal (details->address,
                           address))
      {
        GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                    "Instance address does not match\n");
        TALER_TESTING_interpreter_fail (gis->is);
        return;
      }
    }
    {
      const struct json_t *jurisdiction;

      if (GNUNET_OK !=
          TALER_TESTING_get_trait_jurisdiction (instance_cmd,
                                                &jurisdiction))
        TALER_TESTING_interpreter_fail (gis->is);
      if (1 != json_equal (details->jurisdiction,
                           jurisdiction))
      {
        GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                    "Instance jurisdiction does not match\n");
        TALER_TESTING_interpreter_fail (gis->is);
        return;
      }
    }
    {
      const struct TALER_Amount *default_max_wire_fee;

      if (GNUNET_OK !=
          TALER_TESTING_get_trait_max_wire_fee (instance_cmd,
                                                &default_max_wire_fee))
        TALER_TESTING_interpreter_fail (gis->is);
      if ((GNUNET_OK != TALER_amount_cmp_currency (
             details->default_max_wire_fee,
             default_max_wire_fee)) ||
          (0 != TALER_amount_cmp (details->default_max_wire_fee,
                                  default_max_wire_fee)))
      {
        GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                    "Instance default max wire fee does not match\n");
        TALER_TESTING_interpreter_fail (gis->is);
        return;
      }
    }
    {
      const uint32_t *default_wire_fee_amortization;

      if (GNUNET_OK !=
          TALER_TESTING_get_trait_wire_fee_amortization (instance_cmd,
                                                         &
                                                         default_wire_fee_amortization))
        TALER_TESTING_interpreter_fail (gis->is);
      if (details->default_wire_fee_amortization !=
          *default_wire_fee_amortization)
      {
        GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                    "Instance default wire fee amortization does not match\n");
        TALER_TESTING_interpreter_fail (gis->is);
        return;
      }
    }
    {
      const struct TALER_Amount *default_max_deposit_fee;

      if (GNUNET_OK !=
          TALER_TESTING_get_trait_max_deposit_fee (instance_cmd,
                                                   &default_max_deposit_fee))
        TALER_TESTING_interpreter_fail (gis->is);
      if ( (GNUNET_OK !=
            TALER_amount_cmp_currency (
              details->default_max_deposit_fee,
              default_max_deposit_fee)) ||
           (0 != TALER_amount_cmp (details->default_max_deposit_fee,
                                   default_max_deposit_fee)) )
      {
        GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                    "Instance default max deposit fee %s does not match\n",
                    TALER_amount2s (details->default_max_deposit_fee));
        TALER_TESTING_interpreter_fail (gis->is);
        return;
      }
    }
    {
      const struct GNUNET_TIME_Relative *default_wire_transfer_delay;

      if (GNUNET_OK !=
          TALER_TESTING_get_trait_wire_delay (instance_cmd,
                                              &default_wire_transfer_delay))
        TALER_TESTING_interpreter_fail (gis->is);
      if (details->default_wire_transfer_delay.rel_value_us !=
          default_wire_transfer_delay->rel_value_us)
      {
        GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                    "Instance default wire transfer delay does not match\n");
        TALER_TESTING_interpreter_fail (gis->is);
        return;
      }
    }
    {
      const struct GNUNET_TIME_Relative *default_pay_delay;
      if (GNUNET_OK !=
          TALER_TESTING_get_trait_pay_delay (instance_cmd,
                                             &default_pay_delay))
        TALER_TESTING_interpreter_fail (gis->is);
      if (details->default_pay_delay.rel_value_us !=
          default_pay_delay->rel_value_us)
      {
        GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                    "Instance default pay delay does not match\n");
        TALER_TESTING_interpreter_fail (gis->is);
        return;
      }
    }
    /* We aren't guaranteed an order for the accounts, so we just have to check
       that we can match each account returned with exactly one account
       expected. */
    if (gis->cmp_accounts)
    {
      unsigned int expected_accounts_length =
        gis->active_accounts_length + gis->inactive_accounts_length;
      unsigned int matches[accounts_length];

      if (accounts_length != expected_accounts_length)
      {
        GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                    "Accounts length does not match\n");
        TALER_TESTING_interpreter_fail (gis->is);
        return;
      }

      memset (matches,
              0,
              sizeof (unsigned int) * accounts_length);

      // Compare the accounts
      for (unsigned int i = 0; i < accounts_length; ++i)
      {
        for (unsigned int j = 0; j < gis->active_accounts_length; ++j)
        {
          if ((0 == strcasecmp (accounts[i].payto_uri,
                                gis->active_accounts[j])) &&
              (true == accounts[i].active))
          {
            matches[i] += 1;
          }
        }
        for (unsigned int j = 0; j < gis->inactive_accounts_length; ++j)
        {
          if ((0 == strcasecmp (accounts[i].payto_uri,
                                gis->inactive_accounts[j])) &&
              (false == accounts[i].active))
          {
            matches[i] += 1;
          }
        }
      }

      // Each account should have exactly one match.
      for (unsigned int i = 0; i < accounts_length; ++i)
      {
        if (1 != matches[i])
        {
          GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                      "Instance account does not match\n");
          TALER_TESTING_interpreter_fail (gis->is);
          return;
        }
      }
    }
    break;
  case MHD_HTTP_UNAUTHORIZED:
    break;
  case MHD_HTTP_NOT_FOUND:
    break;
  default:
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "Unhandled HTTP status %u for GET instance ID.\n",
                hr->http_status);
  }
  TALER_TESTING_interpreter_next (gis->is);
}


/**
 * Run the "GET instance" CMD.
 *
 *
 * @param cls closure.
 * @param cmd command being run now.
 * @param is interpreter state.
 */
static void
get_instance_run (void *cls,
                  const struct TALER_TESTING_Command *cmd,
                  struct TALER_TESTING_Interpreter *is)
{
  struct GetInstanceState *gis = cls;

  gis->is = is;
  gis->igh = TALER_MERCHANT_instance_get (is->ctx,
                                          gis->merchant_url,
                                          gis->instance_id,
                                          &get_instance_cb,
                                          gis);
  GNUNET_assert (NULL != gis->igh);
}


/**
 * Free the state of a "GET instance" CMD, and possibly
 * cancel a pending operation thereof.
 *
 * @param cls closure.
 * @param cmd command being run.
 */
static void
get_instance_cleanup (void *cls,
                      const struct TALER_TESTING_Command *cmd)
{
  struct GetInstanceState *gis = cls;

  if (NULL != gis->igh)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "GET /instances/$ID operation did not complete\n");
    TALER_MERCHANT_instance_get_cancel (gis->igh);
  }
  GNUNET_free (gis);
}


struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_get_instance (const char *label,
                                         const char *merchant_url,
                                         const char *instance_id,
                                         unsigned int http_status,
                                         const char *instance_reference)
{
  struct GetInstanceState *gis;

  gis = GNUNET_new (struct GetInstanceState);
  gis->merchant_url = merchant_url;
  gis->instance_id = instance_id;
  gis->http_status = http_status;
  gis->instance_reference = instance_reference;
  gis->cmp_accounts = false;
  {
    struct TALER_TESTING_Command cmd = {
      .cls = gis,
      .label = label,
      .run = &get_instance_run,
      .cleanup = &get_instance_cleanup
    };

    return cmd;
  }
}


struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_get_instance2 (const char *label,
                                          const char *merchant_url,
                                          const char *instance_id,
                                          unsigned int http_status,
                                          const char *instance_reference,
                                          const char *active_accounts[],
                                          unsigned int active_accounts_length,
                                          const char *inactive_accounts[],
                                          unsigned int inactive_accounts_length)
{
  struct GetInstanceState *gis;

  gis = GNUNET_new (struct GetInstanceState);
  gis->merchant_url = merchant_url;
  gis->instance_id = instance_id;
  gis->http_status = http_status;
  gis->instance_reference = instance_reference;
  gis->cmp_accounts = true;
  gis->active_accounts = active_accounts;
  gis->active_accounts_length = active_accounts_length;
  gis->inactive_accounts = inactive_accounts;
  gis->inactive_accounts_length = inactive_accounts_length;
  {
    struct TALER_TESTING_Command cmd = {
      .cls = gis,
      .label = label,
      .run = &get_instance_run,
      .cleanup = &get_instance_cleanup
    };

    return cmd;
  }
}


/* end of testing_api_cmd_get_instance.c */
