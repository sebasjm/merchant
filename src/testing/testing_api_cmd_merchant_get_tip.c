/*
  This file is part of TALER
  Copyright (C) 2020 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3, or
  (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file testing_api_cmd_merchant_get_tip.c
 * @brief command to test GET /private/tips/$TIP_ID.
 * @author Jonathan Buchanan
 */
#include "platform.h"
#include <taler/taler_exchange_service.h>
#include <taler/taler_testing_lib.h>
#include "taler_merchant_service.h"
#include "taler_merchant_testing_lib.h"

/**
 * State for a GET /private/tips/$TIP_ID CMD.
 */
struct MerchantTipGetState
{

  /**
   * The merchant base URL.
   */
  const char *merchant_url;

  /**
   * Expected HTTP response code for this CMD.
   */
  unsigned int http_status;

  /**
   * Whether to fetch and compare pickups.
   */
  bool fetch_pickups;

  /**
   * The length of @e pickups.
   */
  unsigned int pickups_length;

  /**
   * The NULL-terminated list of pickup commands associated with the tip.
   */
  const char **pickups;

  /**
   * The handle to the current GET /tips/$TIP_ID request.
   */
  struct TALER_MERCHANT_TipMerchantGetHandle *tgh;

  /**
   * The interpreter state.
   */
  struct TALER_TESTING_Interpreter *is;

  /**
   * Reference to a command that created a tip.
   */
  const char *tip_reference;
};


/**
 * Callback for a GET /private/tips/$TIP_ID operation.
 *
 * @param cls closure for this function
 * @param hr http response
 * @param total_authorized the total amount authorized for the tip
 * @param total_picked_up the total amount of the tip that has been picked up
 * @param reason why the tip was authorized
 * @param expiration when the tip will expire
 * @param reserve_pub public key of the reserve the tip is drawing from
 * @param pickups_length number of pickups associated with the tip
 * @param pickups the array of pickups associated with the tip
 */
static void
merchant_get_tip_cb (void *cls,
                     const struct TALER_MERCHANT_HttpResponse *hr,
                     const struct TALER_Amount *total_authorized,
                     const struct TALER_Amount *total_picked_up,
                     const char *reason,
                     struct GNUNET_TIME_Timestamp expiration,
                     const struct TALER_ReservePublicKeyP *reserve_pub,
                     unsigned int pickups_length,
                     const struct TALER_MERCHANT_PickupDetail pickups[])
{
  /* FIXME, deeper checks should be implemented here. */
  struct MerchantTipGetState *gts = cls;
  const struct TALER_TESTING_Command *authorize_cmd;
  struct TALER_Amount expected_total_picked_up;

  authorize_cmd = TALER_TESTING_interpreter_lookup_command (gts->is,
                                                            gts->tip_reference);

  gts->tgh = NULL;
  GNUNET_assert (GNUNET_OK ==
                 TALER_amount_set_zero (total_picked_up->currency,
                                        &expected_total_picked_up));
  if (gts->http_status != hr->http_status)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u (%d) to command %s\n",
                hr->http_status,
                (int) hr->ec,
                TALER_TESTING_interpreter_get_current_label (gts->is));
    TALER_TESTING_interpreter_fail (gts->is);
    return;
  }
  switch (hr->http_status)
  {
  case MHD_HTTP_OK:
    // FIXME: use gts->tip_reference here to
    // check if the data returned matches that from the POST / PATCH
    {
      const struct TALER_Amount *initial_amount;

      if (GNUNET_OK !=
          TALER_TESTING_get_trait_amount (authorize_cmd,
                                          &initial_amount))
        TALER_TESTING_FAIL (gts->is);
      if ((GNUNET_OK !=
           TALER_amount_cmp_currency (total_authorized,
                                      initial_amount)) ||
          (0 != TALER_amount_cmp (total_authorized,
                                  initial_amount)))
      {
        GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                    "Tip authorized amount does not match\n");
        TALER_TESTING_interpreter_fail (gts->is);
        return;
      }
    }
    {
      const char **justification;

      if (GNUNET_OK !=
          TALER_TESTING_get_trait_reason (authorize_cmd,
                                          &justification))
        TALER_TESTING_FAIL (gts->is);
      if (0 != strcmp (reason,
                       *justification))
      {
        GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                    "Tip authorized reason does not match\n");
        TALER_TESTING_interpreter_fail (gts->is);
        return;
      }
    }
    {
      const struct GNUNET_TIME_Timestamp *tip_expiration;

      if (GNUNET_OK !=
          TALER_TESTING_get_trait_timestamp (authorize_cmd,
                                             0,
                                             &tip_expiration))
        TALER_TESTING_FAIL (gts->is);
      if (GNUNET_TIME_timestamp_cmp (*tip_expiration,
                                     !=,
                                     expiration))
      {
        GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                    "Tip authorized expiration does not match\n");
        TALER_TESTING_interpreter_fail (gts->is);
        return;
      }
    }
    if (pickups_length != gts->pickups_length)
    {
      GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                  "Length of pickups array does not match\n");
      TALER_TESTING_interpreter_fail (gts->is);
      return;
    }
    {
      for (unsigned int i = 0; i < pickups_length; ++i)
      {
        const struct TALER_TESTING_Command *pickup_cmd;

        pickup_cmd = TALER_TESTING_interpreter_lookup_command (gts->is,
                                                               gts->pickups[i]);
        {
          const uint32_t *num_planchets;

          if (GNUNET_OK !=
              TALER_TESTING_get_trait_num_planchets (pickup_cmd,
                                                     &num_planchets))
            TALER_TESTING_FAIL (gts->is);

          if (*num_planchets != pickups[i].num_planchets)
          {
            GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                        "Pickup planchet count does not match\n");
            TALER_TESTING_interpreter_fail (gts->is);
            return;
          }
        }
        {
          const struct TALER_Amount *total;

          if (GNUNET_OK !=
              TALER_TESTING_get_trait_amount (pickup_cmd,
                                              &total))
            TALER_TESTING_FAIL (gts->is);

          if ( (GNUNET_OK !=
                TALER_amount_cmp_currency (total,
                                           &pickups[i].requested_amount)) ||
               (0 != TALER_amount_cmp (total,
                                       &pickups[i].requested_amount)))
          {
            GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                        "Pickup planchet sum does not match\n");
            TALER_TESTING_interpreter_fail (gts->is);
            return;
          }
          GNUNET_assert (0 < TALER_amount_add (&expected_total_picked_up,
                                               &expected_total_picked_up,
                                               total));
        }
      }
      if ( (GNUNET_OK !=
            TALER_amount_cmp_currency (&expected_total_picked_up,
                                       total_picked_up)) ||
           (0 !=
            TALER_amount_cmp (&expected_total_picked_up,
                              total_picked_up)) )
      {
        GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                    "Tip picked up amount does not match\n");
        TALER_TESTING_interpreter_fail (gts->is);
        return;
      }
    }
    break;
  default:
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "Unhandled HTTP status.\n");
  }
  TALER_TESTING_interpreter_next (gts->is);
}


/**
 * Run the "GET tip" CMD.
 *
 * @param cls closure.
 * @param cmd command being run now.
 * @param is interpreter state.
 */
static void
merchant_get_tip_run (void *cls,
                      const struct TALER_TESTING_Command *cmd,
                      struct TALER_TESTING_Interpreter *is)
{
  struct MerchantTipGetState *tgs = cls;
  const struct TALER_TESTING_Command *tip_cmd;
  const struct GNUNET_HashCode *tip_id;

  tip_cmd = TALER_TESTING_interpreter_lookup_command (is,
                                                      tgs->tip_reference);

  if (GNUNET_OK !=
      TALER_TESTING_get_trait_tip_id (tip_cmd,
                                      &tip_id))
    TALER_TESTING_FAIL (is);

  tgs->is = is;
  tgs->tgh = TALER_MERCHANT_merchant_tip_get (is->ctx,
                                              tgs->merchant_url,
                                              tip_id,
                                              tgs->fetch_pickups,
                                              &merchant_get_tip_cb,
                                              tgs);
  GNUNET_assert (NULL != tgs->tgh);
}


/**
* Free the state of a "GET tip" CMD, and possibly
* cancel a pending operation thereof.
*
* @param cls closure.
* @param cmd command being run.
*/
static void
merchant_get_tip_cleanup (void *cls,
                          const struct TALER_TESTING_Command *cmd)
{
  struct MerchantTipGetState *tgs = cls;

  if (NULL != tgs->tgh)
  {
    TALER_LOG_WARNING ("Get tip operation did not complete\n");
    TALER_MERCHANT_merchant_tip_get_cancel (tgs->tgh);
  }
  GNUNET_array_grow (tgs->pickups,
                     tgs->pickups_length,
                     0);
  GNUNET_free (tgs);
}


struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_get_tip (const char *label,
                                    const char *merchant_url,
                                    const char *tip_reference,
                                    unsigned int http_status)
{
  struct MerchantTipGetState *tgs;

  tgs = GNUNET_new (struct MerchantTipGetState);
  tgs->merchant_url = merchant_url;
  tgs->tip_reference = tip_reference;
  tgs->http_status = http_status;
  {
    struct TALER_TESTING_Command cmd = {
      .cls = tgs,
      .label = label,
      .run = &merchant_get_tip_run,
      .cleanup = &merchant_get_tip_cleanup
    };

    return cmd;
  }
}


struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_get_tip_with_pickups (const char *label,
                                                 const char *merchant_url,
                                                 const char *tip_reference,
                                                 unsigned int http_status,
                                                 ...)
{
  struct MerchantTipGetState *tgs;

  tgs = GNUNET_new (struct MerchantTipGetState);
  tgs->merchant_url = merchant_url;
  tgs->tip_reference = tip_reference;
  tgs->fetch_pickups = true;
  tgs->http_status = http_status;
  {
    const char *clabel;
    va_list ap;

    va_start (ap, http_status);
    while (NULL != (clabel = va_arg (ap, const char *)))
    {
      GNUNET_array_append (tgs->pickups,
                           tgs->pickups_length,
                           clabel);
    }
    va_end (ap);
  }
  {
    struct TALER_TESTING_Command cmd = {
      .cls = tgs,
      .label = label,
      .run = &merchant_get_tip_run,
      .cleanup = &merchant_get_tip_cleanup
    };

    return cmd;
  }
}


/* end of testing_api_cmd_merchant_get_tip.c */
