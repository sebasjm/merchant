#!/bin/bash
# This file is part of TALER
# Copyright (C) 2014-2021 Taler Systems SA
#
# TALER is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3, or
# (at your option) any later version.
#
# TALER is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with TALER; see the file COPYING.  If not, see
# <http://www.gnu.org/licenses/>
#
#
# Note that this test is intentionally NOT run as part of the standard test
# suite, because it is awfully slow (due to necessary 'wait' operations) and
# may even hang on slower computers (with the wallet trying to withdraw and
# failing because all keys have expired) due to the relatively short timeouts
# involved.
#
## Coloring style Text shell script
COLOR='\033[0;35m'
NOCOLOR='\033[0m'


set -eu

# We use VERY short withdraw periods, set flag to
# tell wallet explicitly that this is OK...
export TALER_WALLET_DEBUG_DENOMSEL_ALLOW_LATE=1

# Exit, with status code "skip" (no 'real' failure)
function exit_skip() {
    echo " SKIP: $1"
    exit 77
}

# Exit, with error message (hard failure)
function exit_fail() {
    echo " FAIL: $1"
    exit 1
}

# Cleanup to run whenever we exit
function cleanup()
{
    for n in `jobs -p`
    do
        kill $n 2> /dev/null || true
    done
    rm -rf $CONF $WALLET_DB $TMP_DIR
    wait
}

# Exchange configuration file will be edited, so we create one
# from the template.
CONF=`mktemp test_template.conf-XXXXXX`
cp test_key_rotation.conf $CONF

TMP_DIR=`mktemp -d keys-tmp-XXXXXX`
WALLET_DB=`mktemp test_wallet.json-XXXXXX`

# Install cleanup handler (except for kill -9)
trap cleanup EXIT

# Check we can actually run
echo -n "Testing for jq"
jq -h > /dev/null || exit_skip "jq required"
echo " FOUND"

echo -n "Testing for taler"
taler-exchange-httpd -h > /dev/null || exit_skip " taler-exchange required"
taler-merchant-httpd -h > /dev/null || exit_skip " taler-merchant required"
echo " FOUND"

echo -n "Testing for taler-bank-manage"
taler-bank-manage --help >/dev/null </dev/null || exit_skip " MISSING"
echo " FOUND"
echo -n "Testing for taler-wallet-cli"
taler-wallet-cli -v >/dev/null </dev/null || exit_skip " MISSING"
echo " FOUND"

echo -n "Generating Taler auditor, exchange and merchant configurations ..."

DATA_DIR=`taler-config -f -c $CONF -s PATHS -o TALER_HOME`
rm -rf $DATA_DIR

# obtain key configuration data
MASTER_PRIV_FILE=`taler-config -f -c $CONF -s EXCHANGE -o MASTER_PRIV_FILE`
MASTER_PRIV_DIR=`dirname $MASTER_PRIV_FILE`
mkdir -p $MASTER_PRIV_DIR
gnunet-ecc -g1 $MASTER_PRIV_FILE > /dev/null 2> /dev/null
MASTER_PUB=`gnunet-ecc -p $MASTER_PRIV_FILE`
EXCHANGE_URL=`taler-config -c $CONF -s EXCHANGE -o BASE_URL`
MERCHANT_PORT=`taler-config -c $CONF -s MERCHANT -o PORT`
MERCHANT_URL=http://localhost:${MERCHANT_PORT}/
BANK_PORT=`taler-config -c $CONF -s BANK -o HTTP_PORT`
BANK_URL=http://localhost:${BANK_PORT}/
AUDITOR_URL=http://localhost:8083/
AUDITOR_PRIV_FILE=`taler-config -f -c $CONF -s AUDITOR -o AUDITOR_PRIV_FILE`
AUDITOR_PRIV_DIR=`dirname $AUDITOR_PRIV_FILE`
mkdir -p $AUDITOR_PRIV_DIR
gnunet-ecc -g1 $AUDITOR_PRIV_FILE > /dev/null 2> /dev/null
AUDITOR_PUB=`gnunet-ecc -p $AUDITOR_PRIV_FILE`

# patch configuration
TALER_DB=talercheck
taler-config -c $CONF -s exchange -o MASTER_PUBLIC_KEY -V $MASTER_PUB
taler-config -c $CONF -s merchant-exchange-default -o MASTER_KEY -V $MASTER_PUB
taler-config -c $CONF -s exchangedb-postgres -o CONFIG -V postgres:///$TALER_DB
taler-config -c $CONF -s auditordb-postgres -o CONFIG -V postgres:///$TALER_DB
taler-config -c $CONF -s merchantdb-postgres -o CONFIG -V postgres:///$TALER_DB
taler-config -c $CONF -s bank -o database -V postgres:///$TALER_DB
taler-config -c $CONF -s exchange -o KEYDIR -V "${TMP_DIR}/keydir/"
taler-config -c $CONF -s exchange -o REVOCATION_DIR -V "${TMP_DIR}/revdir/"

echo " OK"

echo -n "Setting up exchange ..."

# reset database
dropdb $TALER_DB >/dev/null 2>/dev/null || true
createdb $TALER_DB || exit_skip "Could not create database $TALER_DB"
taler-exchange-dbinit -c $CONF
taler-merchant-dbinit -c $CONF
taler-auditor-dbinit -c $CONF
taler-auditor-exchange -c $CONF -m $MASTER_PUB -u $EXCHANGE_URL

echo " OK"

# Launch services
echo -n "Launching taler services ..."
taler-bank-manage-testing $CONF postgres:///$TALER_DB serve > taler-bank.log 2> taler-bank.err &
taler-exchange-secmod-eddsa -c $CONF 2> taler-exchange-secmod-eddsa.log &
taler-exchange-secmod-rsa -c $CONF 2> taler-exchange-secmod-rsa.log &
taler-exchange-httpd -c $CONF 2> taler-exchange-httpd.log &
taler-merchant-httpd -c $CONF -L INFO 2> taler-merchant-httpd.log &
taler-exchange-wirewatch -c $CONF 2> taler-exchange-wirewatch.log &
taler-auditor-httpd -L INFO -c $CONF 2> taler-auditor-httpd.log &

echo " OK"

# Wait for bank to be available (usually the slowest)
for n in `seq 1 50`
do
    echo -n "."
    sleep 0.2
    OK=0
    # bank
    wget --tries=1 --timeout=1 http://localhost:8082/ -o /dev/null -O /dev/null >/dev/null || continue
    OK=1
    break
done

if [ 1 != $OK ]
then
    exit_skip "Failed to launch services (bank)"
fi

# Wait for all other taler services to be available
for n in `seq 1 50`
do
    echo -n "."
    sleep 0.1
    OK=0
    # exchange
    wget --tries=1 --timeout=1 http://localhost:8081/seed -o /dev/null -O /dev/null >/dev/null || continue
    # merchant
    wget --tries=1 --timeout=1 http://localhost:9966/ -o /dev/null -O /dev/null >/dev/null || continue
    # auditor
    wget --tries=1 --timeout=1 http://localhost:8083/ -o /dev/null -O /dev/null >/dev/null || continue
    OK=1
    break
done

if [ 1 != $OK ]
then
    exit_skip "Failed to launch taler services"
fi

echo "OK"


echo -n "Setting up merchant instance"
STATUS=$(curl -H "Content-Type: application/json" -X POST \
    http://localhost:9966/management/instances \
    -d '{"auth":{"method":"external"},"payto_uris":["payto://x-taler-bank/localhost/43"],"id":"default","name":"default","address":{},"jurisdiction":{},"default_max_wire_fee":"TESTKUDOS:1", "default_max_deposit_fee":"TESTKUDOS:1","default_wire_fee_amortization":1,"default_wire_transfer_delay":{"d_ms" : 3600000},"default_pay_delay":{"d_ms": 3600000}}' \
    -w "%{http_code}" -s -o /dev/null)

if [ "$STATUS" != "204" ]
then
    echo 'should respond ok, instance created. got:' $STATUS
    exit 1
fi
echo " OK"

echo -n "Setting up orders ..."


ORDER_1=`curl -s -H "Content-Type: application/json" -X POST \
  http://localhost:9966/private/orders \
  -d '{"create_token":false, "order":{"amount":"TESTKUDOS:0.01","summary":"Minimal test order #1"}}' \
 | jq -er '.order_id'`
PAY1=taler+http://pay/localhost:9966/${ORDER_1}/

ORDER_2=`curl -s -H "Content-Type: application/json" -X POST \
  http://localhost:9966/private/orders \
  -d '{"create_token":false, "order":{"amount":"TESTKUDOS:0.01","summary":"Minimal test order #2"}}' \
 | jq -er '.order_id'`
PAY2=taler+http://pay/localhost:9966/${ORDER_2}/

ORDER_3=`curl -s -H "Content-Type: application/json" -X POST \
  http://localhost:9966/private/orders \
  -d '{"create_token":false, "order":{"amount":"TESTKUDOS:0.01","summary":"Minimal test order #3"}}' \
 | jq -er '.order_id'`
PAY3=taler+http://pay/localhost:9966/${ORDER_3}/

ORDER_4=`curl -s -H "Content-Type: application/json" -X POST \
  http://localhost:9966/private/orders \
  -d '{"create_token":false, "order":{"amount":"TESTKUDOS:0.01","summary":"Minimal test order #4"}}' \
 | jq -er '.order_id'`
PAY4=taler+http://pay/localhost:9966/${ORDER_4}/


if [ "$STATUS" != "204" ]
then
    echo 'should respond ok, order created. got:' $STATUS
    exit 1
fi


echo "OK"

export CONF
export AUDITOR_PUB
export AUDITOR_URL
export EXCHANGE_URL
export WALLET_DB

echo -n "Setting up keys ..."
taler-exchange-offline -L INFO -c $CONF \
  download \
  sign \
  enable-account payto://x-taler-bank/localhost/Exchange \
  enable-auditor $AUDITOR_PUB $AUDITOR_URL "TESTKUDOS Auditor" \
  wire-fee now x-taler-bank TESTKUDOS:0.01 TESTKUDOS:0.01 \
  upload &> taler-exchange-offline.log

echo -n "."

for n in `seq 1 30`
do
    echo -n "."
    OK=0
    wget --tries=1 http://localhost:8081/keys -o /dev/null -O /dev/null >/dev/null || continue
    OK=1
    sleep 0.1
    break
done

if [ 1 != $OK ]
then
    exit_skip "Failed to setup keys"
fi

echo " OK"

echo -n "Setting up auditor signatures ..."
taler-auditor-offline -L INFO -c $CONF \
  download sign upload &> taler-auditor-offline.log
echo " OK"


echo -n "First withdraw wallet"
rm $WALLET_DB
taler-wallet-cli --no-throttle --wallet-db=$WALLET_DB api 'withdrawTestBalance' \
  "$(jq -n '
    {
        amount: "TESTKUDOS:1",
        bankBaseUrl: $BANK_URL,
        exchangeBaseUrl: $EXCHANGE_URL
    }' \
    --arg BANK_URL "$BANK_URL" \
    --arg EXCHANGE_URL "$EXCHANGE_URL"
  )" 2>wallet-withdraw-1.err >wallet-withdraw-1.out
taler-wallet-cli --wallet-db=$WALLET_DB run-until-done 2>wallet-withdraw-finish-1.err >wallet-withdraw-finish-1.out
echo " OK"


echo -n "Pay first order ..."
taler-wallet-cli --no-throttle --wallet-db=$WALLET_DB handle-uri ${PAY1} -y 2> wallet-pay1.err > wallet-pay1.log
echo " OK"

echo -n "Wait for keys to rotate, but not ALL to expire..."
sleep 20
echo " OK"


echo -n "Updating keys ..."
taler-exchange-offline -L INFO -c $CONF \
  download \
  sign \
  upload &> taler-exchange-offline-2.log
taler-auditor-offline -L INFO -c $CONF \
  download sign upload &> taler-auditor-offline-2.log
echo " OK"

echo -n "Second withdraw wallet"
rm $WALLET_DB
taler-wallet-cli --no-throttle --wallet-db=$WALLET_DB api 'withdrawTestBalance' \
  "$(jq -n '
    {
        amount: "TESTKUDOS:1",
        bankBaseUrl: $BANK_URL,
        exchangeBaseUrl: $EXCHANGE_URL
    }' \
    --arg BANK_URL "$BANK_URL" \
    --arg EXCHANGE_URL "$EXCHANGE_URL"
  )" 2>wallet-withdraw-2.err >wallet-withdraw-2.out
taler-wallet-cli --wallet-db=$WALLET_DB run-until-done 2>wallet-withdraw-finish-2.err >wallet-withdraw-finish-2.out
echo " OK"

echo -n "Pay second order ..."
taler-wallet-cli --no-throttle --wallet-db=$WALLET_DB handle-uri ${PAY2} -y 2> wallet-pay2.err > wallet-pay2.log
echo " OK"


echo -n "Wait for keys to rotate, and original ones to expire..."
sleep 60
echo " OK"

date
echo -n "Updating keys ..."
taler-exchange-offline -c $CONF \
  download > taler-exchange-offline-download-3.log
taler-exchange-offline -c $CONF \
  download sign > taler-exchange-offline-sign-3.log
taler-exchange-offline -L INFO -c $CONF \
  download \
  sign \
  upload &> taler-exchange-offline-3.log
taler-auditor-offline -L INFO -c $CONF \
  download sign upload &> taler-auditor-offline-3.log
echo " OK"

echo -n "Third withdraw wallet"
rm $WALLET_DB
taler-wallet-cli --no-throttle --wallet-db=$WALLET_DB api 'withdrawTestBalance' \
  "$(jq -n '
    {
        amount: "TESTKUDOS:1",
        bankBaseUrl: $BANK_URL,
        exchangeBaseUrl: $EXCHANGE_URL
    }' \
    --arg BANK_URL "$BANK_URL" \
    --arg EXCHANGE_URL "$EXCHANGE_URL"
  )" 2>wallet-withdraw-3.err >wallet-withdraw-3.out

echo " OK"
date
echo -n "Waiting for wallet to finish ..."
taler-wallet-cli --wallet-db=$WALLET_DB run-until-done 2>wallet-withdraw-finish-3.err >wallet-withdraw-finish-3.out
echo " OK"

echo -n "Pay third order ..."
taler-wallet-cli --no-throttle --wallet-db=$WALLET_DB handle-uri ${PAY3} -y 2> wallet-pay3.err > wallet-pay3.log
echo " OK"


echo -n "Wait for everything to expire..."
sleep 120
echo " OK"

echo -n "Updating keys ..."
taler-exchange-offline -L INFO -c $CONF \
  download \
  sign \
  upload &> taler-exchange-offline-4.log
taler-auditor-offline -L INFO -c $CONF \
  download sign upload &> taler-auditor-offline-4.log
echo " OK"

echo -n "Fourth withdraw wallet"
rm $WALLET_DB
taler-wallet-cli --no-throttle --wallet-db=$WALLET_DB api 'withdrawTestBalance' \
  "$(jq -n '
    {
        amount: "TESTKUDOS:1",
        bankBaseUrl: $BANK_URL,
        exchangeBaseUrl: $EXCHANGE_URL
    }' \
    --arg BANK_URL "$BANK_URL" \
    --arg EXCHANGE_URL "$EXCHANGE_URL"
  )" 2>wallet-withdraw-4.err >wallet-withdraw-4.out
taler-wallet-cli --wallet-db=$WALLET_DB run-until-done 2>wallet-withdraw-finish-4.err >wallet-withdraw-finish-4.out
echo " OK"

echo -n "Pay fourth order ..."
taler-wallet-cli --no-throttle --wallet-db=$WALLET_DB handle-uri ${PAY4} -y 2> wallet-pay4.err > wallet-pay4.log
echo " OK"




exit 0
