#!/bin/bash
# This file is part of TALER
# Copyright (C) 2014-2021 Taler Systems SA
#
# TALER is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3, or
# (at your option) any later version.
#
# TALER is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with TALER; see the file COPYING.  If not, see
# <http://www.gnu.org/licenses/>
#
# Testcase for #6912 (failed to reproduce so far)

. initialize_taler_system.sh

echo -n "First prepare wallet with coins..."
rm $WALLET_DB
taler-wallet-cli --no-throttle --wallet-db=$WALLET_DB api 'withdrawTestBalance' \
  "$(jq -n '
    {
        amount: "TESTKUDOS:99",
        bankBaseUrl: $BANK_URL,
        exchangeBaseUrl: $EXCHANGE_URL
    }' \
    --arg BANK_URL "$BANK_URL" \
    --arg EXCHANGE_URL "$EXCHANGE_URL"
  )" 2>wallet-withdraw-1.err >wallet-withdraw-1.out
taler-wallet-cli --wallet-db=$WALLET_DB run-until-done 2>wallet-withdraw-finish-1.err >wallet-withdraw-finish-1.out
echo " OK"

#
# CREATE INSTANCE FOR TESTING
#

echo -n "Configuring merchant default instance ..."

# create with 2 address
STATUS=$(curl -H "Content-Type: application/json" -X POST \
    -H 'Authorization: Bearer secret-token:super_secret' \
    http://localhost:9966/management/instances \
    -d '{"auth":{"method":"external"},"payto_uris":["payto://x-taler-bank/localhost:8082/Tor","payto://x-taler-bank/localhost:8082/GNUnet"],"id":"default","name":"default","address":{},"jurisdiction":{},"default_max_wire_fee":"TESTKUDOS:1", "default_max_deposit_fee":"TESTKUDOS:1","default_wire_fee_amortization":1,"default_wire_transfer_delay":{"d_ms" : 50000},"default_pay_delay":{"d_ms": 60000}}' \
    -w "%{http_code}" -s -o /dev/null)

if [ "$STATUS" != "204" ]
then
    echo 'should respond ok, instance created. got:' $STATUS
    exit 1
fi

echo OK


echo -n "Configuring merchant test instance ..."

# create with 2 address
STATUS=$(curl -H "Content-Type: application/json" -X POST \
    -H 'Authorization: Bearer secret-token:super_secret' \
    http://localhost:9966/management/instances \
    -d '{"auth":{"method":"external"},"payto_uris":["payto://x-taler-bank/localhost:8082/Survey","payto://x-taler-bank/localhost:8082/Tutorial"],"id":"test","name":"default","address":{},"jurisdiction":{},"default_max_wire_fee":"TESTKUDOS:1", "default_max_deposit_fee":"TESTKUDOS:1","default_wire_fee_amortization":1,"default_wire_transfer_delay":{"d_ms" : 50000},"default_pay_delay":{"d_ms": 60000}}' \
    -w "%{http_code}" -s -o /dev/null)

if [ "$STATUS" != "204" ]
then
    echo 'should respond ok, instance created. got:' $STATUS
    exit 1
fi
echo OK

RANDOM_IMG='data:image/png;base64,abcdefg'

# CREATE ORDER AND SELL IT
echo -n "Creating order to be paid..."
STATUS=$(curl 'http://localhost:9966/instances/test/private/orders' \
    -d '{"order":{"amount":"TESTKUDOS:1","summary":"payme"}}' \
    -w "%{http_code}" -s -o $LAST_RESPONSE)

if [ "$STATUS" != "200" ]
then
    echo 'should respond ok, order created. got:' $STATUS `cat $LAST_RESPONSE`
    exit 1
fi

ORDER_ID=`jq -e -r .order_id < $LAST_RESPONSE`
TOKEN=`jq -e -r .token < $LAST_RESPONSE`

STATUS=$(curl "http://localhost:9966/instances/test/private/orders/${ORDER_ID}" \
    -w "%{http_code}" -s -o $LAST_RESPONSE)

if [ "$STATUS" != "200" ]
then
    echo 'should respond ok, getting order info before claming it. got:' $STATUS `cat $LAST_RESPONSE`
    exit 1
fi
PAY_URL=`jq -e -r .taler_pay_uri < $LAST_RESPONSE`
echo OK

NOW=`date +%s`
echo -n "Pay first order ..."
taler-wallet-cli --no-throttle --wallet-db=$WALLET_DB handle-uri "${PAY_URL}" -y 2> wallet-pay1.err > wallet-pay1.log
NOW2=`date +%s`
echo " OK (took $( echo -n $(($NOW2 - $NOW))) secs)"

STATUS=$(curl "http://localhost:9966/instances/test/private/orders/${ORDER_ID}" \
    -w "%{http_code}" -s -o $LAST_RESPONSE)

if [ "$STATUS" != "200" ]
then
    echo 'should respond ok, after pay. got:' $STATUS `cat $LAST_RESPONSE`
    exit 1
fi

ORDER_STATUS=`jq -r .order_status < $LAST_RESPONSE`

if [ "$ORDER_STATUS" != "paid" ]
then
    echo 'order should be paid. got:' $ORDER_STATUS `cat $LAST_RESPONSE`
    exit 1
fi

#
# WIRE TRANSFER TO MERCHANT AND NOTIFY BACKEND
#

PAY_DEADLINE=`jq -r .contract_terms.pay_deadline.t_ms < $LAST_RESPONSE`
WIRE_DEADLINE=`jq -r .contract_terms.wire_transfer_deadline.t_ms < $LAST_RESPONSE`

NOW=`date +%s`

TO_SLEEP=`echo $(( ($WIRE_DEADLINE /1000) - $NOW ))`
echo "waiting $TO_SLEEP secs for wire transfer"

echo -n "Perform wire transfers ..."
taler-exchange-aggregator -y -c $CONF -T ${TO_SLEEP}000000 -t -L INFO &> aggregator.log
taler-exchange-transfer -c $CONF -t -L INFO &> transfer.log
echo " DONE"

echo -n "Obtaining wire transfer details from bank..."

# First, extract the wire transfer data from the bank.
# As there is no "nice" API, we do this by dumping the
# bank database and grabbing the 'right' wire transfer,
# which is the one outgoing from the exchange (account 2).
export BANKDATA=`taler-bank-manage -c $CONF django dumpdata 2>/dev/null | tail -n1 | jq '.[] | select(.model=="app.banktransaction")' | jq 'select(.fields.debit_account==2)'`
export SUBJECT=`echo $BANKDATA | jq -r .fields.subject`
export WTID=`echo $SUBJECT | awk '{print $1}'`
export WURL=`echo $SUBJECT | awk '{print $2}'`
export CREDIT_AMOUNT=`echo $BANKDATA | jq -r .fields.amount`
export TARGET=`echo $BANKDATA | jq -r .fields.credit_account`
# 'TARGET' is now the numeric value of the account, we need to get the actual account *name*:
BANKADATA=`taler-bank-manage -c $CONF django dumpdata 2>/dev/null | tail -n1 | jq '.[] | select(.model=="auth.user")' | jq 'select(.pk=='$TARGET')'`
ACCOUNT_NAME=`echo $BANKADATA | jq -r .fields.username`
TARGET_PAYTO="payto://x-taler-bank/localhost:8082/$ACCOUNT_NAME"

if [ "$EXCHANGE_URL" != "$WURL" ]
then
    exit_fail "Wrong exchange URL in subject '$SUBJECT', expected $EXCHANGE_URL"
fi

echo " OK"

set +e

export TARGET_PAYTO
export WURL
export WTID
export CREDIT_AMOUNT
export LAST_RESPONSE

echo -n "Notifying merchant of correct wire transfer, but on wrong instance..."

#issue 6912
#here we are notifying the transfer into a wrong instance (default) and the payto_uri of the default instance
STATUS=$(curl 'http://localhost:9966/instances/default/private/transfers' \
    -d '{"credit_amount":"'$CREDIT_AMOUNT'","wtid":"'$WTID'","payto_uri":"payto://x-taler-bank/localhost:8082/Tor","exchange_url":"'$WURL'"}' \
    -m 3 \
    -w "%{http_code}" -s -o $LAST_RESPONSE)

if [ "$STATUS" != "200" ]
then
    jq . < $LAST_RESPONSE
    exit_fail "Expected response ok, after providing transfer data. got: $STATUS"
fi
echo " OK"


echo -n "Fetching wire transfers of DEFAULT instance ..."

STATUS=$(curl 'http://localhost:9966/instances/default/private/transfers' \
    -w "%{http_code}" -s -o $LAST_RESPONSE)

if [ "$STATUS" != "200" ]
then
    jq . < $LAST_RESPONSE
    exit_fail "Expected response 200 Ok. got: $STATUS"
fi

TRANSFERS_LIST_SIZE=`jq -r '.transfers | length' < $LAST_RESPONSE`

if [ "$TRANSFERS_LIST_SIZE" != "1" ]
then
    jq . < $LAST_RESPONSE
    exit_fail "Expected one transfer. got: $TRANSFERS_LIST_SIZE"
fi

echo "OK"

echo -n "Fetching wire transfers of 'test' instance ..."

STATUS=$(curl 'http://localhost:9966/instances/test/private/transfers' \
    -w "%{http_code}" -s -o $LAST_RESPONSE)

if [ "$STATUS" != "200" ]
then
    jq . < $LAST_RESPONSE
    exit_fail "Expected response 200 Ok. got: $STATUS"
fi

TRANSFERS_LIST_SIZE=`jq -r '.transfers | length' < $LAST_RESPONSE`

if [ "$TRANSFERS_LIST_SIZE" != "0" ]
then
    jq . < $LAST_RESPONSE
    exit_fail "Expected response ok. got: $STATUS"
fi

echo "OK"


echo -n "Checking order status ..."
STATUS=$(curl "http://localhost:9966/instances/test/private/orders/${ORDER_ID}?transfer=YES" \
    -w "%{http_code}" -s -o $LAST_RESPONSE)

if [ "$STATUS" != "200" ]
then
    jq . < $LAST_RESPONSE
    exit_fail 'should response ok, after order inquiry. got:' $STATUS `cat $LAST_RESPONSE`
    exit 1
fi

WAS_WIRED=`jq -r .wired < $LAST_RESPONSE`

if [ "$WAS_WIRED" == "true" ]
then
    jq . < $LAST_RESPONSE
    echo '.wired true, expected false'
    exit 1
fi

echo " OK"

echo -n "Notifying merchant of correct wire transfer in the correct instance..."
#this time in the correct instance so the order will be marked as wired...

STATUS=$(curl 'http://localhost:9966/instances/test/private/transfers' \
    -d '{"credit_amount":"'$CREDIT_AMOUNT'","wtid":"'$WTID'","payto_uri":"'$TARGET_PAYTO'","exchange_url":"'$WURL'"}' \
    -m 3 \
    -w "%{http_code}" -s -o $LAST_RESPONSE)

if [ "$STATUS" != "200" ]
then
    jq . < $LAST_RESPONSE
    exit_fail "Expected response ok, after providing transfer data. got: $STATUS"
fi
echo " OK"

echo -n "Fetching wire transfers of TEST instance ..."

STATUS=$(curl 'http://localhost:9966/instances/test/private/transfers' \
    -w "%{http_code}" -s -o $LAST_RESPONSE)

if [ "$STATUS" != "200" ]
then
    jq . < $LAST_RESPONSE
    exit_fail "Expected response 200 Ok. got: $STATUS"
fi

TRANSFERS_LIST_SIZE=`jq -r '.transfers | length' < $LAST_RESPONSE`

if [ "$TRANSFERS_LIST_SIZE" != "1" ]
then
    jq . < $LAST_RESPONSE
    exit_fail "Expected one transfer. got: $TRANSFERS_LIST_SIZE"
fi

echo "OK"

echo -n "Checking order status ..."
STATUS=$(curl "http://localhost:9966/instances/test/private/orders/${ORDER_ID}?transfer=YES" \
    -w "%{http_code}" -s -o $LAST_RESPONSE)

if [ "$STATUS" != "200" ]
then
    jq . < $LAST_RESPONSE
    exit_fail 'should response ok, after order inquiry. got:' $STATUS `cat $LAST_RESPONSE`
    exit 1
fi

WAS_WIRED=`jq -r .wired < $LAST_RESPONSE`

if [ "$WAS_WIRED" != "true" ]
then
    jq . < $LAST_RESPONSE
    echo '.wired false, expected true'
    exit 1
fi

echo " OK"

exit 0
