#!/bin/bash
# This file is part of TALER
# Copyright (C) 2014-2021 Taler Systems SA
#
# TALER is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3, or
# (at your option) any later version.
#
# TALER is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with TALER; see the file COPYING.  If not, see
# <http://www.gnu.org/licenses/>
#

. initialize_taler_system.sh

echo -n "Configuring merchant instance ..."
STATUS=$(curl -H "Content-Type: application/json" -X POST \
    -H 'Authorization: Bearer secret-token:super_secret' \
    http://localhost:9966/management/instances \
    -d '{"auth":{"method":"external"},"payto_uris":["payto://x-taler-bank/localhost:8082/43"],"id":"default","name":"default","address":{},"jurisdiction":{},"default_max_wire_fee":"TESTKUDOS:1", "default_max_deposit_fee":"TESTKUDOS:1","default_wire_fee_amortization":1,"default_wire_transfer_delay":{"d_ms" : 50000},"default_pay_delay":{"d_ms": 60000}}' \
    -w "%{http_code}" -s -o /dev/null)

if [ "$STATUS" != "204" ]
then
    echo 'should respond ok, instance created. got:' $STATUS
    exit 1
fi
echo OK
RANDOM_IMG='data:image/png;base64,abcdefg'

INFINITE_PRODUCT_TEMPLATE='{"product_id":"2","description":"product with id 2 and price :15","price":"TESTKUDOS:15","total_stock":-1,"unit":"","image":"'$RANDOM_IMG'","taxes":[]}'
MANAGED_PRODUCT_TEMPLATE='{"product_id":"3","description":"product with id 3 and price :10","price":"TESTKUDOS:150","total_stock":2,"unit":"","image":"'$RANDOM_IMG'","taxes":[]}'

echo -n "Creating products..."
STATUS=$(curl 'http://localhost:9966/instances/default/private/products' \
    -d "$INFINITE_PRODUCT_TEMPLATE" \
    -w "%{http_code}" -s -o /dev/null)

if [ "$STATUS" != "204" ]
then
    echo 'should respond ok, product created. got:' $STATUS
    exit 1
fi

STATUS=$(curl 'http://localhost:9966/instances/default/private/products' \
    -d "$MANAGED_PRODUCT_TEMPLATE" \
    -w "%{http_code}" -s -o /dev/null)

if [ "$STATUS" != "204" ]
then
    echo 'should respond ok, product created. got:' $STATUS
    exit 1
fi
echo OK


PRODUCT_DATA=$(echo $INFINITE_PRODUCT_TEMPLATE | jq 'del(.product_id) | . + {description: "other description"}')

echo -n "Updating infinite stock product..."
STATUS=$(curl 'http://localhost:9966/instances/default/private/products/2' -X PATCH \
    -d "$PRODUCT_DATA" \
    -w "%{http_code}" -s -o $LAST_RESPONSE)

if [ "$STATUS" != "204" ]
then
    echo 'should respond ok, updating product. got:' $STATUS
    cat $LAST_RESPONSE
    exit 1
fi

STATUS=$(curl 'http://localhost:9966/instances/default/private/products/2' \
    -w "%{http_code}" -s -o $LAST_RESPONSE)

DESCRIPTION=`jq -r .description < $LAST_RESPONSE`

if [ "$DESCRIPTION" != "other description" ]
then
    echo 'should change description. got:' $DESCRIPTION
    cat $LAST_RESPONSE
    exit 1
fi
echo OK

MANAGED_PRODUCT_ID=$(echo $MANAGED_PRODUCT_TEMPLATE | jq -r '.product_id')

echo -n "Locking inventory ..."

STATUS=$(curl "http://localhost:9966/instances/default/private/products/${MANAGED_PRODUCT_ID}/lock" \
    -d '{"lock_uuid":"luck","duration":{"d_ms": 100000},"quantity":10}' \
    -w "%{http_code}" -s -o $LAST_RESPONSE)

if [ "$STATUS" != "410" ]
then
    echo 'should respond gone, lock failed. got:' $STATUS `cat $LAST_RESPONSE`
    exit 1
fi

echo -n "."

STATUS=$(curl "http://localhost:9966/instances/default/private/products/${MANAGED_PRODUCT_ID}/lock" \
    -d '{"lock_uuid":"luck","duration":{"d_ms": 100000},"quantity":1}' \
    -w "%{http_code}" -s -o $LAST_RESPONSE)

if [ "$STATUS" != "204" ]
then
    echo 'should respond ok, lock created. got:' $STATUS `cat $LAST_RESPONSE`
    exit 1
fi

echo " OK"


echo -n "Creating order to be paid..."


STATUS=$(curl 'http://localhost:9966/instances/default/private/orders' \
    -d '{"order":{"amount":"TESTKUDOS:1","summary":"payme"},"inventory_products":[{"product_id":"'${MANAGED_PRODUCT_ID}'","quantity":1}]}' \
    -w "%{http_code}" -s -o $LAST_RESPONSE)

if [ "$STATUS" != "200" ]
then
    echo 'should respond ok, order created. got:' $STATUS `cat $LAST_RESPONSE`
    exit 1
fi

ORDER_ID=`jq -e -r .order_id < $LAST_RESPONSE`
TOKEN=`jq -e -r .token < $LAST_RESPONSE`

STATUS=$(curl "http://localhost:9966/instances/default/private/orders/${ORDER_ID}" \
    -w "%{http_code}" -s -o $LAST_RESPONSE)

if [ "$STATUS" != "200" ]
then
    echo 'should respond ok, getting order info before claming it. got:' $STATUS `cat $LAST_RESPONSE`
    exit 1
fi
PAY_URL=`jq -e -r .taler_pay_uri < $LAST_RESPONSE`

echo -n "."

STATUS=$(curl 'http://localhost:9966/instances/default/private/orders' \
    -d '{"order":{"amount":"TESTKUDOS:1","summary":"payme"},"inventory_products":[{"product_id":"'${MANAGED_PRODUCT_ID}'","quantity":1}]}' \
    -w "%{http_code}" -s -o $LAST_RESPONSE)

if [ "$STATUS" != "410" ]
then
    echo 'should respond out of stock (what remains is locked). got:' $STATUS `cat $LAST_RESPONSE`
    exit 1
fi

echo -n "."

# Using the 'luck' inventory lock, order creation should work.
STATUS=$(curl 'http://localhost:9966/instances/default/private/orders' \
    -d '{"order":{"amount":"TESTKUDOS:1","summary":"payme"},"lock_uuids":["luck"],"inventory_products":[{"product_id":"'$MANAGED_PRODUCT_ID'","quantity":1}]}' \
    -w "%{http_code}" -s -o $LAST_RESPONSE)

if [ "$STATUS" != "200" ]
then
    echo 'should respond ok, lock should apply. got:' $STATUS `cat $LAST_RESPONSE`
    exit 1
fi


echo " OK"

echo -n "First withdraw wallet"
rm $WALLET_DB
taler-wallet-cli --no-throttle --wallet-db=$WALLET_DB api 'withdrawTestBalance' \
  "$(jq -n '
    {
        amount: "TESTKUDOS:5",
        bankBaseUrl: $BANK_URL,
        exchangeBaseUrl: $EXCHANGE_URL
    }' \
    --arg BANK_URL "$BANK_URL" \
    --arg EXCHANGE_URL "$EXCHANGE_URL"
  )" 2>wallet-withdraw-1.err >wallet-withdraw-1.out
taler-wallet-cli --wallet-db=$WALLET_DB run-until-done 2>wallet-withdraw-finish-1.err >wallet-withdraw-finish-1.out
echo " OK"

NOW=`date +%s`

echo -n "Pay first order ..."
taler-wallet-cli --no-throttle --wallet-db=$WALLET_DB handle-uri "${PAY_URL}" -y 2> wallet-pay1.err > wallet-pay1.log
NOW2=`date +%s`
echo " OK (took $( echo -n $(($NOW2 - $NOW)) ) secs )"


STATUS=$(curl "http://localhost:9966/instances/default/private/orders/${ORDER_ID}" \
    -w "%{http_code}" -s -o $LAST_RESPONSE)

if [ "$STATUS" != "200" ]
then
    echo 'should respond ok, after pay. got:' $STATUS `cat $LAST_RESPONSE`
    exit 1
fi

ORDER_STATUS=`jq -r .order_status < $LAST_RESPONSE`

if [ "$ORDER_STATUS" != "paid" ]
then
    echo 'order should be paid. got:' $ORDER_STATUS `cat $LAST_RESPONSE`
    exit 1
fi


echo -n "Updating product..."

PRODUCT_DATA=$(echo $MANAGED_PRODUCT_TEMPLATE | jq 'del(.product_id) | . + {"total_stock": (.total_stock + 2) }')

STATUS=$(curl 'http://localhost:9966/instances/default/private/products/3' -X PATCH \
    -d "$PRODUCT_DATA" \
    -w "%{http_code}" -s -o $LAST_RESPONSE)

if [ "$STATUS" != "204" ]
then
    echo 'should respond ok, updating product. got:' $STATUS
    cat $LAST_RESPONSE
    exit 1
fi

echo " OK"

exit 0
