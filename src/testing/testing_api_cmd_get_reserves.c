/*
  This file is part of TALER
  Copyright (C) 2020 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3, or
  (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file testing_api_cmd_get_reserves.c
 * @brief command to test GET /private/reserves
 * @author Jonathan Buchanan
 */
#include "platform.h"
#include <taler/taler_exchange_service.h>
#include <taler/taler_testing_lib.h>
#include "taler_merchant_service.h"
#include "taler_merchant_testing_lib.h"


/**
 * State of a "GET reserves" CMD
 */
struct GetReservesState
{

  /**
   * Handle for a "GET reserves" request.
   */
  struct TALER_MERCHANT_ReservesGetHandle *rgh;

  /**
   * The interpreter state.
   */
  struct TALER_TESTING_Interpreter *is;

  /**
   * A list of reserves to compare with.
   */
  const char **reserves;

  /**
   * Length of @e reserve_refs.
   */
  unsigned int reserves_length;

  /**
   * Base URL of the merchant serving the request.
   */
  const char *merchant_url;

  /**
   * Expected HTTP response code.
   */
  unsigned int http_status;
};


static void
get_reserves_cb (void *cls,
                 const struct TALER_MERCHANT_HttpResponse *hr,
                 unsigned int reserves_length,
                 const struct TALER_MERCHANT_ReserveSummary reserves[])
{
  /* FIXME, deeper checks should be implemented here. */
  struct GetReservesState *grs = cls;
  bool matched[reserves_length];
  bool fail = false;

  grs->rgh = NULL;
  if (grs->http_status != hr->http_status)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u (%d) to command %s\n",
                hr->http_status,
                (int) hr->ec,
                TALER_TESTING_interpreter_get_current_label (grs->is));
    TALER_TESTING_interpreter_fail (grs->is);
    return;
  }
  switch (hr->http_status)
  {
  case MHD_HTTP_OK:
    if (reserves_length != grs->reserves_length)
    {
      GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                  "Length of reserves found does not match\n");
      TALER_TESTING_interpreter_fail (grs->is);
      return;
    }
    // FIXME: check if the data returned matches that from the POST / PATCH
    memset (matched, 0, sizeof (matched));
    for (unsigned int i = 0; i < reserves_length; ++i)
      for (unsigned int j = 0; j < reserves_length; ++j)
      {
        const struct TALER_TESTING_Command *reserve_cmd;
        bool match = true;

        reserve_cmd = TALER_TESTING_interpreter_lookup_command (
          grs->is,
          grs->reserves[j]);
        {
          const struct TALER_ReservePublicKeyP *reserve_pub;

          if (GNUNET_OK !=
              TALER_TESTING_get_trait_reserve_pub (reserve_cmd,
                                                   &reserve_pub))
          {
            GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                        "Could not fetch reserve public key\n");
            TALER_TESTING_interpreter_fail (grs->is);
            return;
          }
          if (0 != GNUNET_memcmp (&reserves[i].reserve_pub,
                                  reserve_pub))
          {
            GNUNET_log (GNUNET_ERROR_TYPE_INFO,
                        "Reserve public key does not match, got %s\n",
                        TALER_B2S (&reserves[i].reserve_pub));
            GNUNET_log (GNUNET_ERROR_TYPE_INFO,
                        "Reserve public key does not match, expected %s\n",
                        TALER_B2S (reserve_pub));
            match = false;
          }
        }
        {
          const struct TALER_Amount *initial;

          if (GNUNET_OK !=
              TALER_TESTING_get_trait_amount (reserve_cmd,
                                              &initial))
          {
            GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                        "Could not fetch reserve initial balance\n");
            TALER_TESTING_interpreter_fail (grs->is);
            return;
          }
          if ((GNUNET_OK !=
               TALER_amount_cmp_currency (&reserves[i].merchant_initial_amount,
                                          initial)) ||
              (0 != TALER_amount_cmp (&reserves[i].merchant_initial_amount,
                                      initial)))
          {
            GNUNET_log (GNUNET_ERROR_TYPE_INFO,
                        "Reserve initial amount does not match, got %s\n",
                        TALER_amount2s (&reserves[i].merchant_initial_amount));
            GNUNET_log (GNUNET_ERROR_TYPE_INFO,
                        "Reserve initial amount does not match, wanted %s\n",
                        TALER_amount2s (initial));
            match = false;
          }
        }
        if (match)
          matched[i] = true;
      }
    for (unsigned int i = 0; i < reserves_length; ++i)
      if (! matched[i])
        fail = true;
    if (fail)
    {
      TALER_TESTING_interpreter_fail (grs->is);
      return;
    }
    break;
  default:
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "Unhandled HTTP status.\n");
  }
  TALER_TESTING_interpreter_next (grs->is);
}


/**
 * Run the "GET /private/reserves" CMD.
 *
 * @param cls closure.
 * @param cmd command being run now.
 * @param is interpreter state.
 */
static void
get_reserves_run (void *cls,
                  const struct TALER_TESTING_Command *cmd,
                  struct TALER_TESTING_Interpreter *is)
{
  struct GetReservesState *grs = cls;

  grs->is = is;
  grs->rgh = TALER_MERCHANT_reserves_get (is->ctx,
                                          grs->merchant_url,
                                          GNUNET_TIME_UNIT_ZERO_TS,
                                          TALER_EXCHANGE_YNA_ALL,
                                          TALER_EXCHANGE_YNA_ALL,
                                          &get_reserves_cb,
                                          grs);

  GNUNET_assert (NULL != grs->rgh);
}


/**
 * Free the state of a "GET reserves" CMD, and possibly
 * cancel a pending operation thereof.
 *
 * @param cls closure.
 * @param cmd command being run.
 */
static void
get_reserves_cleanup (void *cls,
                      const struct TALER_TESTING_Command *cmd)
{
  struct GetReservesState *grs = cls;

  if (NULL != grs->rgh)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "GET /private/reserves operation did not complete\n");
    TALER_MERCHANT_reserves_get_cancel (grs->rgh);
  }
  GNUNET_array_grow (grs->reserves,
                     grs->reserves_length,
                     0);
  GNUNET_free (grs);
}


struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_get_reserves (const char *label,
                                         const char *merchant_url,
                                         unsigned int http_status,
                                         ...)
{
  struct GetReservesState *grs;

  grs = GNUNET_new (struct GetReservesState);
  grs->merchant_url = merchant_url;
  grs->http_status = http_status;
  {
    const char *clabel;
    va_list ap;

    va_start (ap, http_status);
    while (NULL != (clabel = va_arg (ap, const char *)))
    {
      GNUNET_array_append (grs->reserves,
                           grs->reserves_length,
                           clabel);
    }
    va_end (ap);
  }
  {
    struct TALER_TESTING_Command cmd = {
      .cls = grs,
      .label = label,
      .run = &get_reserves_run,
      .cleanup = &get_reserves_cleanup
    };

    return cmd;
  }
}


/* end of testing_api_cmd_get_reserves.c */
